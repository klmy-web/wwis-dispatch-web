// import axios from 'axios'
import { httpGet } from '@utils/api'
export default (ajax, params) => {
  return httpGet({
    method: 'get',
    url: ajax,
    params
  })
}
