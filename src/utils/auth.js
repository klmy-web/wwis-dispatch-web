/* globals localStorage */
import { fetch } from './api'
import { encrypt } from './common'
// import frontendMenus from './frontendMenus'

export function login (username, password) {
  return fetch({
    method: 'post',
    url: '/dxbase/login',
    data: {
      loginPassword: encrypt(password),
      loginName: username
    }
  }).then(function (data) {
    if (data) {
      localStorage.token = data.token
      return data
    }
  })
}
export function setCurrentNav (nav) {
  localStorage.currentNav = JSON.stringify(nav)
}
export function getCurrentNav () {
  return JSON.parse(localStorage.currentNav)
}
export function getToken () {
  return localStorage.token
}
export function getRealname () {
  return localStorage.realname
}
export function getUserId () {
  return localStorage.userId
}
export function logout (callback) {
  delete localStorage.token
  delete localStorage.realname
  delete localStorage.paths
  delete localStorage.actions
  window.location.href = '/#/login'
  if (callback) callback()
}
export function loggedIn () {
  return !!localStorage.token
}
export function getActions () {
  return localStorage.actions ? localStorage.actions.split(',') : []
}

export function checkActions (action) {
  const actions = getActions()
  return actions.indexOf(action) >= 0
}

export function requireAuth (to, from, next) {
  if (!localStorage.token) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
  } else {
    next()
  }
}

export const pathFormat = (menus) => {
  let paths = {}
  menus.map(menu => {
    if (menu.parentId) {
      const pMenu = menus.find(pMenu => pMenu.id === menu.parentId)
      if (pMenu) {
        const pMenuPath = pMenu.url
        if (!paths[pMenuPath]) {
          paths[pMenuPath] = []
        }
        paths[pMenuPath].push(menu.url)
      }
    }
  })
  return paths
} // 输入全部有权限的路径，输出全部有权限的路径的父子关系，用于路由跳转

export const pathRedirect = (to, system) => {
  if (!localStorage.token) {
    return '/login'
  }
  const paths = JSON.parse(localStorage.paths) // 获取路径的父子关系
  const { matched } = to
  const path = system + matched[matched.length - 1].path // 获取跳转路径
  return paths[path][0].split('#')[1]
} // 输入父模块路径，自动计算拥有的权限的子模块路径

export const getAccount = (callback) => {
  if (!localStorage.realName) {
    fetch({
      method: 'get',
      url: '/dxbase/rbac/user/get'
    }).then(response => {
      // 判断环境设置菜单
      // if (NODE_ENV === 'development') {
      //   response.menus = [...response.menus, ...frontendMenus]
      console.log(response)
      const menus = response.menus
      localStorage.userId = response.id
      localStorage.realname = response.name
      localStorage.menus = JSON.stringify(menus)
      localStorage.paths = JSON.stringify(pathFormat(menus))
      localStorage.actions = response.menus.filter(item => item.type === 'BUTTON').map(item => {
        return item.url
      })
      callback && callback(response)
    })
  }
} // 输入父模块路径，自动计算拥有的权限的子模块路径

export const locationContent = () => {
  const href = window.location.href.split('?')[0]
  return href.replace(window.location.protocol + '//' + window.location.host, '')
}
// 获取当前相对路径

export const changeUrl = (url) => {
  window.location.href = url
} // 跳转到指定路径

export const getSystemLink = (nav) => {
  return nav.children ? getSystemLink(nav.children[0]) : nav.url
}
