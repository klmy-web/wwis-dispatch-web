/*
* 替换json值
*/
export function editArrayValue (name, key, value, ColumnsArray) {
  let newArray = []
  for (let i in ColumnsArray) {
    for (let j in name) {
      newArray[i] = ColumnsArray[i]
      if (name[j] === i) {
        // for (let k in newArray[i]) {
        // 遍历，出现则修改
        if (key instanceof Array) {
          key.forEach((item, index) => {
            newArray[i][item] = value[index]
          })
        } else {
          newArray[i][key] = value
        }
      }
    }
  }
  return newArray
}

export function editColumnsValue (masterColumns, columns) {
  let newArray = []
  for (let i in masterColumns) {
    newArray[i] = masterColumns[i]
    for (let j in columns) {
      console.log(columns)
      if (newArray[i] === j) {
        columns[j].forEach(item => {
          newArray[i][item] = columns[j]
        })
      }
    }
  }
  // return newArray
}
