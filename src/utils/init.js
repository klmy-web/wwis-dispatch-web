import moment from 'moment'
import axios from 'axios'
import swal from 'sweetalert2'
import { swalPrompt } from './common'
import echarts from 'echarts/lib/echarts'

import 'echarts/lib/chart/bar'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/legendScroll'

import 'element-ui/lib/theme-chalk/index.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'sweetalert2/dist/sweetalert2.min.css'
import 'vue-multiselect/dist/vue-multiselect.min.css'
import 'vue-image-lightbox/dist/vue-image-lightbox.min.css'
import 'assets/scss/styles.scss'
import 'assets/css/custom.sass'
import 'assets/css/iconfont.css'

import { momentReset } from '@/utils/reset'
import { Message } from 'element-ui'

momentReset(moment)

const init = {
  install (Vue, options) {
    Vue.prototype.$moment = moment
    Vue.prototype.$swal = swal
    Vue.prototype.$swalPrompt = swalPrompt
    Vue.prototype.$axios = axios
    Vue.prototype.$echarts = echarts
    Vue.prototype.$message = Message
  }
}

export default init
