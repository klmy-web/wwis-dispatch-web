import VueRouter from 'vue-router'

import { logout, getAccount, changeUrl } from '@/utils/auth'

// import Home from 'containers/Home'
import Login from 'containers/Login'
import NotFound from 'containers/Common/NotFound'

const routes = [
  { path: '/',
    beforeEnter (to, from, next) {
      getAccount((response) => {
        // const systems = response.menus.filter(menu => menu.type === 'SYSTEM')
        changeUrl('/dispatch/#/solutionAssessment')
        // changeUrl('/dispatch/#/tap-open-degree')
      })
      next()
    }
  },
  { path: '/login', component: Login },
  { path: '/logout',
    beforeEnter (to, from, next) {
      logout()
    }
  },
  { path: '*', component: NotFound }
]

const router = new VueRouter({
  mode: 'hash',
  base: __dirname,
  routes
})
export default router
