import baseColumns from './baseColumns'
export default {
  columns: {
    ...baseColumns,
    name: {
      label: '版本名称',
      form: {
        type: 'input',
        rules: [{
          type: 'required',
          message: '请输入版本名称'
        }]
      }
    },
    status: {
      label: '状态',
      render: 'select',
      relation: 'pressureAreaVersionStatus',
      form: {
        type: 'text'
      }
    },
    enabled: {
      label: '当前使用版本',
      relation: 'yesOrNo',
      render: 'select'
    }
  },
  areaFormColumns: {
    name: {
      label: '分区名称',
      form: {
        type: 'input',
        rules: [{
          type: 'required',
          message: '请输入分区名称'
        }]
      }
    },
    color: {
      label: '分区颜色',
      isColor: true,
      form: {
        type: 'color',
        width: 80
      }
    },
    noRemoteLargeMeterWaterRate: {
      label: '非远传大表用水比例',
      unit: '%',
      form: {
        type: 'input',
        suffix: {
          text: '%'
        },
        rules: [{
          type: 'required',
          message: '请设置非远传大表用水比例'
        }, {
          type: 'reg',
          value: /^[0-9]+([.]{1}[0-9]+){0,1}$/,
          message: '只允许输入数字'
        }]
      }
    },
    noRemoteHouseholdMeterWaterRate: {
      label: '非远传小表用水比例',
      unit: '%',
      form: {
        type: 'input',
        suffix: {
          text: '%'
        },
        rules: [{
          type: 'required',
          message: '请设置非远传小表用水比例'
        }, {
          type: 'reg',
          value: /^[0-9]+([.]{1}[0-9]+){0,1}$/,
          message: '只允许输入数字'
        }]
      }
    },
    noRemoteWaterCalcBase: {
      label: '非远传水量计算基数',
      render: 'select',
      relation: 'noRemoteWaterCalcBase',
      form: {
        type: 'select',
        live: true,
        rules: [{
          type: 'required',
          message: '请设置非远传水量计算基数'
        }]
      }
    }
  }
}
