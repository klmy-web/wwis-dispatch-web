export default {
  /* 坐标点信息参数 */
  markerList: ['fromAreaId', 'toAreaId', 'enabledBoundary', 'remark'],
  /* 地图窗体信息 */
  mapList: ['siteCode', 'deviceType', 'deviceName', 'diameter', 'enabledBoundary', 'fromAreaName', 'toAreaName'],
  mapColumns: {
    name: {
      label: '分区名称'
    },
    siteCode: {
      label: '设备编号'
    },
    deviceType: {
      label: '设备类型'
    },
    deviceName: {
      label: '设备名称',
      render: 'select',
      form: {
        type: 'text'
      },
      relation: 'deviceNames'
    },
    diameter: {
      label: '口径',
      render: ({data}) => {
        return `DN${data}`
      }
    },
    installTime: {
      label: '安装时间',
      render: {
        type: 'time',
        dateType: 'YYYY-MM-DD HH:mm:ss'
      }
    },
    address: {
      label: '安装地址'
    },
    // 'longitude', 'latitude'
    lnglat: {
      label: '经纬度',
      render: ({full}) => {
        return `${full.longitude}, ${full.latitude}`
      }
    },
    boundary: {
      label: '是否边界设备',
      relation: 'yesOrNo',
      render: 'select'
    },
    fromAreaName: {
      label: '来源分区'
    },
    fromAreaId: {
      label: '来源分区',
      relation: 'pressureAreas',
      render: 'select',
      form: {
        type: 'select',
        live: true,
        rules: [{
          type: 'required',
          message: '请选择来源分区'
        }]
      }
    },
    toAreaName: {
      label: '目标分区'
    },
    toAreaId: {
      label: '目标分区',
      relation: 'pressureAreas',
      render: 'select',
      form: {
        type: 'select',
        live: true,
        rules: [{
          type: 'required',
          message: '请选择目标分区'
        }]
      }
    },
    enabledBoundary: {
      label: '是否边界设备',
      relation: 'yesOrNo',
      render: 'select',
      form: {
        type: 'switch'
      }
    },
    remark: {
      label: '备注',
      form: {
        type: 'textarea'
      }
    }
  }
}
