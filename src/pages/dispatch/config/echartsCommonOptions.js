export default {
  lineOptions: {
    grid: {
      left: '2%',
      right: '2%',
      containLabel: true
    },
    tooltip: {
      trigger: 'axis',
      formatter: (params, ticket, callback) => {
        const result = params
          .map(({ marker, seriesName, data }, index) => {
            return `${marker} ${seriesName} ${data.data} `
          })
          .join('<br>')
        return `${result}`
      },
      axisPointer: {
        // 坐标轴指示器，坐标轴触发有效
        type: 'line' // 默认为直线，可选为：'line' | 'shadow'
      }
    },
    xAxis: {
      type: 'category',
      boundaryGap: true,
      axisTick: {
        alignWithLabel: true
      }
    },
    yAxis: {
      name: '开启度（%）',
      type: 'value',
      axisTick: {
        alignWithLabel: true
      }
      // axisLine: {
      //   symbol: ['none', 'arrow']
      // }
    }
  }
}
