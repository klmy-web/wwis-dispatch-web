import systemColumns from '@/pages/system/config/columns'
export default {
  tableInitList: [
    'name',
    'status',
    'enabled',
    'remark',
    'createUserId',
    'createTime',
    'updateUserId',
    'updateTime'
  ],
  tableFullList: [
    'name',
    'status',
    'enabled',
    'remark',
    'createUserId',
    'createTime',
    'updateUserId',
    'updateTime'
  ],
  filterInitList: ['name', 'status', 'remark'],
  filterFullList: [
    'name',
    'status',
    'enabled',
    'remark',
    'createUserId',
    'createTime',
    'updateUserId',
    'updateTime'
  ],
  createList: [
    'name',
    'status',
    'remark'
  ],
  updateList: [
    'name',
    'status',
    'remark'
  ],
  columnGroupBy: [],
  columns: {
    ...systemColumns,
    id: {
      label: 'id'
    },
    name: {
      label: '版本名称',
      width: 120,
      form: {
        type: 'input',
        rules: [{
          type: 'required'
        }]
      },
      filter: {
        type: 'input',
        like: true,
        width: 120
      }
    },
    status: {
      label: '状态',
      width: 100,
      form: {
        type: 'select',
        rules: [{
          type: 'required'
        }]
      },
      filter: {
        type: 'select'
      },
      render: {
        type: 'select'
      },
      relation: 'pressureAreaVersionStatus'
    },
    remark: {
      label: '备注说明',
      form: {
        type: 'input',
        rules: [{
          type: 'required'
        }]
      },
      filter: {
        type: 'input',
        like: true
      }
    },
    enabled: {
      label: '是否启用',
      width: 100,
      render: {
        type: 'select'
      },
      relation: 'yesOrNo'
    },
    createUserId: {
      label: '创建人ID',
      width: 100
    },
    createTime: {
      label: '创建时间',
      render: {
        type: 'time'
      },
      dateType: 'YYYY-MM-DD HH:mm:ss'
    },
    updateUserId: {
      label: '更新人ID',
      width: 100
    },
    updateTime: {
      label: '更新时间',
      render: {
        type: 'time'
      },
      dateType: 'YYYY-MM-DD HH:mm:ss'
    },
    areas: {
      label: '分区数组'
    }

  }
}
