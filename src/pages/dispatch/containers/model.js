import axios from 'axios'

const domain = {
  http1: axios.create({
    baseURL: '/api'
  }),
  http2: axios.create({
    baseURL: ' /dispatch'
  })
}

// 水泵当前测试数据
export const getCurTestData = params => {
  return domain.http1.get('/dispatch/waterpumpCurve/curTestData', {params})
}
// 水泵查询
export const getPumpsInfo = params => {
  return domain.http1.get('/dispatch/waterworks/waterpumps/info', {params})
}
// 当前测试数据设置
export const setCurTestData = params => {
  return domain.http1.post('/dispatch/waterpumpCurve/curTestData', params)
}
// 当前历史数据
export const getHistoryData = params => {
  return domain.http1.get('/dispatch/waterpumpCurve/historyData', {params})
}
// 清水池详情页数据获取
export const getCleanWaterData = params => {
  return domain.http1.get('/dispatch/cleanWaterPonds/historyData', {params})
}
// 水泵详情页数据获取
export const getWaterPumpData = params => {
  return domain.http1.get('/dispatch/waterPump/openstatue/historyData', {params})
}
// 获取水泵参数
export const getWaterPumpParams = params => {
  return domain.http1.get('/dispatch/waterPump/parameters', {params})
}
// 阀门开启度数据获取
export const getTapOpenData = params => {
  return domain.http1.get('/dispatch/valve/historyData', {params})
}
// 阀门开启度设置
export const setTapOpenStatus = params => {
  return domain.http1.post('/dispatch/valve/curDegree', params)
}
// 获取水量预测页面数据
export const getWaterPredictionData = params => {
  return domain.http1.post('/dispatch/waterVolForecast/info', params)
}
// 水厂当前状况
export const getWaterFactoryCurrentData = params => {
  return domain.http2.get('/dispatchOrder/waterworksInfo', params)
}
