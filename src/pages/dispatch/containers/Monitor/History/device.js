const deviceMeter = {
  targetList: [{
    label: '实时数据',
    value: 'pressureFlux'
  }, {
    label: '用水量',
    value: 'waterQuantity'
  }]
}

export const device = {
  'DMA': deviceMeter,
  'LUF': deviceMeter,
  'DPAF': deviceMeter
}
export default {
  device
}
