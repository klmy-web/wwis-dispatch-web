export default {
  'code': 0,
  'message': '',
  'data': {
    'data': [
      {
        'isRemoteControl': false,
        'data': [
          {
            'recordTime': 1535708400000,
            'plusTotalFlux': 15319.7626953125,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15308.871386528,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8913087845
          },
          {
            'recordTime': 1535708700000,
            'plusTotalFlux': 15319.763671875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15308.8714408875,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8922309875
          },
          {
            'recordTime': 1535709000000,
            'plusTotalFlux': 15319.763671875,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15308.8714408875,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8922309875
          },
          {
            'recordTime': 1535709300000,
            'plusTotalFlux': 15319.763671875,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15308.8714408875,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8922309875
          },
          {
            'recordTime': 1535709600000,
            'plusTotalFlux': 15319.765625,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15308.8733940125,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8922309875
          },
          {
            'recordTime': 1535709900000,
            'plusTotalFlux': 15319.7685546875,
            'flux': 0.0,
            'pressure': 0.5428,
            'totalFlux': 15308.8763237,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8922309875
          },
          {
            'recordTime': 1535710200000,
            'plusTotalFlux': 15319.7763671875,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.8841362,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8922309875
          },
          {
            'recordTime': 1535710500000,
            'plusTotalFlux': 15319.7880859375,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15308.89585495,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8922309875
          },
          {
            'recordTime': 1535710800000,
            'plusTotalFlux': 15319.7880859375,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15308.89585495,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8922309875
          },
          {
            'recordTime': 1535711100000,
            'plusTotalFlux': 15319.7880859375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15308.89585495,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8922309875
          },
          {
            'recordTime': 1535711400000,
            'plusTotalFlux': 15319.7880859375,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.8948717117,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535711700000,
            'plusTotalFlux': 15319.791015625,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15308.8978013992,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535712000000,
            'plusTotalFlux': 15319.79296875,
            'flux': 0.0,
            'pressure': 0.5428,
            'totalFlux': 15308.8997545242,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535712300000,
            'plusTotalFlux': 15319.7939453125,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15308.9007310867,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535712600000,
            'plusTotalFlux': 15319.7998046875,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.9065904617,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535712900000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15308.9075670242,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535713200000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15308.9075670242,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535713500000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15308.9075670242,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535713800000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15308.9075670242,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535714100000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5386,
            'totalFlux': 15308.9075670242,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535714400000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15308.9075670242,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8932142258
          },
          {
            'recordTime': 1535714700000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.9011611938,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8996200562
          },
          {
            'recordTime': 1535715000000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15308.9011611938,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8996200562
          },
          {
            'recordTime': 1535715300000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5422,
            'totalFlux': 15308.9011611938,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8996200562
          },
          {
            'recordTime': 1535715600000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.9011611938,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8996200562
          },
          {
            'recordTime': 1535715900000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15308.9011611938,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8996200562
          },
          {
            'recordTime': 1535716200000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15308.9011611938,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.8996200562
          },
          {
            'recordTime': 1535716500000,
            'plusTotalFlux': 15319.80078125,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15308.897608757,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.903172493
          },
          {
            'recordTime': 1535716800000,
            'plusTotalFlux': 15319.802734375,
            'flux': 0.2246083617,
            'pressure': 0.5431,
            'totalFlux': 15308.8977642059,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9049701691
          },
          {
            'recordTime': 1535717100000,
            'plusTotalFlux': 15319.8037109375,
            'flux': 0.0,
            'pressure': 0.5447,
            'totalFlux': 15308.8987407684,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9049701691
          },
          {
            'recordTime': 1535717400000,
            'plusTotalFlux': 15319.8037109375,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15308.8925218582,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9111890793
          },
          {
            'recordTime': 1535717700000,
            'plusTotalFlux': 15319.8046875,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15308.8900432587,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9146442413
          },
          {
            'recordTime': 1535718000000,
            'plusTotalFlux': 15319.8046875,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15308.886256218,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.918431282
          },
          {
            'recordTime': 1535718300000,
            'plusTotalFlux': 15319.8046875,
            'flux': 0.0,
            'pressure': 0.5416,
            'totalFlux': 15308.8824501038,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9222373962
          },
          {
            'recordTime': 1535718600000,
            'plusTotalFlux': 15319.8046875,
            'flux': 0.0,
            'pressure': 0.5447,
            'totalFlux': 15308.8824501038,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9222373962
          },
          {
            'recordTime': 1535718900000,
            'plusTotalFlux': 15319.8046875,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15308.8824501038,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9222373962
          },
          {
            'recordTime': 1535719200000,
            'plusTotalFlux': 15319.8046875,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15308.8792591095,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9254283905
          },
          {
            'recordTime': 1535719500000,
            'plusTotalFlux': 15319.8056640625,
            'flux': 0.0,
            'pressure': 0.5441,
            'totalFlux': 15308.880235672,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9254283905
          },
          {
            'recordTime': 1535719800000,
            'plusTotalFlux': 15319.8056640625,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15308.880235672,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9254283905
          },
          {
            'recordTime': 1535720100000,
            'plusTotalFlux': 15319.8056640625,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15308.880235672,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9254283905
          },
          {
            'recordTime': 1535720400000,
            'plusTotalFlux': 15319.8056640625,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15308.8793735504,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9262905121
          },
          {
            'recordTime': 1535720700000,
            'plusTotalFlux': 15319.8056640625,
            'flux': 0.0,
            'pressure': 0.5386,
            'totalFlux': 15308.8774547577,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9282093048
          },
          {
            'recordTime': 1535721000000,
            'plusTotalFlux': 15319.8056640625,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.8774547577,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9282093048
          },
          {
            'recordTime': 1535721300000,
            'plusTotalFlux': 15319.810546875,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15308.8823375702,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9282093048
          },
          {
            'recordTime': 1535721600000,
            'plusTotalFlux': 15319.810546875,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15308.8787546158,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9317922592
          },
          {
            'recordTime': 1535721900000,
            'plusTotalFlux': 15319.810546875,
            'flux': 0.0,
            'pressure': 0.5441,
            'totalFlux': 15308.8787546158,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9317922592
          },
          {
            'recordTime': 1535722200000,
            'plusTotalFlux': 15319.810546875,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15308.8751964569,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9353504181
          },
          {
            'recordTime': 1535722500000,
            'plusTotalFlux': 15319.810546875,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15308.8751964569,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9353504181
          },
          {
            'recordTime': 1535722800000,
            'plusTotalFlux': 15319.810546875,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.8743457794,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9362010956
          },
          {
            'recordTime': 1535723100000,
            'plusTotalFlux': 15319.810546875,
            'flux': -0.2610293925,
            'pressure': 0.5456,
            'totalFlux': 15308.8710746765,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9394721985
          },
          {
            'recordTime': 1535723400000,
            'plusTotalFlux': 15319.810546875,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15308.8631677628,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9473791122
          },
          {
            'recordTime': 1535723700000,
            'plusTotalFlux': 15319.810546875,
            'flux': 0.0,
            'pressure': 0.541,
            'totalFlux': 15308.8600254059,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9505214691
          },
          {
            'recordTime': 1535724000000,
            'plusTotalFlux': 15319.8134765625,
            'flux': 0.0,
            'pressure': 0.5471,
            'totalFlux': 15308.8610019684,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9524745941
          },
          {
            'recordTime': 1535724300000,
            'plusTotalFlux': 15319.8154296875,
            'flux': 0.0,
            'pressure': 0.5459,
            'totalFlux': 15308.8629550934,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9524745941
          },
          {
            'recordTime': 1535724600000,
            'plusTotalFlux': 15319.8154296875,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15308.8629550934,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9524745941
          },
          {
            'recordTime': 1535724900000,
            'plusTotalFlux': 15319.81640625,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15308.8608255386,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9555807114
          },
          {
            'recordTime': 1535725200000,
            'plusTotalFlux': 15319.81640625,
            'flux': 0.0,
            'pressure': 0.5471,
            'totalFlux': 15308.8585586548,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9578475952
          },
          {
            'recordTime': 1535725500000,
            'plusTotalFlux': 15319.81640625,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15308.8585586548,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9578475952
          },
          {
            'recordTime': 1535725800000,
            'plusTotalFlux': 15319.81640625,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15308.8566207886,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9597854614
          },
          {
            'recordTime': 1535726100000,
            'plusTotalFlux': 15319.81640625,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15308.8566207886,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9597854614
          },
          {
            'recordTime': 1535726400000,
            'plusTotalFlux': 15319.81640625,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.8519287109,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9644775391
          },
          {
            'recordTime': 1535726700000,
            'plusTotalFlux': 15319.818359375,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15308.8538818359,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9644775391
          },
          {
            'recordTime': 1535727000000,
            'plusTotalFlux': 15319.818359375,
            'flux': 0.0,
            'pressure': 0.5434,
            'totalFlux': 15308.8538818359,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9644775391
          },
          {
            'recordTime': 1535727300000,
            'plusTotalFlux': 15319.818359375,
            'flux': 0.0,
            'pressure': 0.5496,
            'totalFlux': 15308.8538818359,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9644775391
          },
          {
            'recordTime': 1535727600000,
            'plusTotalFlux': 15319.818359375,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15308.8538818359,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9644775391
          },
          {
            'recordTime': 1535727900000,
            'plusTotalFlux': 15319.818359375,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15308.8538818359,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9644775391
          },
          {
            'recordTime': 1535728200000,
            'plusTotalFlux': 15319.8203125,
            'flux': -0.2388232946,
            'pressure': 0.5456,
            'totalFlux': 15308.8553705215,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9649419785
          },
          {
            'recordTime': 1535728500000,
            'plusTotalFlux': 15319.8212890625,
            'flux': 0.0,
            'pressure': 0.5401,
            'totalFlux': 15308.8540306091,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9672584534
          },
          {
            'recordTime': 1535728800000,
            'plusTotalFlux': 15319.8232421875,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15308.8559837341,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9672584534
          },
          {
            'recordTime': 1535729100000,
            'plusTotalFlux': 15319.8232421875,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15308.8559837341,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9672584534
          },
          {
            'recordTime': 1535729400000,
            'plusTotalFlux': 15319.82421875,
            'flux': 0.2506763637,
            'pressure': 0.5444,
            'totalFlux': 15308.8569602966,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9672584534
          },
          {
            'recordTime': 1535729700000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15308.8611192703,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9689588547
          },
          {
            'recordTime': 1535730000000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.8611192703,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9689588547
          },
          {
            'recordTime': 1535730300000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.8602809906,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9697971344
          },
          {
            'recordTime': 1535730600000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15308.8594303131,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9706478119
          },
          {
            'recordTime': 1535730900000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15308.8577413559,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9723367691
          },
          {
            'recordTime': 1535731200000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15308.8567352295,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 10.9733428955
          },
          {
            'recordTime': 1535731500000,
            'plusTotalFlux': 15319.830078125,
            'flux': -0.2504090965,
            'pressure': 0.5456,
            'totalFlux': 15308.8564567566,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9736213684
          },
          {
            'recordTime': 1535731800000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15308.8516044617,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9784736633
          },
          {
            'recordTime': 1535732100000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15308.8516044617,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9784736633
          },
          {
            'recordTime': 1535732400000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15308.8516044617,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9784736633
          },
          {
            'recordTime': 1535732700000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15308.8494768143,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9806013107
          },
          {
            'recordTime': 1535733000000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15308.8452606201,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9848175049
          },
          {
            'recordTime': 1535733300000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15308.8421058655,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9879722595
          },
          {
            'recordTime': 1535733600000,
            'plusTotalFlux': 15319.830078125,
            'flux': 0.0,
            'pressure': 0.0,
            'totalFlux': 15308.8359298706,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9941482544
          },
          {
            'recordTime': 1535733900000,
            'plusTotalFlux': 15319.8330078125,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8388595581,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9941482544
          },
          {
            'recordTime': 1535734200000,
            'plusTotalFlux': 15319.8330078125,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15308.8388595581,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9941482544
          },
          {
            'recordTime': 1535734500000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15308.8408126831,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9941482544
          },
          {
            'recordTime': 1535734800000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15308.8359279633,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9990329742
          },
          {
            'recordTime': 1535735100000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15308.8359279633,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9990329742
          },
          {
            'recordTime': 1535735400000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5554,
            'totalFlux': 15308.8359279633,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9990329742
          },
          {
            'recordTime': 1535735700000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.835064888,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 10.9998960495
          },
          {
            'recordTime': 1535736000000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5535,
            'totalFlux': 15308.8298873901,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0050735474
          },
          {
            'recordTime': 1535736300000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15308.8298873901,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0050735474
          },
          {
            'recordTime': 1535736600000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15308.8274869919,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0074739456
          },
          {
            'recordTime': 1535736900000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8245744705,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.010386467
          },
          {
            'recordTime': 1535737200000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8245744705,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.010386467
          },
          {
            'recordTime': 1535737500000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5526,
            'totalFlux': 15308.8236751556,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0112857819
          },
          {
            'recordTime': 1535737800000,
            'plusTotalFlux': 15319.8349609375,
            'flux': 0.0,
            'pressure': 0.5508,
            'totalFlux': 15308.8236751556,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0112857819
          },
          {
            'recordTime': 1535738100000,
            'plusTotalFlux': 15319.841796875,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8305110931,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0112857819
          },
          {
            'recordTime': 1535738400000,
            'plusTotalFlux': 15319.84375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8324642181,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0112857819
          },
          {
            'recordTime': 1535738700000,
            'plusTotalFlux': 15319.84375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8324642181,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0112857819
          },
          {
            'recordTime': 1535739000000,
            'plusTotalFlux': 15319.84375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8324642181,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0112857819
          },
          {
            'recordTime': 1535739300000,
            'plusTotalFlux': 15319.84375,
            'flux': -0.2040659487,
            'pressure': 0.5529,
            'totalFlux': 15308.8317842484,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0119657516
          },
          {
            'recordTime': 1535739600000,
            'plusTotalFlux': 15319.84375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8316144943,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0121355057
          },
          {
            'recordTime': 1535739900000,
            'plusTotalFlux': 15319.84375,
            'flux': 0.0,
            'pressure': 0.5547,
            'totalFlux': 15308.8316144943,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0121355057
          },
          {
            'recordTime': 1535740200000,
            'plusTotalFlux': 15319.84375,
            'flux': 0.0,
            'pressure': 0.5547,
            'totalFlux': 15308.8293704987,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0143795013
          },
          {
            'recordTime': 1535740500000,
            'plusTotalFlux': 15319.84375,
            'flux': 0.0,
            'pressure': 0.5554,
            'totalFlux': 15308.8293704987,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0143795013
          },
          {
            'recordTime': 1535740800000,
            'plusTotalFlux': 15319.8447265625,
            'flux': 0.0,
            'pressure': 0.5523,
            'totalFlux': 15308.8303470612,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0143795013
          },
          {
            'recordTime': 1535741100000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.2304012477,
            'pressure': 0.5529,
            'totalFlux': 15308.8269920349,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0187110901
          },
          {
            'recordTime': 1535741400000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8269920349,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0187110901
          },
          {
            'recordTime': 1535741700000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5554,
            'totalFlux': 15308.8269920349,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0187110901
          },
          {
            'recordTime': 1535742000000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5523,
            'totalFlux': 15308.8269920349,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0187110901
          },
          {
            'recordTime': 1535742300000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5538,
            'totalFlux': 15308.8269920349,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0187110901
          },
          {
            'recordTime': 1535742600000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5532,
            'totalFlux': 15308.8269920349,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0187110901
          },
          {
            'recordTime': 1535742900000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5538,
            'totalFlux': 15308.8269920349,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0187110901
          },
          {
            'recordTime': 1535743200000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8269920349,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0187110901
          },
          {
            'recordTime': 1535743500000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5535,
            'totalFlux': 15308.8269920349,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0187110901
          },
          {
            'recordTime': 1535743800000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5554,
            'totalFlux': 15308.8269920349,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0187110901
          },
          {
            'recordTime': 1535744100000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8261575699,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535744400000,
            'plusTotalFlux': 15319.845703125,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8261575699,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535744700000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5532,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535745000000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5535,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535745300000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5554,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535745600000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752969820,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535745900000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535746200000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.555,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535746500000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5535,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535746800000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5554,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535747100000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535747400000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5535,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535747700000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5538,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535748000000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535748300000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535748600000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535748900000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5532,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535749200000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535749500000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5535,
            'totalFlux': 15308.8310403824,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0195455551
          },
          {
            'recordTime': 1535749800000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8271932602,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0233926773
          },
          {
            'recordTime': 1535750100000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8263349533,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0242509842
          },
          {
            'recordTime': 1535750400000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5523,
            'totalFlux': 15308.8263349533,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0242509842
          },
          {
            'recordTime': 1535750700000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5547,
            'totalFlux': 15308.8253517151,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0252342224
          },
          {
            'recordTime': 1535751000000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.555,
            'totalFlux': 15308.8253517151,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0252342224
          },
          {
            'recordTime': 1535751300000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8253517151,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0252342224
          },
          {
            'recordTime': 1535751600000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8253517151,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0252342224
          },
          {
            'recordTime': 1535751900000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5547,
            'totalFlux': 15308.8253517151,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.0252342224
          },
          {
            'recordTime': 1535752200000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8244895935,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.026096344
          },
          {
            'recordTime': 1535752500000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8244895935,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.026096344
          },
          {
            'recordTime': 1535752800000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5554,
            'totalFlux': 15308.8244895935,
            'uploadTime': 1535752979407,
            'reverseTotalFlux': 11.026096344
          },
          {
            'recordTime': 1535753100000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8244895935,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.026096344
          },
          {
            'recordTime': 1535753400000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5554,
            'totalFlux': 15308.8244895935,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.026096344
          },
          {
            'recordTime': 1535753700000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15308.8244895935,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.026096344
          },
          {
            'recordTime': 1535754000000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8234539032,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535754300000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8234539032,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535754600000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5554,
            'totalFlux': 15308.8234539032,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535754900000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15308.8234539032,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535755200000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8234539032,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535755500000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15308.8234539032,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535755800000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15308.8234539032,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535756100000,
            'plusTotalFlux': 15319.8505859375,
            'flux': 0.0,
            'pressure': 0.5532,
            'totalFlux': 15308.8234539032,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535756400000,
            'plusTotalFlux': 15319.8515625,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8244304657,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535756700000,
            'plusTotalFlux': 15319.8515625,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15308.8244304657,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535757000000,
            'plusTotalFlux': 15319.8525390625,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15308.8254070282,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535757300000,
            'plusTotalFlux': 15319.8525390625,
            'flux': 0.0,
            'pressure': 0.5502,
            'totalFlux': 15308.8254070282,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0271320343
          },
          {
            'recordTime': 1535757600000,
            'plusTotalFlux': 15319.853515625,
            'flux': 0.0,
            'pressure': 0.5532,
            'totalFlux': 15308.8227100372,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0308055878
          },
          {
            'recordTime': 1535757900000,
            'plusTotalFlux': 15319.8544921875,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15308.8236865997,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0308055878
          },
          {
            'recordTime': 1535758200000,
            'plusTotalFlux': 15319.8544921875,
            'flux': -0.2098588347,
            'pressure': 0.5505,
            'totalFlux': 15308.8193912506,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0351009369
          },
          {
            'recordTime': 1535758500000,
            'plusTotalFlux': 15319.85546875,
            'flux': -0.3199238777,
            'pressure': 0.5529,
            'totalFlux': 15308.8169975281,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0384712219
          },
          {
            'recordTime': 1535758800000,
            'plusTotalFlux': 15319.85546875,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15308.8135166168,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0419521332
          },
          {
            'recordTime': 1535759100000,
            'plusTotalFlux': 15319.85546875,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15308.8135166168,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0419521332
          },
          {
            'recordTime': 1535759400000,
            'plusTotalFlux': 15319.85546875,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 15308.8135166168,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0419521332
          },
          {
            'recordTime': 1535759700000,
            'plusTotalFlux': 15319.85546875,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15308.8135166168,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0419521332
          },
          {
            'recordTime': 1535760000000,
            'plusTotalFlux': 15319.85546875,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15308.8135166168,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0419521332
          },
          {
            'recordTime': 1535760300000,
            'plusTotalFlux': 15319.85546875,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15308.8112125397,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0442562103
          },
          {
            'recordTime': 1535760600000,
            'plusTotalFlux': 15319.857421875,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15308.8045797348,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0528421402
          },
          {
            'recordTime': 1535760900000,
            'plusTotalFlux': 15319.857421875,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.7968482971,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0605735779
          },
          {
            'recordTime': 1535761200000,
            'plusTotalFlux': 15319.861328125,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15308.7998800278,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0614480972
          },
          {
            'recordTime': 1535761500000,
            'plusTotalFlux': 15319.861328125,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15308.7998800278,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0614480972
          },
          {
            'recordTime': 1535761800000,
            'plusTotalFlux': 15319.861328125,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15308.7998800278,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0614480972
          },
          {
            'recordTime': 1535762100000,
            'plusTotalFlux': 15319.8623046875,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15308.8008565903,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0614480972
          },
          {
            'recordTime': 1535762400000,
            'plusTotalFlux': 15319.8623046875,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15308.7989997864,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0633049011
          },
          {
            'recordTime': 1535762700000,
            'plusTotalFlux': 15319.8623046875,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15308.7989997864,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0633049011
          },
          {
            'recordTime': 1535763000000,
            'plusTotalFlux': 15319.8623046875,
            'flux': 0.0,
            'pressure': 0.5386,
            'totalFlux': 15308.7989997864,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0633049011
          },
          {
            'recordTime': 1535763300000,
            'plusTotalFlux': 15319.8623046875,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 15308.7989997864,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0633049011
          },
          {
            'recordTime': 1535763600000,
            'plusTotalFlux': 15319.86328125,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15308.7991380692,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0641431808
          },
          {
            'recordTime': 1535763900000,
            'plusTotalFlux': 15319.86328125,
            'flux': 0.0,
            'pressure': 0.5401,
            'totalFlux': 15308.7982673645,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0650138855
          },
          {
            'recordTime': 1535764200000,
            'plusTotalFlux': 15322.0166015625,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15310.951587677,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0650138855
          },
          {
            'recordTime': 1535764500000,
            'plusTotalFlux': 15322.021484375,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15310.9564704895,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0650138855
          },
          {
            'recordTime': 1535764800000,
            'plusTotalFlux': 15322.0224609375,
            'flux': 0.0,
            'pressure': 0.5288,
            'totalFlux': 15310.9540567398,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535765100000,
            'plusTotalFlux': 15324.0458984375,
            'flux': 29.6679649353,
            'pressure': 0.5163,
            'totalFlux': 15312.9774942398,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535765400000,
            'plusTotalFlux': 15328.91015625,
            'flux': 74.191192627,
            'pressure': 0.5004,
            'totalFlux': 15317.8417520523,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535765700000,
            'plusTotalFlux': 15336.1728515625,
            'flux': 93.8194580078,
            'pressure': 0.4967,
            'totalFlux': 15325.1044473648,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535766000000,
            'plusTotalFlux': 15344.41796875,
            'flux': 102.7067108154,
            'pressure': 0.4897,
            'totalFlux': 15333.3495645523,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535766300000,
            'plusTotalFlux': 15353.3642578125,
            'flux': 111.470413208,
            'pressure': 0.4922,
            'totalFlux': 15342.2958536148,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535766600000,
            'plusTotalFlux': 15362.595703125,
            'flux': 110.2423324585,
            'pressure': 0.4864,
            'totalFlux': 15351.5272989273,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535766900000,
            'plusTotalFlux': 15372.2275390625,
            'flux': 116.7950515747,
            'pressure': 0.4702,
            'totalFlux': 15361.1591348648,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535767200000,
            'plusTotalFlux': 15383.607421875,
            'flux': 113.364692688,
            'pressure': 0.4735,
            'totalFlux': 15372.5390176773,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535767500000,
            'plusTotalFlux': 15390.943359375,
            'flux': 108.6029205322,
            'pressure': 0.4711,
            'totalFlux': 15379.8749551773,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535767800000,
            'plusTotalFlux': 15400.1689453125,
            'flux': 112.7911911011,
            'pressure': 0.4723,
            'totalFlux': 15389.1005411148,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535768100000,
            'plusTotalFlux': 15409.4130859375,
            'flux': 111.4250488281,
            'pressure': 0.4681,
            'totalFlux': 15398.3446817398,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535768400000,
            'plusTotalFlux': 15418.4990234375,
            'flux': 106.7888031006,
            'pressure': 0.4687,
            'totalFlux': 15407.4306192398,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535768700000,
            'plusTotalFlux': 15427.208984375,
            'flux': 103.1354064941,
            'pressure': 0.4665,
            'totalFlux': 15416.1405801773,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535769000000,
            'plusTotalFlux': 15435.703125,
            'flux': 100.0651779175,
            'pressure': 0.4732,
            'totalFlux': 15424.6347208023,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535769300000,
            'plusTotalFlux': 15443.7197265625,
            'flux': 94.3485336304,
            'pressure': 0.4735,
            'totalFlux': 15432.6513223648,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535769600000,
            'plusTotalFlux': 15451.59375,
            'flux': 92.4967498779,
            'pressure': 0.4803,
            'totalFlux': 15440.5253458023,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535769900000,
            'plusTotalFlux': 15459.4716796875,
            'flux': 98.4373703003,
            'pressure': 0.4757,
            'totalFlux': 15448.4032754898,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535770200000,
            'plusTotalFlux': 15467.265625,
            'flux': 89.0721969604,
            'pressure': 0.4739,
            'totalFlux': 15456.1972208023,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535770500000,
            'plusTotalFlux': 15474.4423828125,
            'flux': 12.0884590149,
            'pressure': 0.512,
            'totalFlux': 15463.3739786148,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0684041977
          },
          {
            'recordTime': 1535770800000,
            'plusTotalFlux': 15474.458984375,
            'flux': 0.0,
            'pressure': 0.5144,
            'totalFlux': 15463.3897418976,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0692424774
          },
          {
            'recordTime': 1535771100000,
            'plusTotalFlux': 15474.458984375,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15463.3866243362,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0723600388
          },
          {
            'recordTime': 1535771400000,
            'plusTotalFlux': 15474.458984375,
            'flux': 0.0,
            'pressure': 0.5181,
            'totalFlux': 15463.383723259,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.075261116
          },
          {
            'recordTime': 1535771700000,
            'plusTotalFlux': 15474.4599609375,
            'flux': 0.0,
            'pressure': 0.5157,
            'totalFlux': 15463.3846998215,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.075261116
          },
          {
            'recordTime': 1535772000000,
            'plusTotalFlux': 15474.4599609375,
            'flux': 0.0,
            'pressure': 0.5175,
            'totalFlux': 15463.3846998215,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.075261116
          },
          {
            'recordTime': 1535772300000,
            'plusTotalFlux': 15474.4638671875,
            'flux': 0.0,
            'pressure': 0.5175,
            'totalFlux': 15463.3886060715,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.075261116
          },
          {
            'recordTime': 1535772600000,
            'plusTotalFlux': 15474.4638671875,
            'flux': 0.0,
            'pressure': 0.5175,
            'totalFlux': 15463.3886060715,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.075261116
          },
          {
            'recordTime': 1535772900000,
            'plusTotalFlux': 15474.4638671875,
            'flux': 0.0,
            'pressure': 0.5151,
            'totalFlux': 15463.3886060715,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.075261116
          },
          {
            'recordTime': 1535773200000,
            'plusTotalFlux': 15474.4638671875,
            'flux': 0.0,
            'pressure': 0.5157,
            'totalFlux': 15463.385017395,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0788497925
          },
          {
            'recordTime': 1535773500000,
            'plusTotalFlux': 15474.4638671875,
            'flux': 0.0,
            'pressure': 0.5105,
            'totalFlux': 15463.384095192,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.0797719955
          },
          {
            'recordTime': 1535773800000,
            'plusTotalFlux': 15474.4638671875,
            'flux': 0.0,
            'pressure': 0.5071,
            'totalFlux': 15463.3832445145,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.080622673
          },
          {
            'recordTime': 1535774100000,
            'plusTotalFlux': 15474.4658203125,
            'flux': 0.0,
            'pressure': 0.505,
            'totalFlux': 15463.3851976395,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.080622673
          },
          {
            'recordTime': 1535774400000,
            'plusTotalFlux': 15474.4697265625,
            'flux': 0.0,
            'pressure': 0.5086,
            'totalFlux': 15463.3891038895,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 11.080622673
          },
          {
            'recordTime': 1535774700000,
            'plusTotalFlux': 15474.4697265625,
            'flux': 0.0,
            'pressure': 0.512,
            'totalFlux': 15463.3891038895,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.080622673
          },
          {
            'recordTime': 1535775000000,
            'plusTotalFlux': 15474.47265625,
            'flux': 0.0,
            'pressure': 0.5108,
            'totalFlux': 15463.3910865784,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.0815696716
          },
          {
            'recordTime': 1535775300000,
            'plusTotalFlux': 15474.47265625,
            'flux': 0.0,
            'pressure': 0.5099,
            'totalFlux': 15463.3830947876,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.0895614624
          },
          {
            'recordTime': 1535775600000,
            'plusTotalFlux': 15474.4736328125,
            'flux': 0.0,
            'pressure': 0.5099,
            'totalFlux': 15463.3840713501,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.0895614624
          },
          {
            'recordTime': 1535775900000,
            'plusTotalFlux': 15474.4775390625,
            'flux': 0.0,
            'pressure': 0.512,
            'totalFlux': 15463.3879776001,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.0895614624
          },
          {
            'recordTime': 1535776200000,
            'plusTotalFlux': 15474.4775390625,
            'flux': 0.0,
            'pressure': 0.5166,
            'totalFlux': 15463.3862771988,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.0912618637
          },
          {
            'recordTime': 1535776500000,
            'plusTotalFlux': 15474.4775390625,
            'flux': 0.0,
            'pressure': 0.5187,
            'totalFlux': 15463.3862771988,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.0912618637
          },
          {
            'recordTime': 1535776800000,
            'plusTotalFlux': 15474.4775390625,
            'flux': -0.2214446366,
            'pressure': 0.5132,
            'totalFlux': 15463.3856620789,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.0918769836
          },
          {
            'recordTime': 1535777100000,
            'plusTotalFlux': 15474.4775390625,
            'flux': 0.0,
            'pressure': 0.5102,
            'totalFlux': 15463.3800172806,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.0975217819
          },
          {
            'recordTime': 1535777400000,
            'plusTotalFlux': 15474.478515625,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 15463.3800468445,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.0984687805
          },
          {
            'recordTime': 1535777700000,
            'plusTotalFlux': 15474.478515625,
            'flux': 0.0,
            'pressure': 0.5175,
            'totalFlux': 15463.3800468445,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.0984687805
          },
          {
            'recordTime': 1535778000000,
            'plusTotalFlux': 15474.48046875,
            'flux': 0.0,
            'pressure': 0.5163,
            'totalFlux': 15463.377199173,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.103269577
          },
          {
            'recordTime': 1535778300000,
            'plusTotalFlux': 15474.48046875,
            'flux': 0.0,
            'pressure': 0.5199,
            'totalFlux': 15463.375292778,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.105175972
          },
          {
            'recordTime': 1535778600000,
            'plusTotalFlux': 15474.48046875,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.3735628128,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1069059372
          },
          {
            'recordTime': 1535778900000,
            'plusTotalFlux': 15474.482421875,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15463.3734531403,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1089687347
          },
          {
            'recordTime': 1535779200000,
            'plusTotalFlux': 15474.482421875,
            'flux': 0.0,
            'pressure': 0.5227,
            'totalFlux': 15463.3725185394,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1099033356
          },
          {
            'recordTime': 1535779500000,
            'plusTotalFlux': 15474.4853515625,
            'flux': 0.0,
            'pressure': 0.5254,
            'totalFlux': 15463.3754482269,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1099033356
          },
          {
            'recordTime': 1535779800000,
            'plusTotalFlux': 15474.4853515625,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 15463.3736629486,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1116886139
          },
          {
            'recordTime': 1535780100000,
            'plusTotalFlux': 15474.4853515625,
            'flux': 0.0,
            'pressure': 0.5352,
            'totalFlux': 15463.3726682663,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1126832962
          },
          {
            'recordTime': 1535780400000,
            'plusTotalFlux': 15474.48828125,
            'flux': -0.2185481489,
            'pressure': 0.5322,
            'totalFlux': 15463.3712444305,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1170368195
          },
          {
            'recordTime': 1535780700000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5306,
            'totalFlux': 15463.3709411621,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1173400879
          },
          {
            'recordTime': 1535781000000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15463.3709411621,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1173400879
          },
          {
            'recordTime': 1535781300000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5276,
            'totalFlux': 15463.369011879,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.119269371
          },
          {
            'recordTime': 1535781600000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15463.369011879,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.119269371
          },
          {
            'recordTime': 1535781900000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15463.369011879,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.119269371
          },
          {
            'recordTime': 1535782200000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.3660058975,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1222753525
          },
          {
            'recordTime': 1535782500000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15463.3660058975,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1222753525
          },
          {
            'recordTime': 1535782800000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.3642454147,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1240358353
          },
          {
            'recordTime': 1535783100000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15463.3642454147,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1240358353
          },
          {
            'recordTime': 1535783400000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.3605356216,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1277456284
          },
          {
            'recordTime': 1535783700000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5364,
            'totalFlux': 15463.3605356216,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1277456284
          },
          {
            'recordTime': 1535784000000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.3605356216,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1277456284
          },
          {
            'recordTime': 1535784300000,
            'plusTotalFlux': 15474.48828125,
            'flux': 0.0,
            'pressure': 0.5337,
            'totalFlux': 15463.3578577042,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1304235458
          },
          {
            'recordTime': 1535784600000,
            'plusTotalFlux': 15474.4892578125,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.3588342667,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1304235458
          },
          {
            'recordTime': 1535784900000,
            'plusTotalFlux': 15474.4892578125,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 15463.3564338684,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1328239441
          },
          {
            'recordTime': 1535785200000,
            'plusTotalFlux': 15474.4892578125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.3564338684,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1328239441
          },
          {
            'recordTime': 1535785500000,
            'plusTotalFlux': 15474.4892578125,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.3564338684,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1328239441
          },
          {
            'recordTime': 1535785800000,
            'plusTotalFlux': 15474.4892578125,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.354757309,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1345005035
          },
          {
            'recordTime': 1535786100000,
            'plusTotalFlux': 15474.4892578125,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15463.3508367538,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1384210587
          },
          {
            'recordTime': 1535786400000,
            'plusTotalFlux': 15474.4892578125,
            'flux': -0.3199238181,
            'pressure': 0.5343,
            'totalFlux': 15463.3446588516,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1445989609
          },
          {
            'recordTime': 1535786700000,
            'plusTotalFlux': 15474.490234375,
            'flux': 0.0,
            'pressure': 0.5364,
            'totalFlux': 15463.3424358368,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1477985382
          },
          {
            'recordTime': 1535787000000,
            'plusTotalFlux': 15474.490234375,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.3384847641,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1517496109
          },
          {
            'recordTime': 1535787300000,
            'plusTotalFlux': 15474.490234375,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.3372001648,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1530342102
          },
          {
            'recordTime': 1535787600000,
            'plusTotalFlux': 15474.4921875,
            'flux': 0.0,
            'pressure': 0.5306,
            'totalFlux': 15463.3391532898,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1530342102
          },
          {
            'recordTime': 1535787900000,
            'plusTotalFlux': 15474.4921875,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.3391532898,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1530342102
          },
          {
            'recordTime': 1535788200000,
            'plusTotalFlux': 15474.49609375,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.3430595398,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1530342102
          },
          {
            'recordTime': 1535788500000,
            'plusTotalFlux': 15474.49609375,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 15463.342124939,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.153968811
          },
          {
            'recordTime': 1535788800000,
            'plusTotalFlux': 15474.498046875,
            'flux': 0.3607414067,
            'pressure': 0.5209,
            'totalFlux': 15463.3431787491,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 11.1548681259
          },
          {
            'recordTime': 1535789100000,
            'plusTotalFlux': 15474.5,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15463.3410196304,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1589803696
          },
          {
            'recordTime': 1535789400000,
            'plusTotalFlux': 15474.5,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 15463.3410196304,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1589803696
          },
          {
            'recordTime': 1535789700000,
            'plusTotalFlux': 15474.5,
            'flux': 0.0,
            'pressure': 0.5175,
            'totalFlux': 15463.3410196304,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1589803696
          },
          {
            'recordTime': 1535790000000,
            'plusTotalFlux': 15474.5,
            'flux': 0.0,
            'pressure': 0.5193,
            'totalFlux': 15463.3400964737,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1599035263
          },
          {
            'recordTime': 1535790300000,
            'plusTotalFlux': 15474.5009765625,
            'flux': 0.3578449488,
            'pressure': 0.5193,
            'totalFlux': 15463.3410730362,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1599035263
          },
          {
            'recordTime': 1535790600000,
            'plusTotalFlux': 15474.5048828125,
            'flux': 0.0,
            'pressure': 0.5169,
            'totalFlux': 15463.342965126,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1619176865
          },
          {
            'recordTime': 1535790900000,
            'plusTotalFlux': 15474.5048828125,
            'flux': 0.0,
            'pressure': 0.5138,
            'totalFlux': 15463.3397378922,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1651449203
          },
          {
            'recordTime': 1535791200000,
            'plusTotalFlux': 15474.5048828125,
            'flux': 0.0,
            'pressure': 0.512,
            'totalFlux': 15463.3397378922,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1651449203
          },
          {
            'recordTime': 1535791500000,
            'plusTotalFlux': 15474.5048828125,
            'flux': 0.0,
            'pressure': 0.5163,
            'totalFlux': 15463.3397378922,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1651449203
          },
          {
            'recordTime': 1535791800000,
            'plusTotalFlux': 15474.5048828125,
            'flux': 0.0,
            'pressure': 0.5117,
            'totalFlux': 15463.3319606781,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1729221344
          },
          {
            'recordTime': 1535792100000,
            'plusTotalFlux': 15474.505859375,
            'flux': 0.0,
            'pressure': 0.5102,
            'totalFlux': 15463.329252243,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.176607132
          },
          {
            'recordTime': 1535792400000,
            'plusTotalFlux': 15474.505859375,
            'flux': 0.0,
            'pressure': 0.5151,
            'totalFlux': 15463.329252243,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.176607132
          },
          {
            'recordTime': 1535792700000,
            'plusTotalFlux': 15474.51171875,
            'flux': 0.0,
            'pressure': 0.5151,
            'totalFlux': 15463.335111618,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.176607132
          },
          {
            'recordTime': 1535793000000,
            'plusTotalFlux': 15474.51171875,
            'flux': 0.0,
            'pressure': 0.5175,
            'totalFlux': 15463.335111618,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.176607132
          },
          {
            'recordTime': 1535793300000,
            'plusTotalFlux': 15474.51171875,
            'flux': 0.0,
            'pressure': 0.5154,
            'totalFlux': 15463.335111618,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.176607132
          },
          {
            'recordTime': 1535793600000,
            'plusTotalFlux': 15474.513671875,
            'flux': 0.305708915,
            'pressure': 0.5129,
            'totalFlux': 15463.3341159821,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1795558929
          },
          {
            'recordTime': 1535793900000,
            'plusTotalFlux': 15474.5205078125,
            'flux': 0.3520520329,
            'pressure': 0.5163,
            'totalFlux': 15463.3409519196,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1795558929
          },
          {
            'recordTime': 1535794200000,
            'plusTotalFlux': 15474.5224609375,
            'flux': -0.2590984404,
            'pressure': 0.5114,
            'totalFlux': 15463.3411407471,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1813201904
          },
          {
            'recordTime': 1535794500000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.5157,
            'totalFlux': 15463.3389358521,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1835250854
          },
          {
            'recordTime': 1535794800000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.5175,
            'totalFlux': 15463.3389358521,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1835250854
          },
          {
            'recordTime': 1535795100000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.5154,
            'totalFlux': 15463.333990097,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1884708405
          },
          {
            'recordTime': 1535795400000,
            'plusTotalFlux': 15474.5224609375,
            'flux': -0.2339958847,
            'pressure': 0.5175,
            'totalFlux': 15463.3324298859,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1900310516
          },
          {
            'recordTime': 1535795700000,
            'plusTotalFlux': 15474.5224609375,
            'flux': -0.3083380461,
            'pressure': 0.5135,
            'totalFlux': 15463.3312692642,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1911916733
          },
          {
            'recordTime': 1535796000000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.5163,
            'totalFlux': 15463.3260793686,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 11.1963815689
          },
          {
            'recordTime': 1535796300000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.512,
            'totalFlux': 15463.3260793686,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.1963815689
          },
          {
            'recordTime': 1535796600000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.5126,
            'totalFlux': 15463.323931694,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.1985292435
          },
          {
            'recordTime': 1535796900000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.5163,
            'totalFlux': 15463.3230819702,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.1993789673
          },
          {
            'recordTime': 1535797200000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15463.3212127686,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2012481689
          },
          {
            'recordTime': 1535797500000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 15463.3212127686,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2012481689
          },
          {
            'recordTime': 1535797800000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15463.3212127686,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2012481689
          },
          {
            'recordTime': 1535798100000,
            'plusTotalFlux': 15474.5224609375,
            'flux': 0.0,
            'pressure': 0.5175,
            'totalFlux': 15463.3212127686,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2012481689
          },
          {
            'recordTime': 1535798400000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.204333216,
            'pressure': 0.523,
            'totalFlux': 15463.3241424561,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2012481689
          },
          {
            'recordTime': 1535798700000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5264,
            'totalFlux': 15463.3241424561,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2012481689
          },
          {
            'recordTime': 1535799000000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.3241424561,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2012481689
          },
          {
            'recordTime': 1535799300000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5331,
            'totalFlux': 15463.309923172,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.215467453
          },
          {
            'recordTime': 1535799600000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15463.309923172,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.215467453
          },
          {
            'recordTime': 1535799900000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5267,
            'totalFlux': 15463.309923172,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.215467453
          },
          {
            'recordTime': 1535800200000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5318,
            'totalFlux': 15463.309923172,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.215467453
          },
          {
            'recordTime': 1535800500000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.309923172,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.215467453
          },
          {
            'recordTime': 1535800800000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5373,
            'totalFlux': 15463.309923172,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.215467453
          },
          {
            'recordTime': 1535801100000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5355,
            'totalFlux': 15463.309923172,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.215467453
          },
          {
            'recordTime': 1535801400000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.309923172,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.215467453
          },
          {
            'recordTime': 1535801700000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 15463.309923172,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.215467453
          },
          {
            'recordTime': 1535802000000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15463.3042993546,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2210912704
          },
          {
            'recordTime': 1535802300000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.3011445999,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2242460251
          },
          {
            'recordTime': 1535802600000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.3011445999,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2242460251
          },
          {
            'recordTime': 1535802900000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15463.2990341187,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2263565063
          },
          {
            'recordTime': 1535803200000,
            'plusTotalFlux': 15474.525390625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.2990341187,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2263565063
          },
          {
            'recordTime': 1535803500000,
            'plusTotalFlux': 15474.5263671875,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15463.3000106812,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2263565063
          },
          {
            'recordTime': 1535803800000,
            'plusTotalFlux': 15474.5263671875,
            'flux': 0.0,
            'pressure': 0.5318,
            'totalFlux': 15463.2991361618,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2272310257
          },
          {
            'recordTime': 1535804100000,
            'plusTotalFlux': 15474.5263671875,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15463.2991361618,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2272310257
          },
          {
            'recordTime': 1535804400000,
            'plusTotalFlux': 15474.5263671875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.2991361618,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2272310257
          },
          {
            'recordTime': 1535804700000,
            'plusTotalFlux': 15474.52734375,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15463.3001127243,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2272310257
          },
          {
            'recordTime': 1535805000000,
            'plusTotalFlux': 15474.52734375,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15463.2955713272,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2317724228
          },
          {
            'recordTime': 1535805300000,
            'plusTotalFlux': 15474.52734375,
            'flux': 0.0,
            'pressure': 0.5376,
            'totalFlux': 15463.2955713272,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2317724228
          },
          {
            'recordTime': 1535805600000,
            'plusTotalFlux': 15474.52734375,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15463.2955713272,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2317724228
          },
          {
            'recordTime': 1535805900000,
            'plusTotalFlux': 15474.52734375,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.2955713272,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2317724228
          },
          {
            'recordTime': 1535806200000,
            'plusTotalFlux': 15474.52734375,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.2955713272,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2317724228
          },
          {
            'recordTime': 1535806500000,
            'plusTotalFlux': 15474.5283203125,
            'flux': 0.0,
            'pressure': 0.5288,
            'totalFlux': 15463.2965478897,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2317724228
          },
          {
            'recordTime': 1535806800000,
            'plusTotalFlux': 15474.5283203125,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.2965478897,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2317724228
          },
          {
            'recordTime': 1535807100000,
            'plusTotalFlux': 15474.5283203125,
            'flux': -0.206962347,
            'pressure': 0.5355,
            'totalFlux': 15463.2960882187,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2322320938
          },
          {
            'recordTime': 1535807400000,
            'plusTotalFlux': 15474.53125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.294924736,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.236325264
          },
          {
            'recordTime': 1535807700000,
            'plusTotalFlux': 15474.533203125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.296877861,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.236325264
          },
          {
            'recordTime': 1535808000000,
            'plusTotalFlux': 15474.533203125,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.296877861,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.236325264
          },
          {
            'recordTime': 1535808300000,
            'plusTotalFlux': 15474.533203125,
            'flux': -0.2214446366,
            'pressure': 0.5315,
            'totalFlux': 15463.2903118134,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2428913116
          },
          {
            'recordTime': 1535808600000,
            'plusTotalFlux': 15474.533203125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.2853813171,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2478218079
          },
          {
            'recordTime': 1535808900000,
            'plusTotalFlux': 15474.5341796875,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15463.2863578796,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2478218079
          },
          {
            'recordTime': 1535809200000,
            'plusTotalFlux': 15474.5341796875,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.2863578796,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2478218079
          },
          {
            'recordTime': 1535809500000,
            'plusTotalFlux': 15474.5361328125,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 15463.2883110046,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2478218079
          },
          {
            'recordTime': 1535809800000,
            'plusTotalFlux': 15474.5361328125,
            'flux': 0.0,
            'pressure': 0.5331,
            'totalFlux': 15463.2883110046,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2478218079
          },
          {
            'recordTime': 1535810100000,
            'plusTotalFlux': 15474.5361328125,
            'flux': -0.3981279135,
            'pressure': 0.537,
            'totalFlux': 15463.2840719223,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2520608902
          },
          {
            'recordTime': 1535810400000,
            'plusTotalFlux': 15474.5361328125,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15463.2790403366,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2570924759
          },
          {
            'recordTime': 1535810700000,
            'plusTotalFlux': 15474.5361328125,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15463.2781047821,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2580280304
          },
          {
            'recordTime': 1535811000000,
            'plusTotalFlux': 15474.5361328125,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.2781047821,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2580280304
          },
          {
            'recordTime': 1535811300000,
            'plusTotalFlux': 15474.5361328125,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.2781047821,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2580280304
          },
          {
            'recordTime': 1535811600000,
            'plusTotalFlux': 15474.5390625,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15463.2810344696,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2580280304
          },
          {
            'recordTime': 1535811900000,
            'plusTotalFlux': 15474.5400390625,
            'flux': 0.0,
            'pressure': 0.5349,
            'totalFlux': 15463.2799367905,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.260102272
          },
          {
            'recordTime': 1535812200000,
            'plusTotalFlux': 15474.5400390625,
            'flux': 0.0,
            'pressure': 0.5364,
            'totalFlux': 15463.2799367905,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.260102272
          },
          {
            'recordTime': 1535812500000,
            'plusTotalFlux': 15474.5400390625,
            'flux': 0.0,
            'pressure': 0.5361,
            'totalFlux': 15463.2799367905,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.260102272
          },
          {
            'recordTime': 1535812800000,
            'plusTotalFlux': 15474.54296875,
            'flux': 0.3143982291,
            'pressure': 0.5306,
            'totalFlux': 15463.2818470001,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2611217499
          },
          {
            'recordTime': 1535813100000,
            'plusTotalFlux': 15474.5439453125,
            'flux': 0.0,
            'pressure': 0.5337,
            'totalFlux': 15463.2811450958,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2628002167
          },
          {
            'recordTime': 1535813400000,
            'plusTotalFlux': 15474.5439453125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.2811450958,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2628002167
          },
          {
            'recordTime': 1535813700000,
            'plusTotalFlux': 15474.5439453125,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15463.2811450958,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2628002167
          },
          {
            'recordTime': 1535814000000,
            'plusTotalFlux': 15474.5458984375,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.2822237015,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.263674736
          },
          {
            'recordTime': 1535814300000,
            'plusTotalFlux': 15474.5458984375,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15463.2822237015,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.263674736
          },
          {
            'recordTime': 1535814600000,
            'plusTotalFlux': 15474.5458984375,
            'flux': -0.2069623768,
            'pressure': 0.534,
            'totalFlux': 15463.2817640305,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.264134407
          },
          {
            'recordTime': 1535814900000,
            'plusTotalFlux': 15474.5458984375,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.2779579163,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2679405212
          },
          {
            'recordTime': 1535815200000,
            'plusTotalFlux': 15474.5458984375,
            'flux': 0.0,
            'pressure': 0.5318,
            'totalFlux': 15463.2779579163,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2679405212
          },
          {
            'recordTime': 1535815500000,
            'plusTotalFlux': 15474.5458984375,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.2779579163,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2679405212
          },
          {
            'recordTime': 1535815800000,
            'plusTotalFlux': 15474.546875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.2789344788,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2679405212
          },
          {
            'recordTime': 1535816100000,
            'plusTotalFlux': 15474.546875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.2789344788,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2679405212
          },
          {
            'recordTime': 1535816400000,
            'plusTotalFlux': 15474.546875,
            'flux': 0.0,
            'pressure': 0.527,
            'totalFlux': 15463.2780485153,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2688264847
          },
          {
            'recordTime': 1535816700000,
            'plusTotalFlux': 15474.546875,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15463.2749118805,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2719631195
          },
          {
            'recordTime': 1535817000000,
            'plusTotalFlux': 15474.546875,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.2749118805,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2719631195
          },
          {
            'recordTime': 1535817300000,
            'plusTotalFlux': 15474.546875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.2605628967,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2863121033
          },
          {
            'recordTime': 1535817600000,
            'plusTotalFlux': 15474.546875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.2605628967,
            'uploadTime': 1535817744227,
            'reverseTotalFlux': 11.2863121033
          },
          {
            'recordTime': 1535817900000,
            'plusTotalFlux': 15474.546875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.2605628967,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.2863121033
          },
          {
            'recordTime': 1535818200000,
            'plusTotalFlux': 15474.546875,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15463.2605628967,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.2863121033
          },
          {
            'recordTime': 1535818500000,
            'plusTotalFlux': 15474.546875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.2605628967,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.2863121033
          },
          {
            'recordTime': 1535818800000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.2575883865,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.290263176
          },
          {
            'recordTime': 1535819100000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.252702713,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.2951488495
          },
          {
            'recordTime': 1535819400000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.252702713,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.2951488495
          },
          {
            'recordTime': 1535819700000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5373,
            'totalFlux': 15463.2518644333,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.2959871292
          },
          {
            'recordTime': 1535820000000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.2518644333,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.2959871292
          },
          {
            'recordTime': 1535820300000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.2470941544,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3007574081
          },
          {
            'recordTime': 1535820600000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.538,
            'totalFlux': 15463.2470941544,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3007574081
          },
          {
            'recordTime': 1535820900000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.2470941544,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3007574081
          },
          {
            'recordTime': 1535821200000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.2394895554,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3083620071
          },
          {
            'recordTime': 1535821500000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5392,
            'totalFlux': 15463.2394895554,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3083620071
          },
          {
            'recordTime': 1535821800000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5392,
            'totalFlux': 15463.2394895554,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3083620071
          },
          {
            'recordTime': 1535822100000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.2394895554,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3083620071
          },
          {
            'recordTime': 1535822400000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.541,
            'totalFlux': 15463.2394895554,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3083620071
          },
          {
            'recordTime': 1535822700000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.2383737564,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3094778061
          },
          {
            'recordTime': 1535823000000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.2383737564,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3094778061
          },
          {
            'recordTime': 1535823300000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.2383737564,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3094778061
          },
          {
            'recordTime': 1535823600000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.541,
            'totalFlux': 15463.2352552414,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3125963211
          },
          {
            'recordTime': 1535823900000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.2352552414,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3125963211
          },
          {
            'recordTime': 1535824200000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.2323303223,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3155212402
          },
          {
            'recordTime': 1535824500000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5392,
            'totalFlux': 15463.2323303223,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3155212402
          },
          {
            'recordTime': 1535824800000,
            'plusTotalFlux': 15474.5478515625,
            'flux': -0.2272375226,
            'pressure': 0.541,
            'totalFlux': 15463.2316360474,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3162155151
          },
          {
            'recordTime': 1535825100000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.2313833237,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3164682388
          },
          {
            'recordTime': 1535825400000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15463.2313833237,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3164682388
          },
          {
            'recordTime': 1535825700000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5425,
            'totalFlux': 15463.2313833237,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3164682388
          },
          {
            'recordTime': 1535826000000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5434,
            'totalFlux': 15463.2313833237,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3164682388
          },
          {
            'recordTime': 1535826300000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15463.2313833237,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3164682388
          },
          {
            'recordTime': 1535826600000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5434,
            'totalFlux': 15463.2313833237,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3164682388
          },
          {
            'recordTime': 1535826900000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.2313833237,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3164682388
          },
          {
            'recordTime': 1535827200000,
            'plusTotalFlux': 15474.5478515625,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15463.2313833237,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3164682388
          },
          {
            'recordTime': 1535827500000,
            'plusTotalFlux': 15474.5498046875,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15463.2333364487,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3164682388
          },
          {
            'recordTime': 1535827800000,
            'plusTotalFlux': 15474.5498046875,
            'flux': 0.0,
            'pressure': 0.541,
            'totalFlux': 15463.2258281708,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3239765167
          },
          {
            'recordTime': 1535828100000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5422,
            'totalFlux': 15463.2258329391,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3249483109
          },
          {
            'recordTime': 1535828400000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5416,
            'totalFlux': 15463.2258329391,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3249483109
          },
          {
            'recordTime': 1535828700000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5428,
            'totalFlux': 15463.2190904617,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3316907883
          },
          {
            'recordTime': 1535829000000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.2190904617,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3316907883
          },
          {
            'recordTime': 1535829300000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5422,
            'totalFlux': 15463.2190904617,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3316907883
          },
          {
            'recordTime': 1535829600000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15463.2190904617,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3316907883
          },
          {
            'recordTime': 1535829900000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5441,
            'totalFlux': 15463.2190904617,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3316907883
          },
          {
            'recordTime': 1535830200000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 15463.2173538208,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3334274292
          },
          {
            'recordTime': 1535830500000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.2173538208,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3334274292
          },
          {
            'recordTime': 1535830800000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.2145252228,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3362560272
          },
          {
            'recordTime': 1535831100000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15463.2145252228,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3362560272
          },
          {
            'recordTime': 1535831400000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15463.2145252228,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3362560272
          },
          {
            'recordTime': 1535831700000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5459,
            'totalFlux': 15463.2145252228,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3362560272
          },
          {
            'recordTime': 1535832000000,
            'plusTotalFlux': 15474.55078125,
            'flux': -0.2581329346,
            'pressure': 0.5431,
            'totalFlux': 15463.2079582214,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 11.3428230286
          },
          {
            'recordTime': 1535832300000,
            'plusTotalFlux': 15474.55078125,
            'flux': -0.2417197824,
            'pressure': 0.5431,
            'totalFlux': 15463.2029666901,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3478145599
          },
          {
            'recordTime': 1535832600000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.1995258331,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3512554169
          },
          {
            'recordTime': 1535832900000,
            'plusTotalFlux': 15474.55078125,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.1995258331,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3512554169
          },
          {
            'recordTime': 1535833200000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15463.2014789581,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3512554169
          },
          {
            'recordTime': 1535833500000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.2014789581,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3512554169
          },
          {
            'recordTime': 1535833800000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.2006282806,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3521060944
          },
          {
            'recordTime': 1535834100000,
            'plusTotalFlux': 15474.552734375,
            'flux': -0.2069623768,
            'pressure': 0.5456,
            'totalFlux': 15463.200053215,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.35268116
          },
          {
            'recordTime': 1535834400000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.1980171204,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3547172546
          },
          {
            'recordTime': 1535834700000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15463.1980171204,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3547172546
          },
          {
            'recordTime': 1535835000000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.1980171204,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3547172546
          },
          {
            'recordTime': 1535835300000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.1980171204,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3547172546
          },
          {
            'recordTime': 1535835600000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.1980171204,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3547172546
          },
          {
            'recordTime': 1535835900000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.1960506439,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3566837311
          },
          {
            'recordTime': 1535836200000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.1951646805,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3575696945
          },
          {
            'recordTime': 1535836500000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15463.1943025589,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3584318161
          },
          {
            'recordTime': 1535836800000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5428,
            'totalFlux': 15463.1943025589,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3584318161
          },
          {
            'recordTime': 1535837100000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5483,
            'totalFlux': 15463.1943025589,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3584318161
          },
          {
            'recordTime': 1535837400000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15463.1943025589,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3584318161
          },
          {
            'recordTime': 1535837700000,
            'plusTotalFlux': 15474.552734375,
            'flux': -0.2330303788,
            'pressure': 0.545,
            'totalFlux': 15463.1901626587,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3625717163
          },
          {
            'recordTime': 1535838000000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.1859111786,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3668231964
          },
          {
            'recordTime': 1535838300000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 15463.1859111786,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3668231964
          },
          {
            'recordTime': 1535838600000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15463.1859111786,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3668231964
          },
          {
            'recordTime': 1535838900000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.1859111786,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3668231964
          },
          {
            'recordTime': 1535839200000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.1859111786,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 11.3668231964
          },
          {
            'recordTime': 1535839500000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15463.1859111786,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3668231964
          },
          {
            'recordTime': 1535839800000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15463.1859111786,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3668231964
          },
          {
            'recordTime': 1535840100000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15463.1818094254,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3709249496
          },
          {
            'recordTime': 1535840400000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.1818094254,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3709249496
          },
          {
            'recordTime': 1535840700000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.1808633804,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3718709946
          },
          {
            'recordTime': 1535841000000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5465,
            'totalFlux': 15463.1800003052,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3727340698
          },
          {
            'recordTime': 1535841300000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.1789932251,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3737411499
          },
          {
            'recordTime': 1535841600000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 15463.1789932251,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3737411499
          },
          {
            'recordTime': 1535841900000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.1789932251,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3737411499
          },
          {
            'recordTime': 1535842200000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15463.1780948639,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3746395111
          },
          {
            'recordTime': 1535842500000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5459,
            'totalFlux': 15463.1771602631,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3755741119
          },
          {
            'recordTime': 1535842800000,
            'plusTotalFlux': 15474.552734375,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.1771602631,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3755741119
          },
          {
            'recordTime': 1535843100000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15463.1781368256,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3755741119
          },
          {
            'recordTime': 1535843400000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.1781368256,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3755741119
          },
          {
            'recordTime': 1535843700000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.1781368256,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3755741119
          },
          {
            'recordTime': 1535844000000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15463.1772136688,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3764972687
          },
          {
            'recordTime': 1535844300000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5441,
            'totalFlux': 15463.1772136688,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3764972687
          },
          {
            'recordTime': 1535844600000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.1772136688,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3764972687
          },
          {
            'recordTime': 1535844900000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15463.1772136688,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3764972687
          },
          {
            'recordTime': 1535845200000,
            'plusTotalFlux': 15474.5537109375,
            'flux': -0.2166171968,
            'pressure': 0.5431,
            'totalFlux': 15463.1719427109,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3817682266
          },
          {
            'recordTime': 1535845500000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.1716423035,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.382068634
          },
          {
            'recordTime': 1535845800000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.1706705093,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3830404282
          },
          {
            'recordTime': 1535846100000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.1706705093,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3830404282
          },
          {
            'recordTime': 1535846400000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.1660413742,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3876695633
          },
          {
            'recordTime': 1535846700000,
            'plusTotalFlux': 15474.5537109375,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15463.1660413742,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3876695633
          },
          {
            'recordTime': 1535847000000,
            'plusTotalFlux': 15474.5546875,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15463.1670179367,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3876695633
          },
          {
            'recordTime': 1535847300000,
            'plusTotalFlux': 15474.5546875,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.1670179367,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3876695633
          },
          {
            'recordTime': 1535847600000,
            'plusTotalFlux': 15474.5546875,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.1670179367,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3876695633
          },
          {
            'recordTime': 1535847900000,
            'plusTotalFlux': 15474.5546875,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15463.1670179367,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3876695633
          },
          {
            'recordTime': 1535848200000,
            'plusTotalFlux': 15474.560546875,
            'flux': -0.2677877843,
            'pressure': 0.5346,
            'totalFlux': 15463.1701488495,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3903980255
          },
          {
            'recordTime': 1535848500000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.1697769165,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3907699585
          },
          {
            'recordTime': 1535848800000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.5386,
            'totalFlux': 15463.168806076,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.391740799
          },
          {
            'recordTime': 1535849100000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.5428,
            'totalFlux': 15463.168806076,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.391740799
          },
          {
            'recordTime': 1535849400000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15463.168806076,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.391740799
          },
          {
            'recordTime': 1535849700000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.5337,
            'totalFlux': 15463.168806076,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.391740799
          },
          {
            'recordTime': 1535850000000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15463.168806076,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.391740799
          },
          {
            'recordTime': 1535850300000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 15463.168806076,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.391740799
          },
          {
            'recordTime': 1535850600000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.168806076,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.391740799
          },
          {
            'recordTime': 1535850900000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.168806076,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.391740799
          },
          {
            'recordTime': 1535851200000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15463.168806076,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.391740799
          },
          {
            'recordTime': 1535851500000,
            'plusTotalFlux': 15474.560546875,
            'flux': 0.0,
            'pressure': 0.498,
            'totalFlux': 15463.1679315567,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3926153183
          },
          {
            'recordTime': 1535851800000,
            'plusTotalFlux': 15475.583984375,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15464.1913690567,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3926153183
          },
          {
            'recordTime': 1535852100000,
            'plusTotalFlux': 15475.583984375,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15464.1913690567,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3926153183
          },
          {
            'recordTime': 1535852400000,
            'plusTotalFlux': 15475.5859375,
            'flux': 0.0,
            'pressure': 0.5209,
            'totalFlux': 15464.1933221817,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3926153183
          },
          {
            'recordTime': 1535852700000,
            'plusTotalFlux': 15475.5869140625,
            'flux': 0.0,
            'pressure': 0.5248,
            'totalFlux': 15464.1942987442,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3926153183
          },
          {
            'recordTime': 1535853000000,
            'plusTotalFlux': 15475.5888671875,
            'flux': 0.0,
            'pressure': 0.5264,
            'totalFlux': 15464.1962518692,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3926153183
          },
          {
            'recordTime': 1535853300000,
            'plusTotalFlux': 15475.5888671875,
            'flux': 0.0,
            'pressure': 0.5135,
            'totalFlux': 15464.1942863464,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3945808411
          },
          {
            'recordTime': 1535853600000,
            'plusTotalFlux': 15475.5888671875,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15464.1942863464,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3945808411
          },
          {
            'recordTime': 1535853900000,
            'plusTotalFlux': 15475.591796875,
            'flux': 0.0,
            'pressure': 0.5096,
            'totalFlux': 15464.1953344345,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3964624405
          },
          {
            'recordTime': 1535854200000,
            'plusTotalFlux': 15475.5947265625,
            'flux': 0.0,
            'pressure': 0.512,
            'totalFlux': 15464.198264122,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3964624405
          },
          {
            'recordTime': 1535854500000,
            'plusTotalFlux': 15475.5947265625,
            'flux': 0.0,
            'pressure': 0.5169,
            'totalFlux': 15464.1959838867,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.3987426758
          },
          {
            'recordTime': 1535854800000,
            'plusTotalFlux': 15475.5966796875,
            'flux': 0.0,
            'pressure': 0.5248,
            'totalFlux': 15464.1962242126,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4004554749
          },
          {
            'recordTime': 1535855100000,
            'plusTotalFlux': 15475.5966796875,
            'flux': 0.0,
            'pressure': 0.5163,
            'totalFlux': 15464.1942338943,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4024457932
          },
          {
            'recordTime': 1535855400000,
            'plusTotalFlux': 15475.6015625,
            'flux': 0.0,
            'pressure': 0.5187,
            'totalFlux': 15464.1991167068,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4024457932
          },
          {
            'recordTime': 1535855700000,
            'plusTotalFlux': 15475.6015625,
            'flux': 0.0,
            'pressure': 0.5206,
            'totalFlux': 15464.196059227,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.405503273
          },
          {
            'recordTime': 1535856000000,
            'plusTotalFlux': 15475.6015625,
            'flux': 0.0,
            'pressure': 0.5175,
            'totalFlux': 15464.1940813065,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4074811935
          },
          {
            'recordTime': 1535856300000,
            'plusTotalFlux': 15475.6015625,
            'flux': 0.0,
            'pressure': 0.5157,
            'totalFlux': 15464.1918621063,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4097003937
          },
          {
            'recordTime': 1535856600000,
            'plusTotalFlux': 15475.6015625,
            'flux': 0.0,
            'pressure': 0.5187,
            'totalFlux': 15464.1918621063,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4097003937
          },
          {
            'recordTime': 1535856900000,
            'plusTotalFlux': 15475.6015625,
            'flux': 0.0,
            'pressure': 0.5178,
            'totalFlux': 15464.1901130676,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4114494324
          },
          {
            'recordTime': 1535857200000,
            'plusTotalFlux': 15475.609375,
            'flux': 0.0,
            'pressure': 0.5151,
            'totalFlux': 15464.1979255676,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4114494324
          },
          {
            'recordTime': 1535857500000,
            'plusTotalFlux': 15475.615234375,
            'flux': 0.0,
            'pressure': 0.5184,
            'totalFlux': 15464.2037849426,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4114494324
          },
          {
            'recordTime': 1535857800000,
            'plusTotalFlux': 15475.615234375,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15464.2037849426,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4114494324
          },
          {
            'recordTime': 1535858100000,
            'plusTotalFlux': 15475.615234375,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15464.2037849426,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4114494324
          },
          {
            'recordTime': 1535858400000,
            'plusTotalFlux': 15475.615234375,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15464.2019033432,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4133310318
          },
          {
            'recordTime': 1535858700000,
            'plusTotalFlux': 15475.615234375,
            'flux': 0.0,
            'pressure': 0.5248,
            'totalFlux': 15464.2019033432,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4133310318
          },
          {
            'recordTime': 1535859000000,
            'plusTotalFlux': 15475.615234375,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 15464.2019033432,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4133310318
          },
          {
            'recordTime': 1535859300000,
            'plusTotalFlux': 15475.615234375,
            'flux': 0.0,
            'pressure': 0.5288,
            'totalFlux': 15464.2006063461,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4146280289
          },
          {
            'recordTime': 1535859600000,
            'plusTotalFlux': 15475.615234375,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 15464.2006063461,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4146280289
          },
          {
            'recordTime': 1535859900000,
            'plusTotalFlux': 15475.615234375,
            'flux': 0.0,
            'pressure': 0.5254,
            'totalFlux': 15464.1997566223,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4154777527
          },
          {
            'recordTime': 1535860200000,
            'plusTotalFlux': 15475.615234375,
            'flux': 0.2304012477,
            'pressure': 0.5239,
            'totalFlux': 15464.1997566223,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.4154777527
          },
          {
            'recordTime': 1535860500000,
            'plusTotalFlux': 15475.6162109375,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15464.1968660355,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.419344902
          },
          {
            'recordTime': 1535860800000,
            'plusTotalFlux': 15475.6201171875,
            'flux': 0.0,
            'pressure': 0.5288,
            'totalFlux': 15464.2007722855,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 11.419344902
          },
          {
            'recordTime': 1535861100000,
            'plusTotalFlux': 15475.6201171875,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15464.2007722855,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.419344902
          },
          {
            'recordTime': 1535861400000,
            'plusTotalFlux': 15475.6201171875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.2007722855,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.419344902
          },
          {
            'recordTime': 1535861700000,
            'plusTotalFlux': 15475.6201171875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.2007722855,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.419344902
          },
          {
            'recordTime': 1535862000000,
            'plusTotalFlux': 15475.6220703125,
            'flux': 0.0,
            'pressure': 0.5318,
            'totalFlux': 15464.2027254105,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.419344902
          },
          {
            'recordTime': 1535862300000,
            'plusTotalFlux': 15475.6220703125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.2027254105,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.419344902
          },
          {
            'recordTime': 1535862600000,
            'plusTotalFlux': 15475.6220703125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.20175457,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4203157425
          },
          {
            'recordTime': 1535862900000,
            'plusTotalFlux': 15475.6220703125,
            'flux': 0.0,
            'pressure': 0.5331,
            'totalFlux': 15464.20175457,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4203157425
          },
          {
            'recordTime': 1535863200000,
            'plusTotalFlux': 15475.625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15464.2037496567,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4212503433
          },
          {
            'recordTime': 1535863500000,
            'plusTotalFlux': 15475.625,
            'flux': 0.0,
            'pressure': 0.5306,
            'totalFlux': 15464.2037496567,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4212503433
          },
          {
            'recordTime': 1535863800000,
            'plusTotalFlux': 15475.626953125,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15464.2048406601,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4221124649
          },
          {
            'recordTime': 1535864100000,
            'plusTotalFlux': 15475.626953125,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15464.2012519836,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4257011414
          },
          {
            'recordTime': 1535864400000,
            'plusTotalFlux': 15475.626953125,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15464.19576931,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.431183815
          },
          {
            'recordTime': 1535864700000,
            'plusTotalFlux': 15475.626953125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.1917285919,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4352245331
          },
          {
            'recordTime': 1535865000000,
            'plusTotalFlux': 15475.626953125,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15464.1917285919,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4352245331
          },
          {
            'recordTime': 1535865300000,
            'plusTotalFlux': 15475.626953125,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15464.1917285919,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4352245331
          },
          {
            'recordTime': 1535865600000,
            'plusTotalFlux': 15475.626953125,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15464.1907091141,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4362440109
          },
          {
            'recordTime': 1535865900000,
            'plusTotalFlux': 15475.626953125,
            'flux': 0.0,
            'pressure': 0.5325,
            'totalFlux': 15464.1907091141,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4362440109
          },
          {
            'recordTime': 1535866200000,
            'plusTotalFlux': 15475.62890625,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15464.1926622391,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4362440109
          },
          {
            'recordTime': 1535866500000,
            'plusTotalFlux': 15475.62890625,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15464.182677269,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.446228981
          },
          {
            'recordTime': 1535866800000,
            'plusTotalFlux': 15475.62890625,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15464.182677269,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.446228981
          },
          {
            'recordTime': 1535867100000,
            'plusTotalFlux': 15475.6298828125,
            'flux': 0.0,
            'pressure': 0.5361,
            'totalFlux': 15464.1836538315,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.446228981
          },
          {
            'recordTime': 1535867400000,
            'plusTotalFlux': 15475.6298828125,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15464.1817483902,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4481344223
          },
          {
            'recordTime': 1535867700000,
            'plusTotalFlux': 15475.6298828125,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15464.1817483902,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4481344223
          },
          {
            'recordTime': 1535868000000,
            'plusTotalFlux': 15475.6298828125,
            'flux': 0.0,
            'pressure': 0.5352,
            'totalFlux': 15464.1809101105,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.448972702
          },
          {
            'recordTime': 1535868300000,
            'plusTotalFlux': 15475.6298828125,
            'flux': -0.2446161807,
            'pressure': 0.5334,
            'totalFlux': 15464.1802988052,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4495840073
          },
          {
            'recordTime': 1535868600000,
            'plusTotalFlux': 15475.6328125,
            'flux': 0.0,
            'pressure': 0.5352,
            'totalFlux': 15464.1778812408,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4549312592
          },
          {
            'recordTime': 1535868900000,
            'plusTotalFlux': 15475.6328125,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15464.1759757996,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4568367004
          },
          {
            'recordTime': 1535869200000,
            'plusTotalFlux': 15475.63671875,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15464.1790189743,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4576997757
          },
          {
            'recordTime': 1535869500000,
            'plusTotalFlux': 15475.63671875,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15464.1748342514,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4618844986
          },
          {
            'recordTime': 1535869800000,
            'plusTotalFlux': 15475.63671875,
            'flux': 0.0,
            'pressure': 0.5364,
            'totalFlux': 15464.1748342514,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4618844986
          },
          {
            'recordTime': 1535870100000,
            'plusTotalFlux': 15475.63671875,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15464.1748342514,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4618844986
          },
          {
            'recordTime': 1535870400000,
            'plusTotalFlux': 15475.63671875,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15464.1748342514,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4618844986
          },
          {
            'recordTime': 1535870700000,
            'plusTotalFlux': 15475.63671875,
            'flux': 0.0,
            'pressure': 0.5349,
            'totalFlux': 15464.1712446213,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4654741287
          },
          {
            'recordTime': 1535871000000,
            'plusTotalFlux': 15475.63671875,
            'flux': -0.2069623768,
            'pressure': 0.5376,
            'totalFlux': 15464.1669015884,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4698171616
          },
          {
            'recordTime': 1535871300000,
            'plusTotalFlux': 15475.63671875,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15464.1617946625,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4749240875
          },
          {
            'recordTime': 1535871600000,
            'plusTotalFlux': 15475.6435546875,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15464.1639823914,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4795722961
          },
          {
            'recordTime': 1535871900000,
            'plusTotalFlux': 15475.6435546875,
            'flux': 0.0,
            'pressure': 0.5364,
            'totalFlux': 15464.1610450745,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.482509613
          },
          {
            'recordTime': 1535872200000,
            'plusTotalFlux': 15475.6435546875,
            'flux': 0.0,
            'pressure': 0.5386,
            'totalFlux': 15464.1610450745,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.482509613
          },
          {
            'recordTime': 1535872500000,
            'plusTotalFlux': 15475.6474609375,
            'flux': 0.2371596098,
            'pressure': 0.5383,
            'totalFlux': 15464.1649513245,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.482509613
          },
          {
            'recordTime': 1535872800000,
            'plusTotalFlux': 15475.6484375,
            'flux': 0.0,
            'pressure': 0.5349,
            'totalFlux': 15464.165927887,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.482509613
          },
          {
            'recordTime': 1535873100000,
            'plusTotalFlux': 15475.6484375,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15464.1650533676,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4833841324
          },
          {
            'recordTime': 1535873400000,
            'plusTotalFlux': 15475.6484375,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15464.1632680893,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4851694107
          },
          {
            'recordTime': 1535873700000,
            'plusTotalFlux': 15475.650390625,
            'flux': 0.2912266552,
            'pressure': 0.5334,
            'totalFlux': 15464.1652212143,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4851694107
          },
          {
            'recordTime': 1535874000000,
            'plusTotalFlux': 15475.65234375,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.1671743393,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4851694107
          },
          {
            'recordTime': 1535874300000,
            'plusTotalFlux': 15475.65234375,
            'flux': -0.3749563694,
            'pressure': 0.5346,
            'totalFlux': 15464.1631441116,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4891996384
          },
          {
            'recordTime': 1535874600000,
            'plusTotalFlux': 15475.6533203125,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15464.1625289917,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4907913208
          },
          {
            'recordTime': 1535874900000,
            'plusTotalFlux': 15475.6533203125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.1555719376,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4977483749
          },
          {
            'recordTime': 1535875200000,
            'plusTotalFlux': 15475.654296875,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15464.1565485001,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 11.4977483749
          },
          {
            'recordTime': 1535875500000,
            'plusTotalFlux': 15475.654296875,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15464.1450891495,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5092077255
          },
          {
            'recordTime': 1535875800000,
            'plusTotalFlux': 15475.654296875,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15464.1450891495,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5092077255
          },
          {
            'recordTime': 1535876100000,
            'plusTotalFlux': 15475.6552734375,
            'flux': 0.0,
            'pressure': 0.5254,
            'totalFlux': 15464.1451301575,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.51014328
          },
          {
            'recordTime': 1535876400000,
            'plusTotalFlux': 15475.6552734375,
            'flux': -0.2619948983,
            'pressure': 0.523,
            'totalFlux': 15464.1391925812,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5160808563
          },
          {
            'recordTime': 1535876700000,
            'plusTotalFlux': 15475.6552734375,
            'flux': 0.0,
            'pressure': 0.5267,
            'totalFlux': 15464.1376962662,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5175771713
          },
          {
            'recordTime': 1535877000000,
            'plusTotalFlux': 15475.6552734375,
            'flux': 0.0,
            'pressure': 0.5199,
            'totalFlux': 15464.1367740631,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5184993744
          },
          {
            'recordTime': 1535877300000,
            'plusTotalFlux': 15475.6552734375,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 15464.1367740631,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5184993744
          },
          {
            'recordTime': 1535877600000,
            'plusTotalFlux': 15475.6552734375,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15464.1367740631,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5184993744
          },
          {
            'recordTime': 1535877900000,
            'plusTotalFlux': 15475.6552734375,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15464.1358509064,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5194225311
          },
          {
            'recordTime': 1535878200000,
            'plusTotalFlux': 15475.6552734375,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15464.1283349991,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5269384384
          },
          {
            'recordTime': 1535878500000,
            'plusTotalFlux': 15475.658203125,
            'flux': 0.0,
            'pressure': 0.5215,
            'totalFlux': 15464.1312646866,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5269384384
          },
          {
            'recordTime': 1535878800000,
            'plusTotalFlux': 15475.6591796875,
            'flux': -0.2243410647,
            'pressure': 0.5273,
            'totalFlux': 15464.1273155212,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5318641663
          },
          {
            'recordTime': 1535879100000,
            'plusTotalFlux': 15475.6591796875,
            'flux': 0.0,
            'pressure': 0.5248,
            'totalFlux': 15464.1260051727,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5331745148
          },
          {
            'recordTime': 1535879400000,
            'plusTotalFlux': 15475.6591796875,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 15464.1260051727,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5331745148
          },
          {
            'recordTime': 1535879700000,
            'plusTotalFlux': 15475.6591796875,
            'flux': 0.0,
            'pressure': 0.5202,
            'totalFlux': 15464.1260051727,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5331745148
          },
          {
            'recordTime': 1535880000000,
            'plusTotalFlux': 15475.6611328125,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15464.1227664948,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5383663177
          },
          {
            'recordTime': 1535880300000,
            'plusTotalFlux': 15475.6611328125,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15464.1201543808,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5409784317
          },
          {
            'recordTime': 1535880600000,
            'plusTotalFlux': 15475.6611328125,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15464.1150779724,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5460548401
          },
          {
            'recordTime': 1535880900000,
            'plusTotalFlux': 15475.6611328125,
            'flux': 0.0,
            'pressure': 0.5288,
            'totalFlux': 15464.1130390167,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5480937958
          },
          {
            'recordTime': 1535881200000,
            'plusTotalFlux': 15475.6611328125,
            'flux': 0.0,
            'pressure': 0.5239,
            'totalFlux': 15464.1130390167,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5480937958
          },
          {
            'recordTime': 1535881500000,
            'plusTotalFlux': 15475.6650390625,
            'flux': -0.2359268665,
            'pressure': 0.5276,
            'totalFlux': 15464.1164216995,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.548617363
          },
          {
            'recordTime': 1535881800000,
            'plusTotalFlux': 15475.666015625,
            'flux': 0.0,
            'pressure': 0.5264,
            'totalFlux': 15464.1121320724,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5538835526
          },
          {
            'recordTime': 1535882100000,
            'plusTotalFlux': 15475.666015625,
            'flux': 0.0,
            'pressure': 0.5306,
            'totalFlux': 15464.1102027893,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5558128357
          },
          {
            'recordTime': 1535882400000,
            'plusTotalFlux': 15475.666015625,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15464.1102027893,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 11.5558128357
          },
          {
            'recordTime': 1535882700000,
            'plusTotalFlux': 15475.666015625,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15464.1102027893,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5558128357
          },
          {
            'recordTime': 1535883000000,
            'plusTotalFlux': 15475.666015625,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15464.1062774658,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5597381592
          },
          {
            'recordTime': 1535883300000,
            'plusTotalFlux': 15475.666015625,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 15464.1062774658,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5597381592
          },
          {
            'recordTime': 1535883600000,
            'plusTotalFlux': 15475.666015625,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15464.1062774658,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5597381592
          },
          {
            'recordTime': 1535883900000,
            'plusTotalFlux': 15475.66796875,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15464.1082305908,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5597381592
          },
          {
            'recordTime': 1535884200000,
            'plusTotalFlux': 15475.669921875,
            'flux': 0.2246083617,
            'pressure': 0.5297,
            'totalFlux': 15464.1101837158,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5597381592
          },
          {
            'recordTime': 1535884500000,
            'plusTotalFlux': 15475.6728515625,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15464.1131134033,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5597381592
          },
          {
            'recordTime': 1535884800000,
            'plusTotalFlux': 15475.677734375,
            'flux': 0.2564692795,
            'pressure': 0.5273,
            'totalFlux': 15464.1179962158,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5597381592
          },
          {
            'recordTime': 1535885100000,
            'plusTotalFlux': 15475.6826171875,
            'flux': 0.0,
            'pressure': 0.5251,
            'totalFlux': 15464.1228790283,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5597381592
          },
          {
            'recordTime': 1535885400000,
            'plusTotalFlux': 15475.6826171875,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 15464.1210813522,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5615358353
          },
          {
            'recordTime': 1535885700000,
            'plusTotalFlux': 15475.6826171875,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 15464.1210813522,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5615358353
          },
          {
            'recordTime': 1535886000000,
            'plusTotalFlux': 15475.6826171875,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15464.1210813522,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5615358353
          },
          {
            'recordTime': 1535886300000,
            'plusTotalFlux': 15475.6826171875,
            'flux': -0.3430954218,
            'pressure': 0.5273,
            'totalFlux': 15464.1182451248,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5643720627
          },
          {
            'recordTime': 1535886600000,
            'plusTotalFlux': 15475.6826171875,
            'flux': -0.2106863856,
            'pressure': 0.526,
            'totalFlux': 15464.1086521149,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5739650726
          },
          {
            'recordTime': 1535886900000,
            'plusTotalFlux': 15475.6826171875,
            'flux': -0.2446161807,
            'pressure': 0.5303,
            'totalFlux': 15464.1032934189,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5793237686
          },
          {
            'recordTime': 1535887200000,
            'plusTotalFlux': 15475.6826171875,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15464.1001462936,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5824708939
          },
          {
            'recordTime': 1535887500000,
            'plusTotalFlux': 15475.6826171875,
            'flux': -0.3488883078,
            'pressure': 0.5322,
            'totalFlux': 15464.0993709564,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5832462311
          },
          {
            'recordTime': 1535887800000,
            'plusTotalFlux': 15475.6826171875,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15464.0964126587,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5862045288
          },
          {
            'recordTime': 1535888100000,
            'plusTotalFlux': 15475.68359375,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15464.0973892212,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5862045288
          },
          {
            'recordTime': 1535888400000,
            'plusTotalFlux': 15475.68359375,
            'flux': 0.0,
            'pressure': 0.5276,
            'totalFlux': 15464.0973892212,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5862045288
          },
          {
            'recordTime': 1535888700000,
            'plusTotalFlux': 15475.68359375,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 15464.0963459015,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5872478485
          },
          {
            'recordTime': 1535889000000,
            'plusTotalFlux': 15475.68359375,
            'flux': -0.5168823004,
            'pressure': 0.5267,
            'totalFlux': 15464.0951967239,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5883970261
          },
          {
            'recordTime': 1535889300000,
            'plusTotalFlux': 15475.68359375,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15464.0904998779,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5930938721
          },
          {
            'recordTime': 1535889600000,
            'plusTotalFlux': 15475.68359375,
            'flux': -0.2446161807,
            'pressure': 0.5273,
            'totalFlux': 15464.0871276855,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5964660645
          },
          {
            'recordTime': 1535889900000,
            'plusTotalFlux': 15475.68359375,
            'flux': 0.0,
            'pressure': 0.5254,
            'totalFlux': 15464.0857887268,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5978050232
          },
          {
            'recordTime': 1535890200000,
            'plusTotalFlux': 15475.68359375,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15464.0857887268,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.5978050232
          },
          {
            'recordTime': 1535890500000,
            'plusTotalFlux': 15475.68359375,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15464.0825271606,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6010665894
          },
          {
            'recordTime': 1535890800000,
            'plusTotalFlux': 15475.68359375,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15464.0825271606,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6010665894
          },
          {
            'recordTime': 1535891100000,
            'plusTotalFlux': 15475.68359375,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15464.0770788193,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6065149307
          },
          {
            'recordTime': 1535891400000,
            'plusTotalFlux': 15475.6875,
            'flux': 0.0,
            'pressure': 0.5331,
            'totalFlux': 15464.0790805817,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6084194183
          },
          {
            'recordTime': 1535891700000,
            'plusTotalFlux': 15475.6875,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15464.0770778656,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6104221344
          },
          {
            'recordTime': 1535892000000,
            'plusTotalFlux': 15475.6875,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15464.0770778656,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6104221344
          },
          {
            'recordTime': 1535892300000,
            'plusTotalFlux': 15475.6875,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15464.074092865,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.613407135
          },
          {
            'recordTime': 1535892600000,
            'plusTotalFlux': 15475.6884765625,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 15464.0742197037,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6142568588
          },
          {
            'recordTime': 1535892900000,
            'plusTotalFlux': 15475.69140625,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.075255394,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.616150856
          },
          {
            'recordTime': 1535893200000,
            'plusTotalFlux': 15475.6923828125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.0762319565,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.616150856
          },
          {
            'recordTime': 1535893500000,
            'plusTotalFlux': 15475.6923828125,
            'flux': 0.0,
            'pressure': 0.5318,
            'totalFlux': 15464.0762319565,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.616150856
          },
          {
            'recordTime': 1535893800000,
            'plusTotalFlux': 15475.6923828125,
            'flux': -0.206962347,
            'pressure': 0.5346,
            'totalFlux': 15464.0704126358,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6219701767
          },
          {
            'recordTime': 1535894100000,
            'plusTotalFlux': 15475.6923828125,
            'flux': 0.0,
            'pressure': 0.5303,
            'totalFlux': 15464.0673847198,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6249980927
          },
          {
            'recordTime': 1535894400000,
            'plusTotalFlux': 15475.6923828125,
            'flux': 0.0,
            'pressure': 0.5376,
            'totalFlux': 15464.0673847198,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6249980927
          },
          {
            'recordTime': 1535894700000,
            'plusTotalFlux': 15475.693359375,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15464.0633440018,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6300153732
          },
          {
            'recordTime': 1535895000000,
            'plusTotalFlux': 15475.6962890625,
            'flux': 0.0,
            'pressure': 0.5312,
            'totalFlux': 15464.0662736893,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6300153732
          },
          {
            'recordTime': 1535895300000,
            'plusTotalFlux': 15475.697265625,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15464.0672502518,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6300153732
          },
          {
            'recordTime': 1535895600000,
            'plusTotalFlux': 15475.697265625,
            'flux': -0.3083380759,
            'pressure': 0.5276,
            'totalFlux': 15464.0596179962,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6376476288
          },
          {
            'recordTime': 1535895900000,
            'plusTotalFlux': 15475.697265625,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15464.0589332581,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6383323669
          },
          {
            'recordTime': 1535896200000,
            'plusTotalFlux': 15475.697265625,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15464.0589332581,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6383323669
          },
          {
            'recordTime': 1535896500000,
            'plusTotalFlux': 15475.69921875,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15464.0608863831,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6383323669
          },
          {
            'recordTime': 1535896800000,
            'plusTotalFlux': 15475.69921875,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15464.0586061478,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 11.6406126022
          },
          {
            'recordTime': 1535897100000,
            'plusTotalFlux': 15475.69921875,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15464.0586061478,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6406126022
          },
          {
            'recordTime': 1535897400000,
            'plusTotalFlux': 15475.69921875,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15464.0586061478,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6406126022
          },
          {
            'recordTime': 1535897700000,
            'plusTotalFlux': 15475.69921875,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15464.0536060333,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6456127167
          },
          {
            'recordTime': 1535898000000,
            'plusTotalFlux': 15475.69921875,
            'flux': -0.2938558161,
            'pressure': 0.5358,
            'totalFlux': 15464.0493497849,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6498689651
          },
          {
            'recordTime': 1535898300000,
            'plusTotalFlux': 15475.70703125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15464.0547122955,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6523189545
          },
          {
            'recordTime': 1535898600000,
            'plusTotalFlux': 15475.7080078125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15464.0539150238,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6540927887
          },
          {
            'recordTime': 1535898900000,
            'plusTotalFlux': 15475.7080078125,
            'flux': 0.0,
            'pressure': 0.5318,
            'totalFlux': 15464.0539150238,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6540927887
          },
          {
            'recordTime': 1535899200000,
            'plusTotalFlux': 15475.7080078125,
            'flux': -0.2214446366,
            'pressure': 0.5297,
            'totalFlux': 15464.0446557999,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6633520126
          },
          {
            'recordTime': 1535899500000,
            'plusTotalFlux': 15475.7080078125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.0408163071,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6671915054
          },
          {
            'recordTime': 1535899800000,
            'plusTotalFlux': 15475.7080078125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15464.0408163071,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6671915054
          },
          {
            'recordTime': 1535900100000,
            'plusTotalFlux': 15475.7080078125,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15464.0366125107,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6713953018
          },
          {
            'recordTime': 1535900400000,
            'plusTotalFlux': 15475.7080078125,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15464.0366125107,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6713953018
          },
          {
            'recordTime': 1535900700000,
            'plusTotalFlux': 15475.7080078125,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 15464.0356302261,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6723775864
          },
          {
            'recordTime': 1535901000000,
            'plusTotalFlux': 15475.7099609375,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15464.0358219147,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6741390228
          },
          {
            'recordTime': 1535901300000,
            'plusTotalFlux': 15475.712890625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15464.0387516022,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6741390228
          },
          {
            'recordTime': 1535901600000,
            'plusTotalFlux': 15475.7138671875,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15464.0353374481,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6785297394
          },
          {
            'recordTime': 1535901900000,
            'plusTotalFlux': 15475.71875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.0402202606,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6785297394
          },
          {
            'recordTime': 1535902200000,
            'plusTotalFlux': 15475.71875,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15464.0402202606,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6785297394
          },
          {
            'recordTime': 1535902500000,
            'plusTotalFlux': 15475.71875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15464.0402202606,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6785297394
          },
          {
            'recordTime': 1535902800000,
            'plusTotalFlux': 15475.71875,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15464.0402202606,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6785297394
          },
          {
            'recordTime': 1535903100000,
            'plusTotalFlux': 15475.71875,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15464.0381336212,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6806163788
          },
          {
            'recordTime': 1535903400000,
            'plusTotalFlux': 15475.71875,
            'flux': -0.2996487021,
            'pressure': 0.5383,
            'totalFlux': 15464.0340099335,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6847400665
          },
          {
            'recordTime': 1535903700000,
            'plusTotalFlux': 15475.71875,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15464.0278625488,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6908874512
          },
          {
            'recordTime': 1535904000000,
            'plusTotalFlux': 15475.71875,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15464.0278625488,
            'uploadTime': 1535904168563,
            'reverseTotalFlux': 11.6908874512
          },
          {
            'recordTime': 1535904300000,
            'plusTotalFlux': 15475.71875,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15464.0278625488,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.6908874512
          },
          {
            'recordTime': 1535904600000,
            'plusTotalFlux': 15475.720703125,
            'flux': 0.0,
            'pressure': 0.5428,
            'totalFlux': 15464.0280303955,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.6926727295
          },
          {
            'recordTime': 1535904900000,
            'plusTotalFlux': 15475.720703125,
            'flux': 0.0,
            'pressure': 0.5398,
            'totalFlux': 15464.0261726379,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.6945304871
          },
          {
            'recordTime': 1535905200000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15464.0251350403,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.6965446472
          },
          {
            'recordTime': 1535905500000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15464.0251350403,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.6965446472
          },
          {
            'recordTime': 1535905800000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5398,
            'totalFlux': 15464.0251350403,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.6965446472
          },
          {
            'recordTime': 1535906100000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15464.0200147629,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7016649246
          },
          {
            'recordTime': 1535906400000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15464.0181083679,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7035713196
          },
          {
            'recordTime': 1535906700000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15464.0181083679,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7035713196
          },
          {
            'recordTime': 1535907000000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15464.0161790848,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7055006027
          },
          {
            'recordTime': 1535907300000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15464.0161790848,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7055006027
          },
          {
            'recordTime': 1535907600000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5434,
            'totalFlux': 15464.0135974884,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7080821991
          },
          {
            'recordTime': 1535907900000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15464.0135974884,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7080821991
          },
          {
            'recordTime': 1535908200000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5447,
            'totalFlux': 15464.0135974884,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7080821991
          },
          {
            'recordTime': 1535908500000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5447,
            'totalFlux': 15464.0077114105,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.713968277
          },
          {
            'recordTime': 1535908800000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15464.0077114105,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.713968277
          },
          {
            'recordTime': 1535909100000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15464.0077114105,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.713968277
          },
          {
            'recordTime': 1535909400000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5441,
            'totalFlux': 15464.0077114105,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.713968277
          },
          {
            'recordTime': 1535909700000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15464.0058984756,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7157812119
          },
          {
            'recordTime': 1535910000000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5447,
            'totalFlux': 15464.0049514771,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7167282104
          },
          {
            'recordTime': 1535910300000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15464.0049514771,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7167282104
          },
          {
            'recordTime': 1535910600000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15464.0049514771,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7167282104
          },
          {
            'recordTime': 1535910900000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15464.0049514771,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7167282104
          },
          {
            'recordTime': 1535911200000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15464.0049514771,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7167282104
          },
          {
            'recordTime': 1535911500000,
            'plusTotalFlux': 15475.7216796875,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15464.0049514771,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7167282104
          },
          {
            'recordTime': 1535911800000,
            'plusTotalFlux': 15475.724609375,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15464.0049562454,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7196531296
          },
          {
            'recordTime': 1535912100000,
            'plusTotalFlux': 15475.724609375,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15464.0049562454,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7196531296
          },
          {
            'recordTime': 1535912400000,
            'plusTotalFlux': 15475.724609375,
            'flux': -0.2533055544,
            'pressure': 0.5492,
            'totalFlux': 15464.0041828156,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7204265594
          },
          {
            'recordTime': 1535912700000,
            'plusTotalFlux': 15475.724609375,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15464.0029067993,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7217025757
          },
          {
            'recordTime': 1535913000000,
            'plusTotalFlux': 15475.724609375,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15464.0009765625,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7236328125
          },
          {
            'recordTime': 1535913300000,
            'plusTotalFlux': 15475.7275390625,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15464.00390625,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7236328125
          },
          {
            'recordTime': 1535913600000,
            'plusTotalFlux': 15475.7275390625,
            'flux': 0.0,
            'pressure': 0.5502,
            'totalFlux': 15464.00390625,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7236328125
          },
          {
            'recordTime': 1535913900000,
            'plusTotalFlux': 15475.7275390625,
            'flux': 0.0,
            'pressure': 0.5489,
            'totalFlux': 15464.00390625,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7236328125
          },
          {
            'recordTime': 1535914200000,
            'plusTotalFlux': 15475.7275390625,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15464.00390625,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7236328125
          },
          {
            'recordTime': 1535914500000,
            'plusTotalFlux': 15475.7275390625,
            'flux': 0.0,
            'pressure': 0.5465,
            'totalFlux': 15464.00390625,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7236328125
          },
          {
            'recordTime': 1535914800000,
            'plusTotalFlux': 15475.7275390625,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15464.0030441284,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7244949341
          },
          {
            'recordTime': 1535915100000,
            'plusTotalFlux': 15475.7275390625,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15464.0030441284,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7244949341
          },
          {
            'recordTime': 1535915400000,
            'plusTotalFlux': 15475.7275390625,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15464.0030441284,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7244949341
          },
          {
            'recordTime': 1535915700000,
            'plusTotalFlux': 15475.7275390625,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15464.0030441284,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7244949341
          },
          {
            'recordTime': 1535916000000,
            'plusTotalFlux': 15475.728515625,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15464.0005645752,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7279510498
          },
          {
            'recordTime': 1535916300000,
            'plusTotalFlux': 15475.728515625,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15464.0005645752,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7279510498
          },
          {
            'recordTime': 1535916600000,
            'plusTotalFlux': 15475.728515625,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15464.0005645752,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7279510498
          },
          {
            'recordTime': 1535916900000,
            'plusTotalFlux': 15475.728515625,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15464.0005645752,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7279510498
          },
          {
            'recordTime': 1535917200000,
            'plusTotalFlux': 15475.728515625,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15464.0005645752,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7279510498
          },
          {
            'recordTime': 1535917500000,
            'plusTotalFlux': 15475.728515625,
            'flux': -0.2590984404,
            'pressure': 0.548,
            'totalFlux': 15463.9975414276,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7309741974
          },
          {
            'recordTime': 1535917800000,
            'plusTotalFlux': 15475.728515625,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15463.9953479767,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7331676483
          },
          {
            'recordTime': 1535918100000,
            'plusTotalFlux': 15475.728515625,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.9953479767,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7331676483
          },
          {
            'recordTime': 1535918400000,
            'plusTotalFlux': 15475.728515625,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.9953479767,
            'uploadTime': 1535925778890,
            'reverseTotalFlux': 11.7331676483
          },
          {
            'recordTime': 1535918700000,
            'plusTotalFlux': 15475.728515625,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15463.9953479767,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7331676483
          },
          {
            'recordTime': 1535919000000,
            'plusTotalFlux': 15475.728515625,
            'flux': 0.0,
            'pressure': 0.5502,
            'totalFlux': 15463.9953479767,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7331676483
          },
          {
            'recordTime': 1535919300000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.9973011017,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7331676483
          },
          {
            'recordTime': 1535919600000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15463.9956007004,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7348680496
          },
          {
            'recordTime': 1535919900000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15463.9956007004,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7348680496
          },
          {
            'recordTime': 1535920200000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.9956007004,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7348680496
          },
          {
            'recordTime': 1535920500000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15463.9956007004,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7348680496
          },
          {
            'recordTime': 1535920800000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.9956007004,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7348680496
          },
          {
            'recordTime': 1535921100000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15463.9956007004,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7348680496
          },
          {
            'recordTime': 1535921400000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15463.9956007004,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7348680496
          },
          {
            'recordTime': 1535921700000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15463.9890518188,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7414169312
          },
          {
            'recordTime': 1535922000000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15463.9880561829,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7424125671
          },
          {
            'recordTime': 1535922300000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.9861268997,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7443418503
          },
          {
            'recordTime': 1535922600000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5514,
            'totalFlux': 15463.9861268997,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7443418503
          },
          {
            'recordTime': 1535922900000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15463.9861268997,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7443418503
          },
          {
            'recordTime': 1535923200000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15463.9830694199,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7473993301
          },
          {
            'recordTime': 1535923500000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15463.9830694199,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7473993301
          },
          {
            'recordTime': 1535923800000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15463.9760007858,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7544679642
          },
          {
            'recordTime': 1535924100000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15463.9720926285,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7583761215
          },
          {
            'recordTime': 1535924400000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15463.9720926285,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7583761215
          },
          {
            'recordTime': 1535924700000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15463.9720926285,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7583761215
          },
          {
            'recordTime': 1535925000000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15463.968870163,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.761598587
          },
          {
            'recordTime': 1535925300000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15463.968870163,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.761598587
          },
          {
            'recordTime': 1535925600000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15463.9671211243,
            'uploadTime': 1535925819793,
            'reverseTotalFlux': 11.7633476257
          },
          {
            'recordTime': 1535925900000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.9671211243,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7633476257
          },
          {
            'recordTime': 1535926200000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5514,
            'totalFlux': 15463.9671211243,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7633476257
          },
          {
            'recordTime': 1535926500000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15463.9671211243,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7633476257
          },
          {
            'recordTime': 1535926800000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5508,
            'totalFlux': 15463.9671211243,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7633476257
          },
          {
            'recordTime': 1535927100000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15463.9661979675,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7642707825
          },
          {
            'recordTime': 1535927400000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.9661979675,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7642707825
          },
          {
            'recordTime': 1535927700000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15463.9661979675,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7642707825
          },
          {
            'recordTime': 1535928000000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.9642562866,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7662124634
          },
          {
            'recordTime': 1535928300000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15463.9642562866,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7662124634
          },
          {
            'recordTime': 1535928600000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.9642562866,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7662124634
          },
          {
            'recordTime': 1535928900000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15463.9642562866,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7662124634
          },
          {
            'recordTime': 1535929200000,
            'plusTotalFlux': 15475.73046875,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15463.9624471664,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7680215836
          },
          {
            'recordTime': 1535929500000,
            'plusTotalFlux': 15475.7314453125,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15463.9634237289,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7680215836
          },
          {
            'recordTime': 1535929800000,
            'plusTotalFlux': 15475.7314453125,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15463.9590616226,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7723836899
          },
          {
            'recordTime': 1535930100000,
            'plusTotalFlux': 15475.7314453125,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.9562568665,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.775188446
          },
          {
            'recordTime': 1535930400000,
            'plusTotalFlux': 15475.7314453125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.9553833008,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7760620117
          },
          {
            'recordTime': 1535930700000,
            'plusTotalFlux': 15475.7314453125,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15463.9553833008,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7760620117
          },
          {
            'recordTime': 1535931000000,
            'plusTotalFlux': 15475.7314453125,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15463.9528627396,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7785825729
          },
          {
            'recordTime': 1535931300000,
            'plusTotalFlux': 15475.7314453125,
            'flux': 0.0,
            'pressure': 0.5441,
            'totalFlux': 15463.9528627396,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7785825729
          },
          {
            'recordTime': 1535931600000,
            'plusTotalFlux': 15475.7314453125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.9528627396,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7785825729
          },
          {
            'recordTime': 1535931900000,
            'plusTotalFlux': 15475.7314453125,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15463.9528627396,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7785825729
          },
          {
            'recordTime': 1535932200000,
            'plusTotalFlux': 15475.7314453125,
            'flux': 0.0,
            'pressure': 0.5425,
            'totalFlux': 15463.9528627396,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7785825729
          },
          {
            'recordTime': 1535932500000,
            'plusTotalFlux': 15475.732421875,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15463.9508056641,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7816162109
          },
          {
            'recordTime': 1535932800000,
            'plusTotalFlux': 15475.732421875,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15463.9508056641,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7816162109
          },
          {
            'recordTime': 1535933100000,
            'plusTotalFlux': 15475.7333984375,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.9517822266,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7816162109
          },
          {
            'recordTime': 1535933400000,
            'plusTotalFlux': 15475.7333984375,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15463.9517822266,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7816162109
          },
          {
            'recordTime': 1535933700000,
            'plusTotalFlux': 15475.7333984375,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.9517822266,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7816162109
          },
          {
            'recordTime': 1535934000000,
            'plusTotalFlux': 15475.7333984375,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15463.9517822266,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7816162109
          },
          {
            'recordTime': 1535934300000,
            'plusTotalFlux': 15475.7333984375,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.9517822266,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7816162109
          },
          {
            'recordTime': 1535934600000,
            'plusTotalFlux': 15475.7333984375,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.9507627487,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7826356888
          },
          {
            'recordTime': 1535934900000,
            'plusTotalFlux': 15475.7333984375,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.9498996735,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.783498764
          },
          {
            'recordTime': 1535935200000,
            'plusTotalFlux': 15475.7333984375,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 15463.9441804886,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7892179489
          },
          {
            'recordTime': 1535935500000,
            'plusTotalFlux': 15475.7353515625,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 15463.9461336136,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7892179489
          },
          {
            'recordTime': 1535935800000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.941945076,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7973127365
          },
          {
            'recordTime': 1535936100000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.941945076,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7973127365
          },
          {
            'recordTime': 1535936400000,
            'plusTotalFlux': 15475.7392578125,
            'flux': -0.2562020123,
            'pressure': 0.5325,
            'totalFlux': 15463.940454483,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.7988033295
          },
          {
            'recordTime': 1535936700000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.9349651337,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.8042926788
          },
          {
            'recordTime': 1535937000000,
            'plusTotalFlux': 15475.7392578125,
            'flux': -0.2619948983,
            'pressure': 0.5318,
            'totalFlux': 15463.933339119,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.8059186935
          },
          {
            'recordTime': 1535937300000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 15463.9287576675,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.810500145
          },
          {
            'recordTime': 1535937600000,
            'plusTotalFlux': 15475.7392578125,
            'flux': -0.2214446366,
            'pressure': 0.5254,
            'totalFlux': 15463.9282035828,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.8110542297
          },
          {
            'recordTime': 1535937900000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15463.9239997864,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.8152580261
          },
          {
            'recordTime': 1535938200000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15463.923125267,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.8161325455
          },
          {
            'recordTime': 1535938500000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.5199,
            'totalFlux': 15463.923125267,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.8161325455
          },
          {
            'recordTime': 1535938800000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 15463.923125267,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.8161325455
          },
          {
            'recordTime': 1535939100000,
            'plusTotalFlux': 15475.7392578125,
            'flux': -0.2938558161,
            'pressure': 0.5282,
            'totalFlux': 15463.9118003845,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.827457428
          },
          {
            'recordTime': 1535939400000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.5178,
            'totalFlux': 15463.9082384109,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.8310194016
          },
          {
            'recordTime': 1535939700000,
            'plusTotalFlux': 15475.7392578125,
            'flux': -0.2619948983,
            'pressure': 0.5294,
            'totalFlux': 15463.9050750732,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.8341827393
          },
          {
            'recordTime': 1535940000000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.5239,
            'totalFlux': 15463.8974380493,
            'uploadTime': 1535947319177,
            'reverseTotalFlux': 11.8418197632
          },
          {
            'recordTime': 1535940300000,
            'plusTotalFlux': 15475.7392578125,
            'flux': 0.0,
            'pressure': 0.5193,
            'totalFlux': 15463.8974380493,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8418197632
          },
          {
            'recordTime': 1535940600000,
            'plusTotalFlux': 15475.744140625,
            'flux': 0.0,
            'pressure': 0.5202,
            'totalFlux': 15463.9003915787,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8437490463
          },
          {
            'recordTime': 1535940900000,
            'plusTotalFlux': 15475.744140625,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15463.8993005753,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8448400497
          },
          {
            'recordTime': 1535941200000,
            'plusTotalFlux': 15475.744140625,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 15463.8993005753,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8448400497
          },
          {
            'recordTime': 1535941500000,
            'plusTotalFlux': 15475.74609375,
            'flux': 0.0,
            'pressure': 0.5196,
            'totalFlux': 15463.9012537003,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8448400497
          },
          {
            'recordTime': 1535941800000,
            'plusTotalFlux': 15475.74609375,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 15463.9012537003,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8448400497
          },
          {
            'recordTime': 1535942100000,
            'plusTotalFlux': 15475.7470703125,
            'flux': 0.0,
            'pressure': 0.5215,
            'totalFlux': 15463.9022302628,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8448400497
          },
          {
            'recordTime': 1535942400000,
            'plusTotalFlux': 15475.75,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.9051599503,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8448400497
          },
          {
            'recordTime': 1535942700000,
            'plusTotalFlux': 15475.7509765625,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15463.9061365128,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8448400497
          },
          {
            'recordTime': 1535943000000,
            'plusTotalFlux': 15475.7509765625,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 15463.9061365128,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8448400497
          },
          {
            'recordTime': 1535943300000,
            'plusTotalFlux': 15475.7509765625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.8988742828,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8521022797
          },
          {
            'recordTime': 1535943600000,
            'plusTotalFlux': 15475.7509765625,
            'flux': -0.201169461,
            'pressure': 0.5264,
            'totalFlux': 15463.8933582306,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8576183319
          },
          {
            'recordTime': 1535943900000,
            'plusTotalFlux': 15475.7509765625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.8930225372,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8579540253
          },
          {
            'recordTime': 1535944200000,
            'plusTotalFlux': 15475.7509765625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.8921842575,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.858792305
          },
          {
            'recordTime': 1535944500000,
            'plusTotalFlux': 15475.7509765625,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15463.8921842575,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.858792305
          },
          {
            'recordTime': 1535944800000,
            'plusTotalFlux': 15475.751953125,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 15463.89316082,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.858792305
          },
          {
            'recordTime': 1535945100000,
            'plusTotalFlux': 15475.751953125,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15463.8897180557,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8622350693
          },
          {
            'recordTime': 1535945400000,
            'plusTotalFlux': 15475.7529296875,
            'flux': -0.2677877545,
            'pressure': 0.5233,
            'totalFlux': 15463.8848199844,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8681097031
          },
          {
            'recordTime': 1535945700000,
            'plusTotalFlux': 15475.7529296875,
            'flux': 0.0,
            'pressure': 0.519,
            'totalFlux': 15463.8821430206,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8707866669
          },
          {
            'recordTime': 1535946000000,
            'plusTotalFlux': 15475.7587890625,
            'flux': 0.0,
            'pressure': 0.5251,
            'totalFlux': 15463.8861694336,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8726196289
          },
          {
            'recordTime': 1535946300000,
            'plusTotalFlux': 15475.7587890625,
            'flux': 0.0,
            'pressure': 0.5303,
            'totalFlux': 15463.8861694336,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8726196289
          },
          {
            'recordTime': 1535946600000,
            'plusTotalFlux': 15475.7587890625,
            'flux': 0.0,
            'pressure': 0.5312,
            'totalFlux': 15463.8861694336,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8726196289
          },
          {
            'recordTime': 1535946900000,
            'plusTotalFlux': 15475.7587890625,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 15463.8861694336,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8726196289
          },
          {
            'recordTime': 1535947200000,
            'plusTotalFlux': 15475.763671875,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.8910522461,
            'uploadTime': 1535947342663,
            'reverseTotalFlux': 11.8726196289
          },
          {
            'recordTime': 1535947500000,
            'plusTotalFlux': 15475.763671875,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15463.8910522461,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8726196289
          },
          {
            'recordTime': 1535947800000,
            'plusTotalFlux': 15475.763671875,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.8910522461,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8726196289
          },
          {
            'recordTime': 1535948100000,
            'plusTotalFlux': 15475.763671875,
            'flux': 0.0,
            'pressure': 0.5337,
            'totalFlux': 15463.8892908096,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8743810654
          },
          {
            'recordTime': 1535948400000,
            'plusTotalFlux': 15475.763671875,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15463.8817138672,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8819580078
          },
          {
            'recordTime': 1535948700000,
            'plusTotalFlux': 15475.763671875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.8817138672,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8819580078
          },
          {
            'recordTime': 1535949000000,
            'plusTotalFlux': 15475.763671875,
            'flux': 0.0,
            'pressure': 0.5294,
            'totalFlux': 15463.8800010681,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8836708069
          },
          {
            'recordTime': 1535949300000,
            'plusTotalFlux': 15475.763671875,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15463.8791027069,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8845691681
          },
          {
            'recordTime': 1535949600000,
            'plusTotalFlux': 15475.763671875,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15463.8772573471,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8864145279
          },
          {
            'recordTime': 1535949900000,
            'plusTotalFlux': 15475.763671875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.8753395081,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8883323669
          },
          {
            'recordTime': 1535950200000,
            'plusTotalFlux': 15475.765625,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.8762969971,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8893280029
          },
          {
            'recordTime': 1535950500000,
            'plusTotalFlux': 15475.767578125,
            'flux': 0.0,
            'pressure': 0.5306,
            'totalFlux': 15463.8772554398,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8903226852
          },
          {
            'recordTime': 1535950800000,
            'plusTotalFlux': 15475.767578125,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.8772554398,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8903226852
          },
          {
            'recordTime': 1535951100000,
            'plusTotalFlux': 15475.767578125,
            'flux': 0.0,
            'pressure': 0.5349,
            'totalFlux': 15463.8772554398,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8903226852
          },
          {
            'recordTime': 1535951400000,
            'plusTotalFlux': 15475.767578125,
            'flux': -0.2562019527,
            'pressure': 0.5346,
            'totalFlux': 15463.8755235672,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8920545578
          },
          {
            'recordTime': 1535951700000,
            'plusTotalFlux': 15475.771484375,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.8779830933,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8935012817
          },
          {
            'recordTime': 1535952000000,
            'plusTotalFlux': 15475.7744140625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.8809127808,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8935012817
          },
          {
            'recordTime': 1535952300000,
            'plusTotalFlux': 15475.7744140625,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.8769140244,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.8975000381
          },
          {
            'recordTime': 1535952600000,
            'plusTotalFlux': 15475.7744140625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.8706159592,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9037981033
          },
          {
            'recordTime': 1535952900000,
            'plusTotalFlux': 15475.7763671875,
            'flux': 0.0,
            'pressure': 0.5318,
            'totalFlux': 15463.8725690842,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9037981033
          },
          {
            'recordTime': 1535953200000,
            'plusTotalFlux': 15475.7763671875,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 15463.8725690842,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9037981033
          },
          {
            'recordTime': 1535953500000,
            'plusTotalFlux': 15475.77734375,
            'flux': 0.2217119038,
            'pressure': 0.5392,
            'totalFlux': 15463.8710126877,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9063310623
          },
          {
            'recordTime': 1535953800000,
            'plusTotalFlux': 15475.779296875,
            'flux': -0.2475126386,
            'pressure': 0.5346,
            'totalFlux': 15463.8691806793,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9101161957
          },
          {
            'recordTime': 1535954100000,
            'plusTotalFlux': 15475.779296875,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15463.8667173386,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9125795364
          },
          {
            'recordTime': 1535954400000,
            'plusTotalFlux': 15475.779296875,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.8667173386,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9125795364
          },
          {
            'recordTime': 1535954700000,
            'plusTotalFlux': 15475.779296875,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15463.8636713028,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9156255722
          },
          {
            'recordTime': 1535955000000,
            'plusTotalFlux': 15475.779296875,
            'flux': 0.0,
            'pressure': 0.53,
            'totalFlux': 15463.8636713028,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9156255722
          },
          {
            'recordTime': 1535955300000,
            'plusTotalFlux': 15475.779296875,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 15463.8613195419,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9179773331
          },
          {
            'recordTime': 1535955600000,
            'plusTotalFlux': 15475.779296875,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15463.8613195419,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9179773331
          },
          {
            'recordTime': 1535955900000,
            'plusTotalFlux': 15475.779296875,
            'flux': 0.0,
            'pressure': 0.5331,
            'totalFlux': 15463.8548612595,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9244356155
          },
          {
            'recordTime': 1535956200000,
            'plusTotalFlux': 15475.783203125,
            'flux': 0.3433627188,
            'pressure': 0.5346,
            'totalFlux': 15463.8587675095,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9244356155
          },
          {
            'recordTime': 1535956500000,
            'plusTotalFlux': 15475.7841796875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.859744072,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9244356155
          },
          {
            'recordTime': 1535956800000,
            'plusTotalFlux': 15475.7841796875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.8588695526,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9253101349
          },
          {
            'recordTime': 1535957100000,
            'plusTotalFlux': 15475.7880859375,
            'flux': 0.2188154459,
            'pressure': 0.5355,
            'totalFlux': 15463.8627758026,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9253101349
          },
          {
            'recordTime': 1535957400000,
            'plusTotalFlux': 15475.7880859375,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15463.8618774414,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9262084961
          },
          {
            'recordTime': 1535957700000,
            'plusTotalFlux': 15475.7880859375,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15463.8601760864,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9279098511
          },
          {
            'recordTime': 1535958000000,
            'plusTotalFlux': 15475.7880859375,
            'flux': 0.0,
            'pressure': 0.5328,
            'totalFlux': 15463.8601760864,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9279098511
          },
          {
            'recordTime': 1535958300000,
            'plusTotalFlux': 15475.7880859375,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.8601760864,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9279098511
          },
          {
            'recordTime': 1535958600000,
            'plusTotalFlux': 15475.7880859375,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15463.8601760864,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9279098511
          },
          {
            'recordTime': 1535958900000,
            'plusTotalFlux': 15475.7900390625,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15463.8593997955,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.930639267
          },
          {
            'recordTime': 1535959200000,
            'plusTotalFlux': 15475.7900390625,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 15463.8540239334,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9360151291
          },
          {
            'recordTime': 1535959500000,
            'plusTotalFlux': 15475.7900390625,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 15463.8540239334,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9360151291
          },
          {
            'recordTime': 1535959800000,
            'plusTotalFlux': 15475.7900390625,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15463.8540239334,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9360151291
          },
          {
            'recordTime': 1535960100000,
            'plusTotalFlux': 15475.7900390625,
            'flux': 0.0,
            'pressure': 0.5248,
            'totalFlux': 15463.8540239334,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9360151291
          },
          {
            'recordTime': 1535960400000,
            'plusTotalFlux': 15475.7900390625,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15463.851319313,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9387197495
          },
          {
            'recordTime': 1535960700000,
            'plusTotalFlux': 15475.7900390625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.849316597,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9407224655
          },
          {
            'recordTime': 1535961000000,
            'plusTotalFlux': 15475.79296875,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15463.8522462845,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9407224655
          },
          {
            'recordTime': 1535961300000,
            'plusTotalFlux': 15475.798828125,
            'flux': 0.0,
            'pressure': 0.5193,
            'totalFlux': 15463.8570861816,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9417419434
          },
          {
            'recordTime': 1535961600000,
            'plusTotalFlux': 15475.798828125,
            'flux': 0.0,
            'pressure': 0.5102,
            'totalFlux': 15463.8570861816,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 11.9417419434
          },
          {
            'recordTime': 1535961900000,
            'plusTotalFlux': 15475.798828125,
            'flux': 0.0,
            'pressure': 0.5144,
            'totalFlux': 15463.8570861816,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9417419434
          },
          {
            'recordTime': 1535962200000,
            'plusTotalFlux': 15475.798828125,
            'flux': 0.0,
            'pressure': 0.5184,
            'totalFlux': 15463.8570861816,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9417419434
          },
          {
            'recordTime': 1535962500000,
            'plusTotalFlux': 15475.8017578125,
            'flux': 0.0,
            'pressure': 0.5102,
            'totalFlux': 15463.8600158691,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9417419434
          },
          {
            'recordTime': 1535962800000,
            'plusTotalFlux': 15475.8017578125,
            'flux': -0.4097137153,
            'pressure': 0.5114,
            'totalFlux': 15463.8565006256,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9452571869
          },
          {
            'recordTime': 1535963100000,
            'plusTotalFlux': 15475.802734375,
            'flux': 0.0,
            'pressure': 0.509,
            'totalFlux': 15463.8556261063,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9471082687
          },
          {
            'recordTime': 1535963400000,
            'plusTotalFlux': 15475.8046875,
            'flux': 0.0,
            'pressure': 0.5074,
            'totalFlux': 15463.8575792313,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9471082687
          },
          {
            'recordTime': 1535963700000,
            'plusTotalFlux': 15475.8046875,
            'flux': -0.2082037032,
            'pressure': 0.5047,
            'totalFlux': 15463.8542795181,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9504079819
          },
          {
            'recordTime': 1535964000000,
            'plusTotalFlux': 15475.8046875,
            'flux': -0.2330303788,
            'pressure': 0.5028,
            'totalFlux': 15463.8533573151,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9513301849
          },
          {
            'recordTime': 1535964300000,
            'plusTotalFlux': 15475.8046875,
            'flux': 0.0,
            'pressure': 0.5035,
            'totalFlux': 15463.8518972397,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9527902603
          },
          {
            'recordTime': 1535964600000,
            'plusTotalFlux': 15475.8046875,
            'flux': 0.0,
            'pressure': 0.5059,
            'totalFlux': 15463.8518972397,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9527902603
          },
          {
            'recordTime': 1535964900000,
            'plusTotalFlux': 15475.8046875,
            'flux': 0.0,
            'pressure': 0.5065,
            'totalFlux': 15463.8518972397,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9527902603
          },
          {
            'recordTime': 1535965200000,
            'plusTotalFlux': 15475.8046875,
            'flux': 0.0,
            'pressure': 0.5105,
            'totalFlux': 15463.8510389328,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9536485672
          },
          {
            'recordTime': 1535965500000,
            'plusTotalFlux': 15475.8046875,
            'flux': 0.0,
            'pressure': 0.5105,
            'totalFlux': 15463.8510389328,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9536485672
          },
          {
            'recordTime': 1535965800000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5117,
            'totalFlux': 15463.8520154953,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9536485672
          },
          {
            'recordTime': 1535966100000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5117,
            'totalFlux': 15463.8520154953,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9536485672
          },
          {
            'recordTime': 1535966400000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.512,
            'totalFlux': 15463.8490171432,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9566469193
          },
          {
            'recordTime': 1535966700000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.512,
            'totalFlux': 15463.8490171432,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9566469193
          },
          {
            'recordTime': 1535967000000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5163,
            'totalFlux': 15463.8490171432,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9566469193
          },
          {
            'recordTime': 1535967300000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5151,
            'totalFlux': 15463.8490171432,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9566469193
          },
          {
            'recordTime': 1535967600000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5199,
            'totalFlux': 15463.8490171432,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9566469193
          },
          {
            'recordTime': 1535967900000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5138,
            'totalFlux': 15463.8490171432,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9566469193
          },
          {
            'recordTime': 1535968200000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5135,
            'totalFlux': 15463.8490171432,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9566469193
          },
          {
            'recordTime': 1535968500000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5199,
            'totalFlux': 15463.8490171432,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9566469193
          },
          {
            'recordTime': 1535968800000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5163,
            'totalFlux': 15463.842912674,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 11.9627513885
          },
          {
            'recordTime': 1535969100000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5193,
            'totalFlux': 15463.842912674,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9627513885
          },
          {
            'recordTime': 1535969400000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5199,
            'totalFlux': 15463.842912674,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9627513885
          },
          {
            'recordTime': 1535969700000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5184,
            'totalFlux': 15463.842912674,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9627513885
          },
          {
            'recordTime': 1535970000000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15463.842912674,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9627513885
          },
          {
            'recordTime': 1535970300000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 15463.838968277,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9666957855
          },
          {
            'recordTime': 1535970600000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.519,
            'totalFlux': 15463.838968277,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9666957855
          },
          {
            'recordTime': 1535970900000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5151,
            'totalFlux': 15463.838968277,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9666957855
          },
          {
            'recordTime': 1535971200000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5199,
            'totalFlux': 15463.8359947205,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.969669342
          },
          {
            'recordTime': 1535971500000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5102,
            'totalFlux': 15463.8359947205,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.969669342
          },
          {
            'recordTime': 1535971800000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5123,
            'totalFlux': 15463.8359947205,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.969669342
          },
          {
            'recordTime': 1535972100000,
            'plusTotalFlux': 15475.8056640625,
            'flux': 0.0,
            'pressure': 0.5163,
            'totalFlux': 15463.8266992569,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9789648056
          },
          {
            'recordTime': 1535972400000,
            'plusTotalFlux': 15475.806640625,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15463.8276758194,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9789648056
          },
          {
            'recordTime': 1535972700000,
            'plusTotalFlux': 15475.8095703125,
            'flux': 0.0,
            'pressure': 0.5175,
            'totalFlux': 15463.828578949,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9809913635
          },
          {
            'recordTime': 1535973000000,
            'plusTotalFlux': 15475.8095703125,
            'flux': 0.0,
            'pressure': 0.5151,
            'totalFlux': 15463.828578949,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9809913635
          },
          {
            'recordTime': 1535973300000,
            'plusTotalFlux': 15475.8095703125,
            'flux': -0.311234504,
            'pressure': 0.5187,
            'totalFlux': 15463.8266029358,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9829673767
          },
          {
            'recordTime': 1535973600000,
            'plusTotalFlux': 15475.8095703125,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 15463.8190412521,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9905290604
          },
          {
            'recordTime': 1535973900000,
            'plusTotalFlux': 15475.8095703125,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15463.8190412521,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9905290604
          },
          {
            'recordTime': 1535974200000,
            'plusTotalFlux': 15475.8095703125,
            'flux': 0.0,
            'pressure': 0.5248,
            'totalFlux': 15463.8190412521,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9905290604
          },
          {
            'recordTime': 1535974500000,
            'plusTotalFlux': 15475.8095703125,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 15463.81731987,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9922504425
          },
          {
            'recordTime': 1535974800000,
            'plusTotalFlux': 15475.8095703125,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.81731987,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9922504425
          },
          {
            'recordTime': 1535975100000,
            'plusTotalFlux': 15475.8095703125,
            'flux': 0.0,
            'pressure': 0.5251,
            'totalFlux': 15463.8161315918,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9934387207
          },
          {
            'recordTime': 1535975400000,
            'plusTotalFlux': 15475.8095703125,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15463.8152093887,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9943609238
          },
          {
            'recordTime': 1535975700000,
            'plusTotalFlux': 15475.8095703125,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.8152093887,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9943609238
          },
          {
            'recordTime': 1535976000000,
            'plusTotalFlux': 15475.810546875,
            'flux': 0.0,
            'pressure': 0.53,
            'totalFlux': 15463.8150701523,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9954767227
          },
          {
            'recordTime': 1535976300000,
            'plusTotalFlux': 15475.810546875,
            'flux': 0.0,
            'pressure': 0.5279,
            'totalFlux': 15463.8150701523,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9954767227
          },
          {
            'recordTime': 1535976600000,
            'plusTotalFlux': 15475.810546875,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.8150701523,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9954767227
          },
          {
            'recordTime': 1535976900000,
            'plusTotalFlux': 15475.810546875,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15463.8139057159,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9966411591
          },
          {
            'recordTime': 1535977200000,
            'plusTotalFlux': 15475.8115234375,
            'flux': 0.0,
            'pressure': 0.5288,
            'totalFlux': 15463.8148822784,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9966411591
          },
          {
            'recordTime': 1535977500000,
            'plusTotalFlux': 15475.8115234375,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15463.8148822784,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9966411591
          },
          {
            'recordTime': 1535977800000,
            'plusTotalFlux': 15475.8115234375,
            'flux': 0.0,
            'pressure': 0.5254,
            'totalFlux': 15463.8148822784,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9966411591
          },
          {
            'recordTime': 1535978100000,
            'plusTotalFlux': 15475.8115234375,
            'flux': 0.0,
            'pressure': 0.5325,
            'totalFlux': 15463.8119497299,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9995737076
          },
          {
            'recordTime': 1535978400000,
            'plusTotalFlux': 15475.8115234375,
            'flux': 0.0,
            'pressure': 0.5312,
            'totalFlux': 15463.8119497299,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9995737076
          },
          {
            'recordTime': 1535978700000,
            'plusTotalFlux': 15475.8115234375,
            'flux': 0.0,
            'pressure': 0.5328,
            'totalFlux': 15463.8119497299,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9995737076
          },
          {
            'recordTime': 1535979000000,
            'plusTotalFlux': 15475.814453125,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15463.8148794174,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9995737076
          },
          {
            'recordTime': 1535979300000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15463.8168325424,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 11.9995737076
          },
          {
            'recordTime': 1535979600000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5294,
            'totalFlux': 15463.8158855438,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0005207062
          },
          {
            'recordTime': 1535979900000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.8158855438,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0005207062
          },
          {
            'recordTime': 1535980200000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5279,
            'totalFlux': 15463.8150396347,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0013666153
          },
          {
            'recordTime': 1535980500000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.8150396347,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0013666153
          },
          {
            'recordTime': 1535980800000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15463.8150396347,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0013666153
          },
          {
            'recordTime': 1535981100000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15463.8141527176,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0022535324
          },
          {
            'recordTime': 1535981400000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5303,
            'totalFlux': 15463.8141527176,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0022535324
          },
          {
            'recordTime': 1535981700000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.8086051941,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0078010559
          },
          {
            'recordTime': 1535982000000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5312,
            'totalFlux': 15463.8086051941,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0078010559
          },
          {
            'recordTime': 1535982300000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.7991962433,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0172100067
          },
          {
            'recordTime': 1535982600000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5355,
            'totalFlux': 15463.7974710464,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0189352036
          },
          {
            'recordTime': 1535982900000,
            'plusTotalFlux': 15475.81640625,
            'flux': 0.0,
            'pressure': 0.5337,
            'totalFlux': 15463.7974710464,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0189352036
          },
          {
            'recordTime': 1535983200000,
            'plusTotalFlux': 15475.8232421875,
            'flux': -0.2426852584,
            'pressure': 0.5279,
            'totalFlux': 15463.8011112213,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 12.0221309662
          },
          {
            'recordTime': 1535983500000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.8005723953,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0226697922
          },
          {
            'recordTime': 1535983800000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5294,
            'totalFlux': 15463.7923231125,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.030919075
          },
          {
            'recordTime': 1535984100000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15463.7923231125,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.030919075
          },
          {
            'recordTime': 1535984400000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.7831249237,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0401172638
          },
          {
            'recordTime': 1535984700000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535985000000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535985300000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535985600000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535985900000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535986200000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535986500000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535986800000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535987100000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5349,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535987400000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535987700000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535988000000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15463.7747135162,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0485286713
          },
          {
            'recordTime': 1535988300000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.7738513947,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0493907928
          },
          {
            'recordTime': 1535988600000,
            'plusTotalFlux': 15475.8232421875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15463.7720537186,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0511884689
          },
          {
            'recordTime': 1535988900000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.7680950165,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0580768585
          },
          {
            'recordTime': 1535989200000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5254,
            'totalFlux': 15463.7680950165,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0580768585
          },
          {
            'recordTime': 1535989500000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.7680950165,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0580768585
          },
          {
            'recordTime': 1535989800000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15463.7642068863,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0619649887
          },
          {
            'recordTime': 1535990100000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.7642068863,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0619649887
          },
          {
            'recordTime': 1535990400000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.7642068863,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 12.0619649887
          },
          {
            'recordTime': 1535990700000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.7642068863,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0619649887
          },
          {
            'recordTime': 1535991000000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5364,
            'totalFlux': 15463.7642068863,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0619649887
          },
          {
            'recordTime': 1535991300000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.538,
            'totalFlux': 15463.7642068863,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0619649887
          },
          {
            'recordTime': 1535991600000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5386,
            'totalFlux': 15463.7642068863,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0619649887
          },
          {
            'recordTime': 1535991900000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.7611570358,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0650148392
          },
          {
            'recordTime': 1535992200000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15463.7611570358,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0650148392
          },
          {
            'recordTime': 1535992500000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.7611570358,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0650148392
          },
          {
            'recordTime': 1535992800000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15463.7611570358,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0650148392
          },
          {
            'recordTime': 1535993100000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.7611570358,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0650148392
          },
          {
            'recordTime': 1535993400000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.7611570358,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0650148392
          },
          {
            'recordTime': 1535993700000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5441,
            'totalFlux': 15463.7611570358,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0650148392
          },
          {
            'recordTime': 1535994000000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5434,
            'totalFlux': 15463.7594804764,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0666913986
          },
          {
            'recordTime': 1535994300000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.7594804764,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0666913986
          },
          {
            'recordTime': 1535994600000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5401,
            'totalFlux': 15463.7577886581,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0683832169
          },
          {
            'recordTime': 1535994900000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.7539567947,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0722150803
          },
          {
            'recordTime': 1535995200000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.7539567947,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0722150803
          },
          {
            'recordTime': 1535995500000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15463.7513370514,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0748348236
          },
          {
            'recordTime': 1535995800000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.7513370514,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0748348236
          },
          {
            'recordTime': 1535996100000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.7513370514,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0748348236
          },
          {
            'recordTime': 1535996400000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5422,
            'totalFlux': 15463.7513370514,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0748348236
          },
          {
            'recordTime': 1535996700000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.7467041016,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0794677734
          },
          {
            'recordTime': 1535997000000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15463.7449064255,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0812654495
          },
          {
            'recordTime': 1535997300000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.7449064255,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0812654495
          },
          {
            'recordTime': 1535997600000,
            'plusTotalFlux': 15475.826171875,
            'flux': 0.0,
            'pressure': 0.5434,
            'totalFlux': 15463.7449064255,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0812654495
          },
          {
            'recordTime': 1535997900000,
            'plusTotalFlux': 15475.8291015625,
            'flux': 0.2883301973,
            'pressure': 0.5444,
            'totalFlux': 15463.747836113,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0812654495
          },
          {
            'recordTime': 1535998200000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.7507658005,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0812654495
          },
          {
            'recordTime': 1535998500000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15463.7507658005,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0812654495
          },
          {
            'recordTime': 1535998800000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5428,
            'totalFlux': 15463.7507658005,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0812654495
          },
          {
            'recordTime': 1535999100000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5425,
            'totalFlux': 15463.7507658005,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0812654495
          },
          {
            'recordTime': 1535999400000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.7507658005,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0812654495
          },
          {
            'recordTime': 1535999700000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.7507658005,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0812654495
          },
          {
            'recordTime': 1536000000000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.7470436096,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0849876404
          },
          {
            'recordTime': 1536000300000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5428,
            'totalFlux': 15463.7470436096,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0849876404
          },
          {
            'recordTime': 1536000600000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.7444400787,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0875911713
          },
          {
            'recordTime': 1536000900000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15463.7444400787,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0875911713
          },
          {
            'recordTime': 1536001200000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.7444400787,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0875911713
          },
          {
            'recordTime': 1536001500000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5441,
            'totalFlux': 15463.7444400787,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0875911713
          },
          {
            'recordTime': 1536001800000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.7427558899,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0892753601
          },
          {
            'recordTime': 1536002100000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.7427558899,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0892753601
          },
          {
            'recordTime': 1536002400000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.7427558899,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0892753601
          },
          {
            'recordTime': 1536002700000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.7427558899,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0892753601
          },
          {
            'recordTime': 1536003000000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.7427558899,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0892753601
          },
          {
            'recordTime': 1536003300000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 15463.7427558899,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0892753601
          },
          {
            'recordTime': 1536003600000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.7376594543,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0943717957
          },
          {
            'recordTime': 1536003900000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15463.7356939316,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0963373184
          },
          {
            'recordTime': 1536004200000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 15463.7356939316,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0963373184
          },
          {
            'recordTime': 1536004500000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15463.7328977585,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0991334915
          },
          {
            'recordTime': 1536004800000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15463.7328977585,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 12.0991334915
          },
          {
            'recordTime': 1536005100000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.7328977585,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.0991334915
          },
          {
            'recordTime': 1536005400000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.7328977585,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.0991334915
          },
          {
            'recordTime': 1536005700000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15463.7328977585,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.0991334915
          },
          {
            'recordTime': 1536006000000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15463.7328977585,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.0991334915
          },
          {
            'recordTime': 1536006300000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.7328977585,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.0991334915
          },
          {
            'recordTime': 1536006600000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.7318181992,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1002130508
          },
          {
            'recordTime': 1536006900000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.7307987213,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1012325287
          },
          {
            'recordTime': 1536007200000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 15463.7307987213,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1012325287
          },
          {
            'recordTime': 1536007500000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.7307987213,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1012325287
          },
          {
            'recordTime': 1536007800000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15463.7307987213,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1012325287
          },
          {
            'recordTime': 1536008100000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15463.7307987213,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1012325287
          },
          {
            'recordTime': 1536008400000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5502,
            'totalFlux': 15463.7307987213,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1012325287
          },
          {
            'recordTime': 1536008700000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15463.7290620804,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536009000000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15463.7290620804,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536009300000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.7290620804,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536009600000,
            'plusTotalFlux': 15475.83203125,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.7290620804,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536009900000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5483,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536010200000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5438,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536010500000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536010800000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536011100000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536011400000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536011700000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5441,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536012000000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536012300000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536012600000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536012900000,
            'plusTotalFlux': 15475.8330078125,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15463.7300386429,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1029691696
          },
          {
            'recordTime': 1536013200000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15463.730140686,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.103843689
          },
          {
            'recordTime': 1536013500000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5459,
            'totalFlux': 15463.730140686,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.103843689
          },
          {
            'recordTime': 1536013800000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15463.730140686,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.103843689
          },
          {
            'recordTime': 1536014100000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15463.730140686,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.103843689
          },
          {
            'recordTime': 1536014400000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15463.7275495529,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1064348221
          },
          {
            'recordTime': 1536014700000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.7275495529,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1064348221
          },
          {
            'recordTime': 1536015000000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5471,
            'totalFlux': 15463.7275495529,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1064348221
          },
          {
            'recordTime': 1536015300000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15463.7275495529,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1064348221
          },
          {
            'recordTime': 1536015600000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15463.7275495529,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1064348221
          },
          {
            'recordTime': 1536015900000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15463.7275495529,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1064348221
          },
          {
            'recordTime': 1536016200000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15463.7275495529,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1064348221
          },
          {
            'recordTime': 1536016500000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15463.7275495529,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1064348221
          },
          {
            'recordTime': 1536016800000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15463.7275495529,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1064348221
          },
          {
            'recordTime': 1536017100000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15463.7275495529,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1064348221
          },
          {
            'recordTime': 1536017400000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15463.7258405685,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1081438065
          },
          {
            'recordTime': 1536017700000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.7258405685,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1081438065
          },
          {
            'recordTime': 1536018000000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 15463.7258405685,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1081438065
          },
          {
            'recordTime': 1536018300000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 15463.7258405685,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1081438065
          },
          {
            'recordTime': 1536018600000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15463.7258405685,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1081438065
          },
          {
            'recordTime': 1536018900000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15463.7258405685,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1081438065
          },
          {
            'recordTime': 1536019200000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15463.7258405685,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1081438065
          },
          {
            'recordTime': 1536019500000,
            'plusTotalFlux': 15475.833984375,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15463.7240114212,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1099729538
          },
          {
            'recordTime': 1536019800000,
            'plusTotalFlux': 15475.833984375,
            'flux': -0.2127552927,
            'pressure': 0.5337,
            'totalFlux': 15463.7222080231,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1117763519
          },
          {
            'recordTime': 1536020100000,
            'plusTotalFlux': 15475.8349609375,
            'flux': 0.0,
            'pressure': 0.5288,
            'totalFlux': 15463.7228899002,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1120710373
          },
          {
            'recordTime': 1536020400000,
            'plusTotalFlux': 15475.8349609375,
            'flux': 0.0,
            'pressure': 0.5306,
            'totalFlux': 15463.722026825,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1129341125
          },
          {
            'recordTime': 1536020700000,
            'plusTotalFlux': 15475.8369140625,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15463.7214107513,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1155033112
          },
          {
            'recordTime': 1536021000000,
            'plusTotalFlux': 15475.8369140625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.7214107513,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1155033112
          },
          {
            'recordTime': 1536021300000,
            'plusTotalFlux': 15475.8369140625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.7214107513,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1155033112
          },
          {
            'recordTime': 1536021600000,
            'plusTotalFlux': 15475.8369140625,
            'flux': -0.2523400486,
            'pressure': 0.5303,
            'totalFlux': 15463.7186994553,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1182146072
          },
          {
            'recordTime': 1536021900000,
            'plusTotalFlux': 15475.8369140625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.7152271271,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536022200000,
            'plusTotalFlux': 15475.8369140625,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15463.7152271271,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536022500000,
            'plusTotalFlux': 15475.8369140625,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15463.7152271271,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536022800000,
            'plusTotalFlux': 15475.8369140625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15463.7152271271,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536023100000,
            'plusTotalFlux': 15475.8369140625,
            'flux': 0.0,
            'pressure': 0.5267,
            'totalFlux': 15463.7152271271,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536023400000,
            'plusTotalFlux': 15475.8369140625,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 15463.7152271271,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536023700000,
            'plusTotalFlux': 15475.8369140625,
            'flux': 0.0,
            'pressure': 0.5267,
            'totalFlux': 15463.7152271271,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536024000000,
            'plusTotalFlux': 15479.3076171875,
            'flux': 81.9116744995,
            'pressure': 0.5004,
            'totalFlux': 15467.1859302521,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536024300000,
            'plusTotalFlux': 15486.4111328125,
            'flux': 90.4151611328,
            'pressure': 0.4821,
            'totalFlux': 15474.2894458771,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536024600000,
            'plusTotalFlux': 15494.5244140625,
            'flux': 106.2780609131,
            'pressure': 0.4711,
            'totalFlux': 15482.4027271271,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536024900000,
            'plusTotalFlux': 15503.9208984375,
            'flux': 118.0163879395,
            'pressure': 0.4699,
            'totalFlux': 15491.7992115021,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536025200000,
            'plusTotalFlux': 15514.1875,
            'flux': 127.8951950073,
            'pressure': 0.4638,
            'totalFlux': 15502.0658130646,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536025500000,
            'plusTotalFlux': 15525.4296875,
            'flux': 139.713684082,
            'pressure': 0.4626,
            'totalFlux': 15513.3080005646,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536025800000,
            'plusTotalFlux': 15537.25390625,
            'flux': 140.9823150635,
            'pressure': 0.4507,
            'totalFlux': 15525.1322193146,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536026100000,
            'plusTotalFlux': 15549.2177734375,
            'flux': 143.8778076172,
            'pressure': 0.4565,
            'totalFlux': 15537.0960865021,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536026400000,
            'plusTotalFlux': 15563.7333984375,
            'flux': 140.7110137939,
            'pressure': 0.454,
            'totalFlux': 15551.6117115021,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536026700000,
            'plusTotalFlux': 15573.283203125,
            'flux': 143.0426635742,
            'pressure': 0.4421,
            'totalFlux': 15561.1615161896,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536027000000,
            'plusTotalFlux': 15585.24609375,
            'flux': 142.2393798828,
            'pressure': 0.4391,
            'totalFlux': 15573.1244068146,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536027300000,
            'plusTotalFlux': 15597.2109375,
            'flux': 144.1239929199,
            'pressure': 0.4394,
            'totalFlux': 15585.0892505646,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536027600000,
            'plusTotalFlux': 15609.2763671875,
            'flux': 145.3926696777,
            'pressure': 0.4433,
            'totalFlux': 15597.1546802521,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536027900000,
            'plusTotalFlux': 15621.2587890625,
            'flux': 144.6897735596,
            'pressure': 0.447,
            'totalFlux': 15609.1371021271,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536028200000,
            'plusTotalFlux': 15633.05078125,
            'flux': 142.3359375,
            'pressure': 0.4381,
            'totalFlux': 15620.9290943146,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536028500000,
            'plusTotalFlux': 15644.779296875,
            'flux': 136.7341918945,
            'pressure': 0.4442,
            'totalFlux': 15632.6576099396,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536028800000,
            'plusTotalFlux': 15656.1884765625,
            'flux': 138.6265411377,
            'pressure': 0.4479,
            'totalFlux': 15644.0667896271,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536029100000,
            'plusTotalFlux': 15667.435546875,
            'flux': 135.1015472412,
            'pressure': 0.4522,
            'totalFlux': 15655.3138599396,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536029400000,
            'plusTotalFlux': 15678.5791015625,
            'flux': 128.0969848633,
            'pressure': 0.4558,
            'totalFlux': 15666.4574146271,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536029700000,
            'plusTotalFlux': 15689.3994140625,
            'flux': 130.1216278076,
            'pressure': 0.4619,
            'totalFlux': 15677.2777271271,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536030000000,
            'plusTotalFlux': 15702.2568359375,
            'flux': 127.9792251587,
            'pressure': 0.4702,
            'totalFlux': 15690.1351490021,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536030300000,
            'plusTotalFlux': 15710.576171875,
            'flux': 118.7887802124,
            'pressure': 0.4729,
            'totalFlux': 15698.4544849396,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536030600000,
            'plusTotalFlux': 15719.4052734375,
            'flux': 99.2879714966,
            'pressure': 0.472,
            'totalFlux': 15707.2835865021,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536030900000,
            'plusTotalFlux': 15727.4013671875,
            'flux': 93.9942016602,
            'pressure': 0.4681,
            'totalFlux': 15715.2796802521,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536031200000,
            'plusTotalFlux': 15735.1845703125,
            'flux': 91.4414749146,
            'pressure': 0.4735,
            'totalFlux': 15723.0628833771,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536031500000,
            'plusTotalFlux': 15742.9580078125,
            'flux': 93.003616333,
            'pressure': 0.4748,
            'totalFlux': 15730.8363208771,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536031800000,
            'plusTotalFlux': 15749.8583984375,
            'flux': 45.2933349609,
            'pressure': 0.4943,
            'totalFlux': 15737.7367115021,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536032100000,
            'plusTotalFlux': 15753.7373046875,
            'flux': 48.3095054626,
            'pressure': 0.4964,
            'totalFlux': 15741.6156177521,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536032400000,
            'plusTotalFlux': 15756.7158203125,
            'flux': 0.0,
            'pressure': 0.5047,
            'totalFlux': 15744.5941333771,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1216869354
          },
          {
            'recordTime': 1536032700000,
            'plusTotalFlux': 15756.7158203125,
            'flux': -0.2533055246,
            'pressure': 0.5126,
            'totalFlux': 15744.5926494598,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1231708527
          },
          {
            'recordTime': 1536033000000,
            'plusTotalFlux': 15756.7158203125,
            'flux': 0.0,
            'pressure': 0.5193,
            'totalFlux': 15744.5896606445,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.126159668
          },
          {
            'recordTime': 1536033300000,
            'plusTotalFlux': 15756.716796875,
            'flux': 0.0,
            'pressure': 0.5199,
            'totalFlux': 15744.5887432098,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1280536652
          },
          {
            'recordTime': 1536033600000,
            'plusTotalFlux': 15756.716796875,
            'flux': 0.0,
            'pressure': 0.5199,
            'totalFlux': 15744.5812644958,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 12.1355323792
          },
          {
            'recordTime': 1536033900000,
            'plusTotalFlux': 15756.7177734375,
            'flux': 0.0,
            'pressure': 0.5215,
            'totalFlux': 15744.5822410583,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1355323792
          },
          {
            'recordTime': 1536034200000,
            'plusTotalFlux': 15756.7177734375,
            'flux': 0.0,
            'pressure': 0.5245,
            'totalFlux': 15744.5770597458,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1407136917
          },
          {
            'recordTime': 1536034500000,
            'plusTotalFlux': 15756.72265625,
            'flux': 0.204333216,
            'pressure': 0.5285,
            'totalFlux': 15744.5819425583,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1407136917
          },
          {
            'recordTime': 1536034800000,
            'plusTotalFlux': 15756.72265625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15744.5819425583,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1407136917
          },
          {
            'recordTime': 1536035100000,
            'plusTotalFlux': 15756.724609375,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15744.5838956833,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1407136917
          },
          {
            'recordTime': 1536035400000,
            'plusTotalFlux': 15756.724609375,
            'flux': 0.0,
            'pressure': 0.5312,
            'totalFlux': 15744.5838956833,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1407136917
          },
          {
            'recordTime': 1536035700000,
            'plusTotalFlux': 15756.7275390625,
            'flux': 0.214953512,
            'pressure': 0.5334,
            'totalFlux': 15744.5868253708,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1407136917
          },
          {
            'recordTime': 1536036000000,
            'plusTotalFlux': 15756.7353515625,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15744.5898303986,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1455211639
          },
          {
            'recordTime': 1536036300000,
            'plusTotalFlux': 15756.7353515625,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15744.5871210098,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1482305527
          },
          {
            'recordTime': 1536036600000,
            'plusTotalFlux': 15756.7353515625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15744.5789785385,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.156373024
          },
          {
            'recordTime': 1536036900000,
            'plusTotalFlux': 15756.7353515625,
            'flux': -0.31702739,
            'pressure': 0.5297,
            'totalFlux': 15744.5772867203,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1580648422
          },
          {
            'recordTime': 1536037200000,
            'plusTotalFlux': 15756.736328125,
            'flux': 0.0,
            'pressure': 0.5303,
            'totalFlux': 15744.5722942352,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1640338898
          },
          {
            'recordTime': 1536037500000,
            'plusTotalFlux': 15756.736328125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15744.5722942352,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1640338898
          },
          {
            'recordTime': 1536037800000,
            'plusTotalFlux': 15756.736328125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15744.5690069199,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1673212051
          },
          {
            'recordTime': 1536038100000,
            'plusTotalFlux': 15756.736328125,
            'flux': 0.0,
            'pressure': 0.5331,
            'totalFlux': 15744.5690069199,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1673212051
          },
          {
            'recordTime': 1536038400000,
            'plusTotalFlux': 15756.73828125,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15744.5709600449,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1673212051
          },
          {
            'recordTime': 1536038700000,
            'plusTotalFlux': 15756.7431640625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15744.5704269409,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1727371216
          },
          {
            'recordTime': 1536039000000,
            'plusTotalFlux': 15756.744140625,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15744.5697145462,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1744260788
          },
          {
            'recordTime': 1536039300000,
            'plusTotalFlux': 15756.7451171875,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 15744.5653648376,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1797523499
          },
          {
            'recordTime': 1536039600000,
            'plusTotalFlux': 15756.7451171875,
            'flux': 0.0,
            'pressure': 0.5364,
            'totalFlux': 15744.5645141602,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1806030273
          },
          {
            'recordTime': 1536039900000,
            'plusTotalFlux': 15756.7451171875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15744.5584888458,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1866283417
          },
          {
            'recordTime': 1536040200000,
            'plusTotalFlux': 15756.748046875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15744.5597419739,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1883049011
          },
          {
            'recordTime': 1536040500000,
            'plusTotalFlux': 15756.748046875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15744.5537109375,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.1943359375
          },
          {
            'recordTime': 1536040800000,
            'plusTotalFlux': 15756.748046875,
            'flux': -0.2359268367,
            'pressure': 0.5361,
            'totalFlux': 15744.5470914841,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2009553909
          },
          {
            'recordTime': 1536041100000,
            'plusTotalFlux': 15756.748046875,
            'flux': -0.2504090965,
            'pressure': 0.5361,
            'totalFlux': 15744.5388936996,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2091531754
          },
          {
            'recordTime': 1536041400000,
            'plusTotalFlux': 15756.748046875,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15744.5384759903,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2095708847
          },
          {
            'recordTime': 1536041700000,
            'plusTotalFlux': 15756.75390625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15744.5443353653,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2095708847
          },
          {
            'recordTime': 1536042000000,
            'plusTotalFlux': 15756.75390625,
            'flux': 0.0,
            'pressure': 0.5349,
            'totalFlux': 15744.5341539383,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2197523117
          },
          {
            'recordTime': 1536042300000,
            'plusTotalFlux': 15756.75390625,
            'flux': 0.0,
            'pressure': 0.5349,
            'totalFlux': 15744.5323209763,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2215852737
          },
          {
            'recordTime': 1536042600000,
            'plusTotalFlux': 15756.75390625,
            'flux': 0.0,
            'pressure': 0.5349,
            'totalFlux': 15744.5323209763,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2215852737
          },
          {
            'recordTime': 1536042900000,
            'plusTotalFlux': 15756.7548828125,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15744.5332975388,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2215852737
          },
          {
            'recordTime': 1536043200000,
            'plusTotalFlux': 15756.7548828125,
            'flux': -0.3257167339,
            'pressure': 0.537,
            'totalFlux': 15744.5264282227,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2284545898
          },
          {
            'recordTime': 1536043500000,
            'plusTotalFlux': 15756.7578125,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15744.5261487961,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2316637039
          },
          {
            'recordTime': 1536043800000,
            'plusTotalFlux': 15756.7578125,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15744.5239534378,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2338590622
          },
          {
            'recordTime': 1536044100000,
            'plusTotalFlux': 15756.759765625,
            'flux': -0.2793735564,
            'pressure': 0.5431,
            'totalFlux': 15744.5252075195,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2345581055
          },
          {
            'recordTime': 1536044400000,
            'plusTotalFlux': 15756.759765625,
            'flux': 0.0,
            'pressure': 0.5392,
            'totalFlux': 15744.5213222504,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2384433746
          },
          {
            'recordTime': 1536044700000,
            'plusTotalFlux': 15756.759765625,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15744.5213222504,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2384433746
          },
          {
            'recordTime': 1536045000000,
            'plusTotalFlux': 15756.76171875,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15744.5232753754,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2384433746
          },
          {
            'recordTime': 1536045300000,
            'plusTotalFlux': 15756.76171875,
            'flux': 0.0,
            'pressure': 0.5401,
            'totalFlux': 15744.516459465,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.245259285
          },
          {
            'recordTime': 1536045600000,
            'plusTotalFlux': 15756.76171875,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15744.5153560638,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2463626862
          },
          {
            'recordTime': 1536045900000,
            'plusTotalFlux': 15756.76171875,
            'flux': 0.0,
            'pressure': 0.5318,
            'totalFlux': 15744.5153560638,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2463626862
          },
          {
            'recordTime': 1536046200000,
            'plusTotalFlux': 15756.7626953125,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15744.5127267838,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2499685287
          },
          {
            'recordTime': 1536046500000,
            'plusTotalFlux': 15756.7626953125,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15744.5127267838,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2499685287
          },
          {
            'recordTime': 1536046800000,
            'plusTotalFlux': 15756.7626953125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15744.5127267838,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2499685287
          },
          {
            'recordTime': 1536047100000,
            'plusTotalFlux': 15756.7626953125,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 15744.5127267838,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2499685287
          },
          {
            'recordTime': 1536047400000,
            'plusTotalFlux': 15756.763671875,
            'flux': 0.0,
            'pressure': 0.5352,
            'totalFlux': 15744.5137033463,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2499685287
          },
          {
            'recordTime': 1536047700000,
            'plusTotalFlux': 15756.763671875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15744.5083961487,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2552757263
          },
          {
            'recordTime': 1536048000000,
            'plusTotalFlux': 15756.763671875,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15744.5083961487,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 12.2552757263
          },
          {
            'recordTime': 1536048300000,
            'plusTotalFlux': 15756.763671875,
            'flux': 0.0,
            'pressure': 0.5288,
            'totalFlux': 15744.4999809265,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2636909485
          },
          {
            'recordTime': 1536048600000,
            'plusTotalFlux': 15756.763671875,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15744.4999809265,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2636909485
          },
          {
            'recordTime': 1536048900000,
            'plusTotalFlux': 15756.763671875,
            'flux': 0.0,
            'pressure': 0.5267,
            'totalFlux': 15744.4999809265,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2636909485
          },
          {
            'recordTime': 1536049200000,
            'plusTotalFlux': 15756.763671875,
            'flux': 0.0,
            'pressure': 0.5248,
            'totalFlux': 15744.4999809265,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2636909485
          },
          {
            'recordTime': 1536049500000,
            'plusTotalFlux': 15756.7646484375,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 15744.500957489,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2636909485
          },
          {
            'recordTime': 1536049800000,
            'plusTotalFlux': 15756.7685546875,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15744.504863739,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2636909485
          },
          {
            'recordTime': 1536050100000,
            'plusTotalFlux': 15756.7685546875,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15744.5035791397,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2649755478
          },
          {
            'recordTime': 1536050400000,
            'plusTotalFlux': 15756.7685546875,
            'flux': 0.0,
            'pressure': 0.5166,
            'totalFlux': 15744.5018062592,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2667484283
          },
          {
            'recordTime': 1536050700000,
            'plusTotalFlux': 15756.7705078125,
            'flux': 0.0,
            'pressure': 0.5199,
            'totalFlux': 15744.5037593842,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2667484283
          },
          {
            'recordTime': 1536051000000,
            'plusTotalFlux': 15756.7705078125,
            'flux': 0.0,
            'pressure': 0.5187,
            'totalFlux': 15744.5037593842,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2667484283
          },
          {
            'recordTime': 1536051300000,
            'plusTotalFlux': 15756.7705078125,
            'flux': 0.0,
            'pressure': 0.5187,
            'totalFlux': 15744.5037593842,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2667484283
          },
          {
            'recordTime': 1536051600000,
            'plusTotalFlux': 15756.7705078125,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15744.5037593842,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2667484283
          },
          {
            'recordTime': 1536051900000,
            'plusTotalFlux': 15756.7705078125,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15744.5037593842,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2667484283
          },
          {
            'recordTime': 1536052200000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 15744.5047359467,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2667484283
          },
          {
            'recordTime': 1536052500000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5187,
            'totalFlux': 15744.5047359467,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2667484283
          },
          {
            'recordTime': 1536052800000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.523,
            'totalFlux': 15744.5047359467,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2667484283
          },
          {
            'recordTime': 1536053100000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5187,
            'totalFlux': 15744.498963356,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.272521019
          },
          {
            'recordTime': 1536053400000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 15744.498963356,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.272521019
          },
          {
            'recordTime': 1536053700000,
            'plusTotalFlux': 15756.771484375,
            'flux': -0.3344061077,
            'pressure': 0.5251,
            'totalFlux': 15744.496222496,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.275261879
          },
          {
            'recordTime': 1536054000000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 15744.49355793,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.277926445
          },
          {
            'recordTime': 1536054300000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15744.4916276932,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2798566818
          },
          {
            'recordTime': 1536054600000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 15744.4863138199,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2851705551
          },
          {
            'recordTime': 1536054900000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15744.4831962585,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2882881165
          },
          {
            'recordTime': 1536055200000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5239,
            'totalFlux': 15744.4831962585,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 12.2882881165
          },
          {
            'recordTime': 1536055500000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 15744.4831962585,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2882881165
          },
          {
            'recordTime': 1536055800000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15744.4831962585,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2882881165
          },
          {
            'recordTime': 1536056100000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 15744.4831962585,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2882881165
          },
          {
            'recordTime': 1536056400000,
            'plusTotalFlux': 15756.771484375,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 15744.4831962585,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2882881165
          },
          {
            'recordTime': 1536056700000,
            'plusTotalFlux': 15757.0009765625,
            'flux': 0.0,
            'pressure': 0.5276,
            'totalFlux': 15744.712688446,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2882881165
          },
          {
            'recordTime': 1536057000000,
            'plusTotalFlux': 15757.001953125,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15744.7109622955,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2909908295
          },
          {
            'recordTime': 1536057300000,
            'plusTotalFlux': 15757.009765625,
            'flux': 0.0,
            'pressure': 0.5251,
            'totalFlux': 15744.7187747955,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2909908295
          },
          {
            'recordTime': 1536057600000,
            'plusTotalFlux': 15757.009765625,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 15744.7187747955,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2909908295
          },
          {
            'recordTime': 1536057900000,
            'plusTotalFlux': 15757.009765625,
            'flux': 0.0,
            'pressure': 0.5276,
            'totalFlux': 15744.7165317535,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2932338715
          },
          {
            'recordTime': 1536058200000,
            'plusTotalFlux': 15757.01171875,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15744.7184848785,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2932338715
          },
          {
            'recordTime': 1536058500000,
            'plusTotalFlux': 15757.01171875,
            'flux': 0.0,
            'pressure': 0.5264,
            'totalFlux': 15744.7184848785,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2932338715
          },
          {
            'recordTime': 1536058800000,
            'plusTotalFlux': 15757.01171875,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15744.7175617218,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2941570282
          },
          {
            'recordTime': 1536059100000,
            'plusTotalFlux': 15757.01171875,
            'flux': 0.0,
            'pressure': 0.5291,
            'totalFlux': 15744.7175617218,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.2941570282
          },
          {
            'recordTime': 1536059400000,
            'plusTotalFlux': 15757.01171875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15744.711848259,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.299870491
          },
          {
            'recordTime': 1536059700000,
            'plusTotalFlux': 15757.01171875,
            'flux': 0.0,
            'pressure': 0.5318,
            'totalFlux': 15744.7084646225,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3032541275
          },
          {
            'recordTime': 1536060000000,
            'plusTotalFlux': 15757.0126953125,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 15744.7072820663,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3054132462
          },
          {
            'recordTime': 1536060300000,
            'plusTotalFlux': 15757.0126953125,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15744.7072820663,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3054132462
          },
          {
            'recordTime': 1536060600000,
            'plusTotalFlux': 15757.0126953125,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15744.705411911,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3072834015
          },
          {
            'recordTime': 1536060900000,
            'plusTotalFlux': 15757.017578125,
            'flux': 0.0,
            'pressure': 0.5352,
            'totalFlux': 15744.7102947235,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3072834015
          },
          {
            'recordTime': 1536061200000,
            'plusTotalFlux': 15757.021484375,
            'flux': 0.0,
            'pressure': 0.5309,
            'totalFlux': 15744.7142009735,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3072834015
          },
          {
            'recordTime': 1536061500000,
            'plusTotalFlux': 15757.025390625,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15744.7181072235,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3072834015
          },
          {
            'recordTime': 1536061800000,
            'plusTotalFlux': 15757.025390625,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15744.7181072235,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3072834015
          },
          {
            'recordTime': 1536062100000,
            'plusTotalFlux': 15757.025390625,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15744.7150373459,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3103532791
          },
          {
            'recordTime': 1536062400000,
            'plusTotalFlux': 15757.025390625,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 15744.7150373459,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3103532791
          },
          {
            'recordTime': 1536062700000,
            'plusTotalFlux': 15757.0283203125,
            'flux': 0.2072296441,
            'pressure': 0.541,
            'totalFlux': 15744.7179670334,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3103532791
          },
          {
            'recordTime': 1536063000000,
            'plusTotalFlux': 15757.029296875,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15744.7167844772,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3125123978
          },
          {
            'recordTime': 1536063300000,
            'plusTotalFlux': 15757.029296875,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15744.7146863937,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3146104813
          },
          {
            'recordTime': 1536063600000,
            'plusTotalFlux': 15757.0322265625,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15744.7176160812,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3146104813
          },
          {
            'recordTime': 1536063900000,
            'plusTotalFlux': 15757.0322265625,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 15744.7152147293,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3170118332
          },
          {
            'recordTime': 1536064200000,
            'plusTotalFlux': 15757.03515625,
            'flux': 0.0,
            'pressure': 0.5352,
            'totalFlux': 15744.7181444168,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3170118332
          },
          {
            'recordTime': 1536064500000,
            'plusTotalFlux': 15757.0400390625,
            'flux': 0.2199740112,
            'pressure': 0.5334,
            'totalFlux': 15744.7221775055,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.317861557
          },
          {
            'recordTime': 1536064800000,
            'plusTotalFlux': 15757.04296875,
            'flux': 0.0,
            'pressure': 0.5376,
            'totalFlux': 15744.7232131958,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3197555542
          },
          {
            'recordTime': 1536065100000,
            'plusTotalFlux': 15757.04296875,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15744.7232131958,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3197555542
          },
          {
            'recordTime': 1536065400000,
            'plusTotalFlux': 15757.0458984375,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15744.7261428833,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3197555542
          },
          {
            'recordTime': 1536065700000,
            'plusTotalFlux': 15757.0458984375,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15744.7193393707,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3265590668
          },
          {
            'recordTime': 1536066000000,
            'plusTotalFlux': 15757.0498046875,
            'flux': 0.2275047898,
            'pressure': 0.5318,
            'totalFlux': 15744.7232456207,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3265590668
          },
          {
            'recordTime': 1536066300000,
            'plusTotalFlux': 15757.05078125,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15744.7220029831,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3287782669
          },
          {
            'recordTime': 1536066600000,
            'plusTotalFlux': 15757.05078125,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15744.7220029831,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3287782669
          },
          {
            'recordTime': 1536066900000,
            'plusTotalFlux': 15757.05078125,
            'flux': 0.0,
            'pressure': 0.541,
            'totalFlux': 15744.7199888229,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3307924271
          },
          {
            'recordTime': 1536067200000,
            'plusTotalFlux': 15757.9248046875,
            'flux': 27.9146499634,
            'pressure': 0.5337,
            'totalFlux': 15745.5940122604,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3307924271
          },
          {
            'recordTime': 1536067500000,
            'plusTotalFlux': 15759.5517578125,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15747.2209653854,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3307924271
          },
          {
            'recordTime': 1536067800000,
            'plusTotalFlux': 15759.5517578125,
            'flux': 0.0,
            'pressure': 0.5404,
            'totalFlux': 15747.2209653854,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3307924271
          },
          {
            'recordTime': 1536068100000,
            'plusTotalFlux': 15759.5537109375,
            'flux': 0.0,
            'pressure': 0.5331,
            'totalFlux': 15747.2229185104,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3307924271
          },
          {
            'recordTime': 1536068400000,
            'plusTotalFlux': 15759.5537109375,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15747.2209644318,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3327465057
          },
          {
            'recordTime': 1536068700000,
            'plusTotalFlux': 15759.5556640625,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15747.2229175568,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3327465057
          },
          {
            'recordTime': 1536069000000,
            'plusTotalFlux': 15759.5556640625,
            'flux': 0.0,
            'pressure': 0.5376,
            'totalFlux': 15747.2229175568,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3327465057
          },
          {
            'recordTime': 1536069300000,
            'plusTotalFlux': 15759.5556640625,
            'flux': -0.2475126088,
            'pressure': 0.5285,
            'totalFlux': 15747.2197208405,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.335943222
          },
          {
            'recordTime': 1536069600000,
            'plusTotalFlux': 15759.556640625,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15747.2153244019,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 12.3413162231
          },
          {
            'recordTime': 1536069900000,
            'plusTotalFlux': 15759.556640625,
            'flux': 0.0,
            'pressure': 0.5349,
            'totalFlux': 15747.2106800079,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3459606171
          },
          {
            'recordTime': 1536070200000,
            'plusTotalFlux': 15759.556640625,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15747.2106800079,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3459606171
          },
          {
            'recordTime': 1536070500000,
            'plusTotalFlux': 15759.556640625,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15747.2058906555,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3507499695
          },
          {
            'recordTime': 1536070800000,
            'plusTotalFlux': 15759.55859375,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 15747.2078437805,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3507499695
          },
          {
            'recordTime': 1536071100000,
            'plusTotalFlux': 15759.55859375,
            'flux': 0.0,
            'pressure': 0.5337,
            'totalFlux': 15747.2078437805,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3507499695
          },
          {
            'recordTime': 1536071400000,
            'plusTotalFlux': 15759.55859375,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 15747.2069940567,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3515996933
          },
          {
            'recordTime': 1536071700000,
            'plusTotalFlux': 15759.560546875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15747.2089471817,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3515996933
          },
          {
            'recordTime': 1536072000000,
            'plusTotalFlux': 15759.560546875,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15747.2069444656,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3536024094
          },
          {
            'recordTime': 1536072300000,
            'plusTotalFlux': 15759.560546875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 15747.2069444656,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3536024094
          },
          {
            'recordTime': 1536072600000,
            'plusTotalFlux': 15759.560546875,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 15747.2069444656,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3536024094
          },
          {
            'recordTime': 1536072900000,
            'plusTotalFlux': 15759.5615234375,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15747.2055444717,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3559789658
          },
          {
            'recordTime': 1536073200000,
            'plusTotalFlux': 15759.5615234375,
            'flux': 0.0,
            'pressure': 0.5352,
            'totalFlux': 15747.2035903931,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3579330444
          },
          {
            'recordTime': 1536073500000,
            'plusTotalFlux': 15759.5615234375,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15747.2035903931,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3579330444
          },
          {
            'recordTime': 1536073800000,
            'plusTotalFlux': 15759.5615234375,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15747.2035903931,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3579330444
          },
          {
            'recordTime': 1536074100000,
            'plusTotalFlux': 15759.5615234375,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 15747.2035903931,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3579330444
          },
          {
            'recordTime': 1536074400000,
            'plusTotalFlux': 15759.5615234375,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15747.1982555389,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3632678986
          },
          {
            'recordTime': 1536074700000,
            'plusTotalFlux': 15759.5615234375,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 15747.1982555389,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3632678986
          },
          {
            'recordTime': 1536075000000,
            'plusTotalFlux': 15759.5625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15747.1983699799,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3641300201
          },
          {
            'recordTime': 1536075300000,
            'plusTotalFlux': 15759.5625,
            'flux': 0.0,
            'pressure': 0.5355,
            'totalFlux': 15747.1966085434,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3658914566
          },
          {
            'recordTime': 1536075600000,
            'plusTotalFlux': 15759.5625,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 15747.192399025,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.370100975
          },
          {
            'recordTime': 1536075900000,
            'plusTotalFlux': 15759.5634765625,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15747.1875391006,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3759374619
          },
          {
            'recordTime': 1536076200000,
            'plusTotalFlux': 15759.5634765625,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15747.1875391006,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3759374619
          },
          {
            'recordTime': 1536076500000,
            'plusTotalFlux': 15759.5654296875,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15747.1894922256,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3759374619
          },
          {
            'recordTime': 1536076800000,
            'plusTotalFlux': 15759.5673828125,
            'flux': -0.2330303788,
            'pressure': 0.537,
            'totalFlux': 15747.1900815964,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 12.3773012161
          },
          {
            'recordTime': 1536077100000,
            'plusTotalFlux': 15759.568359375,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15747.1859388351,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.3824205399
          },
          {
            'recordTime': 1536077400000,
            'plusTotalFlux': 15759.568359375,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15747.1859388351,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.3824205399
          },
          {
            'recordTime': 1536077700000,
            'plusTotalFlux': 15759.568359375,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15747.1859388351,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.3824205399
          },
          {
            'recordTime': 1536078000000,
            'plusTotalFlux': 15759.568359375,
            'flux': 0.0,
            'pressure': 0.541,
            'totalFlux': 15747.1859388351,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.3824205399
          },
          {
            'recordTime': 1536078300000,
            'plusTotalFlux': 15759.5693359375,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 15747.1869153976,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.3824205399
          },
          {
            'recordTime': 1536078600000,
            'plusTotalFlux': 15759.5693359375,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15747.1869153976,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.3824205399
          },
          {
            'recordTime': 1536078900000,
            'plusTotalFlux': 15759.5693359375,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 15747.1869153976,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.3824205399
          },
          {
            'recordTime': 1536079200000,
            'plusTotalFlux': 15759.5703125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15747.183429718,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.386882782
          },
          {
            'recordTime': 1536079500000,
            'plusTotalFlux': 15759.5703125,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15747.183429718,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.386882782
          },
          {
            'recordTime': 1536079800000,
            'plusTotalFlux': 15759.5703125,
            'flux': -0.2011694908,
            'pressure': 0.5428,
            'totalFlux': 15747.1774654388,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.3928470612
          },
          {
            'recordTime': 1536080100000,
            'plusTotalFlux': 15759.5703125,
            'flux': 0.0,
            'pressure': 0.5447,
            'totalFlux': 15747.1733217239,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.3969907761
          },
          {
            'recordTime': 1536080400000,
            'plusTotalFlux': 15759.5703125,
            'flux': -0.2040659189,
            'pressure': 0.5431,
            'totalFlux': 15747.1707811356,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.3995313644
          },
          {
            'recordTime': 1536080700000,
            'plusTotalFlux': 15759.5712890625,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15747.170498848,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4007902145
          },
          {
            'recordTime': 1536081000000,
            'plusTotalFlux': 15759.5732421875,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 15747.1707639694,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4024782181
          },
          {
            'recordTime': 1536081300000,
            'plusTotalFlux': 15759.5732421875,
            'flux': 0.0,
            'pressure': 0.5465,
            'totalFlux': 15747.1707639694,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4024782181
          },
          {
            'recordTime': 1536081600000,
            'plusTotalFlux': 15759.5732421875,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15747.1699180603,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4033241272
          },
          {
            'recordTime': 1536081900000,
            'plusTotalFlux': 15759.5732421875,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15747.1699180603,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4033241272
          },
          {
            'recordTime': 1536082200000,
            'plusTotalFlux': 15759.5732421875,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15747.1699180603,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4033241272
          },
          {
            'recordTime': 1536082500000,
            'plusTotalFlux': 15759.5732421875,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15747.1652441025,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.407998085
          },
          {
            'recordTime': 1536082800000,
            'plusTotalFlux': 15759.5751953125,
            'flux': -0.2156517506,
            'pressure': 0.5447,
            'totalFlux': 15747.1655912399,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4096040726
          },
          {
            'recordTime': 1536083100000,
            'plusTotalFlux': 15759.5771484375,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15747.1673049927,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4098434448
          },
          {
            'recordTime': 1536083400000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15747.1692581177,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4098434448
          },
          {
            'recordTime': 1536083700000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15747.1692581177,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4098434448
          },
          {
            'recordTime': 1536084000000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 15747.1654520035,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.413649559
          },
          {
            'recordTime': 1536084300000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5483,
            'totalFlux': 15747.1654520035,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.413649559
          },
          {
            'recordTime': 1536084600000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15747.1598005295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.419301033
          },
          {
            'recordTime': 1536084900000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15747.1598005295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.419301033
          },
          {
            'recordTime': 1536085200000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 15747.1588897705,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.420211792
          },
          {
            'recordTime': 1536085500000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5483,
            'totalFlux': 15747.1549816132,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536085800000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15747.1549816132,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536086100000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 15747.1549816132,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536086400000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15747.1549816132,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536086700000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15747.1549816132,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536087000000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5474,
            'totalFlux': 15747.1549816132,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536087300000,
            'plusTotalFlux': 15759.5791015625,
            'flux': 0.0,
            'pressure': 0.5489,
            'totalFlux': 15747.1549816132,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536087600000,
            'plusTotalFlux': 15759.580078125,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15747.1559581757,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536087900000,
            'plusTotalFlux': 15759.580078125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15747.1559581757,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536088200000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5499,
            'totalFlux': 15747.1579113007,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536088500000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15747.1579113007,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536088800000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15747.1579113007,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4241199493
          },
          {
            'recordTime': 1536089100000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15747.1550350189,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4269962311
          },
          {
            'recordTime': 1536089400000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15747.1550350189,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4269962311
          },
          {
            'recordTime': 1536089700000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15747.1550350189,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4269962311
          },
          {
            'recordTime': 1536090000000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15747.1550350189,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4269962311
          },
          {
            'recordTime': 1536090300000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15747.1540393829,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4279918671
          },
          {
            'recordTime': 1536090600000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5489,
            'totalFlux': 15747.1540393829,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4279918671
          },
          {
            'recordTime': 1536090900000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5535,
            'totalFlux': 15747.1540393829,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4279918671
          },
          {
            'recordTime': 1536091200000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15747.1540393829,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 12.4279918671
          },
          {
            'recordTime': 1536091500000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15747.1506319046,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4313993454
          },
          {
            'recordTime': 1536091800000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5496,
            'totalFlux': 15747.1506319046,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4313993454
          },
          {
            'recordTime': 1536092100000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15747.1462049484,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4358263016
          },
          {
            'recordTime': 1536092400000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15747.1429786682,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4390525818
          },
          {
            'recordTime': 1536092700000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15747.14179039,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.44024086
          },
          {
            'recordTime': 1536093000000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5526,
            'totalFlux': 15747.14179039,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.44024086
          },
          {
            'recordTime': 1536093300000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15747.139497757,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.442533493
          },
          {
            'recordTime': 1536093600000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15747.139497757,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.442533493
          },
          {
            'recordTime': 1536093900000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 15747.1339054108,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4481258392
          },
          {
            'recordTime': 1536094200000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5535,
            'totalFlux': 15747.133055687,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.448975563
          },
          {
            'recordTime': 1536094500000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15747.133055687,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.448975563
          },
          {
            'recordTime': 1536094800000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5496,
            'totalFlux': 15747.133055687,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.448975563
          },
          {
            'recordTime': 1536095100000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5514,
            'totalFlux': 15747.1322174072,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4498138428
          },
          {
            'recordTime': 1536095400000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15747.1322174072,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4498138428
          },
          {
            'recordTime': 1536095700000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15747.1322174072,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4498138428
          },
          {
            'recordTime': 1536096000000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15747.1322174072,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4498138428
          },
          {
            'recordTime': 1536096300000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 15747.1322174072,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4498138428
          },
          {
            'recordTime': 1536096600000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15747.1281042099,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4539270401
          },
          {
            'recordTime': 1536096900000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5496,
            'totalFlux': 15747.1281042099,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4539270401
          },
          {
            'recordTime': 1536097200000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15747.1281042099,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4539270401
          },
          {
            'recordTime': 1536097500000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15747.1281042099,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4539270401
          },
          {
            'recordTime': 1536097800000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15747.1281042099,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4539270401
          },
          {
            'recordTime': 1536098100000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5535,
            'totalFlux': 15747.1281042099,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4539270401
          },
          {
            'recordTime': 1536098400000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5526,
            'totalFlux': 15747.1262350082,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 12.4557962418
          },
          {
            'recordTime': 1536098700000,
            'plusTotalFlux': 15759.58203125,
            'flux': -0.2069623768,
            'pressure': 0.5505,
            'totalFlux': 15747.1239595413,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4580717087
          },
          {
            'recordTime': 1536099000000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 15747.1227855682,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4592456818
          },
          {
            'recordTime': 1536099300000,
            'plusTotalFlux': 15759.58203125,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15747.1227855682,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4592456818
          },
          {
            'recordTime': 1536099600000,
            'plusTotalFlux': 15759.583984375,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15747.1247386932,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4592456818
          },
          {
            'recordTime': 1536099900000,
            'plusTotalFlux': 15759.583984375,
            'flux': 0.0,
            'pressure': 0.5544,
            'totalFlux': 15747.1238155365,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4601688385
          },
          {
            'recordTime': 1536100200000,
            'plusTotalFlux': 15759.583984375,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 15747.1238155365,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4601688385
          },
          {
            'recordTime': 1536100500000,
            'plusTotalFlux': 15759.583984375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15747.1211080551,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4628763199
          },
          {
            'recordTime': 1536100800000,
            'plusTotalFlux': 15759.5849609375,
            'flux': 0.0,
            'pressure': 0.5535,
            'totalFlux': 15747.1220846176,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4628763199
          },
          {
            'recordTime': 1536101100000,
            'plusTotalFlux': 15759.5849609375,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 15747.1211938858,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4637670517
          },
          {
            'recordTime': 1536101400000,
            'plusTotalFlux': 15759.5849609375,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15747.1211938858,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4637670517
          },
          {
            'recordTime': 1536101700000,
            'plusTotalFlux': 15759.5849609375,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 15747.1211938858,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4637670517
          },
          {
            'recordTime': 1536102000000,
            'plusTotalFlux': 15759.587890625,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 15747.1212472916,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4666433334
          },
          {
            'recordTime': 1536102300000,
            'plusTotalFlux': 15759.587890625,
            'flux': 0.0,
            'pressure': 0.5547,
            'totalFlux': 15747.1194381714,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4684524536
          },
          {
            'recordTime': 1536102600000,
            'plusTotalFlux': 15759.587890625,
            'flux': 0.0,
            'pressure': 0.5554,
            'totalFlux': 15747.1194381714,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4684524536
          },
          {
            'recordTime': 1536102900000,
            'plusTotalFlux': 15759.587890625,
            'flux': -0.2504090965,
            'pressure': 0.5468,
            'totalFlux': 15747.1147375107,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4731531143
          },
          {
            'recordTime': 1536103200000,
            'plusTotalFlux': 15759.5888671875,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15747.1132555008,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4756116867
          },
          {
            'recordTime': 1536103500000,
            'plusTotalFlux': 15759.58984375,
            'flux': -0.2098588347,
            'pressure': 0.5511,
            'totalFlux': 15747.1136493683,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4761943817
          },
          {
            'recordTime': 1536103800000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15747.1106071472,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4792366028
          },
          {
            'recordTime': 1536104100000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5489,
            'totalFlux': 15747.1106071472,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4792366028
          },
          {
            'recordTime': 1536104400000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 15747.1106071472,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4792366028
          },
          {
            'recordTime': 1536104700000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15747.1106071472,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4792366028
          },
          {
            'recordTime': 1536105000000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 15747.1106071472,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4792366028
          },
          {
            'recordTime': 1536105300000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5416,
            'totalFlux': 15747.1047010422,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4851427078
          },
          {
            'recordTime': 1536105600000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 15747.1047010422,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4851427078
          },
          {
            'recordTime': 1536105900000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5434,
            'totalFlux': 15747.1047010422,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4851427078
          },
          {
            'recordTime': 1536106200000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15747.1047010422,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4851427078
          },
          {
            'recordTime': 1536106500000,
            'plusTotalFlux': 15759.58984375,
            'flux': -0.2156517506,
            'pressure': 0.5392,
            'totalFlux': 15747.0908050537,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4990386963
          },
          {
            'recordTime': 1536106800000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5376,
            'totalFlux': 15747.0905046463,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4993391037
          },
          {
            'recordTime': 1536107100000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 15747.0905046463,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4993391037
          },
          {
            'recordTime': 1536107400000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5386,
            'totalFlux': 15747.0905046463,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.4993391037
          },
          {
            'recordTime': 1536107700000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5407,
            'totalFlux': 15747.0885028839,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.5013408661
          },
          {
            'recordTime': 1536108000000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 15747.0885028839,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.5013408661
          },
          {
            'recordTime': 1536108300000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5395,
            'totalFlux': 15747.0885028839,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.5013408661
          },
          {
            'recordTime': 1536108600000,
            'plusTotalFlux': 15759.58984375,
            'flux': -0.2803390324,
            'pressure': 0.5441,
            'totalFlux': 15747.0814352036,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.5084085464
          },
          {
            'recordTime': 1536108900000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 15747.0766105652,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.5132331848
          },
          {
            'recordTime': 1536109200000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 15747.0694332123,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.5204105377
          },
          {
            'recordTime': 1536109500000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 15747.0694332123,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.5204105377
          },
          {
            'recordTime': 1536109800000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 15747.0694332123,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.5204105377
          },
          {
            'recordTime': 1536110100000,
            'plusTotalFlux': 15759.58984375,
            'flux': 0.0,
            'pressure': 0.5398,
            'totalFlux': 15747.0685110092,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.5213327408
          },
          {
            'recordTime': 1536110400000,
            'plusTotalFlux': 15759.58984375,
            'flux': -0.3604741096,
            'pressure': 0.5297,
            'totalFlux': 15747.0513076782,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.5385360718
          },
          {
            'recordTime': 1536110700000,
            'plusTotalFlux': 15759.5927734375,
            'flux': 0.0,
            'pressure': 0.5331,
            'totalFlux': 15747.0526304245,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536111000000,
            'plusTotalFlux': 15759.595703125,
            'flux': 0.0,
            'pressure': 0.538,
            'totalFlux': 15747.055560112,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536111300000,
            'plusTotalFlux': 15763.0478515625,
            'flux': 83.259979248,
            'pressure': 0.5105,
            'totalFlux': 15750.5077085495,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536111600000,
            'plusTotalFlux': 15770.9375,
            'flux': 102.868927002,
            'pressure': 0.4858,
            'totalFlux': 15758.397356987,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536111900000,
            'plusTotalFlux': 15780.9208984375,
            'flux': 126.6468276978,
            'pressure': 0.483,
            'totalFlux': 15768.3807554245,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536112200000,
            'plusTotalFlux': 15791.94921875,
            'flux': 139.015625,
            'pressure': 0.4778,
            'totalFlux': 15779.409075737,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536112500000,
            'plusTotalFlux': 15803.595703125,
            'flux': 141.8319396973,
            'pressure': 0.4659,
            'totalFlux': 15791.055560112,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536112800000,
            'plusTotalFlux': 15818.2900390625,
            'flux': 148.3981933594,
            'pressure': 0.458,
            'totalFlux': 15805.7498960495,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536113100000,
            'plusTotalFlux': 15828.3603515625,
            'flux': 150.8283233643,
            'pressure': 0.458,
            'totalFlux': 15815.8202085495,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536113400000,
            'plusTotalFlux': 15841.0234375,
            'flux': 152.9610595703,
            'pressure': 0.4473,
            'totalFlux': 15828.483294487,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536113700000,
            'plusTotalFlux': 15854.0771484375,
            'flux': 159.7339172363,
            'pressure': 0.451,
            'totalFlux': 15841.5370054245,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536114000000,
            'plusTotalFlux': 15867.1142578125,
            'flux': 154.2972869873,
            'pressure': 0.4543,
            'totalFlux': 15854.5741147995,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536114300000,
            'plusTotalFlux': 15880.0625,
            'flux': 153.1464538574,
            'pressure': 0.4589,
            'totalFlux': 15867.522356987,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536114600000,
            'plusTotalFlux': 15892.818359375,
            'flux': 152.9890594482,
            'pressure': 0.4513,
            'totalFlux': 15880.278216362,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536114900000,
            'plusTotalFlux': 15905.490234375,
            'flux': 151.4240264893,
            'pressure': 0.4507,
            'totalFlux': 15892.950091362,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536115200000,
            'plusTotalFlux': 15918.3017578125,
            'flux': 153.6639099121,
            'pressure': 0.454,
            'totalFlux': 15905.7616147995,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536115500000,
            'plusTotalFlux': 15931.2802734375,
            'flux': 156.0660247803,
            'pressure': 0.4589,
            'totalFlux': 15918.7401304245,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536115800000,
            'plusTotalFlux': 15944.33984375,
            'flux': 159.3998718262,
            'pressure': 0.4552,
            'totalFlux': 15931.799700737,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536116100000,
            'plusTotalFlux': 15957.3603515625,
            'flux': 154.1563262939,
            'pressure': 0.4595,
            'totalFlux': 15944.8202085495,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536116400000,
            'plusTotalFlux': 15972.5322265625,
            'flux': 152.1491088867,
            'pressure': 0.4641,
            'totalFlux': 15959.9920835495,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536116700000,
            'plusTotalFlux': 15982.779296875,
            'flux': 154.9846954346,
            'pressure': 0.4586,
            'totalFlux': 15970.239153862,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536117000000,
            'plusTotalFlux': 15995.5966796875,
            'flux': 155.1353149414,
            'pressure': 0.4613,
            'totalFlux': 15983.0565366745,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536117300000,
            'plusTotalFlux': 16008.1533203125,
            'flux': 149.7855987549,
            'pressure': 0.454,
            'totalFlux': 15995.6131772995,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536117600000,
            'plusTotalFlux': 16020.7412109375,
            'flux': 149.3105621338,
            'pressure': 0.4616,
            'totalFlux': 16008.2010679245,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536117900000,
            'plusTotalFlux': 16033.09765625,
            'flux': 147.9357147217,
            'pressure': 0.4674,
            'totalFlux': 16020.557513237,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536118200000,
            'plusTotalFlux': 16045.3388671875,
            'flux': 146.2876434326,
            'pressure': 0.4619,
            'totalFlux': 16032.7987241745,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536118500000,
            'plusTotalFlux': 16057.5107421875,
            'flux': 145.0537414551,
            'pressure': 0.4632,
            'totalFlux': 16044.9705991745,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536118800000,
            'plusTotalFlux': 16069.5966796875,
            'flux': 145.6137390137,
            'pressure': 0.4503,
            'totalFlux': 16057.0565366745,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536119100000,
            'plusTotalFlux': 16081.7763671875,
            'flux': 146.1186676025,
            'pressure': 0.4705,
            'totalFlux': 16069.2362241745,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536119400000,
            'plusTotalFlux': 16093.533203125,
            'flux': 137.2922363281,
            'pressure': 0.4674,
            'totalFlux': 16080.993060112,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536119700000,
            'plusTotalFlux': 16104.6318359375,
            'flux': 132.651184082,
            'pressure': 0.4708,
            'totalFlux': 16092.0916929245,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536120000000,
            'plusTotalFlux': 16117.373046875,
            'flux': 124.0429382324,
            'pressure': 0.4748,
            'totalFlux': 16104.832903862,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536120300000,
            'plusTotalFlux': 16125.265625,
            'flux': 116.0371398926,
            'pressure': 0.4809,
            'totalFlux': 16112.725481987,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536120600000,
            'plusTotalFlux': 16135.16796875,
            'flux': 120.8703536987,
            'pressure': 0.4842,
            'totalFlux': 16122.627825737,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536120900000,
            'plusTotalFlux': 16145.189453125,
            'flux': 117.6070251465,
            'pressure': 0.4858,
            'totalFlux': 16132.649310112,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536121200000,
            'plusTotalFlux': 16154.966796875,
            'flux': 115.8440475464,
            'pressure': 0.4812,
            'totalFlux': 16142.426653862,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536121500000,
            'plusTotalFlux': 16164.7783203125,
            'flux': 117.5442733765,
            'pressure': 0.4858,
            'totalFlux': 16152.2381772995,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536121800000,
            'plusTotalFlux': 16174.6220703125,
            'flux': 117.7122573853,
            'pressure': 0.4833,
            'totalFlux': 16162.0819272995,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536122100000,
            'plusTotalFlux': 16184.3779296875,
            'flux': 116.3461074829,
            'pressure': 0.4809,
            'totalFlux': 16171.8377866745,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536122400000,
            'plusTotalFlux': 16193.9482421875,
            'flux': 119.1073913574,
            'pressure': 0.4833,
            'totalFlux': 16181.4080991745,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536122700000,
            'plusTotalFlux': 16204.0771484375,
            'flux': 126.8215713501,
            'pressure': 0.4797,
            'totalFlux': 16191.5370054245,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536123000000,
            'plusTotalFlux': 16214.7236328125,
            'flux': 126.8698577881,
            'pressure': 0.5315,
            'totalFlux': 16202.1834897995,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536123300000,
            'plusTotalFlux': 16215.8369140625,
            'flux': 0.6330074668,
            'pressure': 0.5224,
            'totalFlux': 16203.2967710495,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536123600000,
            'plusTotalFlux': 16215.8623046875,
            'flux': 0.2680550814,
            'pressure': 0.5248,
            'totalFlux': 16203.3221616745,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536123900000,
            'plusTotalFlux': 16215.869140625,
            'flux': 0.2332976758,
            'pressure': 0.5334,
            'totalFlux': 16203.328997612,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536124200000,
            'plusTotalFlux': 16215.8837890625,
            'flux': 0.2593657076,
            'pressure': 0.5322,
            'totalFlux': 16203.3436460495,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536124500000,
            'plusTotalFlux': 16215.8974609375,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 16203.3573179245,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536124800000,
            'plusTotalFlux': 16215.90234375,
            'flux': 0.0,
            'pressure': 0.527,
            'totalFlux': 16203.362200737,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536125100000,
            'plusTotalFlux': 16215.90625,
            'flux': 0.205298692,
            'pressure': 0.5315,
            'totalFlux': 16203.366106987,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536125400000,
            'plusTotalFlux': 16215.9169921875,
            'flux': 0.4254286587,
            'pressure': 0.5297,
            'totalFlux': 16203.3768491745,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536125700000,
            'plusTotalFlux': 16215.931640625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 16203.391497612,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536126000000,
            'plusTotalFlux': 16215.9404296875,
            'flux': 0.3607414067,
            'pressure': 0.5383,
            'totalFlux': 16203.4002866745,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536126300000,
            'plusTotalFlux': 16215.9453125,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 16203.405169487,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536126600000,
            'plusTotalFlux': 16215.9541015625,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 16203.4139585495,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536126900000,
            'plusTotalFlux': 16215.96484375,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 16203.424700737,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536127200000,
            'plusTotalFlux': 16215.9892578125,
            'flux': 0.2419870496,
            'pressure': 0.5303,
            'totalFlux': 16203.4491147995,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536127500000,
            'plusTotalFlux': 16215.9951171875,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 16203.4549741745,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536127800000,
            'plusTotalFlux': 16215.998046875,
            'flux': 0.0,
            'pressure': 0.5306,
            'totalFlux': 16203.457903862,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536128100000,
            'plusTotalFlux': 16216.0087890625,
            'flux': 0.3983952403,
            'pressure': 0.5343,
            'totalFlux': 16203.4686460495,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536128400000,
            'plusTotalFlux': 16216.01953125,
            'flux': 0.2728825212,
            'pressure': 0.5306,
            'totalFlux': 16203.479388237,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536128700000,
            'plusTotalFlux': 16216.03125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 16203.491106987,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536129000000,
            'plusTotalFlux': 16216.0517578125,
            'flux': 0.4128774703,
            'pressure': 0.5346,
            'totalFlux': 16203.5116147995,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536129300000,
            'plusTotalFlux': 16216.068359375,
            'flux': 0.0,
            'pressure': 0.5318,
            'totalFlux': 16203.528216362,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536129600000,
            'plusTotalFlux': 16216.0751953125,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 16203.5350522995,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536129900000,
            'plusTotalFlux': 16216.08203125,
            'flux': 0.0,
            'pressure': 0.5349,
            'totalFlux': 16203.541888237,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536130200000,
            'plusTotalFlux': 16216.0966796875,
            'flux': 0.2806063294,
            'pressure': 0.5297,
            'totalFlux': 16203.5565366745,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536130500000,
            'plusTotalFlux': 16216.111328125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 16203.571185112,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536130800000,
            'plusTotalFlux': 16216.125,
            'flux': 0.0,
            'pressure': 0.5337,
            'totalFlux': 16203.584856987,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536131100000,
            'plusTotalFlux': 16216.1259765625,
            'flux': 0.3115017712,
            'pressure': 0.5431,
            'totalFlux': 16203.5858335495,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536131400000,
            'plusTotalFlux': 16216.1337890625,
            'flux': 0.2564692795,
            'pressure': 0.5297,
            'totalFlux': 16203.5936460495,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536131700000,
            'plusTotalFlux': 16216.15234375,
            'flux': 0.0,
            'pressure': 0.5343,
            'totalFlux': 16203.612200737,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536132000000,
            'plusTotalFlux': 16216.1552734375,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 16203.6151304245,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536132300000,
            'plusTotalFlux': 16216.16015625,
            'flux': 0.0,
            'pressure': 0.5306,
            'totalFlux': 16203.620013237,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536132600000,
            'plusTotalFlux': 16216.16015625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 16203.620013237,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536132900000,
            'plusTotalFlux': 16216.169921875,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 16203.629778862,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536133200000,
            'plusTotalFlux': 16216.173828125,
            'flux': 0.210126102,
            'pressure': 0.5346,
            'totalFlux': 16203.633685112,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536133500000,
            'plusTotalFlux': 16216.1845703125,
            'flux': 0.0,
            'pressure': 0.5264,
            'totalFlux': 16203.6444272995,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536133800000,
            'plusTotalFlux': 16216.19140625,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 16203.651263237,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536134100000,
            'plusTotalFlux': 16216.203125,
            'flux': 0.2680550814,
            'pressure': 0.5254,
            'totalFlux': 16203.662981987,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536134400000,
            'plusTotalFlux': 16216.2197265625,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 16203.6795835495,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536134700000,
            'plusTotalFlux': 16216.2255859375,
            'flux': 0.0,
            'pressure': 0.5264,
            'totalFlux': 16203.6854429245,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536135000000,
            'plusTotalFlux': 16216.232421875,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 16203.692278862,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536135300000,
            'plusTotalFlux': 16216.2392578125,
            'flux': 0.4650135636,
            'pressure': 0.5285,
            'totalFlux': 16203.6991147995,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536135600000,
            'plusTotalFlux': 16216.2646484375,
            'flux': 0.3549485207,
            'pressure': 0.5199,
            'totalFlux': 16203.7245054245,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536135900000,
            'plusTotalFlux': 16216.2763671875,
            'flux': 0.212057054,
            'pressure': 0.526,
            'totalFlux': 16203.7362241745,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536136200000,
            'plusTotalFlux': 16216.2822265625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 16203.7420835495,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536136500000,
            'plusTotalFlux': 16216.2919921875,
            'flux': 0.2709515095,
            'pressure': 0.5221,
            'totalFlux': 16203.7518491745,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536136800000,
            'plusTotalFlux': 16216.296875,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 16203.756731987,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536137100000,
            'plusTotalFlux': 16216.2998046875,
            'flux': 0.2709515095,
            'pressure': 0.5242,
            'totalFlux': 16203.7596616745,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536137400000,
            'plusTotalFlux': 16216.32421875,
            'flux': 0.3983952701,
            'pressure': 0.5209,
            'totalFlux': 16203.784075737,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536137700000,
            'plusTotalFlux': 16216.33984375,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 16203.799700737,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536138000000,
            'plusTotalFlux': 16216.3525390625,
            'flux': 0.2738479674,
            'pressure': 0.5236,
            'totalFlux': 16203.8123960495,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536138300000,
            'plusTotalFlux': 16216.36328125,
            'flux': 0.0,
            'pressure': 0.5218,
            'totalFlux': 16203.823138237,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536138600000,
            'plusTotalFlux': 16216.380859375,
            'flux': 0.0,
            'pressure': 0.5233,
            'totalFlux': 16203.840716362,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536138900000,
            'plusTotalFlux': 16216.3837890625,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 16203.8436460495,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536139200000,
            'plusTotalFlux': 16216.3935546875,
            'flux': 0.0,
            'pressure': 0.5227,
            'totalFlux': 16203.8534116745,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536139500000,
            'plusTotalFlux': 16216.400390625,
            'flux': 0.0,
            'pressure': 0.5248,
            'totalFlux': 16203.860247612,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536139800000,
            'plusTotalFlux': 16216.416015625,
            'flux': 0.0,
            'pressure': 0.5254,
            'totalFlux': 16203.875872612,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536140100000,
            'plusTotalFlux': 16216.431640625,
            'flux': 0.2033677101,
            'pressure': 0.5297,
            'totalFlux': 16203.891497612,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536140400000,
            'plusTotalFlux': 16216.4423828125,
            'flux': 0.2680550516,
            'pressure': 0.5334,
            'totalFlux': 16203.9022397995,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536140700000,
            'plusTotalFlux': 16216.458984375,
            'flux': 0.205298692,
            'pressure': 0.5236,
            'totalFlux': 16203.918841362,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536141000000,
            'plusTotalFlux': 16216.470703125,
            'flux': 0.0,
            'pressure': 0.5328,
            'totalFlux': 16203.930560112,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536141300000,
            'plusTotalFlux': 16216.4912109375,
            'flux': 0.2246083617,
            'pressure': 0.5254,
            'totalFlux': 16203.9510679245,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536141600000,
            'plusTotalFlux': 16216.50390625,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 16203.963763237,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536141900000,
            'plusTotalFlux': 16216.5126953125,
            'flux': 0.4563241899,
            'pressure': 0.5239,
            'totalFlux': 16203.9725522995,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536142200000,
            'plusTotalFlux': 16216.51953125,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 16203.979388237,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536142500000,
            'plusTotalFlux': 16216.5322265625,
            'flux': 0.2632276416,
            'pressure': 0.5218,
            'totalFlux': 16203.9920835495,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536142800000,
            'plusTotalFlux': 16216.5361328125,
            'flux': 0.0,
            'pressure': 0.5212,
            'totalFlux': 16203.9959897995,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536143100000,
            'plusTotalFlux': 16216.544921875,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 16204.004778862,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536143400000,
            'plusTotalFlux': 16216.5546875,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 16204.014544487,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536143700000,
            'plusTotalFlux': 16216.55859375,
            'flux': 0.4041881263,
            'pressure': 0.5291,
            'totalFlux': 16204.018450737,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536144000000,
            'plusTotalFlux': 16216.5673828125,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 16204.0272397995,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536144300000,
            'plusTotalFlux': 16216.587890625,
            'flux': 0.2188154459,
            'pressure': 0.5239,
            'totalFlux': 16204.047747612,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536144600000,
            'plusTotalFlux': 16216.6044921875,
            'flux': 0.0,
            'pressure': 0.5245,
            'totalFlux': 16204.0643491745,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536144900000,
            'plusTotalFlux': 16216.6220703125,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 16204.0819272995,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536145200000,
            'plusTotalFlux': 16216.6337890625,
            'flux': 0.3752236366,
            'pressure': 0.5273,
            'totalFlux': 16204.0936460495,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536145500000,
            'plusTotalFlux': 16216.64453125,
            'flux': 0.0,
            'pressure': 0.5282,
            'totalFlux': 16204.104388237,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536145800000,
            'plusTotalFlux': 16216.6572265625,
            'flux': 0.2033677101,
            'pressure': 0.5349,
            'totalFlux': 16204.1170835495,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536146100000,
            'plusTotalFlux': 16216.673828125,
            'flux': 0.3115017712,
            'pressure': 0.526,
            'totalFlux': 16204.133685112,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536146400000,
            'plusTotalFlux': 16216.6826171875,
            'flux': 0.3983952105,
            'pressure': 0.5242,
            'totalFlux': 16204.1424741745,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536146700000,
            'plusTotalFlux': 16216.6943359375,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 16204.1541929245,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536147000000,
            'plusTotalFlux': 16216.6982421875,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 16204.1580991745,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.540143013
          },
          {
            'recordTime': 1536147300000,
            'plusTotalFlux': 16216.7021484375,
            'flux': 0.0,
            'pressure': 0.5297,
            'totalFlux': 16204.1610336304,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536147600000,
            'plusTotalFlux': 16216.7080078125,
            'flux': 0.4080500305,
            'pressure': 0.5242,
            'totalFlux': 16204.1668930054,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536147900000,
            'plusTotalFlux': 16216.72265625,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 16204.1815414429,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536148200000,
            'plusTotalFlux': 16216.7373046875,
            'flux': 0.0,
            'pressure': 0.5224,
            'totalFlux': 16204.1961898804,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536148500000,
            'plusTotalFlux': 16216.740234375,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 16204.1991195679,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536148800000,
            'plusTotalFlux': 16216.763671875,
            'flux': 0.2516418397,
            'pressure': 0.5306,
            'totalFlux': 16204.2225570679,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536149100000,
            'plusTotalFlux': 16216.7734375,
            'flux': 0.2651585937,
            'pressure': 0.5318,
            'totalFlux': 16204.2323226929,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536149400000,
            'plusTotalFlux': 16216.78125,
            'flux': 0.0,
            'pressure': 0.5303,
            'totalFlux': 16204.2401351929,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536149700000,
            'plusTotalFlux': 16216.7880859375,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 16204.2469711304,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536150000000,
            'plusTotalFlux': 16216.79296875,
            'flux': 0.0,
            'pressure': 0.5193,
            'totalFlux': 16204.2518539429,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536150300000,
            'plusTotalFlux': 16216.8125,
            'flux': 0.0,
            'pressure': 0.5285,
            'totalFlux': 16204.2713851929,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536150600000,
            'plusTotalFlux': 16216.8212890625,
            'flux': 0.0,
            'pressure': 0.527,
            'totalFlux': 16204.2801742554,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536150900000,
            'plusTotalFlux': 16216.8251953125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 16204.2840805054,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536151200000,
            'plusTotalFlux': 16216.8310546875,
            'flux': 0.0,
            'pressure': 0.538,
            'totalFlux': 16204.2899398804,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536151500000,
            'plusTotalFlux': 16216.8330078125,
            'flux': 0.0,
            'pressure': 0.5236,
            'totalFlux': 16204.2918930054,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536151800000,
            'plusTotalFlux': 16216.8388671875,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 16204.2977523804,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536152100000,
            'plusTotalFlux': 16216.8486328125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 16204.3075180054,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536152400000,
            'plusTotalFlux': 16216.853515625,
            'flux': 0.3172946572,
            'pressure': 0.537,
            'totalFlux': 16204.3124008179,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536152700000,
            'plusTotalFlux': 16216.865234375,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 16204.3241195679,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536153000000,
            'plusTotalFlux': 16216.8759765625,
            'flux': 0.0,
            'pressure': 0.5361,
            'totalFlux': 16204.3348617554,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536153300000,
            'plusTotalFlux': 16216.8876953125,
            'flux': 0.3559139967,
            'pressure': 0.5383,
            'totalFlux': 16204.3465805054,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536153600000,
            'plusTotalFlux': 16216.908203125,
            'flux': 0.3172946572,
            'pressure': 0.5395,
            'totalFlux': 16204.3670883179,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5411148071
          },
          {
            'recordTime': 1536153900000,
            'plusTotalFlux': 16216.9140625,
            'flux': 0.204333216,
            'pressure': 0.5322,
            'totalFlux': 16204.3701610565,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536154200000,
            'plusTotalFlux': 16216.9267578125,
            'flux': 0.2564692795,
            'pressure': 0.5322,
            'totalFlux': 16204.382856369,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536154500000,
            'plusTotalFlux': 16216.9404296875,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 16204.396528244,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536154800000,
            'plusTotalFlux': 16216.9609375,
            'flux': 0.2294357419,
            'pressure': 0.5315,
            'totalFlux': 16204.4170360565,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536155100000,
            'plusTotalFlux': 16216.9775390625,
            'flux': 0.205298692,
            'pressure': 0.5389,
            'totalFlux': 16204.433637619,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536155400000,
            'plusTotalFlux': 16216.9853515625,
            'flux': 0.0,
            'pressure': 0.5306,
            'totalFlux': 16204.441450119,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536155700000,
            'plusTotalFlux': 16216.998046875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 16204.4541454315,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536156000000,
            'plusTotalFlux': 16217.00390625,
            'flux': 0.0,
            'pressure': 0.537,
            'totalFlux': 16204.4600048065,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536156300000,
            'plusTotalFlux': 16217.013671875,
            'flux': 0.0,
            'pressure': 0.5331,
            'totalFlux': 16204.4697704315,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536156600000,
            'plusTotalFlux': 16217.0322265625,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 16204.488325119,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536156900000,
            'plusTotalFlux': 16217.0390625,
            'flux': 0.2593657076,
            'pressure': 0.5343,
            'totalFlux': 16204.4951610565,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536157200000,
            'plusTotalFlux': 16217.0498046875,
            'flux': 0.0,
            'pressure': 0.5306,
            'totalFlux': 16204.505903244,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536157500000,
            'plusTotalFlux': 16217.05078125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 16204.5068798065,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536157800000,
            'plusTotalFlux': 16217.0537109375,
            'flux': 0.2497108877,
            'pressure': 0.5297,
            'totalFlux': 16204.509809494,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536158100000,
            'plusTotalFlux': 16217.0625,
            'flux': 0.0,
            'pressure': 0.5325,
            'totalFlux': 16204.5185985565,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536158400000,
            'plusTotalFlux': 16217.0673828125,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 16204.523481369,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536158700000,
            'plusTotalFlux': 16217.0810546875,
            'flux': 0.0,
            'pressure': 0.5361,
            'totalFlux': 16204.537153244,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536159000000,
            'plusTotalFlux': 16217.0947265625,
            'flux': 0.0,
            'pressure': 0.5315,
            'totalFlux': 16204.550825119,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536159300000,
            'plusTotalFlux': 16217.107421875,
            'flux': 0.2014367878,
            'pressure': 0.5322,
            'totalFlux': 16204.5635204315,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536159600000,
            'plusTotalFlux': 16217.119140625,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 16204.5752391815,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536159900000,
            'plusTotalFlux': 16217.1240234375,
            'flux': 0.3028123975,
            'pressure': 0.5346,
            'totalFlux': 16204.580121994,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536160200000,
            'plusTotalFlux': 16217.1396484375,
            'flux': 0.0,
            'pressure': 0.5361,
            'totalFlux': 16204.595746994,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536160500000,
            'plusTotalFlux': 16217.1484375,
            'flux': 0.0,
            'pressure': 0.538,
            'totalFlux': 16204.6045360565,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536160800000,
            'plusTotalFlux': 16217.1513671875,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 16204.607465744,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536161100000,
            'plusTotalFlux': 16217.1689453125,
            'flux': 0.2439180017,
            'pressure': 0.5389,
            'totalFlux': 16204.625043869,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536161400000,
            'plusTotalFlux': 16217.1865234375,
            'flux': 0.0,
            'pressure': 0.5413,
            'totalFlux': 16204.642621994,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536161700000,
            'plusTotalFlux': 16217.1923828125,
            'flux': 0.205298692,
            'pressure': 0.538,
            'totalFlux': 16204.648481369,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536162000000,
            'plusTotalFlux': 16217.2001953125,
            'flux': 0.0,
            'pressure': 0.5358,
            'totalFlux': 16204.656293869,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536162300000,
            'plusTotalFlux': 16217.212890625,
            'flux': 0.0,
            'pressure': 0.5389,
            'totalFlux': 16204.6689891815,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536162600000,
            'plusTotalFlux': 16217.2236328125,
            'flux': 0.0,
            'pressure': 0.5404,
            'totalFlux': 16204.679731369,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536162900000,
            'plusTotalFlux': 16217.2314453125,
            'flux': 0.4041881263,
            'pressure': 0.5407,
            'totalFlux': 16204.687543869,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536163200000,
            'plusTotalFlux': 16217.2509765625,
            'flux': 0.0,
            'pressure': 0.5404,
            'totalFlux': 16204.707075119,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536163500000,
            'plusTotalFlux': 16217.2646484375,
            'flux': 0.0,
            'pressure': 0.5422,
            'totalFlux': 16204.720746994,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536163800000,
            'plusTotalFlux': 16217.2705078125,
            'flux': 0.3240530491,
            'pressure': 0.5428,
            'totalFlux': 16204.726606369,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536164100000,
            'plusTotalFlux': 16217.2880859375,
            'flux': 0.2342631817,
            'pressure': 0.5413,
            'totalFlux': 16204.744184494,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536164400000,
            'plusTotalFlux': 16217.2900390625,
            'flux': 0.0,
            'pressure': 0.5419,
            'totalFlux': 16204.746137619,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536164700000,
            'plusTotalFlux': 16217.3056640625,
            'flux': 0.2612966597,
            'pressure': 0.5407,
            'totalFlux': 16204.761762619,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536165000000,
            'plusTotalFlux': 16217.3154296875,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 16204.771528244,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536165300000,
            'plusTotalFlux': 16217.3232421875,
            'flux': 0.2738479972,
            'pressure': 0.5416,
            'totalFlux': 16204.779340744,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536165600000,
            'plusTotalFlux': 16217.3427734375,
            'flux': 0.0,
            'pressure': 0.5401,
            'totalFlux': 16204.798871994,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536165900000,
            'plusTotalFlux': 16217.3466796875,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 16204.802778244,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536166200000,
            'plusTotalFlux': 16217.3486328125,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 16204.804731369,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536166500000,
            'plusTotalFlux': 16217.3515625,
            'flux': 0.3404662609,
            'pressure': 0.5447,
            'totalFlux': 16204.8076610565,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536166800000,
            'plusTotalFlux': 16217.369140625,
            'flux': 0.0,
            'pressure': 0.5431,
            'totalFlux': 16204.8252391815,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536167100000,
            'plusTotalFlux': 16217.373046875,
            'flux': 0.0,
            'pressure': 0.5398,
            'totalFlux': 16204.8291454315,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536167400000,
            'plusTotalFlux': 16217.3876953125,
            'flux': 0.0,
            'pressure': 0.5383,
            'totalFlux': 16204.843793869,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536167700000,
            'plusTotalFlux': 16217.39453125,
            'flux': 0.2960540354,
            'pressure': 0.5508,
            'totalFlux': 16204.8506298065,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536168000000,
            'plusTotalFlux': 16217.41015625,
            'flux': 0.3607414067,
            'pressure': 0.5456,
            'totalFlux': 16204.8662548065,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536168300000,
            'plusTotalFlux': 16217.4208984375,
            'flux': 0.0,
            'pressure': 0.5453,
            'totalFlux': 16204.876996994,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536168600000,
            'plusTotalFlux': 16217.4345703125,
            'flux': 0.2458489537,
            'pressure': 0.5492,
            'totalFlux': 16204.890668869,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536168900000,
            'plusTotalFlux': 16217.4404296875,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 16204.896528244,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536169200000,
            'plusTotalFlux': 16217.4521484375,
            'flux': 0.2854337692,
            'pressure': 0.5441,
            'totalFlux': 16204.908246994,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536169500000,
            'plusTotalFlux': 16217.46484375,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 16204.9209423065,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536169800000,
            'plusTotalFlux': 16217.47265625,
            'flux': 0.2072296441,
            'pressure': 0.5431,
            'totalFlux': 16204.9287548065,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536170100000,
            'plusTotalFlux': 16217.474609375,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 16204.9307079315,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536170400000,
            'plusTotalFlux': 16217.48828125,
            'flux': 0.2130225599,
            'pressure': 0.5489,
            'totalFlux': 16204.9443798065,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536170700000,
            'plusTotalFlux': 16217.50390625,
            'flux': 0.2632277012,
            'pressure': 0.5431,
            'totalFlux': 16204.9600048065,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536171000000,
            'plusTotalFlux': 16217.5107421875,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 16204.966840744,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536171300000,
            'plusTotalFlux': 16217.5146484375,
            'flux': 0.0,
            'pressure': 0.5444,
            'totalFlux': 16204.970746994,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536171600000,
            'plusTotalFlux': 16217.533203125,
            'flux': 0.2188154459,
            'pressure': 0.5474,
            'totalFlux': 16204.9893016815,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536171900000,
            'plusTotalFlux': 16217.54296875,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 16204.9990673065,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536172200000,
            'plusTotalFlux': 16217.5517578125,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 16205.007856369,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536172500000,
            'plusTotalFlux': 16217.5546875,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 16205.0107860565,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536172800000,
            'plusTotalFlux': 16217.5634765625,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 16205.019575119,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536173100000,
            'plusTotalFlux': 16217.5732421875,
            'flux': 0.0,
            'pressure': 0.5456,
            'totalFlux': 16205.029340744,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536173400000,
            'plusTotalFlux': 16217.59375,
            'flux': 0.4186703563,
            'pressure': 0.5489,
            'totalFlux': 16205.0498485565,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536173700000,
            'plusTotalFlux': 16217.6181640625,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 16205.074262619,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536174000000,
            'plusTotalFlux': 16217.6435546875,
            'flux': 0.2477799356,
            'pressure': 0.5474,
            'totalFlux': 16205.099653244,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536174300000,
            'plusTotalFlux': 16217.654296875,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 16205.1103954315,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536174600000,
            'plusTotalFlux': 16217.6640625,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 16205.1201610565,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536174900000,
            'plusTotalFlux': 16217.6767578125,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 16205.132856369,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536175200000,
            'plusTotalFlux': 16217.68359375,
            'flux': 0.0,
            'pressure': 0.5465,
            'totalFlux': 16205.1396923065,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536175500000,
            'plusTotalFlux': 16217.6865234375,
            'flux': 0.0,
            'pressure': 0.5462,
            'totalFlux': 16205.142621994,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536175800000,
            'plusTotalFlux': 16217.6943359375,
            'flux': 0.0,
            'pressure': 0.5459,
            'totalFlux': 16205.150434494,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536176100000,
            'plusTotalFlux': 16217.705078125,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 16205.1611766815,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536176400000,
            'plusTotalFlux': 16217.712890625,
            'flux': 0.3433627188,
            'pressure': 0.5486,
            'totalFlux': 16205.1689891815,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536176700000,
            'plusTotalFlux': 16217.72265625,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 16205.1787548065,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536177000000,
            'plusTotalFlux': 16217.7333984375,
            'flux': 0.0,
            'pressure': 0.5492,
            'totalFlux': 16205.189496994,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536177300000,
            'plusTotalFlux': 16217.74609375,
            'flux': 0.2941231132,
            'pressure': 0.5492,
            'totalFlux': 16205.2021923065,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536177600000,
            'plusTotalFlux': 16217.7568359375,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 16205.212934494,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536177900000,
            'plusTotalFlux': 16217.7626953125,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 16205.218793869,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536178200000,
            'plusTotalFlux': 16217.7724609375,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 16205.228559494,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536178500000,
            'plusTotalFlux': 16217.7822265625,
            'flux': 0.2164983153,
            'pressure': 0.5486,
            'totalFlux': 16205.238325119,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536178800000,
            'plusTotalFlux': 16217.7998046875,
            'flux': 0.0,
            'pressure': 0.5483,
            'totalFlux': 16205.255903244,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536179100000,
            'plusTotalFlux': 16217.814453125,
            'flux': 0.3279150128,
            'pressure': 0.5499,
            'totalFlux': 16205.2705516815,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536179400000,
            'plusTotalFlux': 16217.828125,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 16205.2842235565,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536179700000,
            'plusTotalFlux': 16217.8291015625,
            'flux': 0.0,
            'pressure': 0.5468,
            'totalFlux': 16205.285200119,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536180000000,
            'plusTotalFlux': 16217.8359375,
            'flux': 0.2188154459,
            'pressure': 0.5505,
            'totalFlux': 16205.2920360565,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536180300000,
            'plusTotalFlux': 16217.845703125,
            'flux': 0.2883301973,
            'pressure': 0.5505,
            'totalFlux': 16205.3018016815,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536180600000,
            'plusTotalFlux': 16217.8564453125,
            'flux': 0.0,
            'pressure': 0.5508,
            'totalFlux': 16205.312543869,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536180900000,
            'plusTotalFlux': 16217.861328125,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 16205.3174266815,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536181200000,
            'plusTotalFlux': 16217.873046875,
            'flux': 0.0,
            'pressure': 0.5477,
            'totalFlux': 16205.3291454315,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536181500000,
            'plusTotalFlux': 16217.875,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 16205.3310985565,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536181800000,
            'plusTotalFlux': 16217.8837890625,
            'flux': 0.0,
            'pressure': 0.5471,
            'totalFlux': 16205.339887619,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536182100000,
            'plusTotalFlux': 16217.8837890625,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 16205.339887619,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536182400000,
            'plusTotalFlux': 16217.888671875,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 16205.3447704315,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536182700000,
            'plusTotalFlux': 16217.890625,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 16205.3467235565,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536183000000,
            'plusTotalFlux': 16217.90234375,
            'flux': 0.214953512,
            'pressure': 0.5489,
            'totalFlux': 16205.3584423065,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536183300000,
            'plusTotalFlux': 16217.90625,
            'flux': 0.0,
            'pressure': 0.5496,
            'totalFlux': 16205.3623485565,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536183600000,
            'plusTotalFlux': 16217.908203125,
            'flux': 0.0,
            'pressure': 0.5529,
            'totalFlux': 16205.3643016815,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536183900000,
            'plusTotalFlux': 16217.916015625,
            'flux': 0.0,
            'pressure': 0.5514,
            'totalFlux': 16205.3721141815,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536184200000,
            'plusTotalFlux': 16217.9228515625,
            'flux': 0.0,
            'pressure': 0.5511,
            'totalFlux': 16205.378950119,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536184500000,
            'plusTotalFlux': 16217.931640625,
            'flux': 0.0,
            'pressure': 0.5541,
            'totalFlux': 16205.3877391815,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536184800000,
            'plusTotalFlux': 16217.9384765625,
            'flux': 0.2361941338,
            'pressure': 0.5477,
            'totalFlux': 16205.394575119,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536185100000,
            'plusTotalFlux': 16217.9541015625,
            'flux': 0.317294687,
            'pressure': 0.5492,
            'totalFlux': 16205.410200119,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536185400000,
            'plusTotalFlux': 16217.9638671875,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 16205.419965744,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536185700000,
            'plusTotalFlux': 16217.9716796875,
            'flux': 0.0,
            'pressure': 0.552,
            'totalFlux': 16205.427778244,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536186000000,
            'plusTotalFlux': 16217.9775390625,
            'flux': 0.0,
            'pressure': 0.5502,
            'totalFlux': 16205.433637619,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536186300000,
            'plusTotalFlux': 16217.98828125,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 16205.4443798065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536186600000,
            'plusTotalFlux': 16217.99609375,
            'flux': 0.2236428559,
            'pressure': 0.5511,
            'totalFlux': 16205.4521923065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536186900000,
            'plusTotalFlux': 16218.01171875,
            'flux': 0.2651586235,
            'pressure': 0.5514,
            'totalFlux': 16205.4678173065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536187200000,
            'plusTotalFlux': 16218.0302734375,
            'flux': 0.2506763637,
            'pressure': 0.552,
            'totalFlux': 16205.486371994,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536187500000,
            'plusTotalFlux': 16218.0361328125,
            'flux': 0.2236428857,
            'pressure': 0.5502,
            'totalFlux': 16205.492231369,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536187800000,
            'plusTotalFlux': 16218.05078125,
            'flux': 0.0,
            'pressure': 0.5517,
            'totalFlux': 16205.5068798065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536188100000,
            'plusTotalFlux': 16218.05859375,
            'flux': 0.0,
            'pressure': 0.5505,
            'totalFlux': 16205.5146923065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536188400000,
            'plusTotalFlux': 16218.07421875,
            'flux': 0.2738479972,
            'pressure': 0.5468,
            'totalFlux': 16205.5303173065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536188700000,
            'plusTotalFlux': 16218.0791015625,
            'flux': 0.3578449488,
            'pressure': 0.5526,
            'totalFlux': 16205.535200119,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536189000000,
            'plusTotalFlux': 16218.0888671875,
            'flux': 0.0,
            'pressure': 0.5508,
            'totalFlux': 16205.544965744,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536189300000,
            'plusTotalFlux': 16218.09375,
            'flux': 0.3462591469,
            'pressure': 0.5517,
            'totalFlux': 16205.5498485565,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536189600000,
            'plusTotalFlux': 16218.099609375,
            'flux': 0.0,
            'pressure': 0.548,
            'totalFlux': 16205.5557079315,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536189900000,
            'plusTotalFlux': 16218.10546875,
            'flux': 0.0,
            'pressure': 0.5486,
            'totalFlux': 16205.5615673065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536190200000,
            'plusTotalFlux': 16218.1142578125,
            'flux': 0.4157738984,
            'pressure': 0.5459,
            'totalFlux': 16205.570356369,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536190500000,
            'plusTotalFlux': 16218.134765625,
            'flux': 0.0,
            'pressure': 0.545,
            'totalFlux': 16205.5908641815,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536190800000,
            'plusTotalFlux': 16218.142578125,
            'flux': 0.2458489537,
            'pressure': 0.5419,
            'totalFlux': 16205.5986766815,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536191100000,
            'plusTotalFlux': 16218.1669921875,
            'flux': 0.2237808108,
            'pressure': 0.5444,
            'totalFlux': 16205.623090744,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536191400000,
            'plusTotalFlux': 16218.18359375,
            'flux': 0.4708064198,
            'pressure': 0.5425,
            'totalFlux': 16205.6396923065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536191700000,
            'plusTotalFlux': 16218.1943359375,
            'flux': 0.0,
            'pressure': 0.5428,
            'totalFlux': 16205.650434494,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536192000000,
            'plusTotalFlux': 16218.1943359375,
            'flux': 0.0,
            'pressure': 0.538,
            'totalFlux': 16205.650434494,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536192300000,
            'plusTotalFlux': 16218.212890625,
            'flux': 0.221711874,
            'pressure': 0.5407,
            'totalFlux': 16205.6689891815,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536192600000,
            'plusTotalFlux': 16218.2236328125,
            'flux': 0.4910815656,
            'pressure': 0.5334,
            'totalFlux': 16205.679731369,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536192900000,
            'plusTotalFlux': 16218.232421875,
            'flux': 0.2173672318,
            'pressure': 0.5334,
            'totalFlux': 16205.6885204315,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536193200000,
            'plusTotalFlux': 16218.236328125,
            'flux': 0.0,
            'pressure': 0.5334,
            'totalFlux': 16205.6924266815,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536193500000,
            'plusTotalFlux': 16218.23828125,
            'flux': 0.23233217,
            'pressure': 0.5386,
            'totalFlux': 16205.6943798065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536193800000,
            'plusTotalFlux': 16218.2548828125,
            'flux': 0.2390905917,
            'pressure': 0.5303,
            'totalFlux': 16205.710981369,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536194100000,
            'plusTotalFlux': 16218.26171875,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 16205.7178173065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536194400000,
            'plusTotalFlux': 16218.2734375,
            'flux': 0.0,
            'pressure': 0.5273,
            'totalFlux': 16205.7295360565,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536194700000,
            'plusTotalFlux': 16218.28125,
            'flux': 0.3272253573,
            'pressure': 0.5346,
            'totalFlux': 16205.7373485565,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536195000000,
            'plusTotalFlux': 16218.2978515625,
            'flux': 0.0,
            'pressure': 0.5367,
            'totalFlux': 16205.753950119,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536195300000,
            'plusTotalFlux': 16218.3134765625,
            'flux': 0.2072296739,
            'pressure': 0.5367,
            'totalFlux': 16205.769575119,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536195600000,
            'plusTotalFlux': 16218.328125,
            'flux': 0.2970195413,
            'pressure': 0.5358,
            'totalFlux': 16205.7842235565,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536195900000,
            'plusTotalFlux': 16218.3330078125,
            'flux': 0.2506763637,
            'pressure': 0.5407,
            'totalFlux': 16205.789106369,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536196200000,
            'plusTotalFlux': 16218.34765625,
            'flux': 0.0,
            'pressure': 0.5322,
            'totalFlux': 16205.8037548065,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536196500000,
            'plusTotalFlux': 16218.3603515625,
            'flux': 0.2188154459,
            'pressure': 0.526,
            'totalFlux': 16205.816450119,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536196800000,
            'plusTotalFlux': 16218.3662109375,
            'flux': 0.2680550814,
            'pressure': 0.5355,
            'totalFlux': 16205.822309494,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536197100000,
            'plusTotalFlux': 16218.3740234375,
            'flux': 0.215918988,
            'pressure': 0.5312,
            'totalFlux': 16205.830121994,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536197400000,
            'plusTotalFlux': 16218.3798828125,
            'flux': 0.0,
            'pressure': 0.534,
            'totalFlux': 16205.835981369,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536197700000,
            'plusTotalFlux': 16218.3876953125,
            'flux': 0.0,
            'pressure': 0.5346,
            'totalFlux': 16205.843793869,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536198000000,
            'plusTotalFlux': 16218.3994140625,
            'flux': 0.2854337692,
            'pressure': 0.5337,
            'totalFlux': 16205.855512619,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536198300000,
            'plusTotalFlux': 16218.40625,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 16205.8623485565,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536198600000,
            'plusTotalFlux': 16218.416015625,
            'flux': 0.2558899522,
            'pressure': 0.5267,
            'totalFlux': 16205.8721141815,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536198900000,
            'plusTotalFlux': 16218.4345703125,
            'flux': 0.0,
            'pressure': 0.5294,
            'totalFlux': 16205.890668869,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536199200000,
            'plusTotalFlux': 16218.4365234375,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 16205.892621994,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536199500000,
            'plusTotalFlux': 16218.4365234375,
            'flux': 0.0,
            'pressure': 0.526,
            'totalFlux': 16205.892621994,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536199800000,
            'plusTotalFlux': 16218.439453125,
            'flux': 0.0,
            'pressure': 0.5242,
            'totalFlux': 16205.8955516815,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536200100000,
            'plusTotalFlux': 16218.447265625,
            'flux': 0.2796408534,
            'pressure': 0.5193,
            'totalFlux': 16205.9033641815,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536200400000,
            'plusTotalFlux': 16218.455078125,
            'flux': 0.0,
            'pressure': 0.5184,
            'totalFlux': 16205.9111766815,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536200700000,
            'plusTotalFlux': 16218.4599609375,
            'flux': 0.0,
            'pressure': 0.512,
            'totalFlux': 16205.916059494,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536201000000,
            'plusTotalFlux': 16218.4736328125,
            'flux': 0.2738479972,
            'pressure': 0.5132,
            'totalFlux': 16205.929731369,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536201300000,
            'plusTotalFlux': 16218.486328125,
            'flux': 0.2396698892,
            'pressure': 0.5126,
            'totalFlux': 16205.9424266815,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536201600000,
            'plusTotalFlux': 16218.5107421875,
            'flux': 0.2680550516,
            'pressure': 0.5144,
            'totalFlux': 16205.966840744,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536201900000,
            'plusTotalFlux': 16218.5234375,
            'flux': 0.3037779331,
            'pressure': 0.5111,
            'totalFlux': 16205.9795360565,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536202200000,
            'plusTotalFlux': 16218.541015625,
            'flux': 0.3230875432,
            'pressure': 0.5059,
            'totalFlux': 16205.9971141815,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536202500000,
            'plusTotalFlux': 16218.5625,
            'flux': 0.215918988,
            'pressure': 0.5151,
            'totalFlux': 16206.0185985565,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536202800000,
            'plusTotalFlux': 16218.5673828125,
            'flux': 0.22267735,
            'pressure': 0.5138,
            'totalFlux': 16206.023481369,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536203100000,
            'plusTotalFlux': 16218.5771484375,
            'flux': 0.2728825212,
            'pressure': 0.5144,
            'totalFlux': 16206.033246994,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536203400000,
            'plusTotalFlux': 16218.591796875,
            'flux': 0.0,
            'pressure': 0.5138,
            'totalFlux': 16206.0478954315,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536203700000,
            'plusTotalFlux': 16218.59765625,
            'flux': 0.0,
            'pressure': 0.5151,
            'totalFlux': 16206.0537548065,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536204000000,
            'plusTotalFlux': 16218.60546875,
            'flux': 0.0,
            'pressure': 0.5154,
            'totalFlux': 16206.0615673065,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5439014435
          },
          {
            'recordTime': 1536204300000,
            'plusTotalFlux': 16218.609375,
            'flux': 0.0,
            'pressure': 0.5114,
            'totalFlux': 16206.0645513535,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5448236465
          },
          {
            'recordTime': 1536204600000,
            'plusTotalFlux': 16218.62109375,
            'flux': 0.347707361,
            'pressure': 0.5151,
            'totalFlux': 16206.0762701035,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5448236465
          },
          {
            'recordTime': 1536204900000,
            'plusTotalFlux': 16218.6279296875,
            'flux': 0.0,
            'pressure': 0.512,
            'totalFlux': 16206.083106041,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5448236465
          },
          {
            'recordTime': 1536205200000,
            'plusTotalFlux': 16218.634765625,
            'flux': 0.3462591469,
            'pressure': 0.5138,
            'totalFlux': 16206.0899419785,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5448236465
          },
          {
            'recordTime': 1536205500000,
            'plusTotalFlux': 16218.64453125,
            'flux': 0.2883301973,
            'pressure': 0.5157,
            'totalFlux': 16206.0997076035,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5448236465
          },
          {
            'recordTime': 1536205800000,
            'plusTotalFlux': 16218.6533203125,
            'flux': 0.0,
            'pressure': 0.5163,
            'totalFlux': 16206.108496666,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5448236465
          },
          {
            'recordTime': 1536206100000,
            'plusTotalFlux': 16218.6611328125,
            'flux': 0.0,
            'pressure': 0.509,
            'totalFlux': 16206.116309166,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5448236465
          },
          {
            'recordTime': 1536206400000,
            'plusTotalFlux': 16218.6748046875,
            'flux': 0.3346733749,
            'pressure': 0.5148,
            'totalFlux': 16206.129981041,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 12.5448236465
          }
        ],
        'latitude': 45.605505,
        'remark': null,
        'installTime': 1533091180000,
        'deviceId': 61,
        'deviceName': 'LUF',
        'userCode': '001004009',
        'onStage': 'ON_LINE',
        'manufacturer': 1,
        'diameter': 100,
        'provider': '拓安信',
        'sim': null,
        'id': 61,
        'department': '供水研究所',
        'longitude': 84.84931,
        'deviceType': '水表类',
        'address': null,
        'isRemote': true,
        'deviceStage': 'NORMAL',
        'lastCallTime': null,
        'maintainPersonPhone': '15989869508',
        'deviceCode': '1804202234',
        'userName': '克拉玛依物业管理公司',
        'siteName': '克拉玛依物业管理公司',
        'siteCode': '1804202234',
        'siteId': 1804202234,
        'policeFlag': 'COMPLETE',
        'modBusCode': null,
        'deviceModel': 'MA1011011300EH02P-L',
        'maintainPerson': '刘小芳',
        'installPerson': null
      },
      {
        'isRemoteControl': false,
        'data': [
          {
            'recordTime': 1535708400000,
            'plusTotalFlux': 329.2747802734,
            'flux': 0.0,
            'pressure': 0.2235,
            'totalFlux': 191.3557434082,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9190368652
          },
          {
            'recordTime': 1535708700000,
            'plusTotalFlux': 329.2747802734,
            'flux': 0.0,
            'pressure': 0.2205,
            'totalFlux': 191.3542785645,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.920501709
          },
          {
            'recordTime': 1535709000000,
            'plusTotalFlux': 329.2935180664,
            'flux': 0.0,
            'pressure': 0.2248,
            'totalFlux': 191.3662567139,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9272613525
          },
          {
            'recordTime': 1535709300000,
            'plusTotalFlux': 329.3087158203,
            'flux': 0.3448168337,
            'pressure': 0.2177,
            'totalFlux': 191.3814544678,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9272613525
          },
          {
            'recordTime': 1535709600000,
            'plusTotalFlux': 329.3147277832,
            'flux': 0.0,
            'pressure': 0.2199,
            'totalFlux': 191.3800354004,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9346923828
          },
          {
            'recordTime': 1535709900000,
            'plusTotalFlux': 329.3303222656,
            'flux': 0.6006131768,
            'pressure': 0.2153,
            'totalFlux': 191.3924865723,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9378356934
          },
          {
            'recordTime': 1535710200000,
            'plusTotalFlux': 329.3632507324,
            'flux': 0.4461701512,
            'pressure': 0.2208,
            'totalFlux': 191.4254150391,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9378356934
          },
          {
            'recordTime': 1535710500000,
            'plusTotalFlux': 329.3973083496,
            'flux': 1.323600173,
            'pressure': 0.2226,
            'totalFlux': 191.4594726562,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9378356934
          },
          {
            'recordTime': 1535710800000,
            'plusTotalFlux': 329.4380493164,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 191.4901428223,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9479064941
          },
          {
            'recordTime': 1535711100000,
            'plusTotalFlux': 329.4866943359,
            'flux': 0.0,
            'pressure': 0.2269,
            'totalFlux': 191.5387878418,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9479064941
          },
          {
            'recordTime': 1535711400000,
            'plusTotalFlux': 329.5168762207,
            'flux': 0.4618074894,
            'pressure': 0.2263,
            'totalFlux': 191.5689697266,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9479064941
          },
          {
            'recordTime': 1535711700000,
            'plusTotalFlux': 329.5488891602,
            'flux': 0.4374827445,
            'pressure': 0.2235,
            'totalFlux': 191.5978546143,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9510345459
          },
          {
            'recordTime': 1535712000000,
            'plusTotalFlux': 329.5586547852,
            'flux': 0.6517725587,
            'pressure': 0.2196,
            'totalFlux': 191.6042480469,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9544067383
          },
          {
            'recordTime': 1535712300000,
            'plusTotalFlux': 329.5787963867,
            'flux': 0.4693366289,
            'pressure': 0.2205,
            'totalFlux': 191.6243896484,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9544067383
          },
          {
            'recordTime': 1535712600000,
            'plusTotalFlux': 329.5917663574,
            'flux': -1.5171878338,
            'pressure': 0.2168,
            'totalFlux': 191.6265716553,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 137.9651947021
          },
          {
            'recordTime': 1535712900000,
            'plusTotalFlux': 329.5917663574,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 191.5813751221,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.0103912354
          },
          {
            'recordTime': 1535713200000,
            'plusTotalFlux': 329.5949707031,
            'flux': 0.0,
            'pressure': 0.2193,
            'totalFlux': 191.5771484375,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.0178222656
          },
          {
            'recordTime': 1535713500000,
            'plusTotalFlux': 329.5960998535,
            'flux': 0.5127737522,
            'pressure': 0.2199,
            'totalFlux': 191.5736846924,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.0224151611
          },
          {
            'recordTime': 1535713800000,
            'plusTotalFlux': 329.6057128906,
            'flux': 0.3660527468,
            'pressure': 0.2144,
            'totalFlux': 191.5832977295,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.0224151611
          },
          {
            'recordTime': 1535714100000,
            'plusTotalFlux': 329.6171569824,
            'flux': 0.0,
            'pressure': 0.2168,
            'totalFlux': 191.5933074951,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.0238494873
          },
          {
            'recordTime': 1535714400000,
            'plusTotalFlux': 329.6185913086,
            'flux': 0.6459808946,
            'pressure': 0.2174,
            'totalFlux': 191.5904541016,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.028137207
          },
          {
            'recordTime': 1535714700000,
            'plusTotalFlux': 329.6282958984,
            'flux': 0.0,
            'pressure': 0.2217,
            'totalFlux': 191.6001586914,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.028137207
          },
          {
            'recordTime': 1535715000000,
            'plusTotalFlux': 329.6932067871,
            'flux': 1.3120168447,
            'pressure': 0.2235,
            'totalFlux': 191.6650695801,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.028137207
          },
          {
            'recordTime': 1535715300000,
            'plusTotalFlux': 329.7799072266,
            'flux': 0.3506084979,
            'pressure': 0.2202,
            'totalFlux': 191.7517700195,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.028137207
          },
          {
            'recordTime': 1535715600000,
            'plusTotalFlux': 329.796875,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 191.768737793,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.028137207
          },
          {
            'recordTime': 1535715900000,
            'plusTotalFlux': 329.8359375,
            'flux': 0.0,
            'pressure': 0.2254,
            'totalFlux': 191.807800293,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.028137207
          },
          {
            'recordTime': 1535716200000,
            'plusTotalFlux': 329.8666381836,
            'flux': 0.8593053818,
            'pressure': 0.2223,
            'totalFlux': 191.8352203369,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.0314178467
          },
          {
            'recordTime': 1535716500000,
            'plusTotalFlux': 329.935760498,
            'flux': 0.4519617558,
            'pressure': 0.2177,
            'totalFlux': 191.9043426514,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.0314178467
          },
          {
            'recordTime': 1535716800000,
            'plusTotalFlux': 329.9934692383,
            'flux': 0.0,
            'pressure': 0.2263,
            'totalFlux': 191.9620513916,
            'uploadTime': 1535731356200,
            'reverseTotalFlux': 138.0314178467
          },
          {
            'recordTime': 1535717100000,
            'plusTotalFlux': 330.0132751465,
            'flux': 0.4374827147,
            'pressure': 0.219,
            'totalFlux': 191.9818572998,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0314178467
          },
          {
            'recordTime': 1535717400000,
            'plusTotalFlux': 330.0303649902,
            'flux': 0.0,
            'pressure': 0.2226,
            'totalFlux': 191.9989471436,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0314178467
          },
          {
            'recordTime': 1535717700000,
            'plusTotalFlux': 330.0332336426,
            'flux': 0.6170228124,
            'pressure': 0.2239,
            'totalFlux': 191.9858856201,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0473480225
          },
          {
            'recordTime': 1535718000000,
            'plusTotalFlux': 330.0747375488,
            'flux': 0.3940455914,
            'pressure': 0.2235,
            'totalFlux': 192.0273895264,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0473480225
          },
          {
            'recordTime': 1535718300000,
            'plusTotalFlux': 330.0755004883,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 192.0281524658,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0473480225
          },
          {
            'recordTime': 1535718600000,
            'plusTotalFlux': 330.100769043,
            'flux': 0.0,
            'pressure': 0.2248,
            'totalFlux': 192.0534210205,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0473480225
          },
          {
            'recordTime': 1535718900000,
            'plusTotalFlux': 330.1170654297,
            'flux': 0.0,
            'pressure': 0.2208,
            'totalFlux': 192.0648956299,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0521697998
          },
          {
            'recordTime': 1535719200000,
            'plusTotalFlux': 330.1321716309,
            'flux': 0.0,
            'pressure': 0.233,
            'totalFlux': 192.0800018311,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0521697998
          },
          {
            'recordTime': 1535719500000,
            'plusTotalFlux': 330.1585083008,
            'flux': 0.0,
            'pressure': 0.2339,
            'totalFlux': 192.106338501,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0521697998
          },
          {
            'recordTime': 1535719800000,
            'plusTotalFlux': 330.185760498,
            'flux': 0.5301485658,
            'pressure': 0.2394,
            'totalFlux': 192.1320648193,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0536956787
          },
          {
            'recordTime': 1535720100000,
            'plusTotalFlux': 330.1936035156,
            'flux': 0.0,
            'pressure': 0.2544,
            'totalFlux': 192.1399078369,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0536956787
          },
          {
            'recordTime': 1535720400000,
            'plusTotalFlux': 330.1936035156,
            'flux': 0.0,
            'pressure': 0.2565,
            'totalFlux': 192.1399078369,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0536956787
          },
          {
            'recordTime': 1535720700000,
            'plusTotalFlux': 330.2098999023,
            'flux': 0.5880647302,
            'pressure': 0.2565,
            'totalFlux': 192.1513824463,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0585174561
          },
          {
            'recordTime': 1535721000000,
            'plusTotalFlux': 330.2425537109,
            'flux': 0.0,
            'pressure': 0.2614,
            'totalFlux': 192.1840362549,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0585174561
          },
          {
            'recordTime': 1535721300000,
            'plusTotalFlux': 330.2425537109,
            'flux': 0.0,
            'pressure': 0.2751,
            'totalFlux': 192.181137085,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.061416626
          },
          {
            'recordTime': 1535721600000,
            'plusTotalFlux': 330.2472229004,
            'flux': 0.0,
            'pressure': 0.2702,
            'totalFlux': 192.1858062744,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.061416626
          },
          {
            'recordTime': 1535721900000,
            'plusTotalFlux': 330.2613525391,
            'flux': 0.0,
            'pressure': 0.2706,
            'totalFlux': 192.1999359131,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.061416626
          },
          {
            'recordTime': 1535722200000,
            'plusTotalFlux': 330.2613525391,
            'flux': 0.0,
            'pressure': 0.2739,
            'totalFlux': 192.1999359131,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.061416626
          },
          {
            'recordTime': 1535722500000,
            'plusTotalFlux': 330.264465332,
            'flux': 0.0,
            'pressure': 0.2669,
            'totalFlux': 192.2016448975,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0628204346
          },
          {
            'recordTime': 1535722800000,
            'plusTotalFlux': 330.264465332,
            'flux': 0.0,
            'pressure': 0.269,
            'totalFlux': 192.1924743652,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0719909668
          },
          {
            'recordTime': 1535723100000,
            'plusTotalFlux': 330.264465332,
            'flux': 0.0,
            'pressure': 0.266,
            'totalFlux': 192.1910400391,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.073425293
          },
          {
            'recordTime': 1535723400000,
            'plusTotalFlux': 330.2692871094,
            'flux': 0.0,
            'pressure': 0.269,
            'totalFlux': 192.1855773926,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0837097168
          },
          {
            'recordTime': 1535723700000,
            'plusTotalFlux': 330.2779541016,
            'flux': 0.0,
            'pressure': 0.2672,
            'totalFlux': 192.192276001,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0856781006
          },
          {
            'recordTime': 1535724000000,
            'plusTotalFlux': 330.2926635742,
            'flux': 0.0,
            'pressure': 0.2568,
            'totalFlux': 192.2054901123,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.0871734619
          },
          {
            'recordTime': 1535724300000,
            'plusTotalFlux': 330.3039245605,
            'flux': 0.8602707386,
            'pressure': 0.208,
            'totalFlux': 192.2022705078,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1016540527
          },
          {
            'recordTime': 1535724600000,
            'plusTotalFlux': 330.3294372559,
            'flux': 0.4953989089,
            'pressure': 0.2086,
            'totalFlux': 192.2277832031,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1016540527
          },
          {
            'recordTime': 1535724900000,
            'plusTotalFlux': 330.3367004395,
            'flux': 0.0,
            'pressure': 0.204,
            'totalFlux': 192.2285919189,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1081085205
          },
          {
            'recordTime': 1535725200000,
            'plusTotalFlux': 330.3367004395,
            'flux': 0.0,
            'pressure': 0.2037,
            'totalFlux': 192.2285919189,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1081085205
          },
          {
            'recordTime': 1535725500000,
            'plusTotalFlux': 330.3659973145,
            'flux': 0.0,
            'pressure': 0.1976,
            'totalFlux': 192.2508850098,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1151123047
          },
          {
            'recordTime': 1535725800000,
            'plusTotalFlux': 330.3759155273,
            'flux': 0.4606491923,
            'pressure': 0.1958,
            'totalFlux': 192.2537384033,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.122177124
          },
          {
            'recordTime': 1535726100000,
            'plusTotalFlux': 330.3964538574,
            'flux': 0.0,
            'pressure': 0.2031,
            'totalFlux': 192.2742767334,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.122177124
          },
          {
            'recordTime': 1535726400000,
            'plusTotalFlux': 330.4107055664,
            'flux': 0.415281564,
            'pressure': 0.2065,
            'totalFlux': 192.2822723389,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1284332275
          },
          {
            'recordTime': 1535726700000,
            'plusTotalFlux': 330.416595459,
            'flux': 0.0,
            'pressure': 0.2028,
            'totalFlux': 192.2881622314,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1284332275
          },
          {
            'recordTime': 1535727000000,
            'plusTotalFlux': 330.416595459,
            'flux': 0.0,
            'pressure': 0.2043,
            'totalFlux': 192.2881622314,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1284332275
          },
          {
            'recordTime': 1535727300000,
            'plusTotalFlux': 330.4218139648,
            'flux': 0.8544790745,
            'pressure': 0.1985,
            'totalFlux': 192.2849121094,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1369018555
          },
          {
            'recordTime': 1535727600000,
            'plusTotalFlux': 330.4293212891,
            'flux': 0.0,
            'pressure': 0.201,
            'totalFlux': 192.2895202637,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1398010254
          },
          {
            'recordTime': 1535727900000,
            'plusTotalFlux': 330.4328308105,
            'flux': 0.6807305813,
            'pressure': 0.2052,
            'totalFlux': 192.2930297852,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1398010254
          },
          {
            'recordTime': 1535728200000,
            'plusTotalFlux': 330.4556884766,
            'flux': 0.0,
            'pressure': 0.2013,
            'totalFlux': 192.307144165,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1485443115
          },
          {
            'recordTime': 1535728500000,
            'plusTotalFlux': 330.491027832,
            'flux': 0.0,
            'pressure': 0.2061,
            'totalFlux': 192.3424835205,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1485443115
          },
          {
            'recordTime': 1535728800000,
            'plusTotalFlux': 330.49609375,
            'flux': 0.0,
            'pressure': 0.201,
            'totalFlux': 192.3475494385,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1485443115
          },
          {
            'recordTime': 1535729100000,
            'plusTotalFlux': 330.5062866211,
            'flux': 0.0,
            'pressure': 0.2055,
            'totalFlux': 192.3577423096,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1485443115
          },
          {
            'recordTime': 1535729400000,
            'plusTotalFlux': 330.5156860352,
            'flux': 0.0,
            'pressure': 0.2055,
            'totalFlux': 192.3533630371,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.162322998
          },
          {
            'recordTime': 1535729700000,
            'plusTotalFlux': 330.5156860352,
            'flux': 0.0,
            'pressure': 0.2092,
            'totalFlux': 192.3498077393,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1658782959
          },
          {
            'recordTime': 1535730000000,
            'plusTotalFlux': 330.5188903809,
            'flux': 0.0,
            'pressure': 0.2113,
            'totalFlux': 192.3477172852,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1711730957
          },
          {
            'recordTime': 1535730300000,
            'plusTotalFlux': 330.5188903809,
            'flux': 0.0,
            'pressure': 0.201,
            'totalFlux': 192.333984375,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1849060059
          },
          {
            'recordTime': 1535730600000,
            'plusTotalFlux': 330.5205078125,
            'flux': 0.0,
            'pressure': 0.2074,
            'totalFlux': 192.3356018066,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1849060059
          },
          {
            'recordTime': 1535730900000,
            'plusTotalFlux': 330.5205078125,
            'flux': 0.0,
            'pressure': 0.2065,
            'totalFlux': 192.3303985596,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1901092529
          },
          {
            'recordTime': 1535731200000,
            'plusTotalFlux': 330.5205078125,
            'flux': -0.348246634,
            'pressure': 0.215,
            'totalFlux': 192.3266601562,
            'uploadTime': 1535731365637,
            'reverseTotalFlux': 138.1938476562
          },
          {
            'recordTime': 1535731500000,
            'plusTotalFlux': 330.5263366699,
            'flux': 0.0,
            'pressure': 0.2174,
            'totalFlux': 192.3283843994,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.1979522705
          },
          {
            'recordTime': 1535731800000,
            'plusTotalFlux': 330.5308227539,
            'flux': 0.0,
            'pressure': 0.2205,
            'totalFlux': 192.3328704834,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.1979522705
          },
          {
            'recordTime': 1535732100000,
            'plusTotalFlux': 330.5308227539,
            'flux': -0.3646562099,
            'pressure': 0.2184,
            'totalFlux': 192.3317565918,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.1990661621
          },
          {
            'recordTime': 1535732400000,
            'plusTotalFlux': 330.5308227539,
            'flux': 0.0,
            'pressure': 0.2214,
            'totalFlux': 192.3066101074,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.2242126465
          },
          {
            'recordTime': 1535732700000,
            'plusTotalFlux': 330.5308227539,
            'flux': 0.0,
            'pressure': 0.2248,
            'totalFlux': 192.3066101074,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.2242126465
          },
          {
            'recordTime': 1535733000000,
            'plusTotalFlux': 330.532043457,
            'flux': 0.4027330279,
            'pressure': 0.2321,
            'totalFlux': 192.292098999,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.239944458
          },
          {
            'recordTime': 1535733300000,
            'plusTotalFlux': 330.5412902832,
            'flux': 0.4239689708,
            'pressure': 0.2312,
            'totalFlux': 192.291519165,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.2497711182
          },
          {
            'recordTime': 1535733600000,
            'plusTotalFlux': 330.562713623,
            'flux': 0.0,
            'pressure': 0.2358,
            'totalFlux': 192.3129425049,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.2497711182
          },
          {
            'recordTime': 1535733900000,
            'plusTotalFlux': 330.562713623,
            'flux': 0.0,
            'pressure': 0.2385,
            'totalFlux': 192.3129425049,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.2497711182
          },
          {
            'recordTime': 1535734200000,
            'plusTotalFlux': 330.562713623,
            'flux': 0.0,
            'pressure': 0.2431,
            'totalFlux': 192.3129425049,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.2497711182
          },
          {
            'recordTime': 1535734500000,
            'plusTotalFlux': 330.5848388672,
            'flux': 0.0,
            'pressure': 0.248,
            'totalFlux': 192.335067749,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.2497711182
          },
          {
            'recordTime': 1535734800000,
            'plusTotalFlux': 330.5848388672,
            'flux': 0.0,
            'pressure': 0.2513,
            'totalFlux': 192.3256072998,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.2592315674
          },
          {
            'recordTime': 1535735100000,
            'plusTotalFlux': 330.5848388672,
            'flux': 0.0,
            'pressure': 0.2568,
            'totalFlux': 192.3256072998,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.2592315674
          },
          {
            'recordTime': 1535735400000,
            'plusTotalFlux': 330.5848388672,
            'flux': -0.4312598705,
            'pressure': 0.2519,
            'totalFlux': 192.3206939697,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.2641448975
          },
          {
            'recordTime': 1535735700000,
            'plusTotalFlux': 330.5848388672,
            'flux': 0.0,
            'pressure': 0.2599,
            'totalFlux': 192.3200836182,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.264755249
          },
          {
            'recordTime': 1535736000000,
            'plusTotalFlux': 330.5962219238,
            'flux': 0.0,
            'pressure': 0.2654,
            'totalFlux': 192.3314666748,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.264755249
          },
          {
            'recordTime': 1535736300000,
            'plusTotalFlux': 330.5962219238,
            'flux': 0.0,
            'pressure': 0.1686,
            'totalFlux': 192.3314666748,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.264755249
          },
          {
            'recordTime': 1535736600000,
            'plusTotalFlux': 330.5962219238,
            'flux': 0.0,
            'pressure': 0.1509,
            'totalFlux': 192.2943725586,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3018493652
          },
          {
            'recordTime': 1535736900000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1512,
            'totalFlux': 192.2978820801,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3018493652
          },
          {
            'recordTime': 1535737200000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1518,
            'totalFlux': 192.2978820801,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3018493652
          },
          {
            'recordTime': 1535737500000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1558,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535737800000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1582,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535738100000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1585,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535738400000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.161,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535738700000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1628,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535739000000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1628,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535739300000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1628,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535739600000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1594,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535739900000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1649,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535740200000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1686,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535740500000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1704,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535740800000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1713,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535741100000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1695,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535741400000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1649,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535741700000,
            'plusTotalFlux': 330.5997314453,
            'flux': 0.0,
            'pressure': 0.1717,
            'totalFlux': 192.2962341309,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535742000000,
            'plusTotalFlux': 330.6113586426,
            'flux': 0.0,
            'pressure': 0.171,
            'totalFlux': 192.3078613281,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535742300000,
            'plusTotalFlux': 330.6113586426,
            'flux': 0.0,
            'pressure': 0.1717,
            'totalFlux': 192.3078613281,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535742600000,
            'plusTotalFlux': 330.6113586426,
            'flux': 0.0,
            'pressure': 0.1747,
            'totalFlux': 192.3078613281,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535742900000,
            'plusTotalFlux': 330.6113586426,
            'flux': 0.0,
            'pressure': 0.175,
            'totalFlux': 192.3078613281,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535743200000,
            'plusTotalFlux': 330.6113586426,
            'flux': 0.0,
            'pressure': 0.175,
            'totalFlux': 192.3078613281,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535743500000,
            'plusTotalFlux': 330.6167602539,
            'flux': 0.3370946646,
            'pressure': 0.1796,
            'totalFlux': 192.3132629395,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535743800000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1805,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535744100000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1796,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535744400000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1771,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535744700000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1802,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535745000000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1808,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535745300000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1814,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535745600000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1805,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753047923,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535745900000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1811,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535746200000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1842,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535746500000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1845,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535746800000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1839,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535747100000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1842,
            'totalFlux': 192.3137512207,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3034973145
          },
          {
            'recordTime': 1535747400000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1839,
            'totalFlux': 192.3074188232,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535747700000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1823,
            'totalFlux': 192.3074188232,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535748000000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1762,
            'totalFlux': 192.3074188232,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535748300000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1713,
            'totalFlux': 192.3074188232,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535748600000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1713,
            'totalFlux': 192.3074188232,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535748900000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1701,
            'totalFlux': 192.3074188232,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535749200000,
            'plusTotalFlux': 330.6172485352,
            'flux': 0.0,
            'pressure': 0.1713,
            'totalFlux': 192.3074188232,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535749500000,
            'plusTotalFlux': 330.6219482422,
            'flux': 0.0,
            'pressure': 0.1713,
            'totalFlux': 192.3121185303,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535749800000,
            'plusTotalFlux': 330.6301879883,
            'flux': 0.0,
            'pressure': 0.1695,
            'totalFlux': 192.3203582764,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535750100000,
            'plusTotalFlux': 330.6301879883,
            'flux': 0.0,
            'pressure': 0.1704,
            'totalFlux': 192.3203582764,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535750400000,
            'plusTotalFlux': 330.6301879883,
            'flux': 0.0,
            'pressure': 0.1686,
            'totalFlux': 192.3203582764,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535750700000,
            'plusTotalFlux': 330.6301879883,
            'flux': 0.0,
            'pressure': 0.1637,
            'totalFlux': 192.3203582764,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3098297119
          },
          {
            'recordTime': 1535751000000,
            'plusTotalFlux': 330.6301879883,
            'flux': 0.0,
            'pressure': 0.1689,
            'totalFlux': 192.3172149658,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535751300000,
            'plusTotalFlux': 330.6301879883,
            'flux': 0.0,
            'pressure': 0.168,
            'totalFlux': 192.3172149658,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535751600000,
            'plusTotalFlux': 330.6710205078,
            'flux': 0.4606491625,
            'pressure': 0.1671,
            'totalFlux': 192.3580474854,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535751900000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1662,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535752200000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1637,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535752500000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1631,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535752800000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1616,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535753063790,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535753100000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1637,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535753400000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1628,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535753700000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1616,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535754000000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1613,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535754300000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1558,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535754600000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1491,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535754900000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1533,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535755200000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1521,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535755500000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1481,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535755800000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.146,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535756100000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1469,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535756400000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1338,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535756700000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1179,
            'totalFlux': 192.4015655518,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3129730225
          },
          {
            'recordTime': 1535757000000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1091,
            'totalFlux': 192.3986663818,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3158721924
          },
          {
            'recordTime': 1535757300000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.1014,
            'totalFlux': 192.3986663818,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3158721924
          },
          {
            'recordTime': 1535757600000,
            'plusTotalFlux': 330.7145385742,
            'flux': 0.0,
            'pressure': 0.0902,
            'totalFlux': 192.3936920166,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3208465576
          },
          {
            'recordTime': 1535757900000,
            'plusTotalFlux': 330.7145385742,
            'flux': -0.4689052999,
            'pressure': 0.1643,
            'totalFlux': 192.3923950195,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3221435547
          },
          {
            'recordTime': 1535758200000,
            'plusTotalFlux': 330.7870788574,
            'flux': 0.5523496866,
            'pressure': 0.3041,
            'totalFlux': 192.4627532959,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3243255615
          },
          {
            'recordTime': 1535758500000,
            'plusTotalFlux': 330.8012695312,
            'flux': 0.0,
            'pressure': 0.317,
            'totalFlux': 192.4769439697,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3243255615
          },
          {
            'recordTime': 1535758800000,
            'plusTotalFlux': 330.8053894043,
            'flux': 0.0,
            'pressure': 0.3179,
            'totalFlux': 192.4684295654,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3369598389
          },
          {
            'recordTime': 1535759100000,
            'plusTotalFlux': 330.8223266602,
            'flux': 0.4538923204,
            'pressure': 0.305,
            'totalFlux': 192.466217041,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3561096191
          },
          {
            'recordTime': 1535759400000,
            'plusTotalFlux': 330.8326416016,
            'flux': 0.0,
            'pressure': 0.305,
            'totalFlux': 192.4737548828,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3588867188
          },
          {
            'recordTime': 1535759700000,
            'plusTotalFlux': 330.8470458984,
            'flux': 0.0,
            'pressure': 0.2922,
            'totalFlux': 192.4881591797,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3588867188
          },
          {
            'recordTime': 1535760000000,
            'plusTotalFlux': 330.8640441895,
            'flux': 0.0,
            'pressure': 0.2825,
            'totalFlux': 192.5051574707,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3588867188
          },
          {
            'recordTime': 1535760300000,
            'plusTotalFlux': 330.8640441895,
            'flux': 0.0,
            'pressure': 0.2718,
            'totalFlux': 192.4941253662,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3699188232
          },
          {
            'recordTime': 1535760600000,
            'plusTotalFlux': 330.879119873,
            'flux': 0.0,
            'pressure': 0.2657,
            'totalFlux': 192.5092010498,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3699188232
          },
          {
            'recordTime': 1535760900000,
            'plusTotalFlux': 330.879119873,
            'flux': 0.0,
            'pressure': 0.2644,
            'totalFlux': 192.5035095215,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3756103516
          },
          {
            'recordTime': 1535761200000,
            'plusTotalFlux': 330.8807067871,
            'flux': 0.0,
            'pressure': 0.2562,
            'totalFlux': 192.5050964355,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3756103516
          },
          {
            'recordTime': 1535761500000,
            'plusTotalFlux': 330.884552002,
            'flux': 0.5504192114,
            'pressure': 0.2568,
            'totalFlux': 192.5029907227,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3815612793
          },
          {
            'recordTime': 1535761800000,
            'plusTotalFlux': 330.895904541,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 192.5143432617,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3815612793
          },
          {
            'recordTime': 1535762100000,
            'plusTotalFlux': 330.9046936035,
            'flux': 0.0,
            'pressure': 0.2226,
            'totalFlux': 192.5231323242,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3815612793
          },
          {
            'recordTime': 1535762400000,
            'plusTotalFlux': 330.9177856445,
            'flux': 0.0,
            'pressure': 0.2202,
            'totalFlux': 192.5362243652,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3815612793
          },
          {
            'recordTime': 1535762700000,
            'plusTotalFlux': 330.9224853516,
            'flux': 0.0,
            'pressure': 0.2086,
            'totalFlux': 192.5254364014,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3970489502
          },
          {
            'recordTime': 1535763000000,
            'plusTotalFlux': 330.9367980957,
            'flux': 0.7965629697,
            'pressure': 0.2119,
            'totalFlux': 192.5397491455,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3970489502
          },
          {
            'recordTime': 1535763300000,
            'plusTotalFlux': 330.9397583008,
            'flux': 0.0,
            'pressure': 0.1915,
            'totalFlux': 192.5411529541,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.3986053467
          },
          {
            'recordTime': 1535763600000,
            'plusTotalFlux': 330.9397583008,
            'flux': 0.0,
            'pressure': 0.1823,
            'totalFlux': 192.533996582,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4057617188
          },
          {
            'recordTime': 1535763900000,
            'plusTotalFlux': 330.9422302246,
            'flux': 0.374740243,
            'pressure': 0.1857,
            'totalFlux': 192.5364685059,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4057617188
          },
          {
            'recordTime': 1535764200000,
            'plusTotalFlux': 330.9543457031,
            'flux': 0.0,
            'pressure': 0.1735,
            'totalFlux': 192.5485839844,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4057617188
          },
          {
            'recordTime': 1535764500000,
            'plusTotalFlux': 330.9778747559,
            'flux': 0.8612360358,
            'pressure': 0.1762,
            'totalFlux': 192.5721130371,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4057617188
          },
          {
            'recordTime': 1535764800000,
            'plusTotalFlux': 330.996673584,
            'flux': 0.3506084979,
            'pressure': 0.1646,
            'totalFlux': 192.5849761963,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4116973877
          },
          {
            'recordTime': 1535765100000,
            'plusTotalFlux': 331.0487670898,
            'flux': 0.0,
            'pressure': 0.1564,
            'totalFlux': 192.6370697021,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4116973877
          },
          {
            'recordTime': 1535765400000,
            'plusTotalFlux': 331.0487670898,
            'flux': 0.0,
            'pressure': 0.153,
            'totalFlux': 192.6370697021,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4116973877
          },
          {
            'recordTime': 1535765700000,
            'plusTotalFlux': 331.0535888672,
            'flux': 0.3940455914,
            'pressure': 0.1527,
            'totalFlux': 192.6418914795,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4116973877
          },
          {
            'recordTime': 1535766000000,
            'plusTotalFlux': 331.0654602051,
            'flux': 1.3525582552,
            'pressure': 0.2043,
            'totalFlux': 192.6537628174,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4116973877
          },
          {
            'recordTime': 1535766300000,
            'plusTotalFlux': 331.0967407227,
            'flux': 0.0,
            'pressure': 0.2061,
            'totalFlux': 192.6779937744,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4187469482
          },
          {
            'recordTime': 1535766600000,
            'plusTotalFlux': 331.1189880371,
            'flux': 0.0,
            'pressure': 0.204,
            'totalFlux': 192.7002410889,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4187469482
          },
          {
            'recordTime': 1535766900000,
            'plusTotalFlux': 331.127166748,
            'flux': 0.5938562751,
            'pressure': 0.1955,
            'totalFlux': 192.7004089355,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4267578125
          },
          {
            'recordTime': 1535767200000,
            'plusTotalFlux': 331.140838623,
            'flux': 0.0,
            'pressure': 0.1924,
            'totalFlux': 192.7104797363,
            'uploadTime': 1535774689343,
            'reverseTotalFlux': 138.4303588867
          },
          {
            'recordTime': 1535767500000,
            'plusTotalFlux': 331.1544799805,
            'flux': 0.4065940976,
            'pressure': 0.1958,
            'totalFlux': 192.7241210938,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4303588867
          },
          {
            'recordTime': 1535767800000,
            'plusTotalFlux': 331.1623840332,
            'flux': 0.0,
            'pressure': 0.1927,
            'totalFlux': 192.7320251465,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4303588867
          },
          {
            'recordTime': 1535768100000,
            'plusTotalFlux': 331.1637878418,
            'flux': 0.0,
            'pressure': 0.1921,
            'totalFlux': 192.7334289551,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4303588867
          },
          {
            'recordTime': 1535768400000,
            'plusTotalFlux': 331.1694030762,
            'flux': 0.0,
            'pressure': 0.1884,
            'totalFlux': 192.7318267822,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4375762939
          },
          {
            'recordTime': 1535768700000,
            'plusTotalFlux': 331.1880187988,
            'flux': -0.4660094976,
            'pressure': 0.1884,
            'totalFlux': 192.7453765869,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4426422119
          },
          {
            'recordTime': 1535769000000,
            'plusTotalFlux': 331.2009887695,
            'flux': 0.0,
            'pressure': 0.1918,
            'totalFlux': 192.7504577637,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4505310059
          },
          {
            'recordTime': 1535769300000,
            'plusTotalFlux': 331.2263183594,
            'flux': 0.5899953842,
            'pressure': 0.1933,
            'totalFlux': 192.7757873535,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4505310059
          },
          {
            'recordTime': 1535769600000,
            'plusTotalFlux': 331.2841796875,
            'flux': 0.3834276199,
            'pressure': 0.1955,
            'totalFlux': 192.8336486816,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4505310059
          },
          {
            'recordTime': 1535769900000,
            'plusTotalFlux': 331.2972106934,
            'flux': 0.0,
            'pressure': 0.193,
            'totalFlux': 192.8466796875,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4505310059
          },
          {
            'recordTime': 1535770200000,
            'plusTotalFlux': 331.3018188477,
            'flux': 0.0,
            'pressure': 0.1994,
            'totalFlux': 192.8512878418,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4505310059
          },
          {
            'recordTime': 1535770500000,
            'plusTotalFlux': 331.3363647461,
            'flux': 0.0,
            'pressure': 0.1949,
            'totalFlux': 192.8858337402,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4505310059
          },
          {
            'recordTime': 1535770800000,
            'plusTotalFlux': 331.3656005859,
            'flux': 0.4780240357,
            'pressure': 0.1949,
            'totalFlux': 192.9150695801,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4505310059
          },
          {
            'recordTime': 1535771100000,
            'plusTotalFlux': 331.3760070801,
            'flux': 0.8226252198,
            'pressure': 0.1933,
            'totalFlux': 192.9254760742,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4505310059
          },
          {
            'recordTime': 1535771400000,
            'plusTotalFlux': 331.4016113281,
            'flux': 0.8284167647,
            'pressure': 0.1942,
            'totalFlux': 192.9494781494,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4521331787
          },
          {
            'recordTime': 1535771700000,
            'plusTotalFlux': 331.4060974121,
            'flux': 0.0,
            'pressure': 0.2007,
            'totalFlux': 192.9480743408,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4580230713
          },
          {
            'recordTime': 1535772000000,
            'plusTotalFlux': 331.4060974121,
            'flux': 0.0,
            'pressure': 0.1973,
            'totalFlux': 192.9449310303,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4611663818
          },
          {
            'recordTime': 1535772300000,
            'plusTotalFlux': 331.4153137207,
            'flux': 0.0,
            'pressure': 0.2019,
            'totalFlux': 192.9424591064,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4728546143
          },
          {
            'recordTime': 1535772600000,
            'plusTotalFlux': 331.4186096191,
            'flux': 0.0,
            'pressure': 0.1994,
            'totalFlux': 192.9342346191,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.484375
          },
          {
            'recordTime': 1535772900000,
            'plusTotalFlux': 331.4186096191,
            'flux': 0.0,
            'pressure': 0.1979,
            'totalFlux': 192.9226226807,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.4959869385
          },
          {
            'recordTime': 1535773200000,
            'plusTotalFlux': 331.4186096191,
            'flux': -0.5210299492,
            'pressure': 0.1964,
            'totalFlux': 192.8974151611,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.521194458
          },
          {
            'recordTime': 1535773500000,
            'plusTotalFlux': 331.4233703613,
            'flux': 0.0,
            'pressure': 0.1985,
            'totalFlux': 192.8953094482,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.5280609131
          },
          {
            'recordTime': 1535773800000,
            'plusTotalFlux': 331.424407959,
            'flux': 0.4143162668,
            'pressure': 0.2016,
            'totalFlux': 192.8905487061,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.5338592529
          },
          {
            'recordTime': 1535774100000,
            'plusTotalFlux': 331.4330444336,
            'flux': 0.4085246027,
            'pressure': 0.2086,
            'totalFlux': 192.8991851807,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.5338592529
          },
          {
            'recordTime': 1535774400000,
            'plusTotalFlux': 331.453918457,
            'flux': -0.4920717776,
            'pressure': 0.2025,
            'totalFlux': 192.9145050049,
            'uploadTime': 1535774713173,
            'reverseTotalFlux': 138.5394134521
          },
          {
            'recordTime': 1535774700000,
            'plusTotalFlux': 331.468963623,
            'flux': 0.0,
            'pressure': 0.2083,
            'totalFlux': 192.9272766113,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.5416870117
          },
          {
            'recordTime': 1535775000000,
            'plusTotalFlux': 331.470916748,
            'flux': -0.4023016989,
            'pressure': 0.2074,
            'totalFlux': 192.928237915,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.542678833
          },
          {
            'recordTime': 1535775300000,
            'plusTotalFlux': 331.4946594238,
            'flux': 0.929770112,
            'pressure': 0.2086,
            'totalFlux': 192.9470825195,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.5475769043
          },
          {
            'recordTime': 1535775600000,
            'plusTotalFlux': 331.5343017578,
            'flux': 0.4896072745,
            'pressure': 0.2086,
            'totalFlux': 192.9867248535,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.5475769043
          },
          {
            'recordTime': 1535775900000,
            'plusTotalFlux': 331.5383605957,
            'flux': -0.3559688032,
            'pressure': 0.1979,
            'totalFlux': 192.9898986816,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.5484619141
          },
          {
            'recordTime': 1535776200000,
            'plusTotalFlux': 331.5704956055,
            'flux': 0.0,
            'pressure': 0.2092,
            'totalFlux': 193.0214385986,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.5490570068
          },
          {
            'recordTime': 1535776500000,
            'plusTotalFlux': 331.5716247559,
            'flux': 0.4461701512,
            'pressure': 0.2089,
            'totalFlux': 192.9970092773,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.5746154785
          },
          {
            'recordTime': 1535776800000,
            'plusTotalFlux': 331.6054077148,
            'flux': 0.4992599487,
            'pressure': 0.219,
            'totalFlux': 193.0307922363,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.5746154785
          },
          {
            'recordTime': 1535777100000,
            'plusTotalFlux': 331.6217956543,
            'flux': 0.0,
            'pressure': 0.2272,
            'totalFlux': 193.040222168,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.5815734863
          },
          {
            'recordTime': 1535777400000,
            'plusTotalFlux': 331.6295471191,
            'flux': 0.0,
            'pressure': 0.2312,
            'totalFlux': 193.0292358398,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6003112793
          },
          {
            'recordTime': 1535777700000,
            'plusTotalFlux': 331.6777038574,
            'flux': -0.5065509081,
            'pressure': 0.2351,
            'totalFlux': 193.0761260986,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6015777588
          },
          {
            'recordTime': 1535778000000,
            'plusTotalFlux': 331.7481994629,
            'flux': 0.7589172721,
            'pressure': 0.2507,
            'totalFlux': 193.1443481445,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6038513184
          },
          {
            'recordTime': 1535778300000,
            'plusTotalFlux': 331.783416748,
            'flux': 0.7393707633,
            'pressure': 0.2538,
            'totalFlux': 193.1795654297,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6038513184
          },
          {
            'recordTime': 1535778600000,
            'plusTotalFlux': 331.8243103027,
            'flux': 0.0,
            'pressure': 0.2586,
            'totalFlux': 193.2183990479,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6059112549
          },
          {
            'recordTime': 1535778900000,
            'plusTotalFlux': 331.8371582031,
            'flux': 0.3679833114,
            'pressure': 0.251,
            'totalFlux': 193.2312469482,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6059112549
          },
          {
            'recordTime': 1535779200000,
            'plusTotalFlux': 331.8722839355,
            'flux': 1.2048720121,
            'pressure': 0.2577,
            'totalFlux': 193.2663726807,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6059112549
          },
          {
            'recordTime': 1535779500000,
            'plusTotalFlux': 331.8822937012,
            'flux': 0.0,
            'pressure': 0.2605,
            'totalFlux': 193.2681427002,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.614151001
          },
          {
            'recordTime': 1535779800000,
            'plusTotalFlux': 331.9106750488,
            'flux': -1.1146707535,
            'pressure': 0.2107,
            'totalFlux': 193.2937316895,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6169433594
          },
          {
            'recordTime': 1535780100000,
            'plusTotalFlux': 331.9176330566,
            'flux': 0.0,
            'pressure': 0.2095,
            'totalFlux': 193.2900848389,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6275482178
          },
          {
            'recordTime': 1535780400000,
            'plusTotalFlux': 331.937286377,
            'flux': 0.8737844825,
            'pressure': 0.2101,
            'totalFlux': 193.3097381592,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6275482178
          },
          {
            'recordTime': 1535780700000,
            'plusTotalFlux': 331.9880981445,
            'flux': 0.0,
            'pressure': 0.2138,
            'totalFlux': 193.3605499268,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6275482178
          },
          {
            'recordTime': 1535781000000,
            'plusTotalFlux': 332.0043945312,
            'flux': 1.3902038336,
            'pressure': 0.2132,
            'totalFlux': 193.3768463135,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6275482178
          },
          {
            'recordTime': 1535781300000,
            'plusTotalFlux': 332.0123596191,
            'flux': 0.0,
            'pressure': 0.2162,
            'totalFlux': 193.3723602295,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6399993896
          },
          {
            'recordTime': 1535781600000,
            'plusTotalFlux': 332.0188903809,
            'flux': 0.7936671972,
            'pressure': 0.2147,
            'totalFlux': 193.3481903076,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6707000732
          },
          {
            'recordTime': 1535781900000,
            'plusTotalFlux': 332.060333252,
            'flux': 0.3313030303,
            'pressure': 0.2156,
            'totalFlux': 193.3896331787,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6707000732
          },
          {
            'recordTime': 1535782200000,
            'plusTotalFlux': 332.0657653809,
            'flux': 0.0,
            'pressure': 0.2205,
            'totalFlux': 193.3894042969,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.676361084
          },
          {
            'recordTime': 1535782500000,
            'plusTotalFlux': 332.0842285156,
            'flux': 0.0,
            'pressure': 0.2153,
            'totalFlux': 193.4078674316,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.676361084
          },
          {
            'recordTime': 1535782800000,
            'plusTotalFlux': 332.0888061523,
            'flux': 0.5021557808,
            'pressure': 0.2074,
            'totalFlux': 193.4049224854,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.683883667
          },
          {
            'recordTime': 1535783100000,
            'plusTotalFlux': 332.1081848145,
            'flux': -0.3328023255,
            'pressure': 0.2116,
            'totalFlux': 193.4200286865,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6881561279
          },
          {
            'recordTime': 1535783400000,
            'plusTotalFlux': 332.1081848145,
            'flux': 0.0,
            'pressure': 0.2129,
            'totalFlux': 193.4194641113,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6887207031
          },
          {
            'recordTime': 1535783700000,
            'plusTotalFlux': 332.1081848145,
            'flux': 0.0,
            'pressure': 0.2126,
            'totalFlux': 193.4194641113,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.6887207031
          },
          {
            'recordTime': 1535784000000,
            'plusTotalFlux': 332.1081848145,
            'flux': 0.0,
            'pressure': 0.2153,
            'totalFlux': 193.4081726074,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.700012207
          },
          {
            'recordTime': 1535784300000,
            'plusTotalFlux': 332.1161499023,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 193.4161376953,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.700012207
          },
          {
            'recordTime': 1535784600000,
            'plusTotalFlux': 332.1337585449,
            'flux': 0.0,
            'pressure': 0.2071,
            'totalFlux': 193.4337463379,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.700012207
          },
          {
            'recordTime': 1535784900000,
            'plusTotalFlux': 332.1459350586,
            'flux': 0.6199185848,
            'pressure': 0.2031,
            'totalFlux': 193.4295806885,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.7163543701
          },
          {
            'recordTime': 1535785200000,
            'plusTotalFlux': 332.1756286621,
            'flux': 0.0,
            'pressure': 0.204,
            'totalFlux': 193.459274292,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.7163543701
          },
          {
            'recordTime': 1535785500000,
            'plusTotalFlux': 332.1756286621,
            'flux': -0.4555846155,
            'pressure': 0.2113,
            'totalFlux': 193.4276885986,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.7479400635
          },
          {
            'recordTime': 1535785800000,
            'plusTotalFlux': 332.1824035645,
            'flux': 0.4316911101,
            'pressure': 0.2144,
            'totalFlux': 193.4119262695,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.7704772949
          },
          {
            'recordTime': 1535786100000,
            'plusTotalFlux': 332.190032959,
            'flux': 0.34250018,
            'pressure': 0.2113,
            'totalFlux': 193.3986206055,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.7914123535
          },
          {
            'recordTime': 1535786400000,
            'plusTotalFlux': 332.2319335938,
            'flux': 0.0,
            'pressure': 0.2119,
            'totalFlux': 193.434677124,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.7972564697
          },
          {
            'recordTime': 1535786700000,
            'plusTotalFlux': 332.2411499023,
            'flux': 0.0,
            'pressure': 0.2028,
            'totalFlux': 193.4388122559,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.8023376465
          },
          {
            'recordTime': 1535787000000,
            'plusTotalFlux': 332.2411499023,
            'flux': 0.0,
            'pressure': 0.2049,
            'totalFlux': 193.4293060303,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.8118438721
          },
          {
            'recordTime': 1535787300000,
            'plusTotalFlux': 332.2522888184,
            'flux': 0.7820839286,
            'pressure': 0.2043,
            'totalFlux': 193.4237823486,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.8285064697
          },
          {
            'recordTime': 1535787600000,
            'plusTotalFlux': 332.2760314941,
            'flux': 0.0,
            'pressure': 0.2071,
            'totalFlux': 193.4475250244,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.8285064697
          },
          {
            'recordTime': 1535787900000,
            'plusTotalFlux': 332.2760314941,
            'flux': -0.581841886,
            'pressure': 0.1921,
            'totalFlux': 193.4462432861,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.829788208
          },
          {
            'recordTime': 1535788200000,
            'plusTotalFlux': 332.2774353027,
            'flux': 0.0,
            'pressure': 0.1921,
            'totalFlux': 193.4428710938,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.834564209
          },
          {
            'recordTime': 1535788500000,
            'plusTotalFlux': 332.2973937988,
            'flux': 0.0,
            'pressure': 0.1866,
            'totalFlux': 193.4595794678,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.8378143311
          },
          {
            'recordTime': 1535788800000,
            'plusTotalFlux': 332.3096923828,
            'flux': 0.7212718725,
            'pressure': 0.1839,
            'totalFlux': 193.4636535645,
            'uploadTime': 1535796215173,
            'reverseTotalFlux': 138.8460388184
          },
          {
            'recordTime': 1535789100000,
            'plusTotalFlux': 332.3161010742,
            'flux': 0.0,
            'pressure': 0.1787,
            'totalFlux': 193.4700622559,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8460388184
          },
          {
            'recordTime': 1535789400000,
            'plusTotalFlux': 332.3251953125,
            'flux': -0.581841886,
            'pressure': 0.1643,
            'totalFlux': 193.4751434326,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8500518799
          },
          {
            'recordTime': 1535789700000,
            'plusTotalFlux': 332.3406066895,
            'flux': 0.0,
            'pressure': 0.1613,
            'totalFlux': 193.4824676514,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8581390381
          },
          {
            'recordTime': 1535790000000,
            'plusTotalFlux': 332.345703125,
            'flux': 0.0,
            'pressure': 0.1561,
            'totalFlux': 193.4875640869,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8581390381
          },
          {
            'recordTime': 1535790300000,
            'plusTotalFlux': 332.360168457,
            'flux': 0.0,
            'pressure': 0.1472,
            'totalFlux': 193.5001678467,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8600006104
          },
          {
            'recordTime': 1535790600000,
            'plusTotalFlux': 332.3670349121,
            'flux': 0.0,
            'pressure': 0.1399,
            'totalFlux': 193.501739502,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8652954102
          },
          {
            'recordTime': 1535790900000,
            'plusTotalFlux': 332.3698425293,
            'flux': 0.0,
            'pressure': 0.1176,
            'totalFlux': 193.5045471191,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8652954102
          },
          {
            'recordTime': 1535791200000,
            'plusTotalFlux': 332.3736572266,
            'flux': 0.0,
            'pressure': 0.117,
            'totalFlux': 193.5083618164,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8652954102
          },
          {
            'recordTime': 1535791500000,
            'plusTotalFlux': 332.3752441406,
            'flux': 0.7096886635,
            'pressure': 0.1601,
            'totalFlux': 193.5099487305,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8652954102
          },
          {
            'recordTime': 1535791800000,
            'plusTotalFlux': 332.4107971191,
            'flux': 0.0,
            'pressure': 0.1726,
            'totalFlux': 193.545501709,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8652954102
          },
          {
            'recordTime': 1535792100000,
            'plusTotalFlux': 332.4153747559,
            'flux': 0.0,
            'pressure': 0.1765,
            'totalFlux': 193.5500793457,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8652954102
          },
          {
            'recordTime': 1535792400000,
            'plusTotalFlux': 332.4353027344,
            'flux': 0.0,
            'pressure': 0.1775,
            'totalFlux': 193.5700073242,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8652954102
          },
          {
            'recordTime': 1535792700000,
            'plusTotalFlux': 332.4544677734,
            'flux': 0.0,
            'pressure': 0.1796,
            'totalFlux': 193.5891723633,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8652954102
          },
          {
            'recordTime': 1535793000000,
            'plusTotalFlux': 332.4940185547,
            'flux': 0.5581414104,
            'pressure': 0.1869,
            'totalFlux': 193.6287231445,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8652954102
          },
          {
            'recordTime': 1535793300000,
            'plusTotalFlux': 332.5044555664,
            'flux': 0.0,
            'pressure': 0.1842,
            'totalFlux': 193.6391601562,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8652954102
          },
          {
            'recordTime': 1535793600000,
            'plusTotalFlux': 332.5044555664,
            'flux': 0.0,
            'pressure': 0.1833,
            'totalFlux': 193.6357574463,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8686981201
          },
          {
            'recordTime': 1535793900000,
            'plusTotalFlux': 332.5044555664,
            'flux': 0.0,
            'pressure': 0.1866,
            'totalFlux': 193.6235961914,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.880859375
          },
          {
            'recordTime': 1535794200000,
            'plusTotalFlux': 332.5044555664,
            'flux': 0.0,
            'pressure': 0.168,
            'totalFlux': 193.6235961914,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.880859375
          },
          {
            'recordTime': 1535794500000,
            'plusTotalFlux': 332.5143432617,
            'flux': 0.0,
            'pressure': 0.1686,
            'totalFlux': 193.6334838867,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.880859375
          },
          {
            'recordTime': 1535794800000,
            'plusTotalFlux': 332.5240783691,
            'flux': 0.5533150434,
            'pressure': 0.1729,
            'totalFlux': 193.6432189941,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.880859375
          },
          {
            'recordTime': 1535795100000,
            'plusTotalFlux': 332.5350952148,
            'flux': 0.4230036736,
            'pressure': 0.1753,
            'totalFlux': 193.6528015137,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8822937012
          },
          {
            'recordTime': 1535795400000,
            'plusTotalFlux': 332.5422058105,
            'flux': 0.0,
            'pressure': 0.1698,
            'totalFlux': 193.6599121094,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8822937012
          },
          {
            'recordTime': 1535795700000,
            'plusTotalFlux': 332.550994873,
            'flux': 0.0,
            'pressure': 0.1668,
            'totalFlux': 193.6687011719,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8822937012
          },
          {
            'recordTime': 1535796000000,
            'plusTotalFlux': 332.5684814453,
            'flux': 0.0,
            'pressure': 0.172,
            'totalFlux': 193.6861877441,
            'uploadTime': 1535796219963,
            'reverseTotalFlux': 138.8822937012
          },
          {
            'recordTime': 1535796300000,
            'plusTotalFlux': 332.5702819824,
            'flux': 0.0,
            'pressure': 0.1701,
            'totalFlux': 193.6879882812,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.8822937012
          },
          {
            'recordTime': 1535796600000,
            'plusTotalFlux': 332.5784606934,
            'flux': 0.0,
            'pressure': 0.1704,
            'totalFlux': 193.6885681152,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.8898925781
          },
          {
            'recordTime': 1535796900000,
            'plusTotalFlux': 332.6258239746,
            'flux': 0.6614252329,
            'pressure': 0.1713,
            'totalFlux': 193.7359313965,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.8898925781
          },
          {
            'recordTime': 1535797200000,
            'plusTotalFlux': 332.6271057129,
            'flux': 0.0,
            'pressure': 0.1723,
            'totalFlux': 193.7269744873,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9001312256
          },
          {
            'recordTime': 1535797500000,
            'plusTotalFlux': 332.6271057129,
            'flux': 0.0,
            'pressure': 0.1762,
            'totalFlux': 193.7269744873,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9001312256
          },
          {
            'recordTime': 1535797800000,
            'plusTotalFlux': 332.6271057129,
            'flux': 0.0,
            'pressure': 0.1793,
            'totalFlux': 193.7214202881,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9056854248
          },
          {
            'recordTime': 1535798100000,
            'plusTotalFlux': 332.6271057129,
            'flux': 0.0,
            'pressure': 0.1836,
            'totalFlux': 193.7214202881,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9056854248
          },
          {
            'recordTime': 1535798400000,
            'plusTotalFlux': 332.6636657715,
            'flux': 0.6170228124,
            'pressure': 0.1857,
            'totalFlux': 193.7579803467,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9056854248
          },
          {
            'recordTime': 1535798700000,
            'plusTotalFlux': 332.6909484863,
            'flux': 0.5591065884,
            'pressure': 0.1921,
            'totalFlux': 193.7852630615,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9056854248
          },
          {
            'recordTime': 1535799000000,
            'plusTotalFlux': 332.7141418457,
            'flux': 0.0,
            'pressure': 0.1982,
            'totalFlux': 193.8084564209,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9056854248
          },
          {
            'recordTime': 1535799300000,
            'plusTotalFlux': 332.7246398926,
            'flux': -0.5622951984,
            'pressure': 0.1985,
            'totalFlux': 193.808303833,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9163360596
          },
          {
            'recordTime': 1535799600000,
            'plusTotalFlux': 332.7449951172,
            'flux': 0.0,
            'pressure': 0.2074,
            'totalFlux': 193.8190765381,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9259185791
          },
          {
            'recordTime': 1535799900000,
            'plusTotalFlux': 332.7619934082,
            'flux': 0.0,
            'pressure': 0.211,
            'totalFlux': 193.8331604004,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9288330078
          },
          {
            'recordTime': 1535800200000,
            'plusTotalFlux': 332.7821655273,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 193.8533325195,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9288330078
          },
          {
            'recordTime': 1535800500000,
            'plusTotalFlux': 332.7821655273,
            'flux': 0.0,
            'pressure': 0.2214,
            'totalFlux': 193.8533325195,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9288330078
          },
          {
            'recordTime': 1535800800000,
            'plusTotalFlux': 332.8006591797,
            'flux': 0.6083353162,
            'pressure': 0.2214,
            'totalFlux': 193.8684539795,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9322052002
          },
          {
            'recordTime': 1535801100000,
            'plusTotalFlux': 332.8074645996,
            'flux': 0.0,
            'pressure': 0.2193,
            'totalFlux': 193.8596954346,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.947769165
          },
          {
            'recordTime': 1535801400000,
            'plusTotalFlux': 332.8074645996,
            'flux': 0.0,
            'pressure': 0.2187,
            'totalFlux': 193.8596954346,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.947769165
          },
          {
            'recordTime': 1535801700000,
            'plusTotalFlux': 332.8137817383,
            'flux': 0.0,
            'pressure': 0.23,
            'totalFlux': 193.8660125732,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.947769165
          },
          {
            'recordTime': 1535802000000,
            'plusTotalFlux': 332.8137817383,
            'flux': 0.0,
            'pressure': 0.2358,
            'totalFlux': 193.8533782959,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9604034424
          },
          {
            'recordTime': 1535802300000,
            'plusTotalFlux': 332.8137817383,
            'flux': 0.0,
            'pressure': 0.237,
            'totalFlux': 193.8533782959,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9604034424
          },
          {
            'recordTime': 1535802600000,
            'plusTotalFlux': 332.8312988281,
            'flux': 0.0,
            'pressure': 0.2376,
            'totalFlux': 193.8571472168,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9741516113
          },
          {
            'recordTime': 1535802900000,
            'plusTotalFlux': 332.8459777832,
            'flux': -0.3762394786,
            'pressure': 0.2437,
            'totalFlux': 193.8709869385,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9749908447
          },
          {
            'recordTime': 1535803200000,
            'plusTotalFlux': 332.8649902344,
            'flux': 0.0,
            'pressure': 0.2443,
            'totalFlux': 193.883026123,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9819641113
          },
          {
            'recordTime': 1535803500000,
            'plusTotalFlux': 332.8878479004,
            'flux': 0.0,
            'pressure': 0.2452,
            'totalFlux': 193.9058837891,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9819641113
          },
          {
            'recordTime': 1535803800000,
            'plusTotalFlux': 332.8878479004,
            'flux': -0.385892123,
            'pressure': 0.2434,
            'totalFlux': 193.8985748291,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.9892730713
          },
          {
            'recordTime': 1535804100000,
            'plusTotalFlux': 332.8938903809,
            'flux': 0.0,
            'pressure': 0.2422,
            'totalFlux': 193.9038696289,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.990020752
          },
          {
            'recordTime': 1535804400000,
            'plusTotalFlux': 332.8953552246,
            'flux': 0.0,
            'pressure': 0.2458,
            'totalFlux': 193.9053344727,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 138.990020752
          },
          {
            'recordTime': 1535804700000,
            'plusTotalFlux': 332.8953552246,
            'flux': 0.0,
            'pressure': 0.2608,
            'totalFlux': 193.8944091797,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0009460449
          },
          {
            'recordTime': 1535805000000,
            'plusTotalFlux': 332.8953552246,
            'flux': 0.0,
            'pressure': 0.2593,
            'totalFlux': 193.8821411133,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0132141113
          },
          {
            'recordTime': 1535805300000,
            'plusTotalFlux': 332.8994750977,
            'flux': 0.5127737522,
            'pressure': 0.2574,
            'totalFlux': 193.8785247803,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0209503174
          },
          {
            'recordTime': 1535805600000,
            'plusTotalFlux': 332.9041442871,
            'flux': 0.0,
            'pressure': 0.2632,
            'totalFlux': 193.8740692139,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0300750732
          },
          {
            'recordTime': 1535805900000,
            'plusTotalFlux': 332.908996582,
            'flux': 0.0,
            'pressure': 0.2626,
            'totalFlux': 193.8789215088,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0300750732
          },
          {
            'recordTime': 1535806200000,
            'plusTotalFlux': 332.908996582,
            'flux': 0.0,
            'pressure': 0.2699,
            'totalFlux': 193.8789215088,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0300750732
          },
          {
            'recordTime': 1535806500000,
            'plusTotalFlux': 332.928314209,
            'flux': -0.4023017287,
            'pressure': 0.2715,
            'totalFlux': 193.8938598633,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0344543457
          },
          {
            'recordTime': 1535806800000,
            'plusTotalFlux': 332.9560241699,
            'flux': 0.0,
            'pressure': 0.2815,
            'totalFlux': 193.9207763672,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0352478027
          },
          {
            'recordTime': 1535807100000,
            'plusTotalFlux': 332.9611816406,
            'flux': -0.6165916324,
            'pressure': 0.2095,
            'totalFlux': 193.9127349854,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0484466553
          },
          {
            'recordTime': 1535807400000,
            'plusTotalFlux': 332.9678344727,
            'flux': 0.0,
            'pressure': 0.2086,
            'totalFlux': 193.9127044678,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0551300049
          },
          {
            'recordTime': 1535807700000,
            'plusTotalFlux': 333.0002746582,
            'flux': 0.6257102489,
            'pressure': 0.215,
            'totalFlux': 193.9422149658,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0580596924
          },
          {
            'recordTime': 1535808000000,
            'plusTotalFlux': 333.0069274902,
            'flux': 0.0,
            'pressure': 0.2156,
            'totalFlux': 193.9391174316,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0678100586
          },
          {
            'recordTime': 1535808300000,
            'plusTotalFlux': 333.0264282227,
            'flux': 0.0,
            'pressure': 0.2113,
            'totalFlux': 193.9586181641,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0678100586
          },
          {
            'recordTime': 1535808600000,
            'plusTotalFlux': 333.0705871582,
            'flux': 0.8872982264,
            'pressure': 0.2147,
            'totalFlux': 194.0027770996,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0678100586
          },
          {
            'recordTime': 1535808900000,
            'plusTotalFlux': 333.07421875,
            'flux': -0.3472813964,
            'pressure': 0.2141,
            'totalFlux': 193.9967041016,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0775146484
          },
          {
            'recordTime': 1535809200000,
            'plusTotalFlux': 333.0797729492,
            'flux': 0.0,
            'pressure': 0.208,
            'totalFlux': 193.9969024658,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.0828704834
          },
          {
            'recordTime': 1535809500000,
            'plusTotalFlux': 333.0827636719,
            'flux': -0.5577101707,
            'pressure': 0.2031,
            'totalFlux': 193.9747924805,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.1079711914
          },
          {
            'recordTime': 1535809800000,
            'plusTotalFlux': 333.0827636719,
            'flux': 0.0,
            'pressure': 0.2101,
            'totalFlux': 193.9681854248,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.1145782471
          },
          {
            'recordTime': 1535810100000,
            'plusTotalFlux': 333.088684082,
            'flux': 0.0,
            'pressure': 0.2086,
            'totalFlux': 193.9564666748,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.1322174072
          },
          {
            'recordTime': 1535810400000,
            'plusTotalFlux': 333.0949707031,
            'flux': 0.0,
            'pressure': 0.2043,
            'totalFlux': 193.9612731934,
            'uploadTime': 1535817766993,
            'reverseTotalFlux': 139.1336975098
          },
          {
            'recordTime': 1535810700000,
            'plusTotalFlux': 333.0979919434,
            'flux': -0.5615712404,
            'pressure': 0.2065,
            'totalFlux': 193.9525909424,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.145401001
          },
          {
            'recordTime': 1535811000000,
            'plusTotalFlux': 333.0979919434,
            'flux': 0.0,
            'pressure': 0.2058,
            'totalFlux': 193.9514923096,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.1464996338
          },
          {
            'recordTime': 1535811300000,
            'plusTotalFlux': 333.1120605469,
            'flux': 0.0,
            'pressure': 0.2065,
            'totalFlux': 193.9655609131,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.1464996338
          },
          {
            'recordTime': 1535811600000,
            'plusTotalFlux': 333.1120605469,
            'flux': -0.6223832965,
            'pressure': 0.2025,
            'totalFlux': 193.9538574219,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.158203125
          },
          {
            'recordTime': 1535811900000,
            'plusTotalFlux': 333.1120605469,
            'flux': -0.3661040962,
            'pressure': 0.208,
            'totalFlux': 193.932510376,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.1795501709
          },
          {
            'recordTime': 1535812200000,
            'plusTotalFlux': 333.1154174805,
            'flux': 0.0,
            'pressure': 0.2046,
            'totalFlux': 193.9351501465,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.180267334
          },
          {
            'recordTime': 1535812500000,
            'plusTotalFlux': 333.1154174805,
            'flux': -0.5451616645,
            'pressure': 0.201,
            'totalFlux': 193.9180908203,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.1973266602
          },
          {
            'recordTime': 1535812800000,
            'plusTotalFlux': 333.1154174805,
            'flux': 0.0,
            'pressure': 0.2007,
            'totalFlux': 193.8885498047,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2268676758
          },
          {
            'recordTime': 1535813100000,
            'plusTotalFlux': 333.1154174805,
            'flux': 0.0,
            'pressure': 0.208,
            'totalFlux': 193.8710174561,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2444000244
          },
          {
            'recordTime': 1535813400000,
            'plusTotalFlux': 333.1366577148,
            'flux': 0.5967521667,
            'pressure': 0.208,
            'totalFlux': 193.8874053955,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2492523193
          },
          {
            'recordTime': 1535813700000,
            'plusTotalFlux': 333.1689453125,
            'flux': 0.5726204515,
            'pressure': 0.2068,
            'totalFlux': 193.9111480713,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2577972412
          },
          {
            'recordTime': 1535814000000,
            'plusTotalFlux': 333.1737670898,
            'flux': 0.0,
            'pressure': 0.2071,
            'totalFlux': 193.9159698486,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2577972412
          },
          {
            'recordTime': 1535814300000,
            'plusTotalFlux': 333.1737670898,
            'flux': 0.0,
            'pressure': 0.2007,
            'totalFlux': 193.9159698486,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2577972412
          },
          {
            'recordTime': 1535814600000,
            'plusTotalFlux': 333.1737670898,
            'flux': 0.0,
            'pressure': 0.2037,
            'totalFlux': 193.9145355225,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2592315674
          },
          {
            'recordTime': 1535814900000,
            'plusTotalFlux': 333.1919250488,
            'flux': 0.3660528064,
            'pressure': 0.204,
            'totalFlux': 193.9326934814,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2592315674
          },
          {
            'recordTime': 1535815200000,
            'plusTotalFlux': 333.222442627,
            'flux': 0.0,
            'pressure': 0.2043,
            'totalFlux': 193.9632110596,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2592315674
          },
          {
            'recordTime': 1535815500000,
            'plusTotalFlux': 333.2266540527,
            'flux': 0.3679833114,
            'pressure': 0.2083,
            'totalFlux': 193.9674224854,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2592315674
          },
          {
            'recordTime': 1535815800000,
            'plusTotalFlux': 333.2304382324,
            'flux': 0.0,
            'pressure': 0.2135,
            'totalFlux': 193.971206665,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2592315674
          },
          {
            'recordTime': 1535816100000,
            'plusTotalFlux': 333.2333984375,
            'flux': -0.4671678245,
            'pressure': 0.2119,
            'totalFlux': 193.9478149414,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2855834961
          },
          {
            'recordTime': 1535816400000,
            'plusTotalFlux': 333.2333984375,
            'flux': 0.0,
            'pressure': 0.2153,
            'totalFlux': 193.9387664795,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.294631958
          },
          {
            'recordTime': 1535816700000,
            'plusTotalFlux': 333.2333984375,
            'flux': 0.0,
            'pressure': 0.2165,
            'totalFlux': 193.9387664795,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.294631958
          },
          {
            'recordTime': 1535817000000,
            'plusTotalFlux': 333.2333984375,
            'flux': 0.0,
            'pressure': 0.2199,
            'totalFlux': 193.9369659424,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2964324951
          },
          {
            'recordTime': 1535817300000,
            'plusTotalFlux': 333.2333984375,
            'flux': 0.0,
            'pressure': 0.219,
            'totalFlux': 193.9369659424,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.2964324951
          },
          {
            'recordTime': 1535817600000,
            'plusTotalFlux': 333.2427062988,
            'flux': 0.0,
            'pressure': 0.2229,
            'totalFlux': 193.9338684082,
            'uploadTime': 1535817774030,
            'reverseTotalFlux': 139.3088378906
          },
          {
            'recordTime': 1535817900000,
            'plusTotalFlux': 333.2526855469,
            'flux': 0.0,
            'pressure': 0.2242,
            'totalFlux': 193.9438476562,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3088378906
          },
          {
            'recordTime': 1535818200000,
            'plusTotalFlux': 333.2526855469,
            'flux': 0.0,
            'pressure': 0.2287,
            'totalFlux': 193.9438476562,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3088378906
          },
          {
            'recordTime': 1535818500000,
            'plusTotalFlux': 333.2554321289,
            'flux': 0.3737749457,
            'pressure': 0.2242,
            'totalFlux': 193.9432983398,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3121337891
          },
          {
            'recordTime': 1535818800000,
            'plusTotalFlux': 333.2677307129,
            'flux': 0.0,
            'pressure': 0.2358,
            'totalFlux': 193.9555969238,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3121337891
          },
          {
            'recordTime': 1535819100000,
            'plusTotalFlux': 333.2709655762,
            'flux': 0.5706899166,
            'pressure': 0.2364,
            'totalFlux': 193.9531860352,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.317779541
          },
          {
            'recordTime': 1535819400000,
            'plusTotalFlux': 333.2836608887,
            'flux': 0.0,
            'pressure': 0.2425,
            'totalFlux': 193.9658813477,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.317779541
          },
          {
            'recordTime': 1535819700000,
            'plusTotalFlux': 333.2945251465,
            'flux': 0.0,
            'pressure': 0.2437,
            'totalFlux': 193.9747314453,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3197937012
          },
          {
            'recordTime': 1535820000000,
            'plusTotalFlux': 333.3015441895,
            'flux': 0.3708791435,
            'pressure': 0.2498,
            'totalFlux': 193.9718017578,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3297424316
          },
          {
            'recordTime': 1535820300000,
            'plusTotalFlux': 333.3034667969,
            'flux': 0.0,
            'pressure': 0.2556,
            'totalFlux': 193.9737243652,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3297424316
          },
          {
            'recordTime': 1535820600000,
            'plusTotalFlux': 333.3081665039,
            'flux': 0.0,
            'pressure': 0.2556,
            'totalFlux': 193.9784240723,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3297424316
          },
          {
            'recordTime': 1535820900000,
            'plusTotalFlux': 333.3081665039,
            'flux': 0.0,
            'pressure': 0.2626,
            'totalFlux': 193.9784240723,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3297424316
          },
          {
            'recordTime': 1535821200000,
            'plusTotalFlux': 333.3115234375,
            'flux': 0.0,
            'pressure': 0.2773,
            'totalFlux': 193.9817810059,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3297424316
          },
          {
            'recordTime': 1535821500000,
            'plusTotalFlux': 333.3115234375,
            'flux': 0.0,
            'pressure': 0.2831,
            'totalFlux': 193.9817810059,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3297424316
          },
          {
            'recordTime': 1535821800000,
            'plusTotalFlux': 333.3115234375,
            'flux': 0.0,
            'pressure': 0.2174,
            'totalFlux': 193.9601593018,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3513641357
          },
          {
            'recordTime': 1535822100000,
            'plusTotalFlux': 333.3580322266,
            'flux': 0.9818947315,
            'pressure': 0.2147,
            'totalFlux': 194.0066680908,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3513641357
          },
          {
            'recordTime': 1535822400000,
            'plusTotalFlux': 333.3822021484,
            'flux': 0.0,
            'pressure': 0.2184,
            'totalFlux': 194.0308380127,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3513641357
          },
          {
            'recordTime': 1535822700000,
            'plusTotalFlux': 333.3822021484,
            'flux': 0.0,
            'pressure': 0.2156,
            'totalFlux': 194.0308380127,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3513641357
          },
          {
            'recordTime': 1535823000000,
            'plusTotalFlux': 333.3822021484,
            'flux': 0.0,
            'pressure': 0.2187,
            'totalFlux': 194.0308380127,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3513641357
          },
          {
            'recordTime': 1535823300000,
            'plusTotalFlux': 333.3822021484,
            'flux': 0.0,
            'pressure': 0.2202,
            'totalFlux': 194.0308380127,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3513641357
          },
          {
            'recordTime': 1535823600000,
            'plusTotalFlux': 333.3822021484,
            'flux': 0.0,
            'pressure': 0.2202,
            'totalFlux': 194.0293426514,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3528594971
          },
          {
            'recordTime': 1535823900000,
            'plusTotalFlux': 333.3822021484,
            'flux': 0.0,
            'pressure': 0.2181,
            'totalFlux': 194.0293426514,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3528594971
          },
          {
            'recordTime': 1535824200000,
            'plusTotalFlux': 333.384765625,
            'flux': 0.3679833412,
            'pressure': 0.219,
            'totalFlux': 194.0319061279,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3528594971
          },
          {
            'recordTime': 1535824500000,
            'plusTotalFlux': 333.3883056641,
            'flux': 0.0,
            'pressure': 0.2226,
            'totalFlux': 194.035446167,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3528594971
          },
          {
            'recordTime': 1535824800000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2245,
            'totalFlux': 194.0402069092,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3528594971
          },
          {
            'recordTime': 1535825100000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2327,
            'totalFlux': 194.0402069092,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3528594971
          },
          {
            'recordTime': 1535825400000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2348,
            'totalFlux': 194.0402069092,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3528594971
          },
          {
            'recordTime': 1535825700000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2315,
            'totalFlux': 194.0402069092,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3528594971
          },
          {
            'recordTime': 1535826000000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2351,
            'totalFlux': 194.0402069092,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3528594971
          },
          {
            'recordTime': 1535826300000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.237,
            'totalFlux': 194.0368347168,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3562316895
          },
          {
            'recordTime': 1535826600000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2367,
            'totalFlux': 194.0368347168,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3562316895
          },
          {
            'recordTime': 1535826900000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2345,
            'totalFlux': 194.0368347168,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3562316895
          },
          {
            'recordTime': 1535827200000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2406,
            'totalFlux': 194.0368347168,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3562316895
          },
          {
            'recordTime': 1535827500000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2409,
            'totalFlux': 194.0368347168,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3562316895
          },
          {
            'recordTime': 1535827800000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2437,
            'totalFlux': 194.0368347168,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3562316895
          },
          {
            'recordTime': 1535828100000,
            'plusTotalFlux': 333.3930664062,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 194.0368347168,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3562316895
          },
          {
            'recordTime': 1535828400000,
            'plusTotalFlux': 333.3946838379,
            'flux': 0.0,
            'pressure': 0.2458,
            'totalFlux': 194.0384521484,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3562316895
          },
          {
            'recordTime': 1535828700000,
            'plusTotalFlux': 333.3946838379,
            'flux': 0.0,
            'pressure': 0.2419,
            'totalFlux': 194.0384521484,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3562316895
          },
          {
            'recordTime': 1535829000000,
            'plusTotalFlux': 333.3946838379,
            'flux': 0.0,
            'pressure': 0.2443,
            'totalFlux': 194.0384521484,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3562316895
          },
          {
            'recordTime': 1535829300000,
            'plusTotalFlux': 333.3946838379,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 194.036895752,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535829600000,
            'plusTotalFlux': 333.3946838379,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 194.036895752,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535829900000,
            'plusTotalFlux': 333.3946838379,
            'flux': 0.0,
            'pressure': 0.2416,
            'totalFlux': 194.036895752,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535830200000,
            'plusTotalFlux': 333.3946838379,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 194.036895752,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535830500000,
            'plusTotalFlux': 333.3946838379,
            'flux': 0.0,
            'pressure': 0.2458,
            'totalFlux': 194.036895752,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535830800000,
            'plusTotalFlux': 333.3946838379,
            'flux': 0.0,
            'pressure': 0.2492,
            'totalFlux': 194.036895752,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535831100000,
            'plusTotalFlux': 333.3946838379,
            'flux': 0.0,
            'pressure': 0.2434,
            'totalFlux': 194.036895752,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535831400000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2455,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535831700000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2409,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535832000000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2474,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839300537,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535832300000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2467,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535832600000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2452,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535832900000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.248,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535833200000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2464,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535833500000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2471,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535833800000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2489,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535834100000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2455,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535834400000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2504,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535834700000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535835000000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2513,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535835300000,
            'plusTotalFlux': 333.3978271484,
            'flux': 0.0,
            'pressure': 0.2507,
            'totalFlux': 194.0400390625,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535835600000,
            'plusTotalFlux': 333.4078369141,
            'flux': 0.0,
            'pressure': 0.2492,
            'totalFlux': 194.0500488281,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535835900000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2504,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535836200000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2513,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535836500000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2501,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535836800000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2528,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535837100000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2519,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535837400000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2504,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535837700000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2498,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535838000000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535838300000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2443,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535838600000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2467,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535838900000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2455,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535839200000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535839371867,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535839500000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2474,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535839800000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535840100000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.248,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535840400000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2458,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535840700000,
            'plusTotalFlux': 333.4093322754,
            'flux': 0.0,
            'pressure': 0.2455,
            'totalFlux': 194.0515441895,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535841000000,
            'plusTotalFlux': 333.4180603027,
            'flux': 0.0,
            'pressure': 0.2474,
            'totalFlux': 194.0602722168,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535841300000,
            'plusTotalFlux': 333.4180603027,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 194.0602722168,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535841600000,
            'plusTotalFlux': 333.4180603027,
            'flux': 0.0,
            'pressure': 0.2428,
            'totalFlux': 194.0602722168,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535841900000,
            'plusTotalFlux': 333.4516906738,
            'flux': 0.5890299678,
            'pressure': 0.2419,
            'totalFlux': 194.0939025879,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3577880859
          },
          {
            'recordTime': 1535842200000,
            'plusTotalFlux': 333.4928588867,
            'flux': 0.0,
            'pressure': 0.2382,
            'totalFlux': 194.1329040527,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.359954834
          },
          {
            'recordTime': 1535842500000,
            'plusTotalFlux': 333.4928588867,
            'flux': 0.0,
            'pressure': 0.2379,
            'totalFlux': 194.1329040527,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.359954834
          },
          {
            'recordTime': 1535842800000,
            'plusTotalFlux': 333.4928588867,
            'flux': 0.0,
            'pressure': 0.2342,
            'totalFlux': 194.1329040527,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.359954834
          },
          {
            'recordTime': 1535843100000,
            'plusTotalFlux': 333.4928588867,
            'flux': -0.5326131582,
            'pressure': 0.23,
            'totalFlux': 194.1296081543,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3632507324
          },
          {
            'recordTime': 1535843400000,
            'plusTotalFlux': 333.4928588867,
            'flux': 0.0,
            'pressure': 0.2181,
            'totalFlux': 194.1168212891,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3760375977
          },
          {
            'recordTime': 1535843700000,
            'plusTotalFlux': 333.504119873,
            'flux': 0.5687593222,
            'pressure': 0.2119,
            'totalFlux': 194.1280822754,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3760375977
          },
          {
            'recordTime': 1535844000000,
            'plusTotalFlux': 333.5105285645,
            'flux': 0.0,
            'pressure': 0.2116,
            'totalFlux': 194.1344909668,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3760375977
          },
          {
            'recordTime': 1535844300000,
            'plusTotalFlux': 333.5234680176,
            'flux': 1.7666589022,
            'pressure': 0.2055,
            'totalFlux': 194.1429595947,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3805084229
          },
          {
            'recordTime': 1535844600000,
            'plusTotalFlux': 333.530456543,
            'flux': 0.0,
            'pressure': 0.2007,
            'totalFlux': 194.1499481201,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3805084229
          },
          {
            'recordTime': 1535844900000,
            'plusTotalFlux': 333.530456543,
            'flux': -0.7526946664,
            'pressure': 0.197,
            'totalFlux': 194.1432800293,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3871765137
          },
          {
            'recordTime': 1535845200000,
            'plusTotalFlux': 333.5358886719,
            'flux': 0.0,
            'pressure': 0.1945,
            'totalFlux': 194.1416473389,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.394241333
          },
          {
            'recordTime': 1535845500000,
            'plusTotalFlux': 333.5358886719,
            'flux': 0.0,
            'pressure': 0.1894,
            'totalFlux': 194.1401519775,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3957366943
          },
          {
            'recordTime': 1535845800000,
            'plusTotalFlux': 333.5573730469,
            'flux': 0.0,
            'pressure': 0.2748,
            'totalFlux': 194.1616363525,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3957366943
          },
          {
            'recordTime': 1535846100000,
            'plusTotalFlux': 333.5573730469,
            'flux': -0.3521077037,
            'pressure': 0.2696,
            'totalFlux': 194.1607513428,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.3966217041
          },
          {
            'recordTime': 1535846400000,
            'plusTotalFlux': 333.5696716309,
            'flux': 0.0,
            'pressure': 0.2513,
            'totalFlux': 194.1676940918,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4019775391
          },
          {
            'recordTime': 1535846700000,
            'plusTotalFlux': 333.5755615234,
            'flux': 0.0,
            'pressure': 0.2385,
            'totalFlux': 194.1683502197,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4072113037
          },
          {
            'recordTime': 1535847000000,
            'plusTotalFlux': 333.5850830078,
            'flux': 0.0,
            'pressure': 0.2385,
            'totalFlux': 194.1778717041,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4072113037
          },
          {
            'recordTime': 1535847300000,
            'plusTotalFlux': 333.5850830078,
            'flux': 0.0,
            'pressure': 0.2309,
            'totalFlux': 194.1705474854,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4145355225
          },
          {
            'recordTime': 1535847600000,
            'plusTotalFlux': 333.6008605957,
            'flux': 0.0,
            'pressure': 0.2297,
            'totalFlux': 194.1787261963,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4221343994
          },
          {
            'recordTime': 1535847900000,
            'plusTotalFlux': 333.6116638184,
            'flux': -0.340248704,
            'pressure': 0.2239,
            'totalFlux': 194.1793518066,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4323120117
          },
          {
            'recordTime': 1535848200000,
            'plusTotalFlux': 333.6116638184,
            'flux': -0.4660094976,
            'pressure': 0.2211,
            'totalFlux': 194.1776275635,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4340362549
          },
          {
            'recordTime': 1535848500000,
            'plusTotalFlux': 333.6116638184,
            'flux': 0.0,
            'pressure': 0.2165,
            'totalFlux': 194.1753997803,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4362640381
          },
          {
            'recordTime': 1535848800000,
            'plusTotalFlux': 333.6282043457,
            'flux': 0.8602707982,
            'pressure': 0.2043,
            'totalFlux': 194.1888275146,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4393768311
          },
          {
            'recordTime': 1535849100000,
            'plusTotalFlux': 333.6565551758,
            'flux': 0.620883882,
            'pressure': 0.2068,
            'totalFlux': 194.2171783447,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4393768311
          },
          {
            'recordTime': 1535849400000,
            'plusTotalFlux': 333.6869506836,
            'flux': 0.0,
            'pressure': 0.2019,
            'totalFlux': 194.2475738525,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4393768311
          },
          {
            'recordTime': 1535849700000,
            'plusTotalFlux': 333.6955566406,
            'flux': 0.6459808946,
            'pressure': 0.1723,
            'totalFlux': 194.2492828369,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4462738037
          },
          {
            'recordTime': 1535850000000,
            'plusTotalFlux': 333.7544250488,
            'flux': 0.812972486,
            'pressure': 0.1677,
            'totalFlux': 194.3081512451,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4462738037
          },
          {
            'recordTime': 1535850300000,
            'plusTotalFlux': 333.7953186035,
            'flux': 0.7328552008,
            'pressure': 0.1485,
            'totalFlux': 194.3490447998,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4462738037
          },
          {
            'recordTime': 1535850600000,
            'plusTotalFlux': 333.8484802246,
            'flux': 0.4316910803,
            'pressure': 0.1521,
            'totalFlux': 194.4022064209,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4462738037
          },
          {
            'recordTime': 1535850900000,
            'plusTotalFlux': 333.8749389648,
            'flux': 0.0,
            'pressure': 0.1423,
            'totalFlux': 194.4286651611,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4462738037
          },
          {
            'recordTime': 1535851200000,
            'plusTotalFlux': 333.8809204102,
            'flux': 0.0,
            'pressure': 0.1353,
            'totalFlux': 194.4346466064,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4462738037
          },
          {
            'recordTime': 1535851500000,
            'plusTotalFlux': 333.8809204102,
            'flux': 0.0,
            'pressure': 0.1271,
            'totalFlux': 194.4346466064,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4462738037
          },
          {
            'recordTime': 1535851800000,
            'plusTotalFlux': 333.9023132324,
            'flux': 0.0,
            'pressure': 0.1909,
            'totalFlux': 194.4560394287,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4462738037
          },
          {
            'recordTime': 1535852100000,
            'plusTotalFlux': 333.9187011719,
            'flux': 0.0,
            'pressure': 0.1674,
            'totalFlux': 194.4724273682,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4462738037
          },
          {
            'recordTime': 1535852400000,
            'plusTotalFlux': 333.9270935059,
            'flux': 0.0,
            'pressure': 0.1753,
            'totalFlux': 194.4749603271,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4521331787
          },
          {
            'recordTime': 1535852700000,
            'plusTotalFlux': 333.9315795898,
            'flux': 0.0,
            'pressure': 0.1759,
            'totalFlux': 194.4794464111,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4521331787
          },
          {
            'recordTime': 1535853000000,
            'plusTotalFlux': 333.9315795898,
            'flux': 0.0,
            'pressure': 0.1723,
            'totalFlux': 194.4794464111,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4521331787
          },
          {
            'recordTime': 1535853300000,
            'plusTotalFlux': 333.934753418,
            'flux': 0.0,
            'pressure': 0.1607,
            'totalFlux': 194.4728088379,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4619445801
          },
          {
            'recordTime': 1535853600000,
            'plusTotalFlux': 333.9382629395,
            'flux': 0.0,
            'pressure': 0.1567,
            'totalFlux': 194.4763183594,
            'uploadTime': 1535860921580,
            'reverseTotalFlux': 139.4619445801
          },
          {
            'recordTime': 1535853900000,
            'plusTotalFlux': 333.9382629395,
            'flux': 0.0,
            'pressure': 0.143,
            'totalFlux': 194.4652557373,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4730072021
          },
          {
            'recordTime': 1535854200000,
            'plusTotalFlux': 333.9462890625,
            'flux': 0.4278299809,
            'pressure': 0.1384,
            'totalFlux': 194.4658660889,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4804229736
          },
          {
            'recordTime': 1535854500000,
            'plusTotalFlux': 333.961730957,
            'flux': 0.0,
            'pressure': 0.1362,
            'totalFlux': 194.4813079834,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4804229736
          },
          {
            'recordTime': 1535854800000,
            'plusTotalFlux': 333.961730957,
            'flux': 0.0,
            'pressure': 0.1314,
            'totalFlux': 194.4813079834,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4804229736
          },
          {
            'recordTime': 1535855100000,
            'plusTotalFlux': 333.972076416,
            'flux': 0.0,
            'pressure': 0.1741,
            'totalFlux': 194.4916534424,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4804229736
          },
          {
            'recordTime': 1535855400000,
            'plusTotalFlux': 334.0114746094,
            'flux': 0.0,
            'pressure': 0.1918,
            'totalFlux': 194.5310516357,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4804229736
          },
          {
            'recordTime': 1535855700000,
            'plusTotalFlux': 334.0376281738,
            'flux': 0.0,
            'pressure': 0.1945,
            'totalFlux': 194.5523834229,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.485244751
          },
          {
            'recordTime': 1535856000000,
            'plusTotalFlux': 334.0409240723,
            'flux': 0.0,
            'pressure': 0.1942,
            'totalFlux': 194.5556793213,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.485244751
          },
          {
            'recordTime': 1535856300000,
            'plusTotalFlux': 334.0627441406,
            'flux': 0.5092987418,
            'pressure': 0.1909,
            'totalFlux': 194.5774993896,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.485244751
          },
          {
            'recordTime': 1535856600000,
            'plusTotalFlux': 334.0742492676,
            'flux': 0.0,
            'pressure': 0.1936,
            'totalFlux': 194.5890045166,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.485244751
          },
          {
            'recordTime': 1535856900000,
            'plusTotalFlux': 334.1225891113,
            'flux': 0.69617486,
            'pressure': 0.1936,
            'totalFlux': 194.6342926025,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4882965088
          },
          {
            'recordTime': 1535857200000,
            'plusTotalFlux': 334.1651000977,
            'flux': 0.7965629101,
            'pressure': 0.193,
            'totalFlux': 194.6768035889,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4882965088
          },
          {
            'recordTime': 1535857500000,
            'plusTotalFlux': 334.1753845215,
            'flux': 0.0,
            'pressure': 0.1955,
            'totalFlux': 194.6870880127,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4882965088
          },
          {
            'recordTime': 1535857800000,
            'plusTotalFlux': 334.2061462402,
            'flux': 0.0,
            'pressure': 0.1958,
            'totalFlux': 194.7178497314,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4882965088
          },
          {
            'recordTime': 1535858100000,
            'plusTotalFlux': 334.2203063965,
            'flux': 0.4316911399,
            'pressure': 0.1967,
            'totalFlux': 194.721862793,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4984436035
          },
          {
            'recordTime': 1535858400000,
            'plusTotalFlux': 334.2394714355,
            'flux': 0.0,
            'pressure': 0.1967,
            'totalFlux': 194.741027832,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.4984436035
          },
          {
            'recordTime': 1535858700000,
            'plusTotalFlux': 334.2496337891,
            'flux': -0.6426538229,
            'pressure': 0.1997,
            'totalFlux': 194.7371368408,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.5124969482
          },
          {
            'recordTime': 1535859000000,
            'plusTotalFlux': 334.2496337891,
            'flux': 0.0,
            'pressure': 0.2052,
            'totalFlux': 194.7360687256,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.5135650635
          },
          {
            'recordTime': 1535859300000,
            'plusTotalFlux': 334.2582092285,
            'flux': 0.0,
            'pressure': 0.2055,
            'totalFlux': 194.7386779785,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.51953125
          },
          {
            'recordTime': 1535859600000,
            'plusTotalFlux': 334.2582092285,
            'flux': 0.0,
            'pressure': 0.2028,
            'totalFlux': 194.7343292236,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.5238800049
          },
          {
            'recordTime': 1535859900000,
            'plusTotalFlux': 334.2733154297,
            'flux': 0.3564000726,
            'pressure': 0.2052,
            'totalFlux': 194.7494354248,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.5238800049
          },
          {
            'recordTime': 1535860200000,
            'plusTotalFlux': 334.2954406738,
            'flux': 0.4056288302,
            'pressure': 0.2052,
            'totalFlux': 194.7677001953,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.5277404785
          },
          {
            'recordTime': 1535860500000,
            'plusTotalFlux': 334.2961120605,
            'flux': 0.0,
            'pressure': 0.2092,
            'totalFlux': 194.7469177246,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.5491943359
          },
          {
            'recordTime': 1535860800000,
            'plusTotalFlux': 334.3192443848,
            'flux': 0.0,
            'pressure': 0.2138,
            'totalFlux': 194.7652587891,
            'uploadTime': 1535860968543,
            'reverseTotalFlux': 139.5539855957
          },
          {
            'recordTime': 1535861100000,
            'plusTotalFlux': 334.3227539062,
            'flux': -0.4872454703,
            'pressure': 0.2184,
            'totalFlux': 194.7626953125,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5600585938
          },
          {
            'recordTime': 1535861400000,
            'plusTotalFlux': 334.3594970703,
            'flux': 0.3535042703,
            'pressure': 0.2177,
            'totalFlux': 194.7972412109,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5622558594
          },
          {
            'recordTime': 1535861700000,
            'plusTotalFlux': 334.3909912109,
            'flux': -0.5384047627,
            'pressure': 0.2269,
            'totalFlux': 194.8256225586,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5653686523
          },
          {
            'recordTime': 1535862000000,
            'plusTotalFlux': 334.4065246582,
            'flux': 0.3621917069,
            'pressure': 0.2297,
            'totalFlux': 194.8283538818,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5781707764
          },
          {
            'recordTime': 1535862300000,
            'plusTotalFlux': 334.4071350098,
            'flux': 0.0,
            'pressure': 0.2388,
            'totalFlux': 194.8190002441,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5881347656
          },
          {
            'recordTime': 1535862600000,
            'plusTotalFlux': 334.4112243652,
            'flux': 0.0,
            'pressure': 0.2336,
            'totalFlux': 194.8230895996,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5881347656
          },
          {
            'recordTime': 1535862900000,
            'plusTotalFlux': 334.4112243652,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 194.8230895996,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5881347656
          },
          {
            'recordTime': 1535863200000,
            'plusTotalFlux': 334.4252319336,
            'flux': 0.0,
            'pressure': 0.2525,
            'totalFlux': 194.837097168,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5881347656
          },
          {
            'recordTime': 1535863500000,
            'plusTotalFlux': 334.4459533691,
            'flux': 0.4230036438,
            'pressure': 0.2562,
            'totalFlux': 194.8544464111,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.591506958
          },
          {
            'recordTime': 1535863800000,
            'plusTotalFlux': 334.4466552734,
            'flux': 0.0,
            'pressure': 0.2629,
            'totalFlux': 194.850479126,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5961761475
          },
          {
            'recordTime': 1535864100000,
            'plusTotalFlux': 334.4584655762,
            'flux': 0.0,
            'pressure': 0.2751,
            'totalFlux': 194.8608551025,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5976104736
          },
          {
            'recordTime': 1535864400000,
            'plusTotalFlux': 334.4584655762,
            'flux': 0.0,
            'pressure': 0.2782,
            'totalFlux': 194.8608551025,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5976104736
          },
          {
            'recordTime': 1535864700000,
            'plusTotalFlux': 334.4584655762,
            'flux': 0.0,
            'pressure': 0.2791,
            'totalFlux': 194.859375,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.5990905762
          },
          {
            'recordTime': 1535865000000,
            'plusTotalFlux': 334.4677734375,
            'flux': 0.0,
            'pressure': 0.2901,
            'totalFlux': 194.8633270264,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.6044464111
          },
          {
            'recordTime': 1535865300000,
            'plusTotalFlux': 334.4939880371,
            'flux': 0.5648982525,
            'pressure': 0.2886,
            'totalFlux': 194.8856964111,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.608291626
          },
          {
            'recordTime': 1535865600000,
            'plusTotalFlux': 334.5073547363,
            'flux': 0.3696380258,
            'pressure': 0.2947,
            'totalFlux': 194.8990631104,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.608291626
          },
          {
            'recordTime': 1535865900000,
            'plusTotalFlux': 334.5241394043,
            'flux': 0.0,
            'pressure': 0.2922,
            'totalFlux': 194.9107513428,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.6133880615
          },
          {
            'recordTime': 1535866200000,
            'plusTotalFlux': 334.5408325195,
            'flux': -0.5065509081,
            'pressure': 0.2602,
            'totalFlux': 194.9114227295,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.62940979
          },
          {
            'recordTime': 1535866500000,
            'plusTotalFlux': 334.5408325195,
            'flux': 0.0,
            'pressure': 0.2605,
            'totalFlux': 194.8891601562,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.6516723633
          },
          {
            'recordTime': 1535866800000,
            'plusTotalFlux': 334.5570983887,
            'flux': 0.0,
            'pressure': 0.2577,
            'totalFlux': 194.9035339355,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.6535644531
          },
          {
            'recordTime': 1535867100000,
            'plusTotalFlux': 334.5570983887,
            'flux': 0.0,
            'pressure': 0.2614,
            'totalFlux': 194.8955841064,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.6615142822
          },
          {
            'recordTime': 1535867400000,
            'plusTotalFlux': 334.5660095215,
            'flux': 0.0,
            'pressure': 0.2602,
            'totalFlux': 194.9044952393,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.6615142822
          },
          {
            'recordTime': 1535867700000,
            'plusTotalFlux': 334.5736083984,
            'flux': 0.0,
            'pressure': 0.2611,
            'totalFlux': 194.9120941162,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.6615142822
          },
          {
            'recordTime': 1535868000000,
            'plusTotalFlux': 334.5752563477,
            'flux': 0.0,
            'pressure': 0.2574,
            'totalFlux': 194.8904724121,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.6847839355
          },
          {
            'recordTime': 1535868300000,
            'plusTotalFlux': 334.5817565918,
            'flux': 0.0,
            'pressure': 0.2617,
            'totalFlux': 194.8923950195,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.6893615723
          },
          {
            'recordTime': 1535868600000,
            'plusTotalFlux': 334.5817565918,
            'flux': 0.0,
            'pressure': 0.2684,
            'totalFlux': 194.8816680908,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.700088501
          },
          {
            'recordTime': 1535868900000,
            'plusTotalFlux': 334.5912475586,
            'flux': 0.0,
            'pressure': 0.266,
            'totalFlux': 194.8802185059,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7110290527
          },
          {
            'recordTime': 1535869200000,
            'plusTotalFlux': 334.5912475586,
            'flux': 0.0,
            'pressure': 0.2684,
            'totalFlux': 194.8704376221,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7208099365
          },
          {
            'recordTime': 1535869500000,
            'plusTotalFlux': 334.598236084,
            'flux': 0.0,
            'pressure': 0.2706,
            'totalFlux': 194.8774261475,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7208099365
          },
          {
            'recordTime': 1535869800000,
            'plusTotalFlux': 334.6027526855,
            'flux': 0.0,
            'pressure': 0.2678,
            'totalFlux': 194.881942749,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7208099365
          },
          {
            'recordTime': 1535870100000,
            'plusTotalFlux': 334.6207580566,
            'flux': 0.0,
            'pressure': 0.2629,
            'totalFlux': 194.8999481201,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7208099365
          },
          {
            'recordTime': 1535870400000,
            'plusTotalFlux': 334.6207580566,
            'flux': 0.0,
            'pressure': 0.2681,
            'totalFlux': 194.8921203613,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7286376953
          },
          {
            'recordTime': 1535870700000,
            'plusTotalFlux': 334.6241149902,
            'flux': 0.0,
            'pressure': 0.2654,
            'totalFlux': 194.8938598633,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.730255127
          },
          {
            'recordTime': 1535871000000,
            'plusTotalFlux': 334.6553039551,
            'flux': 0.6315019131,
            'pressure': 0.2715,
            'totalFlux': 194.9231109619,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7321929932
          },
          {
            'recordTime': 1535871300000,
            'plusTotalFlux': 334.681854248,
            'flux': -0.3313544095,
            'pressure': 0.2599,
            'totalFlux': 194.9436645508,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7381896973
          },
          {
            'recordTime': 1535871600000,
            'plusTotalFlux': 334.698638916,
            'flux': 0.0,
            'pressure': 0.2541,
            'totalFlux': 194.9367218018,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7619171143
          },
          {
            'recordTime': 1535871900000,
            'plusTotalFlux': 334.698638916,
            'flux': 0.0,
            'pressure': 0.2553,
            'totalFlux': 194.9367218018,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7619171143
          },
          {
            'recordTime': 1535872200000,
            'plusTotalFlux': 334.7090148926,
            'flux': 0.0,
            'pressure': 0.2571,
            'totalFlux': 194.9470977783,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7619171143
          },
          {
            'recordTime': 1535872500000,
            'plusTotalFlux': 334.7090148926,
            'flux': 0.0,
            'pressure': 0.2458,
            'totalFlux': 194.9289398193,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7800750732
          },
          {
            'recordTime': 1535872800000,
            'plusTotalFlux': 334.7236022949,
            'flux': 0.4809198678,
            'pressure': 0.2458,
            'totalFlux': 194.9435272217,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7800750732
          },
          {
            'recordTime': 1535873100000,
            'plusTotalFlux': 334.7265930176,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 194.9427947998,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7837982178
          },
          {
            'recordTime': 1535873400000,
            'plusTotalFlux': 334.7265930176,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 194.937789917,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7888031006
          },
          {
            'recordTime': 1535873700000,
            'plusTotalFlux': 334.7297058105,
            'flux': 0.0,
            'pressure': 0.2458,
            'totalFlux': 194.9324493408,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7972564697
          },
          {
            'recordTime': 1535874000000,
            'plusTotalFlux': 334.7412719727,
            'flux': 0.5340096354,
            'pressure': 0.2437,
            'totalFlux': 194.9440155029,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.7972564697
          },
          {
            'recordTime': 1535874300000,
            'plusTotalFlux': 334.7528076172,
            'flux': 0.6691473722,
            'pressure': 0.2388,
            'totalFlux': 194.9454345703,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.8073730469
          },
          {
            'recordTime': 1535874600000,
            'plusTotalFlux': 334.782989502,
            'flux': 0.0,
            'pressure': 0.237,
            'totalFlux': 194.9756164551,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.8073730469
          },
          {
            'recordTime': 1535874900000,
            'plusTotalFlux': 334.7868041992,
            'flux': 0.0,
            'pressure': 0.226,
            'totalFlux': 194.9621582031,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.8246459961
          },
          {
            'recordTime': 1535875200000,
            'plusTotalFlux': 334.7956542969,
            'flux': -0.4698705971,
            'pressure': 0.2208,
            'totalFlux': 194.9616241455,
            'uploadTime': 1535882527993,
            'reverseTotalFlux': 139.8340301514
          },
          {
            'recordTime': 1535875500000,
            'plusTotalFlux': 334.809753418,
            'flux': 0.0,
            'pressure': 0.2217,
            'totalFlux': 194.974822998,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8349304199
          },
          {
            'recordTime': 1535875800000,
            'plusTotalFlux': 334.813293457,
            'flux': 0.0,
            'pressure': 0.2251,
            'totalFlux': 194.9783630371,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8349304199
          },
          {
            'recordTime': 1535876100000,
            'plusTotalFlux': 334.8415222168,
            'flux': 0.0,
            'pressure': 0.2116,
            'totalFlux': 195.0065917969,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8349304199
          },
          {
            'recordTime': 1535876400000,
            'plusTotalFlux': 334.848449707,
            'flux': 0.4490659237,
            'pressure': 0.2181,
            'totalFlux': 195.0058135986,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8426361084
          },
          {
            'recordTime': 1535876700000,
            'plusTotalFlux': 334.8854675293,
            'flux': 0.9500407577,
            'pressure': 0.2141,
            'totalFlux': 195.0428314209,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8426361084
          },
          {
            'recordTime': 1535877000000,
            'plusTotalFlux': 334.9110412598,
            'flux': 0.0,
            'pressure': 0.2092,
            'totalFlux': 195.0684051514,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8426361084
          },
          {
            'recordTime': 1535877300000,
            'plusTotalFlux': 334.9465637207,
            'flux': 0.0,
            'pressure': 0.2101,
            'totalFlux': 195.1022338867,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.844329834
          },
          {
            'recordTime': 1535877600000,
            'plusTotalFlux': 334.9521179199,
            'flux': 0.0,
            'pressure': 0.2055,
            'totalFlux': 195.1077880859,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.844329834
          },
          {
            'recordTime': 1535877900000,
            'plusTotalFlux': 334.9925842285,
            'flux': 0.0,
            'pressure': 0.2077,
            'totalFlux': 195.1482543945,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.844329834
          },
          {
            'recordTime': 1535878200000,
            'plusTotalFlux': 334.9925842285,
            'flux': 0.0,
            'pressure': 0.2095,
            'totalFlux': 195.1482543945,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.844329834
          },
          {
            'recordTime': 1535878500000,
            'plusTotalFlux': 335.0328063965,
            'flux': 1.314912796,
            'pressure': 0.2126,
            'totalFlux': 195.1884765625,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.844329834
          },
          {
            'recordTime': 1535878800000,
            'plusTotalFlux': 335.0523986816,
            'flux': 0.0,
            'pressure': 0.2104,
            'totalFlux': 195.195526123,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8568725586
          },
          {
            'recordTime': 1535879100000,
            'plusTotalFlux': 335.0821838379,
            'flux': 0.0,
            'pressure': 0.2025,
            'totalFlux': 195.2253112793,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8568725586
          },
          {
            'recordTime': 1535879400000,
            'plusTotalFlux': 335.1313781738,
            'flux': 0.3506084979,
            'pressure': 0.1952,
            'totalFlux': 195.2745056152,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8568725586
          },
          {
            'recordTime': 1535879700000,
            'plusTotalFlux': 335.1525268555,
            'flux': 0.0,
            'pressure': 0.2022,
            'totalFlux': 195.2956542969,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8568725586
          },
          {
            'recordTime': 1535880000000,
            'plusTotalFlux': 335.1832275391,
            'flux': 0.0,
            'pressure': 0.2007,
            'totalFlux': 195.3263549805,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8568725586
          },
          {
            'recordTime': 1535880300000,
            'plusTotalFlux': 335.216796875,
            'flux': 0.9992695451,
            'pressure': 0.1985,
            'totalFlux': 195.3599243164,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8568725586
          },
          {
            'recordTime': 1535880600000,
            'plusTotalFlux': 335.266998291,
            'flux': 1.4249533415,
            'pressure': 0.1991,
            'totalFlux': 195.4101257324,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8568725586
          },
          {
            'recordTime': 1535880900000,
            'plusTotalFlux': 335.3183288574,
            'flux': 0.0,
            'pressure': 0.1982,
            'totalFlux': 195.4614562988,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8568725586
          },
          {
            'recordTime': 1535881200000,
            'plusTotalFlux': 335.3550109863,
            'flux': 0.0,
            'pressure': 0.1967,
            'totalFlux': 195.4981384277,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8568725586
          },
          {
            'recordTime': 1535881500000,
            'plusTotalFlux': 335.3688049316,
            'flux': 0.7994588017,
            'pressure': 0.1906,
            'totalFlux': 195.5055541992,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8632507324
          },
          {
            'recordTime': 1535881800000,
            'plusTotalFlux': 335.3703613281,
            'flux': 0.0,
            'pressure': 0.197,
            'totalFlux': 195.5071105957,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8632507324
          },
          {
            'recordTime': 1535882100000,
            'plusTotalFlux': 335.381439209,
            'flux': 0.0,
            'pressure': 0.1918,
            'totalFlux': 195.5181884766,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8632507324
          },
          {
            'recordTime': 1535882400000,
            'plusTotalFlux': 335.4031982422,
            'flux': 0.4490659237,
            'pressure': 0.1875,
            'totalFlux': 195.5399475098,
            'uploadTime': 1535882578200,
            'reverseTotalFlux': 139.8632507324
          },
          {
            'recordTime': 1535882700000,
            'plusTotalFlux': 335.4104003906,
            'flux': 0.0,
            'pressure': 0.1967,
            'totalFlux': 195.5456085205,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.8647918701
          },
          {
            'recordTime': 1535883000000,
            'plusTotalFlux': 335.4323730469,
            'flux': 0.0,
            'pressure': 0.1982,
            'totalFlux': 195.5675811768,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.8647918701
          },
          {
            'recordTime': 1535883300000,
            'plusTotalFlux': 335.4390258789,
            'flux': 0.0,
            'pressure': 0.2003,
            'totalFlux': 195.5742340088,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.8647918701
          },
          {
            'recordTime': 1535883600000,
            'plusTotalFlux': 335.4390258789,
            'flux': -0.4708358347,
            'pressure': 0.1997,
            'totalFlux': 195.5406951904,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.8983306885
          },
          {
            'recordTime': 1535883900000,
            'plusTotalFlux': 335.4437866211,
            'flux': 0.4664407969,
            'pressure': 0.2025,
            'totalFlux': 195.5414581299,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9023284912
          },
          {
            'recordTime': 1535884200000,
            'plusTotalFlux': 335.4683837891,
            'flux': 0.0,
            'pressure': 0.2071,
            'totalFlux': 195.5660552979,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9023284912
          },
          {
            'recordTime': 1535884500000,
            'plusTotalFlux': 335.4762268066,
            'flux': 0.0,
            'pressure': 0.2034,
            'totalFlux': 195.563949585,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9122772217
          },
          {
            'recordTime': 1535884800000,
            'plusTotalFlux': 335.4870605469,
            'flux': 0.0,
            'pressure': 0.2119,
            'totalFlux': 195.5657043457,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9213562012
          },
          {
            'recordTime': 1535885100000,
            'plusTotalFlux': 335.4907531738,
            'flux': -0.7729651928,
            'pressure': 0.2126,
            'totalFlux': 195.5527954102,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9379577637
          },
          {
            'recordTime': 1535885400000,
            'plusTotalFlux': 335.4907531738,
            'flux': 0.0,
            'pressure': 0.2123,
            'totalFlux': 195.5441131592,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9466400146
          },
          {
            'recordTime': 1535885700000,
            'plusTotalFlux': 335.4907531738,
            'flux': 0.0,
            'pressure': 0.2135,
            'totalFlux': 195.5139923096,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9767608643
          },
          {
            'recordTime': 1535886000000,
            'plusTotalFlux': 335.505065918,
            'flux': 0.3515737653,
            'pressure': 0.2156,
            'totalFlux': 195.5262298584,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9788360596
          },
          {
            'recordTime': 1535886300000,
            'plusTotalFlux': 335.5071105957,
            'flux': 0.0,
            'pressure': 0.2141,
            'totalFlux': 195.5234985352,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9836120605
          },
          {
            'recordTime': 1535886600000,
            'plusTotalFlux': 335.5104370117,
            'flux': 0.0,
            'pressure': 0.2141,
            'totalFlux': 195.5268249512,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9836120605
          },
          {
            'recordTime': 1535886900000,
            'plusTotalFlux': 335.5192260742,
            'flux': 0.5533150434,
            'pressure': 0.2226,
            'totalFlux': 195.5288696289,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9903564453
          },
          {
            'recordTime': 1535887200000,
            'plusTotalFlux': 335.5389709473,
            'flux': 0.4635450244,
            'pressure': 0.2281,
            'totalFlux': 195.548614502,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9903564453
          },
          {
            'recordTime': 1535887500000,
            'plusTotalFlux': 335.5531921387,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 195.5628356934,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9903564453
          },
          {
            'recordTime': 1535887800000,
            'plusTotalFlux': 335.568145752,
            'flux': 0.0,
            'pressure': 0.2266,
            'totalFlux': 195.5777893066,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 139.9903564453
          },
          {
            'recordTime': 1535888100000,
            'plusTotalFlux': 335.568145752,
            'flux': 0.0,
            'pressure': 0.2248,
            'totalFlux': 195.5671691895,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0009765625
          },
          {
            'recordTime': 1535888400000,
            'plusTotalFlux': 335.568145752,
            'flux': 0.0,
            'pressure': 0.2226,
            'totalFlux': 195.5655670166,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0025787354
          },
          {
            'recordTime': 1535888700000,
            'plusTotalFlux': 335.5756530762,
            'flux': -0.6079040766,
            'pressure': 0.2278,
            'totalFlux': 195.5632781982,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0123748779
          },
          {
            'recordTime': 1535889000000,
            'plusTotalFlux': 335.5756530762,
            'flux': 0.0,
            'pressure': 0.2254,
            'totalFlux': 195.553894043,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0217590332
          },
          {
            'recordTime': 1535889300000,
            'plusTotalFlux': 335.5796813965,
            'flux': 0.0,
            'pressure': 0.2287,
            'totalFlux': 195.5535888672,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0260925293
          },
          {
            'recordTime': 1535889600000,
            'plusTotalFlux': 335.5987243652,
            'flux': 0.0,
            'pressure': 0.237,
            'totalFlux': 195.5551147461,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0436096191
          },
          {
            'recordTime': 1535889900000,
            'plusTotalFlux': 335.6002502441,
            'flux': 0.0,
            'pressure': 0.2455,
            'totalFlux': 195.55128479,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0489654541
          },
          {
            'recordTime': 1535890200000,
            'plusTotalFlux': 335.6002502441,
            'flux': 0.0,
            'pressure': 0.2467,
            'totalFlux': 195.55128479,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0489654541
          },
          {
            'recordTime': 1535890500000,
            'plusTotalFlux': 335.6046447754,
            'flux': -0.3356981575,
            'pressure': 0.2464,
            'totalFlux': 195.5407409668,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0639038086
          },
          {
            'recordTime': 1535890800000,
            'plusTotalFlux': 335.6253051758,
            'flux': 0.0,
            'pressure': 0.2492,
            'totalFlux': 195.5607452393,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0645599365
          },
          {
            'recordTime': 1535891100000,
            'plusTotalFlux': 335.640838623,
            'flux': 0.0,
            'pressure': 0.2565,
            'totalFlux': 195.5762786865,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0645599365
          },
          {
            'recordTime': 1535891400000,
            'plusTotalFlux': 335.6639709473,
            'flux': 0.0,
            'pressure': 0.2709,
            'totalFlux': 195.5979309082,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0660400391
          },
          {
            'recordTime': 1535891700000,
            'plusTotalFlux': 335.6639709473,
            'flux': 0.0,
            'pressure': 0.269,
            'totalFlux': 195.5964813232,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.067489624
          },
          {
            'recordTime': 1535892000000,
            'plusTotalFlux': 335.6639709473,
            'flux': 0.0,
            'pressure': 0.2678,
            'totalFlux': 195.5964813232,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.067489624
          },
          {
            'recordTime': 1535892300000,
            'plusTotalFlux': 335.6674194336,
            'flux': 0.0,
            'pressure': 0.2745,
            'totalFlux': 195.5985412598,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0688781738
          },
          {
            'recordTime': 1535892600000,
            'plusTotalFlux': 335.6688537598,
            'flux': 0.0,
            'pressure': 0.2764,
            'totalFlux': 195.5999755859,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0688781738
          },
          {
            'recordTime': 1535892900000,
            'plusTotalFlux': 335.6702880859,
            'flux': 0.0,
            'pressure': 0.2754,
            'totalFlux': 195.5882263184,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0820617676
          },
          {
            'recordTime': 1535893200000,
            'plusTotalFlux': 335.6702880859,
            'flux': 0.0,
            'pressure': 0.2785,
            'totalFlux': 195.5841522217,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0861358643
          },
          {
            'recordTime': 1535893500000,
            'plusTotalFlux': 335.6720275879,
            'flux': 0.0,
            'pressure': 0.2834,
            'totalFlux': 195.5844421387,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0875854492
          },
          {
            'recordTime': 1535893800000,
            'plusTotalFlux': 335.6768798828,
            'flux': 0.0,
            'pressure': 0.2776,
            'totalFlux': 195.5813293457,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0955505371
          },
          {
            'recordTime': 1535894100000,
            'plusTotalFlux': 335.6844482422,
            'flux': 0.0,
            'pressure': 0.2843,
            'totalFlux': 195.5888977051,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0955505371
          },
          {
            'recordTime': 1535894400000,
            'plusTotalFlux': 335.6860351562,
            'flux': 0.0,
            'pressure': 0.28,
            'totalFlux': 195.5904846191,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0955505371
          },
          {
            'recordTime': 1535894700000,
            'plusTotalFlux': 335.6860351562,
            'flux': 0.0,
            'pressure': 0.2794,
            'totalFlux': 195.5889434814,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.0970916748
          },
          {
            'recordTime': 1535895000000,
            'plusTotalFlux': 335.6860351562,
            'flux': -0.4023017287,
            'pressure': 0.2367,
            'totalFlux': 195.5707550049,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.1152801514
          },
          {
            'recordTime': 1535895300000,
            'plusTotalFlux': 335.6860351562,
            'flux': 0.0,
            'pressure': 0.2092,
            'totalFlux': 195.5358428955,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.1501922607
          },
          {
            'recordTime': 1535895600000,
            'plusTotalFlux': 335.6860351562,
            'flux': 0.0,
            'pressure': 0.2077,
            'totalFlux': 195.5358428955,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.1501922607
          },
          {
            'recordTime': 1535895900000,
            'plusTotalFlux': 335.6860351562,
            'flux': -0.4476694167,
            'pressure': 0.2077,
            'totalFlux': 195.5205535889,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.1654815674
          },
          {
            'recordTime': 1535896200000,
            'plusTotalFlux': 335.6904907227,
            'flux': -0.3588646054,
            'pressure': 0.2043,
            'totalFlux': 195.5167541504,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.1737365723
          },
          {
            'recordTime': 1535896500000,
            'plusTotalFlux': 335.6904907227,
            'flux': 0.0,
            'pressure': 0.2037,
            'totalFlux': 195.5085144043,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.1819763184
          },
          {
            'recordTime': 1535896800000,
            'plusTotalFlux': 335.7054443359,
            'flux': 0.0,
            'pressure': 0.1955,
            'totalFlux': 195.5234680176,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.1819763184
          },
          {
            'recordTime': 1535897100000,
            'plusTotalFlux': 335.7054443359,
            'flux': 0.0,
            'pressure': 0.1967,
            'totalFlux': 195.5234680176,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.1819763184
          },
          {
            'recordTime': 1535897400000,
            'plusTotalFlux': 335.7054443359,
            'flux': 0.0,
            'pressure': 0.1967,
            'totalFlux': 195.4941558838,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2112884521
          },
          {
            'recordTime': 1535897700000,
            'plusTotalFlux': 335.7054443359,
            'flux': -0.3849268556,
            'pressure': 0.1964,
            'totalFlux': 195.4903869629,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.215057373
          },
          {
            'recordTime': 1535898000000,
            'plusTotalFlux': 335.7182617188,
            'flux': 0.0,
            'pressure': 0.1949,
            'totalFlux': 195.4877166748,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2305450439
          },
          {
            'recordTime': 1535898300000,
            'plusTotalFlux': 335.7253723145,
            'flux': -0.4167807996,
            'pressure': 0.19,
            'totalFlux': 195.4909973145,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.234375
          },
          {
            'recordTime': 1535898600000,
            'plusTotalFlux': 335.7326965332,
            'flux': 0.0,
            'pressure': 0.1918,
            'totalFlux': 195.493560791,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2391357422
          },
          {
            'recordTime': 1535898900000,
            'plusTotalFlux': 335.7407531738,
            'flux': 0.0,
            'pressure': 0.1915,
            'totalFlux': 195.4961090088,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.244644165
          },
          {
            'recordTime': 1535899200000,
            'plusTotalFlux': 335.746887207,
            'flux': 0.0,
            'pressure': 0.1997,
            'totalFlux': 195.502243042,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.244644165
          },
          {
            'recordTime': 1535899500000,
            'plusTotalFlux': 335.746887207,
            'flux': 0.0,
            'pressure': 0.204,
            'totalFlux': 195.491607666,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.255279541
          },
          {
            'recordTime': 1535899800000,
            'plusTotalFlux': 335.746887207,
            'flux': 0.0,
            'pressure': 0.2043,
            'totalFlux': 195.4848937988,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2619934082
          },
          {
            'recordTime': 1535900100000,
            'plusTotalFlux': 335.7601013184,
            'flux': 0.0,
            'pressure': 0.2101,
            'totalFlux': 195.4981079102,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2619934082
          },
          {
            'recordTime': 1535900400000,
            'plusTotalFlux': 335.767364502,
            'flux': 0.0,
            'pressure': 0.2055,
            'totalFlux': 195.493927002,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2734375
          },
          {
            'recordTime': 1535900700000,
            'plusTotalFlux': 335.767364502,
            'flux': 0.0,
            'pressure': 0.2083,
            'totalFlux': 195.493927002,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2734375
          },
          {
            'recordTime': 1535901000000,
            'plusTotalFlux': 335.7796936035,
            'flux': 0.4017678201,
            'pressure': 0.2034,
            'totalFlux': 195.5062561035,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2734375
          },
          {
            'recordTime': 1535901300000,
            'plusTotalFlux': 335.7904968262,
            'flux': 0.3911497593,
            'pressure': 0.2116,
            'totalFlux': 195.5170593262,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2734375
          },
          {
            'recordTime': 1535901600000,
            'plusTotalFlux': 335.8072814941,
            'flux': 0.0,
            'pressure': 0.215,
            'totalFlux': 195.5240325928,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2832489014
          },
          {
            'recordTime': 1535901900000,
            'plusTotalFlux': 335.8354492188,
            'flux': -0.5963209271,
            'pressure': 0.215,
            'totalFlux': 195.5491943359,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2862548828
          },
          {
            'recordTime': 1535902200000,
            'plusTotalFlux': 335.858215332,
            'flux': 0.8197293282,
            'pressure': 0.215,
            'totalFlux': 195.5671539307,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2910614014
          },
          {
            'recordTime': 1535902500000,
            'plusTotalFlux': 335.8782958984,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 195.5872344971,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2910614014
          },
          {
            'recordTime': 1535902800000,
            'plusTotalFlux': 335.8865966797,
            'flux': 0.0,
            'pressure': 0.2174,
            'totalFlux': 195.5869598389,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2996368408
          },
          {
            'recordTime': 1535903100000,
            'plusTotalFlux': 335.8865966797,
            'flux': 0.0,
            'pressure': 0.2242,
            'totalFlux': 195.5869598389,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2996368408
          },
          {
            'recordTime': 1535903400000,
            'plusTotalFlux': 335.9016418457,
            'flux': 0.0,
            'pressure': 0.2327,
            'totalFlux': 195.6020050049,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.2996368408
          },
          {
            'recordTime': 1535903700000,
            'plusTotalFlux': 335.9034118652,
            'flux': 0.0,
            'pressure': 0.2345,
            'totalFlux': 195.5975494385,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.3058624268
          },
          {
            'recordTime': 1535904000000,
            'plusTotalFlux': 335.9073486328,
            'flux': 0.6054396033,
            'pressure': 0.2385,
            'totalFlux': 195.5964355469,
            'uploadTime': 1535904157220,
            'reverseTotalFlux': 140.3109130859
          },
          {
            'recordTime': 1535904300000,
            'plusTotalFlux': 335.9148864746,
            'flux': 0.7241677046,
            'pressure': 0.2397,
            'totalFlux': 195.6039733887,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3109130859
          },
          {
            'recordTime': 1535904600000,
            'plusTotalFlux': 335.925201416,
            'flux': 0.0,
            'pressure': 0.2471,
            'totalFlux': 195.6142883301,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3109130859
          },
          {
            'recordTime': 1535904900000,
            'plusTotalFlux': 335.925201416,
            'flux': 0.0,
            'pressure': 0.2519,
            'totalFlux': 195.6093292236,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3158721924
          },
          {
            'recordTime': 1535905200000,
            'plusTotalFlux': 335.925201416,
            'flux': 0.0,
            'pressure': 0.2556,
            'totalFlux': 195.6016387939,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3235626221
          },
          {
            'recordTime': 1535905500000,
            'plusTotalFlux': 335.9337158203,
            'flux': 0.0,
            'pressure': 0.259,
            'totalFlux': 195.6040496826,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3296661377
          },
          {
            'recordTime': 1535905800000,
            'plusTotalFlux': 335.9337158203,
            'flux': 0.0,
            'pressure': 0.2599,
            'totalFlux': 195.6040496826,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3296661377
          },
          {
            'recordTime': 1535906100000,
            'plusTotalFlux': 335.9337158203,
            'flux': 0.0,
            'pressure': 0.269,
            'totalFlux': 195.6040496826,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3296661377
          },
          {
            'recordTime': 1535906400000,
            'plusTotalFlux': 335.9337158203,
            'flux': 0.0,
            'pressure': 0.2748,
            'totalFlux': 195.6040496826,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3296661377
          },
          {
            'recordTime': 1535906700000,
            'plusTotalFlux': 335.9337158203,
            'flux': 0.0,
            'pressure': 0.2788,
            'totalFlux': 195.6040496826,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3296661377
          },
          {
            'recordTime': 1535907000000,
            'plusTotalFlux': 335.9337158203,
            'flux': 0.0,
            'pressure': 0.2834,
            'totalFlux': 195.6040496826,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3296661377
          },
          {
            'recordTime': 1535907300000,
            'plusTotalFlux': 335.9348754883,
            'flux': 0.3795665503,
            'pressure': 0.2822,
            'totalFlux': 195.6052093506,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3296661377
          },
          {
            'recordTime': 1535907600000,
            'plusTotalFlux': 335.9440002441,
            'flux': 0.4519617558,
            'pressure': 0.2226,
            'totalFlux': 195.5905303955,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3534698486
          },
          {
            'recordTime': 1535907900000,
            'plusTotalFlux': 335.944519043,
            'flux': 0.0,
            'pressure': 0.219,
            'totalFlux': 195.5895843506,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3549346924
          },
          {
            'recordTime': 1535908200000,
            'plusTotalFlux': 335.944519043,
            'flux': 0.0,
            'pressure': 0.2171,
            'totalFlux': 195.5895843506,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3549346924
          },
          {
            'recordTime': 1535908500000,
            'plusTotalFlux': 335.9517822266,
            'flux': 0.0,
            'pressure': 0.219,
            'totalFlux': 195.5968475342,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3549346924
          },
          {
            'recordTime': 1535908800000,
            'plusTotalFlux': 335.9517822266,
            'flux': 0.0,
            'pressure': 0.2196,
            'totalFlux': 195.5968475342,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3549346924
          },
          {
            'recordTime': 1535909100000,
            'plusTotalFlux': 335.9594116211,
            'flux': 0.0,
            'pressure': 0.2217,
            'totalFlux': 195.6044769287,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3549346924
          },
          {
            'recordTime': 1535909400000,
            'plusTotalFlux': 335.9594116211,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 195.6044769287,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3549346924
          },
          {
            'recordTime': 1535909700000,
            'plusTotalFlux': 335.9594116211,
            'flux': 0.0,
            'pressure': 0.2223,
            'totalFlux': 195.6030883789,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3563232422
          },
          {
            'recordTime': 1535910000000,
            'plusTotalFlux': 335.9594116211,
            'flux': 0.0,
            'pressure': 0.2266,
            'totalFlux': 195.6010284424,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3583831787
          },
          {
            'recordTime': 1535910300000,
            'plusTotalFlux': 335.9594116211,
            'flux': 0.0,
            'pressure': 0.2309,
            'totalFlux': 195.6010284424,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3583831787
          },
          {
            'recordTime': 1535910600000,
            'plusTotalFlux': 335.9594116211,
            'flux': 0.0,
            'pressure': 0.2303,
            'totalFlux': 195.6010284424,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3583831787
          },
          {
            'recordTime': 1535910900000,
            'plusTotalFlux': 335.9594116211,
            'flux': 0.0,
            'pressure': 0.2275,
            'totalFlux': 195.5996551514,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3597564697
          },
          {
            'recordTime': 1535911200000,
            'plusTotalFlux': 335.9594116211,
            'flux': 0.0,
            'pressure': 0.2315,
            'totalFlux': 195.5996551514,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3597564697
          },
          {
            'recordTime': 1535911500000,
            'plusTotalFlux': 335.9594116211,
            'flux': 0.0,
            'pressure': 0.2309,
            'totalFlux': 195.5996551514,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3597564697
          },
          {
            'recordTime': 1535911800000,
            'plusTotalFlux': 335.966796875,
            'flux': 0.0,
            'pressure': 0.2327,
            'totalFlux': 195.6070404053,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3597564697
          },
          {
            'recordTime': 1535912100000,
            'plusTotalFlux': 335.966796875,
            'flux': 0.0,
            'pressure': 0.233,
            'totalFlux': 195.6070404053,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3597564697
          },
          {
            'recordTime': 1535912400000,
            'plusTotalFlux': 335.9715270996,
            'flux': 0.8428958654,
            'pressure': 0.2342,
            'totalFlux': 195.6117706299,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3597564697
          },
          {
            'recordTime': 1535912700000,
            'plusTotalFlux': 335.9889831543,
            'flux': 0.3419210613,
            'pressure': 0.2342,
            'totalFlux': 195.6179504395,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3710327148
          },
          {
            'recordTime': 1535913000000,
            'plusTotalFlux': 335.9909362793,
            'flux': 0.0,
            'pressure': 0.2324,
            'totalFlux': 195.6199035645,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3710327148
          },
          {
            'recordTime': 1535913300000,
            'plusTotalFlux': 336.001953125,
            'flux': 0.0,
            'pressure': 0.2385,
            'totalFlux': 195.6309204102,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3710327148
          },
          {
            'recordTime': 1535913600000,
            'plusTotalFlux': 336.0049133301,
            'flux': 0.0,
            'pressure': 0.2397,
            'totalFlux': 195.6338806152,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3710327148
          },
          {
            'recordTime': 1535913900000,
            'plusTotalFlux': 336.0049133301,
            'flux': 0.0,
            'pressure': 0.2406,
            'totalFlux': 195.6338806152,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3710327148
          },
          {
            'recordTime': 1535914200000,
            'plusTotalFlux': 336.0049133301,
            'flux': 0.0,
            'pressure': 0.2409,
            'totalFlux': 195.6338806152,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3710327148
          },
          {
            'recordTime': 1535914500000,
            'plusTotalFlux': 336.0049133301,
            'flux': 0.0,
            'pressure': 0.2413,
            'totalFlux': 195.6338806152,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3710327148
          },
          {
            'recordTime': 1535914800000,
            'plusTotalFlux': 336.0049133301,
            'flux': 0.0,
            'pressure': 0.24,
            'totalFlux': 195.6338806152,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3710327148
          },
          {
            'recordTime': 1535915100000,
            'plusTotalFlux': 336.0049133301,
            'flux': 0.0,
            'pressure': 0.2409,
            'totalFlux': 195.6307525635,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535915400000,
            'plusTotalFlux': 336.0049133301,
            'flux': 0.0,
            'pressure': 0.2382,
            'totalFlux': 195.6307525635,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535915700000,
            'plusTotalFlux': 336.0049133301,
            'flux': 0.0,
            'pressure': 0.2443,
            'totalFlux': 195.6307525635,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535916000000,
            'plusTotalFlux': 336.0049133301,
            'flux': 0.0,
            'pressure': 0.2422,
            'totalFlux': 195.6307525635,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535916300000,
            'plusTotalFlux': 336.0049133301,
            'flux': 0.0,
            'pressure': 0.2373,
            'totalFlux': 195.6307525635,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535916600000,
            'plusTotalFlux': 336.0063476562,
            'flux': 0.0,
            'pressure': 0.2471,
            'totalFlux': 195.6321868896,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535916900000,
            'plusTotalFlux': 336.0063476562,
            'flux': 0.0,
            'pressure': 0.2458,
            'totalFlux': 195.6321868896,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535917200000,
            'plusTotalFlux': 336.0063476562,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 195.6321868896,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535917500000,
            'plusTotalFlux': 336.0063476562,
            'flux': 0.0,
            'pressure': 0.2455,
            'totalFlux': 195.6321868896,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535917800000,
            'plusTotalFlux': 336.0063476562,
            'flux': 0.0,
            'pressure': 0.2467,
            'totalFlux': 195.6321868896,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535918100000,
            'plusTotalFlux': 336.0063476562,
            'flux': 0.0,
            'pressure': 0.2443,
            'totalFlux': 195.6321868896,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535918400000,
            'plusTotalFlux': 336.0063476562,
            'flux': 0.0,
            'pressure': 0.2464,
            'totalFlux': 195.6321868896,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535918700000,
            'plusTotalFlux': 336.0077514648,
            'flux': 0.5011904836,
            'pressure': 0.2471,
            'totalFlux': 195.6335906982,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535919000000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535919300000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2461,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535919600000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2492,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535919900000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2492,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535920200000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2461,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535920500000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2458,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535920800000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2452,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535921100000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.248,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535921400000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535921700000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.248,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535922000000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.248,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535922300000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2495,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535922600000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2507,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535922900000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2507,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535923200000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2486,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535923500000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2486,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535923800000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.248,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535924100000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2495,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535924400000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.248,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535924700000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2486,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535925000000,
            'plusTotalFlux': 336.0238342285,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 195.6496734619,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535925300000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.2492,
            'totalFlux': 195.6632232666,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535925600000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.248,
            'totalFlux': 195.6632232666,
            'uploadTime': 1535925993670,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535925900000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.2455,
            'totalFlux': 195.6632232666,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535926200000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 195.6632232666,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535926500000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.2385,
            'totalFlux': 195.6632232666,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3741607666
          },
          {
            'recordTime': 1535926800000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.2391,
            'totalFlux': 195.6598052979,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3775787354
          },
          {
            'recordTime': 1535927100000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.2376,
            'totalFlux': 195.6598052979,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3775787354
          },
          {
            'recordTime': 1535927400000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.237,
            'totalFlux': 195.6598052979,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3775787354
          },
          {
            'recordTime': 1535927700000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.2324,
            'totalFlux': 195.6598052979,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3775787354
          },
          {
            'recordTime': 1535928000000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.2315,
            'totalFlux': 195.6598052979,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3775787354
          },
          {
            'recordTime': 1535928300000,
            'plusTotalFlux': 336.0373840332,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 195.6598052979,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3775787354
          },
          {
            'recordTime': 1535928600000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.222,
            'totalFlux': 195.6764678955,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3775787354
          },
          {
            'recordTime': 1535928900000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.2208,
            'totalFlux': 195.6764678955,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3775787354
          },
          {
            'recordTime': 1535929200000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.2184,
            'totalFlux': 195.6764678955,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3775787354
          },
          {
            'recordTime': 1535929500000,
            'plusTotalFlux': 336.0540466309,
            'flux': -0.4824190438,
            'pressure': 0.1887,
            'totalFlux': 195.6646118164,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3894348145
          },
          {
            'recordTime': 1535929800000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.1878,
            'totalFlux': 195.6607208252,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3933258057
          },
          {
            'recordTime': 1535930100000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.1848,
            'totalFlux': 195.6607208252,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3933258057
          },
          {
            'recordTime': 1535930400000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.1781,
            'totalFlux': 195.6607208252,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3933258057
          },
          {
            'recordTime': 1535930700000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.1695,
            'totalFlux': 195.6587982178,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3952484131
          },
          {
            'recordTime': 1535931000000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.161,
            'totalFlux': 195.6587982178,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3952484131
          },
          {
            'recordTime': 1535931300000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.1515,
            'totalFlux': 195.6587982178,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3952484131
          },
          {
            'recordTime': 1535931600000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.1332,
            'totalFlux': 195.6587982178,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.3952484131
          },
          {
            'recordTime': 1535931900000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.103,
            'totalFlux': 195.6488800049,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.405166626
          },
          {
            'recordTime': 1535932200000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.0917,
            'totalFlux': 195.6473236084,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4067230225
          },
          {
            'recordTime': 1535932500000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.0752,
            'totalFlux': 195.6427612305,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4112854004
          },
          {
            'recordTime': 1535932800000,
            'plusTotalFlux': 336.0540466309,
            'flux': 0.0,
            'pressure': 0.0633,
            'totalFlux': 195.6255493164,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4284973145
          },
          {
            'recordTime': 1535933100000,
            'plusTotalFlux': 336.0540466309,
            'flux': -0.3366634846,
            'pressure': 0.0389,
            'totalFlux': 195.6246032715,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4294433594
          },
          {
            'recordTime': 1535933400000,
            'plusTotalFlux': 336.1083068848,
            'flux': 0.7531257868,
            'pressure': 0.1405,
            'totalFlux': 195.6671142578,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.441192627
          },
          {
            'recordTime': 1535933700000,
            'plusTotalFlux': 336.1616210938,
            'flux': 0.0,
            'pressure': 0.1402,
            'totalFlux': 195.7204284668,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.441192627
          },
          {
            'recordTime': 1535934000000,
            'plusTotalFlux': 336.2040710449,
            'flux': 0.0,
            'pressure': 0.208,
            'totalFlux': 195.762878418,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.441192627
          },
          {
            'recordTime': 1535934300000,
            'plusTotalFlux': 336.2338867188,
            'flux': 0.8602707386,
            'pressure': 0.2003,
            'totalFlux': 195.7926940918,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.441192627
          },
          {
            'recordTime': 1535934600000,
            'plusTotalFlux': 336.2986450195,
            'flux': 1.0832480192,
            'pressure': 0.2119,
            'totalFlux': 195.8574523926,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.441192627
          },
          {
            'recordTime': 1535934900000,
            'plusTotalFlux': 336.392791748,
            'flux': 0.7946323156,
            'pressure': 0.2101,
            'totalFlux': 195.9515991211,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.441192627
          },
          {
            'recordTime': 1535935200000,
            'plusTotalFlux': 336.4336853027,
            'flux': 0.5185653567,
            'pressure': 0.2098,
            'totalFlux': 195.9924926758,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.441192627
          },
          {
            'recordTime': 1535935500000,
            'plusTotalFlux': 336.4420471191,
            'flux': 0.0,
            'pressure': 0.2077,
            'totalFlux': 196.0008544922,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.441192627
          },
          {
            'recordTime': 1535935800000,
            'plusTotalFlux': 336.472869873,
            'flux': 0.5407665968,
            'pressure': 0.208,
            'totalFlux': 196.0259552002,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4469146729
          },
          {
            'recordTime': 1535936100000,
            'plusTotalFlux': 336.5004272461,
            'flux': 0.8805414438,
            'pressure': 0.204,
            'totalFlux': 196.0535125732,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4469146729
          },
          {
            'recordTime': 1535936400000,
            'plusTotalFlux': 336.5055847168,
            'flux': 0.3525389731,
            'pressure': 0.2049,
            'totalFlux': 196.0586700439,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4469146729
          },
          {
            'recordTime': 1535936700000,
            'plusTotalFlux': 336.5074768066,
            'flux': -0.4515304565,
            'pressure': 0.208,
            'totalFlux': 196.0593109131,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4481658936
          },
          {
            'recordTime': 1535937000000,
            'plusTotalFlux': 336.5074768066,
            'flux': 0.0,
            'pressure': 0.204,
            'totalFlux': 196.0477142334,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4597625732
          },
          {
            'recordTime': 1535937300000,
            'plusTotalFlux': 336.5111694336,
            'flux': 0.0,
            'pressure': 0.1988,
            'totalFlux': 196.0514068604,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4597625732
          },
          {
            'recordTime': 1535937600000,
            'plusTotalFlux': 336.5194396973,
            'flux': 0.0,
            'pressure': 0.1985,
            'totalFlux': 196.059677124,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4597625732
          },
          {
            'recordTime': 1535937900000,
            'plusTotalFlux': 336.5394897461,
            'flux': 0.0,
            'pressure': 0.1973,
            'totalFlux': 196.0742034912,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4652862549
          },
          {
            'recordTime': 1535938200000,
            'plusTotalFlux': 336.5394897461,
            'flux': 0.0,
            'pressure': 0.1903,
            'totalFlux': 196.0742034912,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4652862549
          },
          {
            'recordTime': 1535938500000,
            'plusTotalFlux': 336.5394897461,
            'flux': -0.5992166996,
            'pressure': 0.1906,
            'totalFlux': 196.0612182617,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4782714844
          },
          {
            'recordTime': 1535938800000,
            'plusTotalFlux': 336.5783691406,
            'flux': 0.5571760535,
            'pressure': 0.1823,
            'totalFlux': 196.0992584229,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4791107178
          },
          {
            'recordTime': 1535939100000,
            'plusTotalFlux': 336.587677002,
            'flux': 0.0,
            'pressure': 0.1796,
            'totalFlux': 196.1055603027,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4821166992
          },
          {
            'recordTime': 1535939400000,
            'plusTotalFlux': 336.587677002,
            'flux': 0.0,
            'pressure': 0.1698,
            'totalFlux': 196.1055603027,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4821166992
          },
          {
            'recordTime': 1535939700000,
            'plusTotalFlux': 336.6037902832,
            'flux': 0.0,
            'pressure': 0.1713,
            'totalFlux': 196.1147613525,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4890289307
          },
          {
            'recordTime': 1535940000000,
            'plusTotalFlux': 336.6113891602,
            'flux': 0.0,
            'pressure': 0.1695,
            'totalFlux': 196.1223602295,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4890289307
          },
          {
            'recordTime': 1535940300000,
            'plusTotalFlux': 336.6113891602,
            'flux': 0.0,
            'pressure': 0.1655,
            'totalFlux': 196.118637085,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4927520752
          },
          {
            'recordTime': 1535940600000,
            'plusTotalFlux': 336.6113891602,
            'flux': 0.0,
            'pressure': 0.1622,
            'totalFlux': 196.118637085,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4927520752
          },
          {
            'recordTime': 1535940900000,
            'plusTotalFlux': 336.6130065918,
            'flux': 0.0,
            'pressure': 0.1503,
            'totalFlux': 196.1202545166,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4927520752
          },
          {
            'recordTime': 1535941200000,
            'plusTotalFlux': 336.622253418,
            'flux': 0.0,
            'pressure': 0.1549,
            'totalFlux': 196.1251220703,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4971313477
          },
          {
            'recordTime': 1535941500000,
            'plusTotalFlux': 336.6448059082,
            'flux': 0.0,
            'pressure': 0.1533,
            'totalFlux': 196.1476745605,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4971313477
          },
          {
            'recordTime': 1535941800000,
            'plusTotalFlux': 336.6689147949,
            'flux': 0.0,
            'pressure': 0.1552,
            'totalFlux': 196.1717834473,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4971313477
          },
          {
            'recordTime': 1535942100000,
            'plusTotalFlux': 336.6689147949,
            'flux': 0.0,
            'pressure': 0.1512,
            'totalFlux': 196.1717834473,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4971313477
          },
          {
            'recordTime': 1535942400000,
            'plusTotalFlux': 336.683013916,
            'flux': 0.0,
            'pressure': 0.1445,
            'totalFlux': 196.1858825684,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4971313477
          },
          {
            'recordTime': 1535942700000,
            'plusTotalFlux': 336.6938781738,
            'flux': 0.0,
            'pressure': 0.1952,
            'totalFlux': 196.1967468262,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.4971313477
          },
          {
            'recordTime': 1535943000000,
            'plusTotalFlux': 336.6938781738,
            'flux': 0.0,
            'pressure': 0.2019,
            'totalFlux': 196.1953277588,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.498550415
          },
          {
            'recordTime': 1535943300000,
            'plusTotalFlux': 336.7051391602,
            'flux': -0.7005700469,
            'pressure': 0.1994,
            'totalFlux': 196.1999359131,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5052032471
          },
          {
            'recordTime': 1535943600000,
            'plusTotalFlux': 336.7451477051,
            'flux': 0.0,
            'pressure': 0.1988,
            'totalFlux': 196.2368011475,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5083465576
          },
          {
            'recordTime': 1535943900000,
            'plusTotalFlux': 336.7611694336,
            'flux': 0.6894180179,
            'pressure': 0.1994,
            'totalFlux': 196.252822876,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5083465576
          },
          {
            'recordTime': 1535944200000,
            'plusTotalFlux': 336.8399658203,
            'flux': 0.4519617558,
            'pressure': 0.2113,
            'totalFlux': 196.3316192627,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5083465576
          },
          {
            'recordTime': 1535944500000,
            'plusTotalFlux': 336.9140625,
            'flux': 0.8747497201,
            'pressure': 0.2129,
            'totalFlux': 196.4057159424,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5083465576
          },
          {
            'recordTime': 1535944800000,
            'plusTotalFlux': 336.9580993652,
            'flux': 0.8872982264,
            'pressure': 0.2098,
            'totalFlux': 196.4497528076,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5083465576
          },
          {
            'recordTime': 1535945100000,
            'plusTotalFlux': 337.0429992676,
            'flux': 0.0,
            'pressure': 0.2144,
            'totalFlux': 196.53465271,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5083465576
          },
          {
            'recordTime': 1535945400000,
            'plusTotalFlux': 337.0634155273,
            'flux': 0.8139377832,
            'pressure': 0.2129,
            'totalFlux': 196.5496520996,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5137634277
          },
          {
            'recordTime': 1535945700000,
            'plusTotalFlux': 337.0963745117,
            'flux': -1.1262537241,
            'pressure': 0.2129,
            'totalFlux': 196.5727996826,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5235748291
          },
          {
            'recordTime': 1535946000000,
            'plusTotalFlux': 337.097442627,
            'flux': 0.4201078713,
            'pressure': 0.2132,
            'totalFlux': 196.5590820312,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5383605957
          },
          {
            'recordTime': 1535946300000,
            'plusTotalFlux': 337.1549987793,
            'flux': 0.0,
            'pressure': 0.2092,
            'totalFlux': 196.6166381836,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5383605957
          },
          {
            'recordTime': 1535946600000,
            'plusTotalFlux': 337.1653747559,
            'flux': 1.0224359035,
            'pressure': 0.2092,
            'totalFlux': 196.6270141602,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5383605957
          },
          {
            'recordTime': 1535946900000,
            'plusTotalFlux': 337.1870117188,
            'flux': 0.0,
            'pressure': 0.2144,
            'totalFlux': 196.6226196289,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5643920898
          },
          {
            'recordTime': 1535947200000,
            'plusTotalFlux': 337.2154541016,
            'flux': 0.0,
            'pressure': 0.219,
            'totalFlux': 196.6363677979,
            'uploadTime': 1535947445487,
            'reverseTotalFlux': 140.5790863037
          },
          {
            'recordTime': 1535947500000,
            'plusTotalFlux': 337.2202148438,
            'flux': 0.0,
            'pressure': 0.215,
            'totalFlux': 196.64112854,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.5790863037
          },
          {
            'recordTime': 1535947800000,
            'plusTotalFlux': 337.2226257324,
            'flux': 0.3882539868,
            'pressure': 0.2135,
            'totalFlux': 196.6279296875,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.5946960449
          },
          {
            'recordTime': 1535948100000,
            'plusTotalFlux': 337.2232666016,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 196.609664917,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6136016846
          },
          {
            'recordTime': 1535948400000,
            'plusTotalFlux': 337.2232666016,
            'flux': -0.781652689,
            'pressure': 0.2181,
            'totalFlux': 196.6000823975,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6231842041
          },
          {
            'recordTime': 1535948700000,
            'plusTotalFlux': 337.2232666016,
            'flux': 0.0,
            'pressure': 0.215,
            'totalFlux': 196.5686187744,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6546478271
          },
          {
            'recordTime': 1535949000000,
            'plusTotalFlux': 337.2304992676,
            'flux': 0.0,
            'pressure': 0.2187,
            'totalFlux': 196.5675506592,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6629486084
          },
          {
            'recordTime': 1535949300000,
            'plusTotalFlux': 337.2583618164,
            'flux': 1.0803521872,
            'pressure': 0.2214,
            'totalFlux': 196.595413208,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6629486084
          },
          {
            'recordTime': 1535949600000,
            'plusTotalFlux': 337.311340332,
            'flux': 0.9818947315,
            'pressure': 0.2254,
            'totalFlux': 196.6483917236,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6629486084
          },
          {
            'recordTime': 1535949900000,
            'plusTotalFlux': 337.3569030762,
            'flux': 0.3506084979,
            'pressure': 0.2416,
            'totalFlux': 196.6846313477,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6722717285
          },
          {
            'recordTime': 1535950200000,
            'plusTotalFlux': 337.4187011719,
            'flux': 1.0745606422,
            'pressure': 0.2504,
            'totalFlux': 196.7464294434,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6722717285
          },
          {
            'recordTime': 1535950500000,
            'plusTotalFlux': 337.463104248,
            'flux': 1.2622089386,
            'pressure': 0.2507,
            'totalFlux': 196.7908325195,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6722717285
          },
          {
            'recordTime': 1535950800000,
            'plusTotalFlux': 337.5730285645,
            'flux': 1.5378898382,
            'pressure': 0.2596,
            'totalFlux': 196.9007568359,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6722717285
          },
          {
            'recordTime': 1535951100000,
            'plusTotalFlux': 337.6543884277,
            'flux': 1.2705100775,
            'pressure': 0.2666,
            'totalFlux': 196.9821166992,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6722717285
          },
          {
            'recordTime': 1535951400000,
            'plusTotalFlux': 337.7610168457,
            'flux': 0.3940455914,
            'pressure': 0.2635,
            'totalFlux': 197.0887451172,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6722717285
          },
          {
            'recordTime': 1535951700000,
            'plusTotalFlux': 337.8481445312,
            'flux': 0.5690351129,
            'pressure': 0.2626,
            'totalFlux': 197.1758728027,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6722717285
          },
          {
            'recordTime': 1535952000000,
            'plusTotalFlux': 337.8949279785,
            'flux': -0.7917880416,
            'pressure': 0.2281,
            'totalFlux': 197.2122955322,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.6826324463
          },
          {
            'recordTime': 1535952300000,
            'plusTotalFlux': 337.9006958008,
            'flux': 0.0,
            'pressure': 0.2248,
            'totalFlux': 197.1795043945,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7211914062
          },
          {
            'recordTime': 1535952600000,
            'plusTotalFlux': 337.9403381348,
            'flux': 0.8052503467,
            'pressure': 0.2263,
            'totalFlux': 197.2132720947,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.72706604
          },
          {
            'recordTime': 1535952900000,
            'plusTotalFlux': 337.9644775391,
            'flux': 0.0,
            'pressure': 0.2275,
            'totalFlux': 197.2360229492,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7284545898
          },
          {
            'recordTime': 1535953200000,
            'plusTotalFlux': 337.9644775391,
            'flux': 0.0,
            'pressure': 0.2321,
            'totalFlux': 197.2360229492,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7284545898
          },
          {
            'recordTime': 1535953500000,
            'plusTotalFlux': 337.9995727539,
            'flux': 0.8631665111,
            'pressure': 0.2239,
            'totalFlux': 197.2679748535,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7315979004
          },
          {
            'recordTime': 1535953800000,
            'plusTotalFlux': 338.0287780762,
            'flux': 0.7762921453,
            'pressure': 0.2281,
            'totalFlux': 197.2971801758,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7315979004
          },
          {
            'recordTime': 1535954100000,
            'plusTotalFlux': 338.0852355957,
            'flux': 0.9326660633,
            'pressure': 0.2355,
            'totalFlux': 197.3536376953,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7315979004
          },
          {
            'recordTime': 1535954400000,
            'plusTotalFlux': 338.1330566406,
            'flux': -0.4023017287,
            'pressure': 0.2309,
            'totalFlux': 197.4004516602,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7326049805
          },
          {
            'recordTime': 1535954700000,
            'plusTotalFlux': 338.1425170898,
            'flux': 0.7878755331,
            'pressure': 0.2333,
            'totalFlux': 197.4056854248,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.736831665
          },
          {
            'recordTime': 1535955000000,
            'plusTotalFlux': 338.2100219727,
            'flux': 0.8033197522,
            'pressure': 0.2355,
            'totalFlux': 197.4731903076,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.736831665
          },
          {
            'recordTime': 1535955300000,
            'plusTotalFlux': 338.2431335449,
            'flux': 0.4490659237,
            'pressure': 0.24,
            'totalFlux': 197.5047302246,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7384033203
          },
          {
            'recordTime': 1535955600000,
            'plusTotalFlux': 338.2546691895,
            'flux': 0.0,
            'pressure': 0.24,
            'totalFlux': 197.5089111328,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7457580566
          },
          {
            'recordTime': 1535955900000,
            'plusTotalFlux': 338.2621154785,
            'flux': 0.0,
            'pressure': 0.2385,
            'totalFlux': 197.5090332031,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7530822754
          },
          {
            'recordTime': 1535956200000,
            'plusTotalFlux': 338.2784118652,
            'flux': 0.4577533603,
            'pressure': 0.2394,
            'totalFlux': 197.5253295898,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7530822754
          },
          {
            'recordTime': 1535956500000,
            'plusTotalFlux': 338.3094177246,
            'flux': 0.5706899166,
            'pressure': 0.2425,
            'totalFlux': 197.5529937744,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7564239502
          },
          {
            'recordTime': 1535956800000,
            'plusTotalFlux': 338.3162231445,
            'flux': 0.3313030601,
            'pressure': 0.2461,
            'totalFlux': 197.5597991943,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7564239502
          },
          {
            'recordTime': 1535957100000,
            'plusTotalFlux': 338.3392333984,
            'flux': 0.4266234636,
            'pressure': 0.2504,
            'totalFlux': 197.5782012939,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7610321045
          },
          {
            'recordTime': 1535957400000,
            'plusTotalFlux': 338.3539428711,
            'flux': 0.0,
            'pressure': 0.2541,
            'totalFlux': 197.5856170654,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7683258057
          },
          {
            'recordTime': 1535957700000,
            'plusTotalFlux': 338.354888916,
            'flux': 0.3853581846,
            'pressure': 0.2443,
            'totalFlux': 197.5648803711,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7900085449
          },
          {
            'recordTime': 1535958000000,
            'plusTotalFlux': 338.3555297852,
            'flux': 0.0,
            'pressure': 0.2471,
            'totalFlux': 197.5563659668,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.7991638184
          },
          {
            'recordTime': 1535958300000,
            'plusTotalFlux': 338.3555297852,
            'flux': 0.0,
            'pressure': 0.2382,
            'totalFlux': 197.5548858643,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8006439209
          },
          {
            'recordTime': 1535958600000,
            'plusTotalFlux': 338.3555297852,
            'flux': 0.0,
            'pressure': 0.2385,
            'totalFlux': 197.550567627,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8049621582
          },
          {
            'recordTime': 1535958900000,
            'plusTotalFlux': 338.3636169434,
            'flux': 0.0,
            'pressure': 0.2382,
            'totalFlux': 197.5586547852,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8049621582
          },
          {
            'recordTime': 1535959200000,
            'plusTotalFlux': 338.3764648438,
            'flux': 0.4925030768,
            'pressure': 0.2419,
            'totalFlux': 197.5699005127,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8065643311
          },
          {
            'recordTime': 1535959500000,
            'plusTotalFlux': 338.3790893555,
            'flux': -0.5470921397,
            'pressure': 0.2309,
            'totalFlux': 197.5680236816,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8110656738
          },
          {
            'recordTime': 1535959800000,
            'plusTotalFlux': 338.3834838867,
            'flux': 0.5533150434,
            'pressure': 0.2293,
            'totalFlux': 197.5657501221,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8177337646
          },
          {
            'recordTime': 1535960100000,
            'plusTotalFlux': 338.3859558105,
            'flux': 0.0,
            'pressure': 0.2229,
            'totalFlux': 197.5682220459,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8177337646
          },
          {
            'recordTime': 1535960400000,
            'plusTotalFlux': 338.3859558105,
            'flux': 0.0,
            'pressure': 0.215,
            'totalFlux': 197.5519256592,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8340301514
          },
          {
            'recordTime': 1535960700000,
            'plusTotalFlux': 338.3988952637,
            'flux': 0.0,
            'pressure': 0.2092,
            'totalFlux': 197.5581359863,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8407592773
          },
          {
            'recordTime': 1535961000000,
            'plusTotalFlux': 338.4087524414,
            'flux': 0.0,
            'pressure': 0.2031,
            'totalFlux': 197.5679931641,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8407592773
          },
          {
            'recordTime': 1535961300000,
            'plusTotalFlux': 338.4124755859,
            'flux': 0.0,
            'pressure': 0.1997,
            'totalFlux': 197.5635681152,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8489074707
          },
          {
            'recordTime': 1535961600000,
            'plusTotalFlux': 338.4331970215,
            'flux': -0.3617603779,
            'pressure': 0.1949,
            'totalFlux': 197.5754699707,
            'uploadTime': 1535969154997,
            'reverseTotalFlux': 140.8577270508
          },
          {
            'recordTime': 1535961900000,
            'plusTotalFlux': 338.4379577637,
            'flux': 0.0,
            'pressure': 0.1875,
            'totalFlux': 197.5779876709,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8599700928
          },
          {
            'recordTime': 1535962200000,
            'plusTotalFlux': 338.4516601562,
            'flux': 0.0,
            'pressure': 0.1778,
            'totalFlux': 197.5916900635,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8599700928
          },
          {
            'recordTime': 1535962500000,
            'plusTotalFlux': 338.4516601562,
            'flux': 0.0,
            'pressure': 0.1674,
            'totalFlux': 197.5869903564,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8646697998
          },
          {
            'recordTime': 1535962800000,
            'plusTotalFlux': 338.4516601562,
            'flux': 0.0,
            'pressure': 0.1674,
            'totalFlux': 197.5781707764,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8734893799
          },
          {
            'recordTime': 1535963100000,
            'plusTotalFlux': 338.4585571289,
            'flux': 0.0,
            'pressure': 0.1604,
            'totalFlux': 197.5757446289,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8828125
          },
          {
            'recordTime': 1535963400000,
            'plusTotalFlux': 338.4809265137,
            'flux': 0.0,
            'pressure': 0.1613,
            'totalFlux': 197.5945281982,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8863983154
          },
          {
            'recordTime': 1535963700000,
            'plusTotalFlux': 338.484588623,
            'flux': 0.0,
            'pressure': 0.1564,
            'totalFlux': 197.5967559814,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535964000000,
            'plusTotalFlux': 338.484588623,
            'flux': 0.0,
            'pressure': 0.1427,
            'totalFlux': 197.5967559814,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535964300000,
            'plusTotalFlux': 338.484588623,
            'flux': 0.0,
            'pressure': 0.1414,
            'totalFlux': 197.5967559814,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535964600000,
            'plusTotalFlux': 338.484588623,
            'flux': 0.0,
            'pressure': 0.1427,
            'totalFlux': 197.5967559814,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535964900000,
            'plusTotalFlux': 338.4910888672,
            'flux': 0.0,
            'pressure': 0.1671,
            'totalFlux': 197.6032562256,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535965200000,
            'plusTotalFlux': 338.5745849609,
            'flux': 0.0,
            'pressure': 0.1875,
            'totalFlux': 197.6867523193,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535965500000,
            'plusTotalFlux': 338.5810546875,
            'flux': 0.4780240655,
            'pressure': 0.1924,
            'totalFlux': 197.6932220459,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535965800000,
            'plusTotalFlux': 338.635345459,
            'flux': 0.0,
            'pressure': 0.1973,
            'totalFlux': 197.7475128174,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535966100000,
            'plusTotalFlux': 338.6653137207,
            'flux': 0.6035090685,
            'pressure': 0.1967,
            'totalFlux': 197.7774810791,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535966400000,
            'plusTotalFlux': 338.6864318848,
            'flux': 0.0,
            'pressure': 0.1924,
            'totalFlux': 197.7985992432,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535966700000,
            'plusTotalFlux': 338.6977844238,
            'flux': 0.7869101763,
            'pressure': 0.1939,
            'totalFlux': 197.8099517822,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535967000000,
            'plusTotalFlux': 338.7530517578,
            'flux': 0.6498419642,
            'pressure': 0.1945,
            'totalFlux': 197.8652191162,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535967300000,
            'plusTotalFlux': 338.7717590332,
            'flux': 0.0,
            'pressure': 0.1909,
            'totalFlux': 197.8839263916,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535967600000,
            'plusTotalFlux': 338.8317260742,
            'flux': 0.0,
            'pressure': 0.1967,
            'totalFlux': 197.9438934326,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535967900000,
            'plusTotalFlux': 338.8711853027,
            'flux': 0.4085246027,
            'pressure': 0.204,
            'totalFlux': 197.9833526611,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535968200000,
            'plusTotalFlux': 338.8831176758,
            'flux': 0.0,
            'pressure': 0.2071,
            'totalFlux': 197.9952850342,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535968500000,
            'plusTotalFlux': 338.9358825684,
            'flux': 0.5098779202,
            'pressure': 0.2025,
            'totalFlux': 198.0480499268,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535968800000,
            'plusTotalFlux': 338.9962463379,
            'flux': 0.425899446,
            'pressure': 0.2016,
            'totalFlux': 198.1084136963,
            'uploadTime': 1535969161217,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535969100000,
            'plusTotalFlux': 339.0390014648,
            'flux': 0.5253223777,
            'pressure': 0.2019,
            'totalFlux': 198.1511688232,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535969400000,
            'plusTotalFlux': 339.0956420898,
            'flux': 1.1208934784,
            'pressure': 0.2046,
            'totalFlux': 198.2078094482,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535969700000,
            'plusTotalFlux': 339.1298217773,
            'flux': 0.0,
            'pressure': 0.2028,
            'totalFlux': 198.2419891357,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.8878326416
          },
          {
            'recordTime': 1535970000000,
            'plusTotalFlux': 339.1481628418,
            'flux': -0.801923275,
            'pressure': 0.208,
            'totalFlux': 198.2492523193,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.8989105225
          },
          {
            'recordTime': 1535970300000,
            'plusTotalFlux': 339.1930541992,
            'flux': 0.0,
            'pressure': 0.2031,
            'totalFlux': 198.2900085449,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9030456543
          },
          {
            'recordTime': 1535970600000,
            'plusTotalFlux': 339.2091064453,
            'flux': 0.3457820714,
            'pressure': 0.2107,
            'totalFlux': 198.3022766113,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.906829834
          },
          {
            'recordTime': 1535970900000,
            'plusTotalFlux': 339.2374572754,
            'flux': 0.3506084681,
            'pressure': 0.2162,
            'totalFlux': 198.3244018555,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9130554199
          },
          {
            'recordTime': 1535971200000,
            'plusTotalFlux': 339.2628479004,
            'flux': 0.0,
            'pressure': 0.2156,
            'totalFlux': 198.3383331299,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9245147705
          },
          {
            'recordTime': 1535971500000,
            'plusTotalFlux': 339.2628479004,
            'flux': 0.0,
            'pressure': 0.2196,
            'totalFlux': 198.3383331299,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9245147705
          },
          {
            'recordTime': 1535971800000,
            'plusTotalFlux': 339.277557373,
            'flux': 0.0,
            'pressure': 0.2245,
            'totalFlux': 198.3494110107,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9281463623
          },
          {
            'recordTime': 1535972100000,
            'plusTotalFlux': 339.2921142578,
            'flux': 0.0,
            'pressure': 0.2324,
            'totalFlux': 198.3557281494,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9363861084
          },
          {
            'recordTime': 1535972400000,
            'plusTotalFlux': 339.3008422852,
            'flux': -0.6831952333,
            'pressure': 0.2428,
            'totalFlux': 198.3578643799,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9429779053
          },
          {
            'recordTime': 1535972700000,
            'plusTotalFlux': 339.3087463379,
            'flux': 1.1469557285,
            'pressure': 0.2428,
            'totalFlux': 198.3585205078,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9502258301
          },
          {
            'recordTime': 1535973000000,
            'plusTotalFlux': 339.3471984863,
            'flux': 0.3824623525,
            'pressure': 0.2513,
            'totalFlux': 198.3969726562,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9502258301
          },
          {
            'recordTime': 1535973300000,
            'plusTotalFlux': 339.3811340332,
            'flux': 0.0,
            'pressure': 0.2586,
            'totalFlux': 198.4281311035,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9530029297
          },
          {
            'recordTime': 1535973600000,
            'plusTotalFlux': 339.4260559082,
            'flux': 0.0,
            'pressure': 0.2562,
            'totalFlux': 198.4730529785,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9530029297
          },
          {
            'recordTime': 1535973900000,
            'plusTotalFlux': 339.4313049316,
            'flux': 0.8457916975,
            'pressure': 0.2577,
            'totalFlux': 198.478302002,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9530029297
          },
          {
            'recordTime': 1535974200000,
            'plusTotalFlux': 339.4575195312,
            'flux': 0.0,
            'pressure': 0.2532,
            'totalFlux': 198.4967803955,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9607391357
          },
          {
            'recordTime': 1535974500000,
            'plusTotalFlux': 339.4589538574,
            'flux': -0.5528837442,
            'pressure': 0.2617,
            'totalFlux': 198.4894866943,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9694671631
          },
          {
            'recordTime': 1535974800000,
            'plusTotalFlux': 339.4589538574,
            'flux': 0.0,
            'pressure': 0.269,
            'totalFlux': 198.4761657715,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9827880859
          },
          {
            'recordTime': 1535975100000,
            'plusTotalFlux': 339.4643554688,
            'flux': -0.3588646352,
            'pressure': 0.2666,
            'totalFlux': 198.4776763916,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9866790771
          },
          {
            'recordTime': 1535975400000,
            'plusTotalFlux': 339.4903564453,
            'flux': 0.0,
            'pressure': 0.2785,
            'totalFlux': 198.501159668,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9891967773
          },
          {
            'recordTime': 1535975700000,
            'plusTotalFlux': 339.4939880371,
            'flux': -0.3694825768,
            'pressure': 0.2791,
            'totalFlux': 198.5025482178,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9914398193
          },
          {
            'recordTime': 1535976000000,
            'plusTotalFlux': 339.5140380859,
            'flux': 0.0,
            'pressure': 0.2812,
            'totalFlux': 198.5172424316,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 140.9967956543
          },
          {
            'recordTime': 1535976300000,
            'plusTotalFlux': 339.5183105469,
            'flux': 0.0,
            'pressure': 0.2312,
            'totalFlux': 198.5142059326,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0041046143
          },
          {
            'recordTime': 1535976600000,
            'plusTotalFlux': 339.5292053223,
            'flux': -0.8250898123,
            'pressure': 0.2333,
            'totalFlux': 198.5211029053,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.008102417
          },
          {
            'recordTime': 1535976900000,
            'plusTotalFlux': 339.5531311035,
            'flux': 0.0,
            'pressure': 0.2348,
            'totalFlux': 198.5407714844,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0123596191
          },
          {
            'recordTime': 1535977200000,
            'plusTotalFlux': 339.5531311035,
            'flux': 0.0,
            'pressure': 0.2379,
            'totalFlux': 198.5377044678,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0154266357
          },
          {
            'recordTime': 1535977500000,
            'plusTotalFlux': 339.5673828125,
            'flux': 0.7560216188,
            'pressure': 0.2342,
            'totalFlux': 198.5519561768,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0154266357
          },
          {
            'recordTime': 1535977800000,
            'plusTotalFlux': 339.5838928223,
            'flux': 0.6807306409,
            'pressure': 0.2339,
            'totalFlux': 198.5619354248,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0219573975
          },
          {
            'recordTime': 1535978100000,
            'plusTotalFlux': 339.6221313477,
            'flux': 0.0,
            'pressure': 0.2373,
            'totalFlux': 198.5985717773,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0235595703
          },
          {
            'recordTime': 1535978400000,
            'plusTotalFlux': 339.63671875,
            'flux': 0.0,
            'pressure': 0.2361,
            'totalFlux': 198.6066894531,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0300292969
          },
          {
            'recordTime': 1535978700000,
            'plusTotalFlux': 339.6581726074,
            'flux': 0.8834371567,
            'pressure': 0.24,
            'totalFlux': 198.6246185303,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0335540771
          },
          {
            'recordTime': 1535979000000,
            'plusTotalFlux': 339.7060852051,
            'flux': 0.7183761597,
            'pressure': 0.2483,
            'totalFlux': 198.6725311279,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0335540771
          },
          {
            'recordTime': 1535979300000,
            'plusTotalFlux': 339.7732543945,
            'flux': 1.2425174713,
            'pressure': 0.2471,
            'totalFlux': 198.7397003174,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0335540771
          },
          {
            'recordTime': 1535979600000,
            'plusTotalFlux': 339.801940918,
            'flux': 0.0,
            'pressure': 0.2525,
            'totalFlux': 198.758392334,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.043548584
          },
          {
            'recordTime': 1535979900000,
            'plusTotalFlux': 339.8107910156,
            'flux': 0.0,
            'pressure': 0.2507,
            'totalFlux': 198.7672424316,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.043548584
          },
          {
            'recordTime': 1535980200000,
            'plusTotalFlux': 339.8118591309,
            'flux': 0.4722324312,
            'pressure': 0.2486,
            'totalFlux': 198.7630004883,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0488586426
          },
          {
            'recordTime': 1535980500000,
            'plusTotalFlux': 339.8385009766,
            'flux': 0.0,
            'pressure': 0.2577,
            'totalFlux': 198.789642334,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0488586426
          },
          {
            'recordTime': 1535980800000,
            'plusTotalFlux': 339.851348877,
            'flux': 0.0,
            'pressure': 0.258,
            'totalFlux': 198.8006591797,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0506896973
          },
          {
            'recordTime': 1535981100000,
            'plusTotalFlux': 339.8729248047,
            'flux': 0.0,
            'pressure': 0.2559,
            'totalFlux': 198.8222351074,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0506896973
          },
          {
            'recordTime': 1535981400000,
            'plusTotalFlux': 339.8755493164,
            'flux': 0.4606491923,
            'pressure': 0.2562,
            'totalFlux': 198.8218688965,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0536804199
          },
          {
            'recordTime': 1535981700000,
            'plusTotalFlux': 339.890838623,
            'flux': 0.0,
            'pressure': 0.259,
            'totalFlux': 198.8371582031,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0536804199
          },
          {
            'recordTime': 1535982000000,
            'plusTotalFlux': 339.8966674805,
            'flux': 0.0,
            'pressure': 0.2486,
            'totalFlux': 198.8429870605,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0536804199
          },
          {
            'recordTime': 1535982300000,
            'plusTotalFlux': 339.9104614258,
            'flux': 0.4326563776,
            'pressure': 0.2492,
            'totalFlux': 198.8524932861,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0579681396
          },
          {
            'recordTime': 1535982600000,
            'plusTotalFlux': 339.9161071777,
            'flux': -0.6831952333,
            'pressure': 0.2553,
            'totalFlux': 198.8539733887,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0621337891
          },
          {
            'recordTime': 1535982900000,
            'plusTotalFlux': 339.9161071777,
            'flux': 0.0,
            'pressure': 0.2519,
            'totalFlux': 198.8512115479,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.0648956299
          },
          {
            'recordTime': 1535983200000,
            'plusTotalFlux': 339.9175415039,
            'flux': -0.408093363,
            'pressure': 0.2504,
            'totalFlux': 198.8371734619,
            'uploadTime': 1535990519840,
            'reverseTotalFlux': 141.080368042
          },
          {
            'recordTime': 1535983500000,
            'plusTotalFlux': 339.9175415039,
            'flux': 0.0,
            'pressure': 0.2477,
            'totalFlux': 198.8289337158,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.0886077881
          },
          {
            'recordTime': 1535983800000,
            'plusTotalFlux': 339.9294128418,
            'flux': -0.3405244648,
            'pressure': 0.2513,
            'totalFlux': 198.8352355957,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.0941772461
          },
          {
            'recordTime': 1535984100000,
            'plusTotalFlux': 339.9311218262,
            'flux': 0.0,
            'pressure': 0.251,
            'totalFlux': 198.8300933838,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1010284424
          },
          {
            'recordTime': 1535984400000,
            'plusTotalFlux': 339.939239502,
            'flux': 0.4316910803,
            'pressure': 0.2519,
            'totalFlux': 198.8313751221,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1078643799
          },
          {
            'recordTime': 1535984700000,
            'plusTotalFlux': 339.9538269043,
            'flux': 0.0,
            'pressure': 0.2528,
            'totalFlux': 198.8400421143,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.11378479
          },
          {
            'recordTime': 1535985000000,
            'plusTotalFlux': 339.9553527832,
            'flux': 0.0,
            'pressure': 0.2544,
            'totalFlux': 198.8415679932,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.11378479
          },
          {
            'recordTime': 1535985300000,
            'plusTotalFlux': 339.9711303711,
            'flux': 0.0,
            'pressure': 0.2541,
            'totalFlux': 198.8573455811,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.11378479
          },
          {
            'recordTime': 1535985600000,
            'plusTotalFlux': 339.9865112305,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 198.8727264404,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.11378479
          },
          {
            'recordTime': 1535985900000,
            'plusTotalFlux': 340.0014953613,
            'flux': 0.0,
            'pressure': 0.2519,
            'totalFlux': 198.8877105713,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.11378479
          },
          {
            'recordTime': 1535986200000,
            'plusTotalFlux': 340.0055236816,
            'flux': 0.0,
            'pressure': 0.2602,
            'totalFlux': 198.8917388916,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.11378479
          },
          {
            'recordTime': 1535986500000,
            'plusTotalFlux': 340.0104064941,
            'flux': 0.0,
            'pressure': 0.2599,
            'totalFlux': 198.8966217041,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.11378479
          },
          {
            'recordTime': 1535986800000,
            'plusTotalFlux': 340.0104064941,
            'flux': 0.0,
            'pressure': 0.2596,
            'totalFlux': 198.8966217041,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.11378479
          },
          {
            'recordTime': 1535987100000,
            'plusTotalFlux': 340.0104064941,
            'flux': 0.0,
            'pressure': 0.2638,
            'totalFlux': 198.894821167,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1155853271
          },
          {
            'recordTime': 1535987400000,
            'plusTotalFlux': 340.0143432617,
            'flux': 0.0,
            'pressure': 0.2657,
            'totalFlux': 198.8987579346,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1155853271
          },
          {
            'recordTime': 1535987700000,
            'plusTotalFlux': 340.0196228027,
            'flux': 0.0,
            'pressure': 0.2657,
            'totalFlux': 198.9040374756,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1155853271
          },
          {
            'recordTime': 1535988000000,
            'plusTotalFlux': 340.0196228027,
            'flux': 0.0,
            'pressure': 0.2666,
            'totalFlux': 198.9040374756,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1155853271
          },
          {
            'recordTime': 1535988300000,
            'plusTotalFlux': 340.0248718262,
            'flux': 0.0,
            'pressure': 0.2706,
            'totalFlux': 198.909286499,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1155853271
          },
          {
            'recordTime': 1535988600000,
            'plusTotalFlux': 340.0332336426,
            'flux': 0.0,
            'pressure': 0.2641,
            'totalFlux': 198.9176483154,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1155853271
          },
          {
            'recordTime': 1535988900000,
            'plusTotalFlux': 340.0374145508,
            'flux': 0.0,
            'pressure': 0.2754,
            'totalFlux': 198.917175293,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1202392578
          },
          {
            'recordTime': 1535989200000,
            'plusTotalFlux': 340.0445556641,
            'flux': 0.0,
            'pressure': 0.2754,
            'totalFlux': 198.9243164062,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1202392578
          },
          {
            'recordTime': 1535989500000,
            'plusTotalFlux': 340.0445556641,
            'flux': 0.0,
            'pressure': 0.2849,
            'totalFlux': 198.9243164062,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1202392578
          },
          {
            'recordTime': 1535989800000,
            'plusTotalFlux': 340.0527954102,
            'flux': -0.4920717776,
            'pressure': 0.2837,
            'totalFlux': 198.9295043945,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1232910156
          },
          {
            'recordTime': 1535990100000,
            'plusTotalFlux': 340.0527954102,
            'flux': 0.0,
            'pressure': 0.2235,
            'totalFlux': 198.9148254395,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1379699707
          },
          {
            'recordTime': 1535990400000,
            'plusTotalFlux': 340.062713623,
            'flux': 0.3824623823,
            'pressure': 0.2242,
            'totalFlux': 198.9247436523,
            'uploadTime': 1535990526893,
            'reverseTotalFlux': 141.1379699707
          },
          {
            'recordTime': 1535990700000,
            'plusTotalFlux': 340.0669250488,
            'flux': 0.0,
            'pressure': 0.2275,
            'totalFlux': 198.9289550781,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1379699707
          },
          {
            'recordTime': 1535991000000,
            'plusTotalFlux': 340.0697631836,
            'flux': 0.0,
            'pressure': 0.222,
            'totalFlux': 198.9182739258,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1514892578
          },
          {
            'recordTime': 1535991300000,
            'plusTotalFlux': 340.0757446289,
            'flux': 0.0,
            'pressure': 0.2315,
            'totalFlux': 198.9242553711,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1514892578
          },
          {
            'recordTime': 1535991600000,
            'plusTotalFlux': 340.0968322754,
            'flux': -0.6397580504,
            'pressure': 0.2342,
            'totalFlux': 198.9433746338,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1534576416
          },
          {
            'recordTime': 1535991900000,
            'plusTotalFlux': 340.0968322754,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 198.9381866455,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1586456299
          },
          {
            'recordTime': 1535992200000,
            'plusTotalFlux': 340.1154174805,
            'flux': 0.0,
            'pressure': 0.2507,
            'totalFlux': 198.9567718506,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1586456299
          },
          {
            'recordTime': 1535992500000,
            'plusTotalFlux': 340.1286010742,
            'flux': 0.3390252292,
            'pressure': 0.2544,
            'totalFlux': 198.9699554443,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1586456299
          },
          {
            'recordTime': 1535992800000,
            'plusTotalFlux': 340.1449279785,
            'flux': 0.0,
            'pressure': 0.2608,
            'totalFlux': 198.9862823486,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1586456299
          },
          {
            'recordTime': 1535993100000,
            'plusTotalFlux': 340.1620178223,
            'flux': 0.0,
            'pressure': 0.2648,
            'totalFlux': 199.0019378662,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1600799561
          },
          {
            'recordTime': 1535993400000,
            'plusTotalFlux': 340.1633911133,
            'flux': 0.0,
            'pressure': 0.2678,
            'totalFlux': 199.0033111572,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1600799561
          },
          {
            'recordTime': 1535993700000,
            'plusTotalFlux': 340.1633911133,
            'flux': 0.0,
            'pressure': 0.2632,
            'totalFlux': 199.0033111572,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1600799561
          },
          {
            'recordTime': 1535994000000,
            'plusTotalFlux': 340.1871032715,
            'flux': 0.5127737522,
            'pressure': 0.2144,
            'totalFlux': 199.0039672852,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1831359863
          },
          {
            'recordTime': 1535994300000,
            'plusTotalFlux': 340.1957397461,
            'flux': 0.0,
            'pressure': 0.2135,
            'totalFlux': 199.0126037598,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1831359863
          },
          {
            'recordTime': 1535994600000,
            'plusTotalFlux': 340.2130432129,
            'flux': 0.0,
            'pressure': 0.2147,
            'totalFlux': 199.0299072266,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1831359863
          },
          {
            'recordTime': 1535994900000,
            'plusTotalFlux': 340.2130432129,
            'flux': 0.0,
            'pressure': 0.2156,
            'totalFlux': 199.0299072266,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1831359863
          },
          {
            'recordTime': 1535995200000,
            'plusTotalFlux': 340.2257995605,
            'flux': 0.3892192543,
            'pressure': 0.2174,
            'totalFlux': 199.0426635742,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1831359863
          },
          {
            'recordTime': 1535995500000,
            'plusTotalFlux': 340.2296142578,
            'flux': 0.0,
            'pressure': 0.219,
            'totalFlux': 199.0464782715,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1831359863
          },
          {
            'recordTime': 1535995800000,
            'plusTotalFlux': 340.2334899902,
            'flux': 0.0,
            'pressure': 0.2205,
            'totalFlux': 199.0444793701,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1890106201
          },
          {
            'recordTime': 1535996100000,
            'plusTotalFlux': 340.2334899902,
            'flux': 0.0,
            'pressure': 0.219,
            'totalFlux': 199.0444793701,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1890106201
          },
          {
            'recordTime': 1535996400000,
            'plusTotalFlux': 340.2444458008,
            'flux': 0.0,
            'pressure': 0.2214,
            'totalFlux': 199.0540466309,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.1903991699
          },
          {
            'recordTime': 1535996700000,
            'plusTotalFlux': 340.2444458008,
            'flux': -0.4196766019,
            'pressure': 0.219,
            'totalFlux': 199.0416717529,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2027740479
          },
          {
            'recordTime': 1535997000000,
            'plusTotalFlux': 340.2444458008,
            'flux': 0.0,
            'pressure': 0.2187,
            'totalFlux': 199.0410919189,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2033538818
          },
          {
            'recordTime': 1535997300000,
            'plusTotalFlux': 340.2444458008,
            'flux': 0.0,
            'pressure': 0.2208,
            'totalFlux': 199.0410919189,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2033538818
          },
          {
            'recordTime': 1535997600000,
            'plusTotalFlux': 340.2489624023,
            'flux': 0.0,
            'pressure': 0.2214,
            'totalFlux': 199.0456085205,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2033538818
          },
          {
            'recordTime': 1535997900000,
            'plusTotalFlux': 340.2489624023,
            'flux': 0.0,
            'pressure': 0.2226,
            'totalFlux': 199.0456085205,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2033538818
          },
          {
            'recordTime': 1535998200000,
            'plusTotalFlux': 340.2489624023,
            'flux': 0.0,
            'pressure': 0.2223,
            'totalFlux': 199.0456085205,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2033538818
          },
          {
            'recordTime': 1535998500000,
            'plusTotalFlux': 340.2506408691,
            'flux': 0.0,
            'pressure': 0.2171,
            'totalFlux': 199.0458984375,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2047424316
          },
          {
            'recordTime': 1535998800000,
            'plusTotalFlux': 340.2522277832,
            'flux': 0.0,
            'pressure': 0.2266,
            'totalFlux': 199.0474853516,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2047424316
          },
          {
            'recordTime': 1535999100000,
            'plusTotalFlux': 340.2522277832,
            'flux': 0.0,
            'pressure': 0.2235,
            'totalFlux': 199.0474853516,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2047424316
          },
          {
            'recordTime': 1535999400000,
            'plusTotalFlux': 340.2522277832,
            'flux': 0.0,
            'pressure': 0.2257,
            'totalFlux': 199.0474853516,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2047424316
          },
          {
            'recordTime': 1535999700000,
            'plusTotalFlux': 340.2550048828,
            'flux': 0.3969413936,
            'pressure': 0.2263,
            'totalFlux': 199.0443572998,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.210647583
          },
          {
            'recordTime': 1536000000000,
            'plusTotalFlux': 340.2555541992,
            'flux': 0.0,
            'pressure': 0.229,
            'totalFlux': 199.0449066162,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.210647583
          },
          {
            'recordTime': 1536000300000,
            'plusTotalFlux': 340.2555541992,
            'flux': 0.0,
            'pressure': 0.2254,
            'totalFlux': 199.0449066162,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.210647583
          },
          {
            'recordTime': 1536000600000,
            'plusTotalFlux': 340.2555541992,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 199.0449066162,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.210647583
          },
          {
            'recordTime': 1536000900000,
            'plusTotalFlux': 340.2555541992,
            'flux': 0.0,
            'pressure': 0.2275,
            'totalFlux': 199.0449066162,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.210647583
          },
          {
            'recordTime': 1536001200000,
            'plusTotalFlux': 340.2569580078,
            'flux': 0.0,
            'pressure': 0.2199,
            'totalFlux': 199.042175293,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536001500000,
            'plusTotalFlux': 340.2569580078,
            'flux': 0.0,
            'pressure': 0.2303,
            'totalFlux': 199.042175293,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536001800000,
            'plusTotalFlux': 340.2569580078,
            'flux': 0.0,
            'pressure': 0.2287,
            'totalFlux': 199.042175293,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536002100000,
            'plusTotalFlux': 340.2569580078,
            'flux': 0.0,
            'pressure': 0.229,
            'totalFlux': 199.042175293,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536002400000,
            'plusTotalFlux': 340.2569580078,
            'flux': 0.0,
            'pressure': 0.2312,
            'totalFlux': 199.042175293,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536002700000,
            'plusTotalFlux': 340.2569580078,
            'flux': 0.0,
            'pressure': 0.2303,
            'totalFlux': 199.042175293,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536003000000,
            'plusTotalFlux': 340.2569580078,
            'flux': 0.0,
            'pressure': 0.2312,
            'totalFlux': 199.042175293,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536003300000,
            'plusTotalFlux': 340.2569580078,
            'flux': 0.0,
            'pressure': 0.2263,
            'totalFlux': 199.042175293,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536003600000,
            'plusTotalFlux': 340.2569580078,
            'flux': 0.0,
            'pressure': 0.2199,
            'totalFlux': 199.042175293,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536003900000,
            'plusTotalFlux': 340.2627563477,
            'flux': 0.0,
            'pressure': 0.2269,
            'totalFlux': 199.0479736328,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536004200000,
            'plusTotalFlux': 340.2627563477,
            'flux': 0.0,
            'pressure': 0.226,
            'totalFlux': 199.0479736328,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536004500000,
            'plusTotalFlux': 340.2627563477,
            'flux': 0.0,
            'pressure': 0.2257,
            'totalFlux': 199.0479736328,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536004800000,
            'plusTotalFlux': 340.2627563477,
            'flux': 0.0,
            'pressure': 0.226,
            'totalFlux': 199.0479736328,
            'uploadTime': 1536012137033,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536005100000,
            'plusTotalFlux': 340.2627563477,
            'flux': 0.0,
            'pressure': 0.2287,
            'totalFlux': 199.0479736328,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536005400000,
            'plusTotalFlux': 340.2627563477,
            'flux': 0.0,
            'pressure': 0.2297,
            'totalFlux': 199.0479736328,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536005700000,
            'plusTotalFlux': 340.2627563477,
            'flux': 0.0,
            'pressure': 0.23,
            'totalFlux': 199.0479736328,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536006000000,
            'plusTotalFlux': 340.2674255371,
            'flux': 0.4490659237,
            'pressure': 0.23,
            'totalFlux': 199.0526428223,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536006300000,
            'plusTotalFlux': 340.2733764648,
            'flux': 0.0,
            'pressure': 0.2248,
            'totalFlux': 199.05859375,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536006600000,
            'plusTotalFlux': 340.2733764648,
            'flux': 0.0,
            'pressure': 0.2306,
            'totalFlux': 199.05859375,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536006900000,
            'plusTotalFlux': 340.3146972656,
            'flux': 0.7984934449,
            'pressure': 0.2293,
            'totalFlux': 199.0999145508,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536007200000,
            'plusTotalFlux': 340.3396911621,
            'flux': 0.0,
            'pressure': 0.2309,
            'totalFlux': 199.1249084473,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536007500000,
            'plusTotalFlux': 340.3396911621,
            'flux': 0.0,
            'pressure': 0.2275,
            'totalFlux': 199.1249084473,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536007800000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2263,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536008100000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2266,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536008400000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2257,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536008700000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2174,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536009000000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536009300000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2217,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536009600000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536009900000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2229,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536010200000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2226,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536010500000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.222,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536010800000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2229,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536011100000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.222,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536011400000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2171,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536011700000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2214,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536012000000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2199,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536012144463,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536012300000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2205,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536012600000,
            'plusTotalFlux': 340.3413085938,
            'flux': 0.0,
            'pressure': 0.2223,
            'totalFlux': 199.1265258789,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536012900000,
            'plusTotalFlux': 340.3427124023,
            'flux': 0.0,
            'pressure': 0.2208,
            'totalFlux': 199.1279296875,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536013200000,
            'plusTotalFlux': 340.3427124023,
            'flux': 0.0,
            'pressure': 0.2196,
            'totalFlux': 199.1279296875,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536013500000,
            'plusTotalFlux': 340.3427124023,
            'flux': 0.0,
            'pressure': 0.2208,
            'totalFlux': 199.1279296875,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536013800000,
            'plusTotalFlux': 340.3427124023,
            'flux': 0.0,
            'pressure': 0.2141,
            'totalFlux': 199.1279296875,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536014100000,
            'plusTotalFlux': 340.3427124023,
            'flux': 0.0,
            'pressure': 0.2211,
            'totalFlux': 199.1279296875,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536014400000,
            'plusTotalFlux': 340.3427124023,
            'flux': 0.0,
            'pressure': 0.2181,
            'totalFlux': 199.1279296875,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536014700000,
            'plusTotalFlux': 340.3456726074,
            'flux': 0.4403785467,
            'pressure': 0.2165,
            'totalFlux': 199.1308898926,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536015000000,
            'plusTotalFlux': 340.3480224609,
            'flux': 0.0,
            'pressure': 0.2162,
            'totalFlux': 199.1332397461,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536015300000,
            'plusTotalFlux': 340.3480224609,
            'flux': 0.0,
            'pressure': 0.1775,
            'totalFlux': 199.1332397461,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2147827148
          },
          {
            'recordTime': 1536015600000,
            'plusTotalFlux': 340.3480224609,
            'flux': 0.0,
            'pressure': 0.1955,
            'totalFlux': 199.1244812012,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2235412598
          },
          {
            'recordTime': 1536015900000,
            'plusTotalFlux': 340.3571166992,
            'flux': 0.0,
            'pressure': 0.1768,
            'totalFlux': 199.1335754395,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2235412598
          },
          {
            'recordTime': 1536016200000,
            'plusTotalFlux': 340.3571166992,
            'flux': 0.0,
            'pressure': 0.1713,
            'totalFlux': 199.1299285889,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2271881104
          },
          {
            'recordTime': 1536016500000,
            'plusTotalFlux': 340.3571166992,
            'flux': 0.0,
            'pressure': 0.1521,
            'totalFlux': 199.1299285889,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2271881104
          },
          {
            'recordTime': 1536016800000,
            'plusTotalFlux': 340.3571166992,
            'flux': 0.0,
            'pressure': 0.1561,
            'totalFlux': 199.1299285889,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2271881104
          },
          {
            'recordTime': 1536017100000,
            'plusTotalFlux': 340.3571166992,
            'flux': 0.0,
            'pressure': 0.1491,
            'totalFlux': 199.1299285889,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2271881104
          },
          {
            'recordTime': 1536017400000,
            'plusTotalFlux': 340.3571166992,
            'flux': 0.0,
            'pressure': 0.1375,
            'totalFlux': 199.1299285889,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2271881104
          },
          {
            'recordTime': 1536017700000,
            'plusTotalFlux': 340.3571166992,
            'flux': 0.0,
            'pressure': 0.1671,
            'totalFlux': 199.1299285889,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2271881104
          },
          {
            'recordTime': 1536018000000,
            'plusTotalFlux': 340.3922729492,
            'flux': 0.0,
            'pressure': 0.2214,
            'totalFlux': 199.1650848389,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2271881104
          },
          {
            'recordTime': 1536018300000,
            'plusTotalFlux': 340.3989562988,
            'flux': 0.0,
            'pressure': 0.2162,
            'totalFlux': 199.1717681885,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2271881104
          },
          {
            'recordTime': 1536018600000,
            'plusTotalFlux': 340.3989562988,
            'flux': 0.0,
            'pressure': 0.2101,
            'totalFlux': 199.1599121094,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2390441895
          },
          {
            'recordTime': 1536018900000,
            'plusTotalFlux': 340.4122619629,
            'flux': 0.7676048875,
            'pressure': 0.1927,
            'totalFlux': 199.1614227295,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2508392334
          },
          {
            'recordTime': 1536019200000,
            'plusTotalFlux': 340.4185791016,
            'flux': 0.0,
            'pressure': 0.1921,
            'totalFlux': 199.1649169922,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2536621094
          },
          {
            'recordTime': 1536019500000,
            'plusTotalFlux': 340.4498596191,
            'flux': 0.0,
            'pressure': 0.2266,
            'totalFlux': 199.1961975098,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2536621094
          },
          {
            'recordTime': 1536019800000,
            'plusTotalFlux': 340.4533691406,
            'flux': 0.5282180309,
            'pressure': 0.2245,
            'totalFlux': 199.1904144287,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2629547119
          },
          {
            'recordTime': 1536020100000,
            'plusTotalFlux': 340.4693908691,
            'flux': 0.0,
            'pressure': 0.2153,
            'totalFlux': 199.2064361572,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2629547119
          },
          {
            'recordTime': 1536020400000,
            'plusTotalFlux': 340.4693908691,
            'flux': 0.0,
            'pressure': 0.2043,
            'totalFlux': 199.2064361572,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2629547119
          },
          {
            'recordTime': 1536020700000,
            'plusTotalFlux': 340.4693908691,
            'flux': 0.0,
            'pressure': 0.2016,
            'totalFlux': 199.2064361572,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2629547119
          },
          {
            'recordTime': 1536021000000,
            'plusTotalFlux': 340.4693908691,
            'flux': 0.0,
            'pressure': 0.1964,
            'totalFlux': 199.1773071289,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2920837402
          },
          {
            'recordTime': 1536021300000,
            'plusTotalFlux': 340.4721374512,
            'flux': 0.0,
            'pressure': 0.1967,
            'totalFlux': 199.1800537109,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2920837402
          },
          {
            'recordTime': 1536021600000,
            'plusTotalFlux': 340.4926757812,
            'flux': 0.0,
            'pressure': 0.1945,
            'totalFlux': 199.200592041,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2920837402
          },
          {
            'recordTime': 1536021900000,
            'plusTotalFlux': 340.4991760254,
            'flux': 0.0,
            'pressure': 0.19,
            'totalFlux': 199.2070922852,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2920837402
          },
          {
            'recordTime': 1536022200000,
            'plusTotalFlux': 340.5326538086,
            'flux': 0.4799546003,
            'pressure': 0.1945,
            'totalFlux': 199.2405700684,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2920837402
          },
          {
            'recordTime': 1536022500000,
            'plusTotalFlux': 340.5714111328,
            'flux': 0.0,
            'pressure': 0.1741,
            'totalFlux': 199.275604248,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.2958068848
          },
          {
            'recordTime': 1536022800000,
            'plusTotalFlux': 340.622833252,
            'flux': 0.4114204645,
            'pressure': 0.2174,
            'totalFlux': 199.3270263672,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.2958068848
          },
          {
            'recordTime': 1536023100000,
            'plusTotalFlux': 340.6306152344,
            'flux': 0.0,
            'pressure': 0.2138,
            'totalFlux': 199.323425293,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.3071899414
          },
          {
            'recordTime': 1536023400000,
            'plusTotalFlux': 340.6775817871,
            'flux': 0.3535042405,
            'pressure': 0.2095,
            'totalFlux': 199.3703918457,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.3071899414
          },
          {
            'recordTime': 1536023700000,
            'plusTotalFlux': 340.693572998,
            'flux': 0.0,
            'pressure': 0.2077,
            'totalFlux': 199.3863830566,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.3071899414
          },
          {
            'recordTime': 1536024000000,
            'plusTotalFlux': 340.7318725586,
            'flux': 0.6228144169,
            'pressure': 0.2007,
            'totalFlux': 199.41065979,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.3212127686
          },
          {
            'recordTime': 1536024300000,
            'plusTotalFlux': 340.7770996094,
            'flux': 0.7907713652,
            'pressure': 0.2003,
            'totalFlux': 199.4558868408,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.3212127686
          },
          {
            'recordTime': 1536024600000,
            'plusTotalFlux': 340.7803649902,
            'flux': 0.0,
            'pressure': 0.2046,
            'totalFlux': 199.4591522217,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.3212127686
          },
          {
            'recordTime': 1536024900000,
            'plusTotalFlux': 340.796081543,
            'flux': 0.8371042013,
            'pressure': 0.2019,
            'totalFlux': 199.4748687744,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.3212127686
          },
          {
            'recordTime': 1536025200000,
            'plusTotalFlux': 340.8266601562,
            'flux': 0.0,
            'pressure': 0.2007,
            'totalFlux': 199.5054473877,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.3212127686
          },
          {
            'recordTime': 1536025500000,
            'plusTotalFlux': 340.8290710449,
            'flux': 0.347712636,
            'pressure': 0.19,
            'totalFlux': 199.5048828125,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.3241882324
          },
          {
            'recordTime': 1536025800000,
            'plusTotalFlux': 340.831237793,
            'flux': 0.0,
            'pressure': 0.1796,
            'totalFlux': 199.4910736084,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.3401641846
          },
          {
            'recordTime': 1536026100000,
            'plusTotalFlux': 340.8499450684,
            'flux': 0.0,
            'pressure': 0.1765,
            'totalFlux': 199.5097808838,
            'uploadTime': 1536033763087,
            'reverseTotalFlux': 141.3401641846
          },
          {
            'recordTime': 1536026400000,
            'plusTotalFlux': 340.8550415039,
            'flux': 0.0,
            'pressure': 0.1692,
            'totalFlux': 199.5061187744,
            'uploadTime': 1536055326183,
            'reverseTotalFlux': 141.3489227295
          },
          {
            'recordTime': 1536026700000,
            'plusTotalFlux': 340.8660583496,
            'flux': 0.0,
            'pressure': 0.171,
            'totalFlux': 199.5171356201,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3489227295
          },
          {
            'recordTime': 1536027000000,
            'plusTotalFlux': 340.8772277832,
            'flux': 0.0,
            'pressure': 0.1637,
            'totalFlux': 199.5243225098,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3529052734
          },
          {
            'recordTime': 1536027300000,
            'plusTotalFlux': 340.8974914551,
            'flux': 0.0,
            'pressure': 0.1616,
            'totalFlux': 199.5445861816,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3529052734
          },
          {
            'recordTime': 1536027600000,
            'plusTotalFlux': 340.9039916992,
            'flux': 0.3679833114,
            'pressure': 0.1582,
            'totalFlux': 199.5510864258,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3529052734
          },
          {
            'recordTime': 1536027900000,
            'plusTotalFlux': 340.9101257324,
            'flux': 0.0,
            'pressure': 0.1555,
            'totalFlux': 199.5558319092,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3542938232
          },
          {
            'recordTime': 1536028200000,
            'plusTotalFlux': 340.9291381836,
            'flux': 0.0,
            'pressure': 0.1643,
            'totalFlux': 199.5748443604,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3542938232
          },
          {
            'recordTime': 1536028500000,
            'plusTotalFlux': 340.9363098145,
            'flux': 0.3911497891,
            'pressure': 0.1665,
            'totalFlux': 199.5820159912,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3542938232
          },
          {
            'recordTime': 1536028800000,
            'plusTotalFlux': 340.9564819336,
            'flux': 0.5475233197,
            'pressure': 0.1604,
            'totalFlux': 199.6021881104,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3542938232
          },
          {
            'recordTime': 1536029100000,
            'plusTotalFlux': 340.9723510742,
            'flux': 0.0,
            'pressure': 0.1646,
            'totalFlux': 199.618057251,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3542938232
          },
          {
            'recordTime': 1536029400000,
            'plusTotalFlux': 341.0075378418,
            'flux': 0.0,
            'pressure': 0.1823,
            'totalFlux': 199.6488800049,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3586578369
          },
          {
            'recordTime': 1536029700000,
            'plusTotalFlux': 341.0344848633,
            'flux': -0.3559688032,
            'pressure': 0.1808,
            'totalFlux': 199.6749420166,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3595428467
          },
          {
            'recordTime': 1536030000000,
            'plusTotalFlux': 341.0625610352,
            'flux': 0.4722324312,
            'pressure': 0.1833,
            'totalFlux': 199.6951141357,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3674468994
          },
          {
            'recordTime': 1536030300000,
            'plusTotalFlux': 341.0633544922,
            'flux': 0.0,
            'pressure': 0.1875,
            'totalFlux': 199.6959075928,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3674468994
          },
          {
            'recordTime': 1536030600000,
            'plusTotalFlux': 341.0723266602,
            'flux': 1.2946419716,
            'pressure': 0.1833,
            'totalFlux': 199.6937408447,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3785858154
          },
          {
            'recordTime': 1536030900000,
            'plusTotalFlux': 341.1195068359,
            'flux': 0.6083353758,
            'pressure': 0.1756,
            'totalFlux': 199.7409210205,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3785858154
          },
          {
            'recordTime': 1536031200000,
            'plusTotalFlux': 341.1480407715,
            'flux': 0.3592959046,
            'pressure': 0.1805,
            'totalFlux': 199.7694549561,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3785858154
          },
          {
            'recordTime': 1536031500000,
            'plusTotalFlux': 341.1876525879,
            'flux': 0.3892192245,
            'pressure': 0.1875,
            'totalFlux': 199.798538208,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3891143799
          },
          {
            'recordTime': 1536031800000,
            'plusTotalFlux': 341.2064208984,
            'flux': 0.0,
            'pressure': 0.1836,
            'totalFlux': 199.8173065186,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3891143799
          },
          {
            'recordTime': 1536032100000,
            'plusTotalFlux': 341.2333679199,
            'flux': -0.3443855047,
            'pressure': 0.1875,
            'totalFlux': 199.8433837891,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3899841309
          },
          {
            'recordTime': 1536032400000,
            'plusTotalFlux': 341.2712402344,
            'flux': 0.0,
            'pressure': 0.1875,
            'totalFlux': 199.8806915283,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3905487061
          },
          {
            'recordTime': 1536032700000,
            'plusTotalFlux': 341.2736816406,
            'flux': 0.414316237,
            'pressure': 0.1884,
            'totalFlux': 199.8831329346,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3905487061
          },
          {
            'recordTime': 1536033000000,
            'plusTotalFlux': 341.3054199219,
            'flux': 0.0,
            'pressure': 0.1875,
            'totalFlux': 199.9148712158,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3905487061
          },
          {
            'recordTime': 1536033300000,
            'plusTotalFlux': 341.3115234375,
            'flux': -0.4023016989,
            'pressure': 0.1857,
            'totalFlux': 199.9130401611,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3984832764
          },
          {
            'recordTime': 1536033600000,
            'plusTotalFlux': 341.3153991699,
            'flux': 0.0,
            'pressure': 0.1927,
            'totalFlux': 199.9162445068,
            'uploadTime': 1536033770820,
            'reverseTotalFlux': 141.3991546631
          },
          {
            'recordTime': 1536033900000,
            'plusTotalFlux': 341.3270568848,
            'flux': 0.0,
            'pressure': 0.1976,
            'totalFlux': 199.9279022217,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3991546631
          },
          {
            'recordTime': 1536034200000,
            'plusTotalFlux': 341.3270568848,
            'flux': 0.0,
            'pressure': 0.2061,
            'totalFlux': 199.9279022217,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.3991546631
          },
          {
            'recordTime': 1536034500000,
            'plusTotalFlux': 341.3270568848,
            'flux': 0.0,
            'pressure': 0.2089,
            'totalFlux': 199.9122924805,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4147644043
          },
          {
            'recordTime': 1536034800000,
            'plusTotalFlux': 341.3645935059,
            'flux': 0.5620024204,
            'pressure': 0.2144,
            'totalFlux': 199.9457550049,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.418838501
          },
          {
            'recordTime': 1536035100000,
            'plusTotalFlux': 341.3886108398,
            'flux': 0.4085246325,
            'pressure': 0.2205,
            'totalFlux': 199.9697723389,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.418838501
          },
          {
            'recordTime': 1536035400000,
            'plusTotalFlux': 341.4106140137,
            'flux': 0.0,
            'pressure': 0.222,
            'totalFlux': 199.9851226807,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.425491333
          },
          {
            'recordTime': 1536035700000,
            'plusTotalFlux': 341.4224243164,
            'flux': -0.4428430498,
            'pressure': 0.2318,
            'totalFlux': 199.9808502197,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4415740967
          },
          {
            'recordTime': 1536036000000,
            'plusTotalFlux': 341.4418640137,
            'flux': 0.8052503467,
            'pressure': 0.2419,
            'totalFlux': 199.9896392822,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4522247314
          },
          {
            'recordTime': 1536036300000,
            'plusTotalFlux': 341.4880981445,
            'flux': 0.3409557641,
            'pressure': 0.2504,
            'totalFlux': 200.0358734131,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4522247314
          },
          {
            'recordTime': 1536036600000,
            'plusTotalFlux': 341.5112915039,
            'flux': 0.0,
            'pressure': 0.2489,
            'totalFlux': 200.0590667725,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4522247314
          },
          {
            'recordTime': 1536036900000,
            'plusTotalFlux': 341.5244445801,
            'flux': 0.0,
            'pressure': 0.2568,
            'totalFlux': 200.0707550049,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4536895752
          },
          {
            'recordTime': 1536037200000,
            'plusTotalFlux': 341.5570068359,
            'flux': 0.5533150434,
            'pressure': 0.2632,
            'totalFlux': 200.0948486328,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4621582031
          },
          {
            'recordTime': 1536037500000,
            'plusTotalFlux': 341.5960388184,
            'flux': 0.0,
            'pressure': 0.2638,
            'totalFlux': 200.1338806152,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4621582031
          },
          {
            'recordTime': 1536037800000,
            'plusTotalFlux': 341.6236572266,
            'flux': 0.0,
            'pressure': 0.2702,
            'totalFlux': 200.159866333,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4637908936
          },
          {
            'recordTime': 1536038100000,
            'plusTotalFlux': 341.648651123,
            'flux': 1.6102852821,
            'pressure': 0.2663,
            'totalFlux': 200.1771392822,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4715118408
          },
          {
            'recordTime': 1536038400000,
            'plusTotalFlux': 341.6913757324,
            'flux': 0.0,
            'pressure': 0.2678,
            'totalFlux': 200.2198638916,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4715118408
          },
          {
            'recordTime': 1536038700000,
            'plusTotalFlux': 341.7110900879,
            'flux': 0.0,
            'pressure': 0.262,
            'totalFlux': 200.2205200195,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4905700684
          },
          {
            'recordTime': 1536039000000,
            'plusTotalFlux': 341.720123291,
            'flux': 0.0,
            'pressure': 0.2727,
            'totalFlux': 200.2215270996,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.4985961914
          },
          {
            'recordTime': 1536039300000,
            'plusTotalFlux': 341.7266540527,
            'flux': 0.0,
            'pressure': 0.2715,
            'totalFlux': 200.2265777588,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536039600000,
            'plusTotalFlux': 341.7481689453,
            'flux': 0.5330443978,
            'pressure': 0.2898,
            'totalFlux': 200.2480926514,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536039900000,
            'plusTotalFlux': 341.7549743652,
            'flux': 0.0,
            'pressure': 0.2907,
            'totalFlux': 200.2548980713,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536040200000,
            'plusTotalFlux': 341.776184082,
            'flux': 0.5475233197,
            'pressure': 0.2953,
            'totalFlux': 200.2761077881,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536040500000,
            'plusTotalFlux': 341.8438415527,
            'flux': 0.5330443978,
            'pressure': 0.2959,
            'totalFlux': 200.3437652588,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536040800000,
            'plusTotalFlux': 341.8795776367,
            'flux': 0.4548575878,
            'pressure': 0.2956,
            'totalFlux': 200.3795013428,
            'uploadTime': 1536055332860,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536041100000,
            'plusTotalFlux': 341.949432373,
            'flux': 1.2454133034,
            'pressure': 0.2919,
            'totalFlux': 200.4493560791,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536041400000,
            'plusTotalFlux': 342.067199707,
            'flux': 0.8573749065,
            'pressure': 0.2904,
            'totalFlux': 200.5671234131,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536041700000,
            'plusTotalFlux': 342.1174621582,
            'flux': 0.8699233532,
            'pressure': 0.295,
            'totalFlux': 200.6173858643,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536042000000,
            'plusTotalFlux': 342.1373901367,
            'flux': 0.7241677046,
            'pressure': 0.2931,
            'totalFlux': 200.6373138428,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536042300000,
            'plusTotalFlux': 342.1968078613,
            'flux': 0.9587282538,
            'pressure': 0.2959,
            'totalFlux': 200.6967315674,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536042600000,
            'plusTotalFlux': 342.2202148438,
            'flux': 1.2541006804,
            'pressure': 0.2968,
            'totalFlux': 200.7201385498,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536042900000,
            'plusTotalFlux': 342.3312072754,
            'flux': 0.842895925,
            'pressure': 0.302,
            'totalFlux': 200.8311309814,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5000762939
          },
          {
            'recordTime': 1536043200000,
            'plusTotalFlux': 342.3466491699,
            'flux': 0.0,
            'pressure': 0.3069,
            'totalFlux': 200.8275146484,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5191345215
          },
          {
            'recordTime': 1536043500000,
            'plusTotalFlux': 342.3466491699,
            'flux': 0.0,
            'pressure': 0.3087,
            'totalFlux': 200.8275146484,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5191345215
          },
          {
            'recordTime': 1536043800000,
            'plusTotalFlux': 342.3466491699,
            'flux': 0.0,
            'pressure': 0.2742,
            'totalFlux': 200.8207092285,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5259399414
          },
          {
            'recordTime': 1536044100000,
            'plusTotalFlux': 342.3481750488,
            'flux': 0.0,
            'pressure': 0.2617,
            'totalFlux': 200.8167724609,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5314025879
          },
          {
            'recordTime': 1536044400000,
            'plusTotalFlux': 342.3481750488,
            'flux': 0.0,
            'pressure': 0.2663,
            'totalFlux': 200.7986602783,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5495147705
          },
          {
            'recordTime': 1536044700000,
            'plusTotalFlux': 342.3481750488,
            'flux': 0.0,
            'pressure': 0.2644,
            'totalFlux': 200.7986602783,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5495147705
          },
          {
            'recordTime': 1536045000000,
            'plusTotalFlux': 342.3653259277,
            'flux': 0.0,
            'pressure': 0.2538,
            'totalFlux': 200.8158111572,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5495147705
          },
          {
            'recordTime': 1536045300000,
            'plusTotalFlux': 342.3653259277,
            'flux': 0.0,
            'pressure': 0.2467,
            'totalFlux': 200.8144226074,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5509033203
          },
          {
            'recordTime': 1536045600000,
            'plusTotalFlux': 342.3723144531,
            'flux': 0.0,
            'pressure': 0.2471,
            'totalFlux': 200.8144989014,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5578155518
          },
          {
            'recordTime': 1536045900000,
            'plusTotalFlux': 342.3742675781,
            'flux': 0.0,
            'pressure': 0.2422,
            'totalFlux': 200.8164520264,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5578155518
          },
          {
            'recordTime': 1536046200000,
            'plusTotalFlux': 342.3848571777,
            'flux': 0.6546683311,
            'pressure': 0.2385,
            'totalFlux': 200.827041626,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5578155518
          },
          {
            'recordTime': 1536046500000,
            'plusTotalFlux': 342.4160766602,
            'flux': 1.3004336357,
            'pressure': 0.2309,
            'totalFlux': 200.8510131836,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5650634766
          },
          {
            'recordTime': 1536046800000,
            'plusTotalFlux': 342.4509887695,
            'flux': -0.5499879718,
            'pressure': 0.2211,
            'totalFlux': 200.884552002,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5664367676
          },
          {
            'recordTime': 1536047100000,
            'plusTotalFlux': 342.486328125,
            'flux': 0.6836263537,
            'pressure': 0.2174,
            'totalFlux': 200.9168548584,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5694732666
          },
          {
            'recordTime': 1536047400000,
            'plusTotalFlux': 342.5340270996,
            'flux': 1.0446373224,
            'pressure': 0.2126,
            'totalFlux': 200.964553833,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5694732666
          },
          {
            'recordTime': 1536047700000,
            'plusTotalFlux': 342.5553588867,
            'flux': 0.0,
            'pressure': 0.2055,
            'totalFlux': 200.9766082764,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5787506104
          },
          {
            'recordTime': 1536048000000,
            'plusTotalFlux': 342.5803222656,
            'flux': 0.0,
            'pressure': 0.2107,
            'totalFlux': 201.0015716553,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5787506104
          },
          {
            'recordTime': 1536048300000,
            'plusTotalFlux': 342.5803222656,
            'flux': 0.0,
            'pressure': 0.1985,
            'totalFlux': 201.0015716553,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5787506104
          },
          {
            'recordTime': 1536048600000,
            'plusTotalFlux': 342.5936279297,
            'flux': 0.0,
            'pressure': 0.1994,
            'totalFlux': 201.0133972168,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5802307129
          },
          {
            'recordTime': 1536048900000,
            'plusTotalFlux': 342.6007995605,
            'flux': 0.0,
            'pressure': 0.1894,
            'totalFlux': 201.0189971924,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5818023682
          },
          {
            'recordTime': 1536049200000,
            'plusTotalFlux': 342.6234741211,
            'flux': 0.0,
            'pressure': 0.1897,
            'totalFlux': 201.0397796631,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.583694458
          },
          {
            'recordTime': 1536049500000,
            'plusTotalFlux': 342.6249694824,
            'flux': -0.3733436763,
            'pressure': 0.186,
            'totalFlux': 201.0339813232,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.5909881592
          },
          {
            'recordTime': 1536049800000,
            'plusTotalFlux': 342.6249694824,
            'flux': 0.0,
            'pressure': 0.179,
            'totalFlux': 201.0180511475,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.606918335
          },
          {
            'recordTime': 1536050100000,
            'plusTotalFlux': 342.6413574219,
            'flux': 0.0,
            'pressure': 0.1854,
            'totalFlux': 201.0344390869,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.606918335
          },
          {
            'recordTime': 1536050400000,
            'plusTotalFlux': 342.6413574219,
            'flux': 0.0,
            'pressure': 0.1787,
            'totalFlux': 201.0344390869,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.606918335
          },
          {
            'recordTime': 1536050700000,
            'plusTotalFlux': 342.6512145996,
            'flux': -0.474696964,
            'pressure': 0.1781,
            'totalFlux': 201.0285339355,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6226806641
          },
          {
            'recordTime': 1536051000000,
            'plusTotalFlux': 342.6572875977,
            'flux': 0.0,
            'pressure': 0.1732,
            'totalFlux': 201.0148468018,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6424407959
          },
          {
            'recordTime': 1536051300000,
            'plusTotalFlux': 342.6572875977,
            'flux': 0.0,
            'pressure': 0.1762,
            'totalFlux': 200.9996337891,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6576538086
          },
          {
            'recordTime': 1536051600000,
            'plusTotalFlux': 342.6618347168,
            'flux': 0.4809198976,
            'pressure': 0.2104,
            'totalFlux': 201.0041809082,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6576538086
          },
          {
            'recordTime': 1536051900000,
            'plusTotalFlux': 342.6839599609,
            'flux': 0.5706899166,
            'pressure': 0.2074,
            'totalFlux': 201.0208587646,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6631011963
          },
          {
            'recordTime': 1536052200000,
            'plusTotalFlux': 342.7095031738,
            'flux': 1.8100959063,
            'pressure': 0.2046,
            'totalFlux': 201.0464019775,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6631011963
          },
          {
            'recordTime': 1536052500000,
            'plusTotalFlux': 342.7305908203,
            'flux': 0.5185653567,
            'pressure': 0.2138,
            'totalFlux': 201.067489624,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6631011963
          },
          {
            'recordTime': 1536052800000,
            'plusTotalFlux': 342.7496948242,
            'flux': 0.7096886635,
            'pressure': 0.2123,
            'totalFlux': 201.0846710205,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6650238037
          },
          {
            'recordTime': 1536053100000,
            'plusTotalFlux': 342.806640625,
            'flux': 0.3612264097,
            'pressure': 0.2199,
            'totalFlux': 201.1416168213,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6650238037
          },
          {
            'recordTime': 1536053400000,
            'plusTotalFlux': 342.8087463379,
            'flux': -1.2710442543,
            'pressure': 0.2211,
            'totalFlux': 201.1360168457,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6727294922
          },
          {
            'recordTime': 1536053700000,
            'plusTotalFlux': 342.816619873,
            'flux': 0.0,
            'pressure': 0.2199,
            'totalFlux': 201.1241149902,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6925048828
          },
          {
            'recordTime': 1536054000000,
            'plusTotalFlux': 342.816619873,
            'flux': 0.0,
            'pressure': 0.2147,
            'totalFlux': 201.1241149902,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6925048828
          },
          {
            'recordTime': 1536054300000,
            'plusTotalFlux': 342.836151123,
            'flux': 0.3969413936,
            'pressure': 0.2229,
            'totalFlux': 201.1436462402,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.6925048828
          },
          {
            'recordTime': 1536054600000,
            'plusTotalFlux': 342.8385009766,
            'flux': -0.6745077968,
            'pressure': 0.2199,
            'totalFlux': 201.1117095947,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.7267913818
          },
          {
            'recordTime': 1536054900000,
            'plusTotalFlux': 342.8414611816,
            'flux': -0.4356035292,
            'pressure': 0.2214,
            'totalFlux': 201.1005554199,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.7409057617
          },
          {
            'recordTime': 1536055200000,
            'plusTotalFlux': 342.8821716309,
            'flux': 0.7067928314,
            'pressure': 0.2196,
            'totalFlux': 201.1388702393,
            'uploadTime': 1536055398840,
            'reverseTotalFlux': 141.7433013916
          },
          {
            'recordTime': 1536055500000,
            'plusTotalFlux': 342.8987426758,
            'flux': 0.0,
            'pressure': 0.1994,
            'totalFlux': 201.1506958008,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.748046875
          },
          {
            'recordTime': 1536055800000,
            'plusTotalFlux': 342.9131164551,
            'flux': 0.3607437909,
            'pressure': 0.2269,
            'totalFlux': 201.1462402344,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.7668762207
          },
          {
            'recordTime': 1536056100000,
            'plusTotalFlux': 342.9386901855,
            'flux': -0.5413005948,
            'pressure': 0.2263,
            'totalFlux': 201.1706085205,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.768081665
          },
          {
            'recordTime': 1536056400000,
            'plusTotalFlux': 342.9405212402,
            'flux': 0.0,
            'pressure': 0.2297,
            'totalFlux': 201.1557922363,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.7847290039
          },
          {
            'recordTime': 1536056700000,
            'plusTotalFlux': 342.9405212402,
            'flux': 0.0,
            'pressure': 0.2306,
            'totalFlux': 201.1488494873,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.7916717529
          },
          {
            'recordTime': 1536057000000,
            'plusTotalFlux': 342.9405212402,
            'flux': 0.0,
            'pressure': 0.2321,
            'totalFlux': 201.1383514404,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8021697998
          },
          {
            'recordTime': 1536057300000,
            'plusTotalFlux': 342.9437255859,
            'flux': 0.0,
            'pressure': 0.2373,
            'totalFlux': 201.1415557861,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8021697998
          },
          {
            'recordTime': 1536057600000,
            'plusTotalFlux': 342.9485473633,
            'flux': 0.0,
            'pressure': 0.237,
            'totalFlux': 201.1463775635,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8021697998
          },
          {
            'recordTime': 1536057900000,
            'plusTotalFlux': 342.9625549316,
            'flux': 0.0,
            'pressure': 0.2348,
            'totalFlux': 201.1580810547,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.804473877
          },
          {
            'recordTime': 1536058200000,
            'plusTotalFlux': 342.9877624512,
            'flux': 0.8602707386,
            'pressure': 0.2315,
            'totalFlux': 201.1812438965,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8065185547
          },
          {
            'recordTime': 1536058500000,
            'plusTotalFlux': 343.0187683105,
            'flux': 0.0,
            'pressure': 0.2339,
            'totalFlux': 201.2122497559,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8065185547
          },
          {
            'recordTime': 1536058800000,
            'plusTotalFlux': 343.0187683105,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 201.208404541,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8103637695
          },
          {
            'recordTime': 1536059100000,
            'plusTotalFlux': 343.0348815918,
            'flux': 0.0,
            'pressure': 0.2486,
            'totalFlux': 201.2224731445,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8124084473
          },
          {
            'recordTime': 1536059400000,
            'plusTotalFlux': 343.0348815918,
            'flux': 0.0,
            'pressure': 0.2455,
            'totalFlux': 201.2169342041,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8179473877
          },
          {
            'recordTime': 1536059700000,
            'plusTotalFlux': 343.0508728027,
            'flux': 0.5475233793,
            'pressure': 0.2522,
            'totalFlux': 201.232925415,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8179473877
          },
          {
            'recordTime': 1536060000000,
            'plusTotalFlux': 343.0614929199,
            'flux': 0.0,
            'pressure': 0.2544,
            'totalFlux': 201.2418365479,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8196563721
          },
          {
            'recordTime': 1536060300000,
            'plusTotalFlux': 343.0867004395,
            'flux': 1.624764204,
            'pressure': 0.2562,
            'totalFlux': 201.2670440674,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8196563721
          },
          {
            'recordTime': 1536060600000,
            'plusTotalFlux': 343.1349487305,
            'flux': 0.0,
            'pressure': 0.2565,
            'totalFlux': 201.3152923584,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8196563721
          },
          {
            'recordTime': 1536060900000,
            'plusTotalFlux': 343.1700134277,
            'flux': 0.0,
            'pressure': 0.2611,
            'totalFlux': 201.3503570557,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8196563721
          },
          {
            'recordTime': 1536061200000,
            'plusTotalFlux': 343.1888427734,
            'flux': 0.0,
            'pressure': 0.2675,
            'totalFlux': 201.3677215576,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8211212158
          },
          {
            'recordTime': 1536061500000,
            'plusTotalFlux': 343.2272338867,
            'flux': 0.0,
            'pressure': 0.269,
            'totalFlux': 201.4061126709,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8211212158
          },
          {
            'recordTime': 1536061800000,
            'plusTotalFlux': 343.2272338867,
            'flux': 0.0,
            'pressure': 0.2678,
            'totalFlux': 201.404586792,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8226470947
          },
          {
            'recordTime': 1536062100000,
            'plusTotalFlux': 343.2397766113,
            'flux': 0.0,
            'pressure': 0.2721,
            'totalFlux': 201.4171295166,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8226470947
          },
          {
            'recordTime': 1536062400000,
            'plusTotalFlux': 343.2526855469,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 201.4122314453,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8404541016
          },
          {
            'recordTime': 1536062700000,
            'plusTotalFlux': 343.2648010254,
            'flux': 0.4751282036,
            'pressure': 0.2275,
            'totalFlux': 201.4243469238,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8404541016
          },
          {
            'recordTime': 1536063000000,
            'plusTotalFlux': 343.2747497559,
            'flux': -0.5673629045,
            'pressure': 0.2327,
            'totalFlux': 201.4268341064,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8479156494
          },
          {
            'recordTime': 1536063300000,
            'plusTotalFlux': 343.2747497559,
            'flux': 0.0,
            'pressure': 0.2333,
            'totalFlux': 201.4237213135,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8510284424
          },
          {
            'recordTime': 1536063600000,
            'plusTotalFlux': 343.2827148438,
            'flux': 0.0,
            'pressure': 0.2324,
            'totalFlux': 201.4263305664,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8563842773
          },
          {
            'recordTime': 1536063900000,
            'plusTotalFlux': 343.308380127,
            'flux': -0.7787568569,
            'pressure': 0.2385,
            'totalFlux': 201.4443969727,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8639831543
          },
          {
            'recordTime': 1536064200000,
            'plusTotalFlux': 343.308380127,
            'flux': 0.0,
            'pressure': 0.2406,
            'totalFlux': 201.4395751953,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8688049316
          },
          {
            'recordTime': 1536064500000,
            'plusTotalFlux': 343.3188171387,
            'flux': -0.4787510931,
            'pressure': 0.2409,
            'totalFlux': 201.4455718994,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8732452393
          },
          {
            'recordTime': 1536064800000,
            'plusTotalFlux': 343.3225708008,
            'flux': 0.0,
            'pressure': 0.2416,
            'totalFlux': 201.4352722168,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.887298584
          },
          {
            'recordTime': 1536065100000,
            'plusTotalFlux': 343.326385498,
            'flux': 0.0,
            'pressure': 0.2461,
            'totalFlux': 201.4360351562,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8903503418
          },
          {
            'recordTime': 1536065400000,
            'plusTotalFlux': 343.3319702148,
            'flux': 0.0,
            'pressure': 0.2641,
            'totalFlux': 201.441619873,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8903503418
          },
          {
            'recordTime': 1536065700000,
            'plusTotalFlux': 343.3493347168,
            'flux': 0.0,
            'pressure': 0.2764,
            'totalFlux': 201.458984375,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8903503418
          },
          {
            'recordTime': 1536066000000,
            'plusTotalFlux': 343.3507080078,
            'flux': 0.0,
            'pressure': 0.2773,
            'totalFlux': 201.4589233398,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.891784668
          },
          {
            'recordTime': 1536066300000,
            'plusTotalFlux': 343.3594360352,
            'flux': 0.0,
            'pressure': 0.277,
            'totalFlux': 201.4676513672,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.891784668
          },
          {
            'recordTime': 1536066600000,
            'plusTotalFlux': 343.3647766113,
            'flux': 0.7067928314,
            'pressure': 0.2788,
            'totalFlux': 201.4729919434,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.891784668
          },
          {
            'recordTime': 1536066900000,
            'plusTotalFlux': 343.3703308105,
            'flux': 0.0,
            'pressure': 0.2788,
            'totalFlux': 201.4785461426,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.891784668
          },
          {
            'recordTime': 1536067200000,
            'plusTotalFlux': 343.3703308105,
            'flux': 0.0,
            'pressure': 0.2764,
            'totalFlux': 201.4785461426,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.891784668
          },
          {
            'recordTime': 1536067500000,
            'plusTotalFlux': 343.3725280762,
            'flux': 0.0,
            'pressure': 0.2907,
            'totalFlux': 201.4807434082,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.891784668
          },
          {
            'recordTime': 1536067800000,
            'plusTotalFlux': 343.374206543,
            'flux': -0.3965101242,
            'pressure': 0.2901,
            'totalFlux': 201.4782867432,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.8959197998
          },
          {
            'recordTime': 1536068100000,
            'plusTotalFlux': 343.374206543,
            'flux': 0.0,
            'pressure': 0.219,
            'totalFlux': 201.4725494385,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.9016571045
          },
          {
            'recordTime': 1536068400000,
            'plusTotalFlux': 343.3834228516,
            'flux': 0.0,
            'pressure': 0.2141,
            'totalFlux': 201.4610595703,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.9223632812
          },
          {
            'recordTime': 1536068700000,
            'plusTotalFlux': 343.3920288086,
            'flux': 0.0,
            'pressure': 0.2113,
            'totalFlux': 201.4661560059,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.9258728027
          },
          {
            'recordTime': 1536069000000,
            'plusTotalFlux': 343.4063415527,
            'flux': 0.0,
            'pressure': 0.2116,
            'totalFlux': 201.4771575928,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.92918396
          },
          {
            'recordTime': 1536069300000,
            'plusTotalFlux': 343.416595459,
            'flux': 0.0,
            'pressure': 0.2113,
            'totalFlux': 201.487411499,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.92918396
          },
          {
            'recordTime': 1536069600000,
            'plusTotalFlux': 343.416595459,
            'flux': 0.0,
            'pressure': 0.208,
            'totalFlux': 201.4783172607,
            'uploadTime': 1536076930050,
            'reverseTotalFlux': 141.9382781982
          },
          {
            'recordTime': 1536069900000,
            'plusTotalFlux': 343.425567627,
            'flux': 0.4538923204,
            'pressure': 0.2022,
            'totalFlux': 201.4872894287,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9382781982
          },
          {
            'recordTime': 1536070200000,
            'plusTotalFlux': 343.4298706055,
            'flux': 0.0,
            'pressure': 0.204,
            'totalFlux': 201.4915924072,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9382781982
          },
          {
            'recordTime': 1536070500000,
            'plusTotalFlux': 343.4298706055,
            'flux': -0.3907185197,
            'pressure': 0.2052,
            'totalFlux': 201.4907226562,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9391479492
          },
          {
            'recordTime': 1536070800000,
            'plusTotalFlux': 343.4298706055,
            'flux': 0.0,
            'pressure': 0.2031,
            'totalFlux': 201.4811706543,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9486999512
          },
          {
            'recordTime': 1536071100000,
            'plusTotalFlux': 343.4298706055,
            'flux': -0.4370514154,
            'pressure': 0.2058,
            'totalFlux': 201.4802093506,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9496612549
          },
          {
            'recordTime': 1536071400000,
            'plusTotalFlux': 343.4298706055,
            'flux': 0.0,
            'pressure': 0.1952,
            'totalFlux': 201.4733428955,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.95652771
          },
          {
            'recordTime': 1536071700000,
            'plusTotalFlux': 343.4298706055,
            'flux': 0.0,
            'pressure': 0.1967,
            'totalFlux': 201.4661254883,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9637451172
          },
          {
            'recordTime': 1536072000000,
            'plusTotalFlux': 343.4738769531,
            'flux': 0.4693366289,
            'pressure': 0.2019,
            'totalFlux': 201.5101318359,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9637451172
          },
          {
            'recordTime': 1536072300000,
            'plusTotalFlux': 343.5098266602,
            'flux': 0.7521605492,
            'pressure': 0.2025,
            'totalFlux': 201.5420532227,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9677734375
          },
          {
            'recordTime': 1536072600000,
            'plusTotalFlux': 343.5472412109,
            'flux': 0.0,
            'pressure': 0.1994,
            'totalFlux': 201.5794677734,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9677734375
          },
          {
            'recordTime': 1536072900000,
            'plusTotalFlux': 343.571105957,
            'flux': 0.0,
            'pressure': 0.1976,
            'totalFlux': 201.6033325195,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9677734375
          },
          {
            'recordTime': 1536073200000,
            'plusTotalFlux': 343.5922851562,
            'flux': 0.0,
            'pressure': 0.2083,
            'totalFlux': 201.6245117188,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9677734375
          },
          {
            'recordTime': 1536073500000,
            'plusTotalFlux': 343.5982055664,
            'flux': 0.0,
            'pressure': 0.2086,
            'totalFlux': 201.6286773682,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9695281982
          },
          {
            'recordTime': 1536073800000,
            'plusTotalFlux': 343.6106567383,
            'flux': 0.3399905264,
            'pressure': 0.2135,
            'totalFlux': 201.6354217529,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9752349854
          },
          {
            'recordTime': 1536074100000,
            'plusTotalFlux': 343.6283569336,
            'flux': 0.0,
            'pressure': 0.215,
            'totalFlux': 201.6531219482,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9752349854
          },
          {
            'recordTime': 1536074400000,
            'plusTotalFlux': 343.6283569336,
            'flux': -0.3762394786,
            'pressure': 0.2113,
            'totalFlux': 201.6522827148,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9760742188
          },
          {
            'recordTime': 1536074700000,
            'plusTotalFlux': 343.6415405273,
            'flux': 0.5253222585,
            'pressure': 0.2144,
            'totalFlux': 201.6612091064,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9803314209
          },
          {
            'recordTime': 1536075000000,
            'plusTotalFlux': 343.6731567383,
            'flux': 0.0,
            'pressure': 0.2187,
            'totalFlux': 201.6928253174,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9803314209
          },
          {
            'recordTime': 1536075300000,
            'plusTotalFlux': 343.6890869141,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 201.7087554932,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9803314209
          },
          {
            'recordTime': 1536075600000,
            'plusTotalFlux': 343.6987915039,
            'flux': 0.0,
            'pressure': 0.2205,
            'totalFlux': 201.718460083,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9803314209
          },
          {
            'recordTime': 1536075900000,
            'plusTotalFlux': 343.7222290039,
            'flux': 0.0,
            'pressure': 0.2263,
            'totalFlux': 201.741897583,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9803314209
          },
          {
            'recordTime': 1536076200000,
            'plusTotalFlux': 343.7280578613,
            'flux': -0.4544262886,
            'pressure': 0.2312,
            'totalFlux': 201.7453460693,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.982711792
          },
          {
            'recordTime': 1536076500000,
            'plusTotalFlux': 343.7280578613,
            'flux': 0.0,
            'pressure': 0.2303,
            'totalFlux': 201.7443389893,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9837188721
          },
          {
            'recordTime': 1536076800000,
            'plusTotalFlux': 343.7397766113,
            'flux': 0.0,
            'pressure': 0.2324,
            'totalFlux': 201.7560577393,
            'uploadTime': 1536076937423,
            'reverseTotalFlux': 141.9837188721
          },
          {
            'recordTime': 1536077100000,
            'plusTotalFlux': 343.7483520508,
            'flux': 0.3592959344,
            'pressure': 0.2312,
            'totalFlux': 201.7646331787,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9837188721
          },
          {
            'recordTime': 1536077400000,
            'plusTotalFlux': 343.7779846191,
            'flux': 0.3361294568,
            'pressure': 0.2428,
            'totalFlux': 201.7942657471,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9837188721
          },
          {
            'recordTime': 1536077700000,
            'plusTotalFlux': 343.8288269043,
            'flux': 0.7415425777,
            'pressure': 0.2458,
            'totalFlux': 201.8451080322,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9837188721
          },
          {
            'recordTime': 1536078000000,
            'plusTotalFlux': 343.8583679199,
            'flux': 1.2454133034,
            'pressure': 0.2519,
            'totalFlux': 201.8746490479,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9837188721
          },
          {
            'recordTime': 1536078300000,
            'plusTotalFlux': 343.8874206543,
            'flux': 0.6430851221,
            'pressure': 0.2483,
            'totalFlux': 201.9037017822,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9837188721
          },
          {
            'recordTime': 1536078600000,
            'plusTotalFlux': 343.891204834,
            'flux': 0.0,
            'pressure': 0.2559,
            'totalFlux': 201.9074859619,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9837188721
          },
          {
            'recordTime': 1536078900000,
            'plusTotalFlux': 343.891204834,
            'flux': 0.0,
            'pressure': 0.2614,
            'totalFlux': 201.9074859619,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9837188721
          },
          {
            'recordTime': 1536079200000,
            'plusTotalFlux': 343.891204834,
            'flux': 0.0,
            'pressure': 0.2696,
            'totalFlux': 201.9041595459,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9870452881
          },
          {
            'recordTime': 1536079500000,
            'plusTotalFlux': 343.891204834,
            'flux': 0.0,
            'pressure': 0.2718,
            'totalFlux': 201.9041595459,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9870452881
          },
          {
            'recordTime': 1536079800000,
            'plusTotalFlux': 343.891204834,
            'flux': 0.0,
            'pressure': 0.2739,
            'totalFlux': 201.9041595459,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9870452881
          },
          {
            'recordTime': 1536080100000,
            'plusTotalFlux': 343.9029541016,
            'flux': 0.3564000726,
            'pressure': 0.2764,
            'totalFlux': 201.9159088135,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9870452881
          },
          {
            'recordTime': 1536080400000,
            'plusTotalFlux': 343.9052734375,
            'flux': 0.0,
            'pressure': 0.2834,
            'totalFlux': 201.9182281494,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 141.9870452881
          },
          {
            'recordTime': 1536080700000,
            'plusTotalFlux': 343.9205627441,
            'flux': 0.0,
            'pressure': 0.2223,
            'totalFlux': 201.9203643799,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0001983643
          },
          {
            'recordTime': 1536081000000,
            'plusTotalFlux': 343.9205627441,
            'flux': 0.0,
            'pressure': 0.2214,
            'totalFlux': 201.9203643799,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0001983643
          },
          {
            'recordTime': 1536081300000,
            'plusTotalFlux': 343.9205627441,
            'flux': 0.0,
            'pressure': 0.2223,
            'totalFlux': 201.9203643799,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0001983643
          },
          {
            'recordTime': 1536081600000,
            'plusTotalFlux': 343.9205627441,
            'flux': 0.0,
            'pressure': 0.2229,
            'totalFlux': 201.9203643799,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0001983643
          },
          {
            'recordTime': 1536081900000,
            'plusTotalFlux': 343.9205627441,
            'flux': 0.0,
            'pressure': 0.2205,
            'totalFlux': 201.9203643799,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0001983643
          },
          {
            'recordTime': 1536082200000,
            'plusTotalFlux': 343.924407959,
            'flux': 0.3766707182,
            'pressure': 0.2177,
            'totalFlux': 201.9242095947,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0001983643
          },
          {
            'recordTime': 1536082500000,
            'plusTotalFlux': 343.9249267578,
            'flux': 0.0,
            'pressure': 0.2184,
            'totalFlux': 201.9190063477,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0059204102
          },
          {
            'recordTime': 1536082800000,
            'plusTotalFlux': 343.9249267578,
            'flux': 0.0,
            'pressure': 0.2248,
            'totalFlux': 201.9190063477,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0059204102
          },
          {
            'recordTime': 1536083100000,
            'plusTotalFlux': 343.9249267578,
            'flux': 0.0,
            'pressure': 0.2235,
            'totalFlux': 201.9190063477,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0059204102
          },
          {
            'recordTime': 1536083400000,
            'plusTotalFlux': 343.9249267578,
            'flux': 0.0,
            'pressure': 0.2284,
            'totalFlux': 201.9190063477,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0059204102
          },
          {
            'recordTime': 1536083700000,
            'plusTotalFlux': 343.9249267578,
            'flux': 0.0,
            'pressure': 0.2266,
            'totalFlux': 201.9190063477,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0059204102
          },
          {
            'recordTime': 1536084000000,
            'plusTotalFlux': 343.9419555664,
            'flux': 0.3766707182,
            'pressure': 0.2312,
            'totalFlux': 201.9360351562,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0059204102
          },
          {
            'recordTime': 1536084300000,
            'plusTotalFlux': 343.9424743652,
            'flux': 0.0,
            'pressure': 0.2312,
            'totalFlux': 201.9365539551,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0059204102
          },
          {
            'recordTime': 1536084600000,
            'plusTotalFlux': 343.9424743652,
            'flux': 0.0,
            'pressure': 0.2327,
            'totalFlux': 201.9365539551,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0059204102
          },
          {
            'recordTime': 1536084900000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.2342,
            'totalFlux': 201.9431762695,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0059204102
          },
          {
            'recordTime': 1536085200000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.2342,
            'totalFlux': 201.9431762695,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0059204102
          },
          {
            'recordTime': 1536085500000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.2348,
            'totalFlux': 201.933303833,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536085800000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.2361,
            'totalFlux': 201.933303833,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536086100000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.2364,
            'totalFlux': 201.933303833,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536086400000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.2403,
            'totalFlux': 201.933303833,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536086700000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.2397,
            'totalFlux': 201.933303833,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536087000000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.2409,
            'totalFlux': 201.933303833,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536087300000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.2419,
            'totalFlux': 201.933303833,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536087600000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.24,
            'totalFlux': 201.933303833,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536087900000,
            'plusTotalFlux': 343.9490966797,
            'flux': 0.0,
            'pressure': 0.2434,
            'totalFlux': 201.933303833,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536088200000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2428,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536088500000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2437,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536088800000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2428,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536089100000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536089400000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2449,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536089700000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2446,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536090000000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2452,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536090300000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.244,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536090600000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2452,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536090900000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2452,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536091200000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2501,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098523007,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536091500000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2532,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536091800000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2553,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536092100000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2544,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536092400000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2535,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536092700000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2544,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536093000000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2556,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536093300000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2565,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536093600000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2565,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536093900000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2565,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536094200000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2565,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536094500000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2568,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536094800000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2593,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536095100000,
            'plusTotalFlux': 343.9506530762,
            'flux': 0.0,
            'pressure': 0.2568,
            'totalFlux': 201.9348602295,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536095400000,
            'plusTotalFlux': 343.9521179199,
            'flux': 0.0,
            'pressure': 0.2565,
            'totalFlux': 201.9363250732,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536095700000,
            'plusTotalFlux': 343.9521179199,
            'flux': 0.0,
            'pressure': 0.2568,
            'totalFlux': 201.9363250732,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536096000000,
            'plusTotalFlux': 343.9521179199,
            'flux': 0.0,
            'pressure': 0.2565,
            'totalFlux': 201.9363250732,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536096300000,
            'plusTotalFlux': 343.9521179199,
            'flux': 0.0,
            'pressure': 0.2544,
            'totalFlux': 201.9363250732,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536096600000,
            'plusTotalFlux': 343.9521179199,
            'flux': 0.0,
            'pressure': 0.2553,
            'totalFlux': 201.9363250732,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536096900000,
            'plusTotalFlux': 343.9521179199,
            'flux': 0.0,
            'pressure': 0.2489,
            'totalFlux': 201.9363250732,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536097200000,
            'plusTotalFlux': 343.9521179199,
            'flux': 0.0,
            'pressure': 0.2519,
            'totalFlux': 201.9363250732,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536097500000,
            'plusTotalFlux': 343.9521179199,
            'flux': 0.0,
            'pressure': 0.2516,
            'totalFlux': 201.9363250732,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536097800000,
            'plusTotalFlux': 343.9521179199,
            'flux': 0.0,
            'pressure': 0.2538,
            'totalFlux': 201.9363250732,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536098100000,
            'plusTotalFlux': 343.9558410645,
            'flux': 0.0,
            'pressure': 0.2495,
            'totalFlux': 201.9400482178,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536098400000,
            'plusTotalFlux': 343.9558410645,
            'flux': 0.0,
            'pressure': 0.2495,
            'totalFlux': 201.9400482178,
            'uploadTime': 1536098591987,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536098700000,
            'plusTotalFlux': 343.9558410645,
            'flux': 0.0,
            'pressure': 0.2492,
            'totalFlux': 201.9400482178,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536099000000,
            'plusTotalFlux': 343.9558410645,
            'flux': 0.0,
            'pressure': 0.2449,
            'totalFlux': 201.9400482178,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536099300000,
            'plusTotalFlux': 343.9558410645,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 201.9400482178,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536099600000,
            'plusTotalFlux': 343.9558410645,
            'flux': 0.0,
            'pressure': 0.2492,
            'totalFlux': 201.9400482178,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536099900000,
            'plusTotalFlux': 343.9558410645,
            'flux': 0.0,
            'pressure': 0.2458,
            'totalFlux': 201.9400482178,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536100200000,
            'plusTotalFlux': 343.9558410645,
            'flux': 0.0,
            'pressure': 0.2501,
            'totalFlux': 201.9400482178,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536100500000,
            'plusTotalFlux': 343.9558410645,
            'flux': 0.0,
            'pressure': 0.2278,
            'totalFlux': 201.9400482178,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0157928467
          },
          {
            'recordTime': 1536100800000,
            'plusTotalFlux': 343.9651184082,
            'flux': 0.3583298922,
            'pressure': 0.2223,
            'totalFlux': 201.9424591064,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536101100000,
            'plusTotalFlux': 343.9656982422,
            'flux': 0.0,
            'pressure': 0.2248,
            'totalFlux': 201.9430389404,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536101400000,
            'plusTotalFlux': 343.9656982422,
            'flux': 0.0,
            'pressure': 0.2187,
            'totalFlux': 201.9430389404,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536101700000,
            'plusTotalFlux': 343.9733276367,
            'flux': 0.0,
            'pressure': 0.2156,
            'totalFlux': 201.950668335,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536102000000,
            'plusTotalFlux': 343.9789733887,
            'flux': 0.0,
            'pressure': 0.2181,
            'totalFlux': 201.9563140869,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536102300000,
            'plusTotalFlux': 343.986907959,
            'flux': 0.0,
            'pressure': 0.2101,
            'totalFlux': 201.9642486572,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536102600000,
            'plusTotalFlux': 343.986907959,
            'flux': 0.0,
            'pressure': 0.2052,
            'totalFlux': 201.9642486572,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536102900000,
            'plusTotalFlux': 343.986907959,
            'flux': 0.0,
            'pressure': 0.2049,
            'totalFlux': 201.9642486572,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536103200000,
            'plusTotalFlux': 343.986907959,
            'flux': 0.0,
            'pressure': 0.1994,
            'totalFlux': 201.9642486572,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536103500000,
            'plusTotalFlux': 343.9945678711,
            'flux': 0.0,
            'pressure': 0.1921,
            'totalFlux': 201.9719085693,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536103800000,
            'plusTotalFlux': 343.9962768555,
            'flux': 0.6807305813,
            'pressure': 0.2638,
            'totalFlux': 201.9736175537,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0226593018
          },
          {
            'recordTime': 1536104100000,
            'plusTotalFlux': 344.0272827148,
            'flux': 0.0,
            'pressure': 0.2556,
            'totalFlux': 202.0015563965,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0257263184
          },
          {
            'recordTime': 1536104400000,
            'plusTotalFlux': 344.0371704102,
            'flux': 0.0,
            'pressure': 0.2501,
            'totalFlux': 202.0114440918,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0257263184
          },
          {
            'recordTime': 1536104700000,
            'plusTotalFlux': 344.0371704102,
            'flux': 0.0,
            'pressure': 0.2373,
            'totalFlux': 202.0065765381,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0305938721
          },
          {
            'recordTime': 1536105000000,
            'plusTotalFlux': 344.0371704102,
            'flux': 0.0,
            'pressure': 0.2348,
            'totalFlux': 202.0065765381,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0305938721
          },
          {
            'recordTime': 1536105300000,
            'plusTotalFlux': 344.0371704102,
            'flux': 0.0,
            'pressure': 0.2284,
            'totalFlux': 201.9983978271,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.038772583
          },
          {
            'recordTime': 1536105600000,
            'plusTotalFlux': 344.0371704102,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 201.9983978271,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.038772583
          },
          {
            'recordTime': 1536105900000,
            'plusTotalFlux': 344.061920166,
            'flux': 0.0,
            'pressure': 0.2144,
            'totalFlux': 202.023147583,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.038772583
          },
          {
            'recordTime': 1536106200000,
            'plusTotalFlux': 344.063293457,
            'flux': -0.5673629045,
            'pressure': 0.2031,
            'totalFlux': 202.0062408447,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0570526123
          },
          {
            'recordTime': 1536106500000,
            'plusTotalFlux': 344.0711975098,
            'flux': -0.5264078975,
            'pressure': 0.1912,
            'totalFlux': 201.9957580566,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0754394531
          },
          {
            'recordTime': 1536106800000,
            'plusTotalFlux': 344.0753479004,
            'flux': -0.3328023255,
            'pressure': 0.1811,
            'totalFlux': 201.989730835,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0856170654
          },
          {
            'recordTime': 1536107100000,
            'plusTotalFlux': 344.0820617676,
            'flux': -0.4327076972,
            'pressure': 0.1735,
            'totalFlux': 201.9855804443,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.0964813232
          },
          {
            'recordTime': 1536107400000,
            'plusTotalFlux': 344.1030578613,
            'flux': 0.793667078,
            'pressure': 0.2181,
            'totalFlux': 202.0026092529,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.1004486084
          },
          {
            'recordTime': 1536107700000,
            'plusTotalFlux': 344.106048584,
            'flux': -0.3907184899,
            'pressure': 0.2211,
            'totalFlux': 201.997756958,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.108291626
          },
          {
            'recordTime': 1536108000000,
            'plusTotalFlux': 344.1235961914,
            'flux': 0.0,
            'pressure': 0.2196,
            'totalFlux': 202.0116882324,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.111907959
          },
          {
            'recordTime': 1536108300000,
            'plusTotalFlux': 344.1292724609,
            'flux': 0.0,
            'pressure': 0.2138,
            'totalFlux': 202.017364502,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.111907959
          },
          {
            'recordTime': 1536108600000,
            'plusTotalFlux': 344.1455688477,
            'flux': 0.0,
            'pressure': 0.2159,
            'totalFlux': 202.0336608887,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.111907959
          },
          {
            'recordTime': 1536108900000,
            'plusTotalFlux': 344.1455688477,
            'flux': 0.0,
            'pressure': 0.208,
            'totalFlux': 202.0336608887,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.111907959
          },
          {
            'recordTime': 1536109200000,
            'plusTotalFlux': 344.1975097656,
            'flux': 0.5069820881,
            'pressure': 0.2043,
            'totalFlux': 202.0822601318,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.1152496338
          },
          {
            'recordTime': 1536109500000,
            'plusTotalFlux': 344.2242431641,
            'flux': 0.0,
            'pressure': 0.2101,
            'totalFlux': 202.1089935303,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.1152496338
          },
          {
            'recordTime': 1536109800000,
            'plusTotalFlux': 344.2253723145,
            'flux': 0.4490659237,
            'pressure': 0.2065,
            'totalFlux': 202.0991210938,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.1262512207
          },
          {
            'recordTime': 1536110100000,
            'plusTotalFlux': 344.2814331055,
            'flux': 0.0,
            'pressure': 0.1958,
            'totalFlux': 202.1551818848,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.1262512207
          },
          {
            'recordTime': 1536110400000,
            'plusTotalFlux': 344.2829284668,
            'flux': 0.5996479392,
            'pressure': 0.197,
            'totalFlux': 202.1566772461,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.1262512207
          },
          {
            'recordTime': 1536110700000,
            'plusTotalFlux': 344.2966003418,
            'flux': 0.0,
            'pressure': 0.1921,
            'totalFlux': 202.167098999,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.1295013428
          },
          {
            'recordTime': 1536111000000,
            'plusTotalFlux': 344.2966003418,
            'flux': -0.4631136656,
            'pressure': 0.1909,
            'totalFlux': 202.1490936279,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.1475067139
          },
          {
            'recordTime': 1536111300000,
            'plusTotalFlux': 344.30078125,
            'flux': 0.0,
            'pressure': 0.1826,
            'totalFlux': 202.1226501465,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.1781311035
          },
          {
            'recordTime': 1536111600000,
            'plusTotalFlux': 344.3060913086,
            'flux': -0.512342453,
            'pressure': 0.1698,
            'totalFlux': 202.1127166748,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.1933746338
          },
          {
            'recordTime': 1536111900000,
            'plusTotalFlux': 344.3060913086,
            'flux': 0.0,
            'pressure': 0.1634,
            'totalFlux': 202.0988616943,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.2072296143
          },
          {
            'recordTime': 1536112200000,
            'plusTotalFlux': 344.3232421875,
            'flux': -0.5123425722,
            'pressure': 0.1655,
            'totalFlux': 202.1053009033,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.2179412842
          },
          {
            'recordTime': 1536112500000,
            'plusTotalFlux': 344.3232421875,
            'flux': 0.0,
            'pressure': 0.1539,
            'totalFlux': 202.0950622559,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.2281799316
          },
          {
            'recordTime': 1536112800000,
            'plusTotalFlux': 344.3329467773,
            'flux': 0.0,
            'pressure': 0.1442,
            'totalFlux': 202.0875091553,
            'uploadTime': 1536120131793,
            'reverseTotalFlux': 142.2454376221
          },
          {
            'recordTime': 1536113100000,
            'plusTotalFlux': 344.3435974121,
            'flux': 0.4316911399,
            'pressure': 0.1512,
            'totalFlux': 202.09815979,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2454376221
          },
          {
            'recordTime': 1536113400000,
            'plusTotalFlux': 344.3723144531,
            'flux': 0.0,
            'pressure': 0.143,
            'totalFlux': 202.1268768311,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2454376221
          },
          {
            'recordTime': 1536113700000,
            'plusTotalFlux': 344.3760375977,
            'flux': 0.0,
            'pressure': 0.1472,
            'totalFlux': 202.1305999756,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2454376221
          },
          {
            'recordTime': 1536114000000,
            'plusTotalFlux': 344.3760375977,
            'flux': 0.0,
            'pressure': 0.1494,
            'totalFlux': 202.1305999756,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2454376221
          },
          {
            'recordTime': 1536114300000,
            'plusTotalFlux': 344.3973388672,
            'flux': 0.6459808946,
            'pressure': 0.1481,
            'totalFlux': 202.1475830078,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2497558594
          },
          {
            'recordTime': 1536114600000,
            'plusTotalFlux': 344.4075317383,
            'flux': 0.4287952781,
            'pressure': 0.1488,
            'totalFlux': 202.1577758789,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2497558594
          },
          {
            'recordTime': 1536114900000,
            'plusTotalFlux': 344.4292297363,
            'flux': 0.6923137903,
            'pressure': 0.1494,
            'totalFlux': 202.179473877,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2497558594
          },
          {
            'recordTime': 1536115200000,
            'plusTotalFlux': 344.4608154297,
            'flux': 0.4606492221,
            'pressure': 0.1958,
            'totalFlux': 202.2096710205,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2511444092
          },
          {
            'recordTime': 1536115500000,
            'plusTotalFlux': 344.463104248,
            'flux': 0.0,
            'pressure': 0.2007,
            'totalFlux': 202.21043396,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2526702881
          },
          {
            'recordTime': 1536115800000,
            'plusTotalFlux': 344.5148925781,
            'flux': 0.0,
            'pressure': 0.2019,
            'totalFlux': 202.26222229,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2526702881
          },
          {
            'recordTime': 1536116100000,
            'plusTotalFlux': 344.5308837891,
            'flux': -0.5282694697,
            'pressure': 0.2092,
            'totalFlux': 202.2729797363,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2579040527
          },
          {
            'recordTime': 1536116400000,
            'plusTotalFlux': 344.5454711914,
            'flux': 0.0,
            'pressure': 0.2129,
            'totalFlux': 202.2634277344,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.282043457
          },
          {
            'recordTime': 1536116700000,
            'plusTotalFlux': 344.6035766602,
            'flux': 2.1402177811,
            'pressure': 0.2126,
            'totalFlux': 202.3215332031,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.282043457
          },
          {
            'recordTime': 1536117000000,
            'plusTotalFlux': 344.690246582,
            'flux': 0.0,
            'pressure': 0.2129,
            'totalFlux': 202.408203125,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.282043457
          },
          {
            'recordTime': 1536117300000,
            'plusTotalFlux': 344.7227172852,
            'flux': 1.468390584,
            'pressure': 0.2165,
            'totalFlux': 202.4406738281,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.282043457
          },
          {
            'recordTime': 1536117600000,
            'plusTotalFlux': 344.7724609375,
            'flux': 0.0,
            'pressure': 0.2214,
            'totalFlux': 202.4904174805,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.282043457
          },
          {
            'recordTime': 1536117900000,
            'plusTotalFlux': 344.7808837891,
            'flux': 0.0,
            'pressure': 0.2202,
            'totalFlux': 202.4922637939,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.2886199951
          },
          {
            'recordTime': 1536118200000,
            'plusTotalFlux': 344.7972717285,
            'flux': 0.0,
            'pressure': 0.2214,
            'totalFlux': 202.4970245361,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.3002471924
          },
          {
            'recordTime': 1536118500000,
            'plusTotalFlux': 344.8234558105,
            'flux': -0.9380263686,
            'pressure': 0.2251,
            'totalFlux': 202.5124969482,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.3109588623
          },
          {
            'recordTime': 1536118800000,
            'plusTotalFlux': 344.8379211426,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 202.513168335,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.3247528076
          },
          {
            'recordTime': 1536119100000,
            'plusTotalFlux': 344.8550720215,
            'flux': 0.3564000726,
            'pressure': 0.2306,
            'totalFlux': 202.5303192139,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.3247528076
          },
          {
            'recordTime': 1536119400000,
            'plusTotalFlux': 344.8995666504,
            'flux': 0.387840271,
            'pressure': 0.226,
            'totalFlux': 202.5748138428,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.3247528076
          },
          {
            'recordTime': 1536119700000,
            'plusTotalFlux': 344.9119262695,
            'flux': 0.0,
            'pressure': 0.2226,
            'totalFlux': 202.5708007812,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.3411254883
          },
          {
            'recordTime': 1536120000000,
            'plusTotalFlux': 344.9591674805,
            'flux': 0.3911497891,
            'pressure': 0.2263,
            'totalFlux': 202.6180419922,
            'uploadTime': 1536120139340,
            'reverseTotalFlux': 142.3411254883
          },
          {
            'recordTime': 1536120300000,
            'plusTotalFlux': 344.9690246582,
            'flux': 0.0,
            'pressure': 0.2211,
            'totalFlux': 202.6278991699,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3411254883
          },
          {
            'recordTime': 1536120600000,
            'plusTotalFlux': 344.9813537598,
            'flux': 0.0,
            'pressure': 0.2168,
            'totalFlux': 202.631072998,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3502807617
          },
          {
            'recordTime': 1536120900000,
            'plusTotalFlux': 345.0054931641,
            'flux': -0.7526946068,
            'pressure': 0.2327,
            'totalFlux': 202.6507568359,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3547363281
          },
          {
            'recordTime': 1536121200000,
            'plusTotalFlux': 345.0187072754,
            'flux': 0.0,
            'pressure': 0.2382,
            'totalFlux': 202.6605224609,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3581848145
          },
          {
            'recordTime': 1536121500000,
            'plusTotalFlux': 345.0248718262,
            'flux': 0.0,
            'pressure': 0.2318,
            'totalFlux': 202.6518707275,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3730010986
          },
          {
            'recordTime': 1536121800000,
            'plusTotalFlux': 345.0493164062,
            'flux': 0.0,
            'pressure': 0.2312,
            'totalFlux': 202.6733398438,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3759765625
          },
          {
            'recordTime': 1536122100000,
            'plusTotalFlux': 345.0873413086,
            'flux': 1.04560256,
            'pressure': 0.24,
            'totalFlux': 202.7113647461,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3759765625
          },
          {
            'recordTime': 1536122400000,
            'plusTotalFlux': 345.1162719727,
            'flux': 0.0,
            'pressure': 0.2522,
            'totalFlux': 202.7402954102,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3759765625
          },
          {
            'recordTime': 1536122700000,
            'plusTotalFlux': 345.1245117188,
            'flux': 0.0,
            'pressure': 0.2562,
            'totalFlux': 202.7405548096,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3839569092
          },
          {
            'recordTime': 1536123000000,
            'plusTotalFlux': 345.1432495117,
            'flux': 0.0,
            'pressure': 0.2666,
            'totalFlux': 202.7577362061,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3855133057
          },
          {
            'recordTime': 1536123300000,
            'plusTotalFlux': 345.1487731934,
            'flux': 0.686522305,
            'pressure': 0.2525,
            'totalFlux': 202.7632598877,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3855133057
          },
          {
            'recordTime': 1536123600000,
            'plusTotalFlux': 345.1632080078,
            'flux': 0.0,
            'pressure': 0.2687,
            'totalFlux': 202.7776947021,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3855133057
          },
          {
            'recordTime': 1536123900000,
            'plusTotalFlux': 345.1888122559,
            'flux': 0.0,
            'pressure': 0.269,
            'totalFlux': 202.8032989502,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3855133057
          },
          {
            'recordTime': 1536124200000,
            'plusTotalFlux': 345.2031555176,
            'flux': 0.0,
            'pressure': 0.2718,
            'totalFlux': 202.8176422119,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3855133057
          },
          {
            'recordTime': 1536124500000,
            'plusTotalFlux': 345.2383117676,
            'flux': 0.0,
            'pressure': 0.2699,
            'totalFlux': 202.8527984619,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3855133057
          },
          {
            'recordTime': 1536124800000,
            'plusTotalFlux': 345.252166748,
            'flux': 0.0,
            'pressure': 0.2736,
            'totalFlux': 202.8651580811,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536125100000,
            'plusTotalFlux': 345.2576599121,
            'flux': 0.414316237,
            'pressure': 0.2776,
            'totalFlux': 202.8706512451,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536125400000,
            'plusTotalFlux': 345.2680053711,
            'flux': 0.0,
            'pressure': 0.2825,
            'totalFlux': 202.8809967041,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536125700000,
            'plusTotalFlux': 345.303314209,
            'flux': 0.6276407838,
            'pressure': 0.2818,
            'totalFlux': 202.916305542,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536126000000,
            'plusTotalFlux': 345.3858642578,
            'flux': 1.0330541134,
            'pressure': 0.2825,
            'totalFlux': 202.9988555908,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536126300000,
            'plusTotalFlux': 345.4419250488,
            'flux': 0.8544790745,
            'pressure': 0.2388,
            'totalFlux': 203.0549163818,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536126600000,
            'plusTotalFlux': 345.5105285645,
            'flux': 0.9918231964,
            'pressure': 0.2425,
            'totalFlux': 203.1235198975,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536126900000,
            'plusTotalFlux': 345.5816345215,
            'flux': 0.7888407707,
            'pressure': 0.2532,
            'totalFlux': 203.1946258545,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536127200000,
            'plusTotalFlux': 345.6564331055,
            'flux': 0.8438612223,
            'pressure': 0.2519,
            'totalFlux': 203.2694244385,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536127500000,
            'plusTotalFlux': 345.6909790039,
            'flux': 0.5185653567,
            'pressure': 0.248,
            'totalFlux': 203.3039703369,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536127800000,
            'plusTotalFlux': 345.7001953125,
            'flux': 0.0,
            'pressure': 0.2474,
            'totalFlux': 203.3131866455,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.387008667
          },
          {
            'recordTime': 1536128100000,
            'plusTotalFlux': 345.7231445312,
            'flux': 0.7241677046,
            'pressure': 0.2446,
            'totalFlux': 203.334564209,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3885803223
          },
          {
            'recordTime': 1536128400000,
            'plusTotalFlux': 345.7801208496,
            'flux': 0.5880647302,
            'pressure': 0.2562,
            'totalFlux': 203.3816375732,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.3984832764
          },
          {
            'recordTime': 1536128700000,
            'plusTotalFlux': 345.7812805176,
            'flux': 0.0,
            'pressure': 0.2586,
            'totalFlux': 203.365814209,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4154663086
          },
          {
            'recordTime': 1536129000000,
            'plusTotalFlux': 345.7850341797,
            'flux': 0.0,
            'pressure': 0.2638,
            'totalFlux': 203.3695678711,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4154663086
          },
          {
            'recordTime': 1536129300000,
            'plusTotalFlux': 345.7937927246,
            'flux': 0.0,
            'pressure': 0.2654,
            'totalFlux': 203.378326416,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4154663086
          },
          {
            'recordTime': 1536129600000,
            'plusTotalFlux': 345.7937927246,
            'flux': 0.0,
            'pressure': 0.2654,
            'totalFlux': 203.378326416,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4154663086
          },
          {
            'recordTime': 1536129900000,
            'plusTotalFlux': 345.7937927246,
            'flux': 0.0,
            'pressure': 0.2706,
            'totalFlux': 203.378326416,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4154663086
          },
          {
            'recordTime': 1536130200000,
            'plusTotalFlux': 345.8011169434,
            'flux': 0.0,
            'pressure': 0.269,
            'totalFlux': 203.3856506348,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4154663086
          },
          {
            'recordTime': 1536130500000,
            'plusTotalFlux': 345.8049316406,
            'flux': 0.0,
            'pressure': 0.2599,
            'totalFlux': 203.3764648438,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4284667969
          },
          {
            'recordTime': 1536130800000,
            'plusTotalFlux': 345.8049316406,
            'flux': -0.5760502815,
            'pressure': 0.2617,
            'totalFlux': 203.3751983643,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4297332764
          },
          {
            'recordTime': 1536131100000,
            'plusTotalFlux': 345.8049316406,
            'flux': 0.0,
            'pressure': 0.2626,
            'totalFlux': 203.3685302734,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4364013672
          },
          {
            'recordTime': 1536131400000,
            'plusTotalFlux': 345.8078613281,
            'flux': 0.0,
            'pressure': 0.2553,
            'totalFlux': 203.3674926758,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4403686523
          },
          {
            'recordTime': 1536131700000,
            'plusTotalFlux': 345.8106994629,
            'flux': 0.0,
            'pressure': 0.255,
            'totalFlux': 203.3703308105,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4403686523
          },
          {
            'recordTime': 1536132000000,
            'plusTotalFlux': 345.825012207,
            'flux': 0.0,
            'pressure': 0.2528,
            'totalFlux': 203.3770294189,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4479827881
          },
          {
            'recordTime': 1536132300000,
            'plusTotalFlux': 345.8283996582,
            'flux': 0.0,
            'pressure': 0.2504,
            'totalFlux': 203.3773498535,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4510498047
          },
          {
            'recordTime': 1536132600000,
            'plusTotalFlux': 345.8304748535,
            'flux': 0.9413533807,
            'pressure': 0.2409,
            'totalFlux': 203.3739471436,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.45652771
          },
          {
            'recordTime': 1536132900000,
            'plusTotalFlux': 345.8449707031,
            'flux': 0.842895925,
            'pressure': 0.2416,
            'totalFlux': 203.38671875,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4582519531
          },
          {
            'recordTime': 1536133200000,
            'plusTotalFlux': 345.8923339844,
            'flux': 0.8457916379,
            'pressure': 0.2434,
            'totalFlux': 203.4340820312,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4582519531
          },
          {
            'recordTime': 1536133500000,
            'plusTotalFlux': 345.9066772461,
            'flux': 0.3969413936,
            'pressure': 0.2373,
            'totalFlux': 203.4411773682,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4654998779
          },
          {
            'recordTime': 1536133800000,
            'plusTotalFlux': 345.9185791016,
            'flux': 0.0,
            'pressure': 0.2434,
            'totalFlux': 203.4530792236,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4654998779
          },
          {
            'recordTime': 1536134100000,
            'plusTotalFlux': 345.928527832,
            'flux': 0.0,
            'pressure': 0.2367,
            'totalFlux': 203.4630279541,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4654998779
          },
          {
            'recordTime': 1536134400000,
            'plusTotalFlux': 345.9426269531,
            'flux': 0.0,
            'pressure': 0.2385,
            'totalFlux': 203.4771270752,
            'uploadTime': 1536141739317,
            'reverseTotalFlux': 142.4654998779
          },
          {
            'recordTime': 1536134700000,
            'plusTotalFlux': 345.9621582031,
            'flux': -0.4225724041,
            'pressure': 0.2425,
            'totalFlux': 203.4957275391,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.4664306641
          },
          {
            'recordTime': 1536135000000,
            'plusTotalFlux': 345.9621582031,
            'flux': 0.0,
            'pressure': 0.2312,
            'totalFlux': 203.4899749756,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.4721832275
          },
          {
            'recordTime': 1536135300000,
            'plusTotalFlux': 345.9889221191,
            'flux': 0.4085246027,
            'pressure': 0.237,
            'totalFlux': 203.5167388916,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.4721832275
          },
          {
            'recordTime': 1536135600000,
            'plusTotalFlux': 345.9945678711,
            'flux': 0.0,
            'pressure': 0.2382,
            'totalFlux': 203.5151977539,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.4793701172
          },
          {
            'recordTime': 1536135900000,
            'plusTotalFlux': 345.9988708496,
            'flux': 0.0,
            'pressure': 0.2348,
            'totalFlux': 203.5180206299,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.4808502197
          },
          {
            'recordTime': 1536136200000,
            'plusTotalFlux': 345.9988708496,
            'flux': -0.4631136656,
            'pressure': 0.2443,
            'totalFlux': 203.5121765137,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.4866943359
          },
          {
            'recordTime': 1536136500000,
            'plusTotalFlux': 346.004119873,
            'flux': 0.0,
            'pressure': 0.2434,
            'totalFlux': 203.5165252686,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.4875946045
          },
          {
            'recordTime': 1536136800000,
            'plusTotalFlux': 346.0090026855,
            'flux': 0.0,
            'pressure': 0.2382,
            'totalFlux': 203.5214080811,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.4875946045
          },
          {
            'recordTime': 1536137100000,
            'plusTotalFlux': 346.0195617676,
            'flux': 0.4606492221,
            'pressure': 0.237,
            'totalFlux': 203.5240478516,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.495513916
          },
          {
            'recordTime': 1536137400000,
            'plusTotalFlux': 346.0244750977,
            'flux': 0.0,
            'pressure': 0.2315,
            'totalFlux': 203.5217590332,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5027160645
          },
          {
            'recordTime': 1536137700000,
            'plusTotalFlux': 346.0416564941,
            'flux': 0.3853581548,
            'pressure': 0.237,
            'totalFlux': 203.5389404297,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5027160645
          },
          {
            'recordTime': 1536138000000,
            'plusTotalFlux': 346.0437927246,
            'flux': 0.0,
            'pressure': 0.2364,
            'totalFlux': 203.5391387939,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5046539307
          },
          {
            'recordTime': 1536138300000,
            'plusTotalFlux': 346.051574707,
            'flux': 0.0,
            'pressure': 0.2425,
            'totalFlux': 203.5444946289,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5070800781
          },
          {
            'recordTime': 1536138600000,
            'plusTotalFlux': 346.0805969238,
            'flux': 0.0,
            'pressure': 0.2385,
            'totalFlux': 203.5735168457,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5070800781
          },
          {
            'recordTime': 1536138900000,
            'plusTotalFlux': 346.0901794434,
            'flux': 0.0,
            'pressure': 0.2358,
            'totalFlux': 203.5830993652,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5070800781
          },
          {
            'recordTime': 1536139200000,
            'plusTotalFlux': 346.1116943359,
            'flux': 0.0,
            'pressure': 0.2373,
            'totalFlux': 203.595916748,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5157775879
          },
          {
            'recordTime': 1536139500000,
            'plusTotalFlux': 346.1116943359,
            'flux': 0.0,
            'pressure': 0.2287,
            'totalFlux': 203.5924224854,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5192718506
          },
          {
            'recordTime': 1536139800000,
            'plusTotalFlux': 346.1314086914,
            'flux': 0.0,
            'pressure': 0.23,
            'totalFlux': 203.6121368408,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5192718506
          },
          {
            'recordTime': 1536140100000,
            'plusTotalFlux': 346.1517944336,
            'flux': 0.6604599357,
            'pressure': 0.2297,
            'totalFlux': 203.6210479736,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.53074646
          },
          {
            'recordTime': 1536140400000,
            'plusTotalFlux': 346.1763916016,
            'flux': 0.0,
            'pressure': 0.2272,
            'totalFlux': 203.6378326416,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.53855896
          },
          {
            'recordTime': 1536140700000,
            'plusTotalFlux': 346.1910705566,
            'flux': 0.6585294008,
            'pressure': 0.2266,
            'totalFlux': 203.6461181641,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5449523926
          },
          {
            'recordTime': 1536141000000,
            'plusTotalFlux': 346.1986083984,
            'flux': 0.0,
            'pressure': 0.2297,
            'totalFlux': 203.6439971924,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5546112061
          },
          {
            'recordTime': 1536141300000,
            'plusTotalFlux': 346.2080383301,
            'flux': 0.0,
            'pressure': 0.2226,
            'totalFlux': 203.653427124,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5546112061
          },
          {
            'recordTime': 1536141600000,
            'plusTotalFlux': 346.2339172363,
            'flux': 0.0,
            'pressure': 0.2321,
            'totalFlux': 203.6666870117,
            'uploadTime': 1536141746383,
            'reverseTotalFlux': 142.5672302246
          },
          {
            'recordTime': 1536141900000,
            'plusTotalFlux': 346.2384338379,
            'flux': 0.0,
            'pressure': 0.2275,
            'totalFlux': 203.6571044922,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.5813293457
          },
          {
            'recordTime': 1536142200000,
            'plusTotalFlux': 346.2384338379,
            'flux': 0.0,
            'pressure': 0.2312,
            'totalFlux': 203.6571044922,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.5813293457
          },
          {
            'recordTime': 1536142500000,
            'plusTotalFlux': 346.2455444336,
            'flux': 0.0,
            'pressure': 0.2272,
            'totalFlux': 203.6642150879,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.5813293457
          },
          {
            'recordTime': 1536142800000,
            'plusTotalFlux': 346.264251709,
            'flux': 0.6025437713,
            'pressure': 0.2342,
            'totalFlux': 203.6755981445,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.5886535645
          },
          {
            'recordTime': 1536143100000,
            'plusTotalFlux': 346.278137207,
            'flux': 0.0,
            'pressure': 0.2306,
            'totalFlux': 203.6874694824,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.5906677246
          },
          {
            'recordTime': 1536143400000,
            'plusTotalFlux': 346.278137207,
            'flux': 0.0,
            'pressure': 0.233,
            'totalFlux': 203.6648101807,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6133270264
          },
          {
            'recordTime': 1536143700000,
            'plusTotalFlux': 346.3016357422,
            'flux': 0.0,
            'pressure': 0.2336,
            'totalFlux': 203.6850891113,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6165466309
          },
          {
            'recordTime': 1536144000000,
            'plusTotalFlux': 346.3223266602,
            'flux': 0.6503245831,
            'pressure': 0.2394,
            'totalFlux': 203.7057800293,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6165466309
          },
          {
            'recordTime': 1536144300000,
            'plusTotalFlux': 346.3775024414,
            'flux': 0.5919258595,
            'pressure': 0.2458,
            'totalFlux': 203.7609558105,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6165466309
          },
          {
            'recordTime': 1536144600000,
            'plusTotalFlux': 346.3984375,
            'flux': 0.0,
            'pressure': 0.24,
            'totalFlux': 203.7818908691,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6165466309
          },
          {
            'recordTime': 1536144900000,
            'plusTotalFlux': 346.3992004395,
            'flux': 0.3448168337,
            'pressure': 0.2428,
            'totalFlux': 203.7755279541,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6236724854
          },
          {
            'recordTime': 1536145200000,
            'plusTotalFlux': 346.4104003906,
            'flux': 0.0,
            'pressure': 0.2461,
            'totalFlux': 203.7867279053,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6236724854
          },
          {
            'recordTime': 1536145500000,
            'plusTotalFlux': 346.4335632324,
            'flux': 0.8400000334,
            'pressure': 0.2434,
            'totalFlux': 203.8098907471,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6236724854
          },
          {
            'recordTime': 1536145800000,
            'plusTotalFlux': 346.4375915527,
            'flux': 0.0,
            'pressure': 0.2361,
            'totalFlux': 203.8089294434,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6286621094
          },
          {
            'recordTime': 1536146100000,
            'plusTotalFlux': 346.4588623047,
            'flux': 0.0,
            'pressure': 0.244,
            'totalFlux': 203.8270568848,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6318054199
          },
          {
            'recordTime': 1536146400000,
            'plusTotalFlux': 346.4766235352,
            'flux': 0.6147062182,
            'pressure': 0.2397,
            'totalFlux': 203.8354644775,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6411590576
          },
          {
            'recordTime': 1536146700000,
            'plusTotalFlux': 346.5961608887,
            'flux': 0.9413534403,
            'pressure': 0.2483,
            'totalFlux': 203.9550018311,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6411590576
          },
          {
            'recordTime': 1536147000000,
            'plusTotalFlux': 346.6081848145,
            'flux': 0.0,
            'pressure': 0.2474,
            'totalFlux': 203.9615783691,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6466064453
          },
          {
            'recordTime': 1536147300000,
            'plusTotalFlux': 346.6096496582,
            'flux': 0.0,
            'pressure': 0.2388,
            'totalFlux': 203.9630432129,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6466064453
          },
          {
            'recordTime': 1536147600000,
            'plusTotalFlux': 346.6172790527,
            'flux': 0.0,
            'pressure': 0.24,
            'totalFlux': 203.961807251,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6554718018
          },
          {
            'recordTime': 1536147900000,
            'plusTotalFlux': 346.6172790527,
            'flux': -0.5297173262,
            'pressure': 0.2358,
            'totalFlux': 203.9606323242,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6566467285
          },
          {
            'recordTime': 1536148200000,
            'plusTotalFlux': 346.6235961914,
            'flux': 0.5417318344,
            'pressure': 0.2492,
            'totalFlux': 203.9641418457,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6594543457
          },
          {
            'recordTime': 1536148500000,
            'plusTotalFlux': 346.6474304199,
            'flux': 0.7907713652,
            'pressure': 0.2577,
            'totalFlux': 203.9879760742,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6594543457
          },
          {
            'recordTime': 1536148800000,
            'plusTotalFlux': 346.6777954102,
            'flux': 0.3322683275,
            'pressure': 0.2568,
            'totalFlux': 204.0183410645,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6594543457
          },
          {
            'recordTime': 1536149100000,
            'plusTotalFlux': 346.7181091309,
            'flux': 0.0,
            'pressure': 0.2623,
            'totalFlux': 204.0586547852,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6594543457
          },
          {
            'recordTime': 1536149400000,
            'plusTotalFlux': 346.7592773438,
            'flux': 0.8255209923,
            'pressure': 0.2535,
            'totalFlux': 204.099822998,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6594543457
          },
          {
            'recordTime': 1536149700000,
            'plusTotalFlux': 346.7825622559,
            'flux': -0.5577101707,
            'pressure': 0.2144,
            'totalFlux': 204.1023864746,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6801757812
          },
          {
            'recordTime': 1536150000000,
            'plusTotalFlux': 346.8038635254,
            'flux': 0.0,
            'pressure': 0.2116,
            'totalFlux': 204.1205444336,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6833190918
          },
          {
            'recordTime': 1536150300000,
            'plusTotalFlux': 346.8038635254,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 204.1205444336,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6833190918
          },
          {
            'recordTime': 1536150600000,
            'plusTotalFlux': 346.8038635254,
            'flux': 0.0,
            'pressure': 0.2156,
            'totalFlux': 204.1070709229,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.6967926025
          },
          {
            'recordTime': 1536150900000,
            'plusTotalFlux': 346.8038635254,
            'flux': 0.0,
            'pressure': 0.2153,
            'totalFlux': 204.0958251953,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7080383301
          },
          {
            'recordTime': 1536151200000,
            'plusTotalFlux': 346.8164672852,
            'flux': 0.0,
            'pressure': 0.2095,
            'totalFlux': 204.1084289551,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7080383301
          },
          {
            'recordTime': 1536151500000,
            'plusTotalFlux': 346.8164672852,
            'flux': 0.0,
            'pressure': 0.2132,
            'totalFlux': 204.0932312012,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.723236084
          },
          {
            'recordTime': 1536151800000,
            'plusTotalFlux': 346.8273620605,
            'flux': 0.0,
            'pressure': 0.215,
            'totalFlux': 204.1020355225,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7253265381
          },
          {
            'recordTime': 1536152100000,
            'plusTotalFlux': 346.8408813477,
            'flux': 0.0,
            'pressure': 0.2129,
            'totalFlux': 204.1155548096,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7253265381
          },
          {
            'recordTime': 1536152400000,
            'plusTotalFlux': 346.8428649902,
            'flux': 0.0,
            'pressure': 0.2242,
            'totalFlux': 204.1175384521,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7253265381
          },
          {
            'recordTime': 1536152700000,
            'plusTotalFlux': 346.8428649902,
            'flux': 0.0,
            'pressure': 0.2263,
            'totalFlux': 204.1084136963,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7344512939
          },
          {
            'recordTime': 1536153000000,
            'plusTotalFlux': 346.848236084,
            'flux': 0.0,
            'pressure': 0.2272,
            'totalFlux': 204.1064147949,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7418212891
          },
          {
            'recordTime': 1536153300000,
            'plusTotalFlux': 346.8665466309,
            'flux': 0.8950204849,
            'pressure': 0.2376,
            'totalFlux': 204.1232910156,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7432556152
          },
          {
            'recordTime': 1536153600000,
            'plusTotalFlux': 346.8842773438,
            'flux': 0.3882539868,
            'pressure': 0.2385,
            'totalFlux': 204.1410217285,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7432556152
          },
          {
            'recordTime': 1536153900000,
            'plusTotalFlux': 346.8919677734,
            'flux': 0.0,
            'pressure': 0.244,
            'totalFlux': 204.1472015381,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7447662354
          },
          {
            'recordTime': 1536154200000,
            'plusTotalFlux': 346.8919677734,
            'flux': 0.0,
            'pressure': 0.2434,
            'totalFlux': 204.1472015381,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7447662354
          },
          {
            'recordTime': 1536154500000,
            'plusTotalFlux': 346.8972473145,
            'flux': 0.0,
            'pressure': 0.2348,
            'totalFlux': 204.1444702148,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7527770996
          },
          {
            'recordTime': 1536154800000,
            'plusTotalFlux': 346.8972473145,
            'flux': 0.0,
            'pressure': 0.2324,
            'totalFlux': 204.1347961426,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7624511719
          },
          {
            'recordTime': 1536155100000,
            'plusTotalFlux': 346.8972473145,
            'flux': 0.0,
            'pressure': 0.2385,
            'totalFlux': 204.121887207,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7753601074
          },
          {
            'recordTime': 1536155400000,
            'plusTotalFlux': 346.8990783691,
            'flux': -0.3936142921,
            'pressure': 0.2385,
            'totalFlux': 204.1139984131,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7850799561
          },
          {
            'recordTime': 1536155700000,
            'plusTotalFlux': 346.8990783691,
            'flux': 0.0,
            'pressure': 0.2367,
            'totalFlux': 204.1093139648,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7897644043
          },
          {
            'recordTime': 1536156000000,
            'plusTotalFlux': 346.9034423828,
            'flux': 0.0,
            'pressure': 0.2367,
            'totalFlux': 204.1122589111,
            'uploadTime': 1536163371187,
            'reverseTotalFlux': 142.7911834717
          },
          {
            'recordTime': 1536156300000,
            'plusTotalFlux': 346.9078063965,
            'flux': 0.0,
            'pressure': 0.2361,
            'totalFlux': 204.1166229248,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.7911834717
          },
          {
            'recordTime': 1536156600000,
            'plusTotalFlux': 346.9237060547,
            'flux': 0.0,
            'pressure': 0.2336,
            'totalFlux': 204.132522583,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.7911834717
          },
          {
            'recordTime': 1536156900000,
            'plusTotalFlux': 346.9483642578,
            'flux': 0.4731977284,
            'pressure': 0.2281,
            'totalFlux': 204.1571807861,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.7911834717
          },
          {
            'recordTime': 1536157200000,
            'plusTotalFlux': 346.9643859863,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 204.1732025146,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.7911834717
          },
          {
            'recordTime': 1536157500000,
            'plusTotalFlux': 346.9643859863,
            'flux': 0.0,
            'pressure': 0.226,
            'totalFlux': 204.1732025146,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.7911834717
          },
          {
            'recordTime': 1536157800000,
            'plusTotalFlux': 346.9643859863,
            'flux': 0.0,
            'pressure': 0.2214,
            'totalFlux': 204.1732025146,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.7911834717
          },
          {
            'recordTime': 1536158100000,
            'plusTotalFlux': 346.9700317383,
            'flux': 0.0,
            'pressure': 0.23,
            'totalFlux': 204.1699829102,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8000488281
          },
          {
            'recordTime': 1536158400000,
            'plusTotalFlux': 346.9700317383,
            'flux': 0.0,
            'pressure': 0.2232,
            'totalFlux': 204.1699829102,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8000488281
          },
          {
            'recordTime': 1536158700000,
            'plusTotalFlux': 346.9789428711,
            'flux': 0.0,
            'pressure': 0.2281,
            'totalFlux': 204.1726837158,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8062591553
          },
          {
            'recordTime': 1536159000000,
            'plusTotalFlux': 346.9789428711,
            'flux': 0.0,
            'pressure': 0.23,
            'totalFlux': 204.1578979492,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8210449219
          },
          {
            'recordTime': 1536159300000,
            'plusTotalFlux': 346.9789428711,
            'flux': 0.0,
            'pressure': 0.2251,
            'totalFlux': 204.1578979492,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8210449219
          },
          {
            'recordTime': 1536159600000,
            'plusTotalFlux': 346.9861450195,
            'flux': 0.0,
            'pressure': 0.2348,
            'totalFlux': 204.1598358154,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8263092041
          },
          {
            'recordTime': 1536159900000,
            'plusTotalFlux': 346.994354248,
            'flux': 0.6286060214,
            'pressure': 0.2336,
            'totalFlux': 204.1680450439,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8263092041
          },
          {
            'recordTime': 1536160200000,
            'plusTotalFlux': 347.014831543,
            'flux': 0.0,
            'pressure': 0.2373,
            'totalFlux': 204.1885223389,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8263092041
          },
          {
            'recordTime': 1536160500000,
            'plusTotalFlux': 347.0201416016,
            'flux': 0.0,
            'pressure': 0.2409,
            'totalFlux': 204.1938323975,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8263092041
          },
          {
            'recordTime': 1536160800000,
            'plusTotalFlux': 347.0293273926,
            'flux': 0.0,
            'pressure': 0.2333,
            'totalFlux': 204.2030181885,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8263092041
          },
          {
            'recordTime': 1536161100000,
            'plusTotalFlux': 347.0504760742,
            'flux': 0.4953989089,
            'pressure': 0.2397,
            'totalFlux': 204.2177124023,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8327636719
          },
          {
            'recordTime': 1536161400000,
            'plusTotalFlux': 347.0530395508,
            'flux': 0.0,
            'pressure': 0.2422,
            'totalFlux': 204.2202758789,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8327636719
          },
          {
            'recordTime': 1536161700000,
            'plusTotalFlux': 347.0530395508,
            'flux': 0.0,
            'pressure': 0.2471,
            'totalFlux': 204.2202758789,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8327636719
          },
          {
            'recordTime': 1536162000000,
            'plusTotalFlux': 347.0530395508,
            'flux': 0.0,
            'pressure': 0.2507,
            'totalFlux': 204.2202758789,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8327636719
          },
          {
            'recordTime': 1536162300000,
            'plusTotalFlux': 347.0538024902,
            'flux': 0.3872887194,
            'pressure': 0.2538,
            'totalFlux': 204.2191772461,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8346252441
          },
          {
            'recordTime': 1536162600000,
            'plusTotalFlux': 347.0677185059,
            'flux': 0.0,
            'pressure': 0.2577,
            'totalFlux': 204.2330932617,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8346252441
          },
          {
            'recordTime': 1536162900000,
            'plusTotalFlux': 347.0677185059,
            'flux': 0.0,
            'pressure': 0.2495,
            'totalFlux': 204.2302398682,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8374786377
          },
          {
            'recordTime': 1536163200000,
            'plusTotalFlux': 347.0715026855,
            'flux': 0.0,
            'pressure': 0.2605,
            'totalFlux': 204.2340240479,
            'uploadTime': 1536163379270,
            'reverseTotalFlux': 142.8374786377
          },
          {
            'recordTime': 1536163500000,
            'plusTotalFlux': 347.0715026855,
            'flux': 0.0,
            'pressure': 0.2736,
            'totalFlux': 204.2340240479,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8374786377
          },
          {
            'recordTime': 1536163800000,
            'plusTotalFlux': 347.0715026855,
            'flux': 0.0,
            'pressure': 0.2757,
            'totalFlux': 204.2340240479,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8374786377
          },
          {
            'recordTime': 1536164100000,
            'plusTotalFlux': 347.0739135742,
            'flux': 0.3370946646,
            'pressure': 0.2773,
            'totalFlux': 204.2350616455,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8388519287
          },
          {
            'recordTime': 1536164400000,
            'plusTotalFlux': 347.0772705078,
            'flux': -1.1523160934,
            'pressure': 0.2061,
            'totalFlux': 204.2293243408,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.847946167
          },
          {
            'recordTime': 1536164700000,
            'plusTotalFlux': 347.0772705078,
            'flux': 0.0,
            'pressure': 0.2003,
            'totalFlux': 204.2216949463,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8555755615
          },
          {
            'recordTime': 1536165000000,
            'plusTotalFlux': 347.0772705078,
            'flux': 0.0,
            'pressure': 0.208,
            'totalFlux': 204.2162017822,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8610687256
          },
          {
            'recordTime': 1536165300000,
            'plusTotalFlux': 347.0772705078,
            'flux': 0.0,
            'pressure': 0.2095,
            'totalFlux': 204.2162017822,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8610687256
          },
          {
            'recordTime': 1536165600000,
            'plusTotalFlux': 347.0772705078,
            'flux': 0.0,
            'pressure': 0.2116,
            'totalFlux': 204.2116851807,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8655853271
          },
          {
            'recordTime': 1536165900000,
            'plusTotalFlux': 347.0921325684,
            'flux': 0.0,
            'pressure': 0.2165,
            'totalFlux': 204.2265472412,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8655853271
          },
          {
            'recordTime': 1536166200000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2165,
            'totalFlux': 204.2281341553,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8655853271
          },
          {
            'recordTime': 1536166500000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2171,
            'totalFlux': 204.2281341553,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8655853271
          },
          {
            'recordTime': 1536166800000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2242,
            'totalFlux': 204.2281341553,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8655853271
          },
          {
            'recordTime': 1536167100000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2229,
            'totalFlux': 204.2281341553,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8655853271
          },
          {
            'recordTime': 1536167400000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2177,
            'totalFlux': 204.2281341553,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8655853271
          },
          {
            'recordTime': 1536167700000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2235,
            'totalFlux': 204.2281341553,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.8655853271
          },
          {
            'recordTime': 1536168000000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2278,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536168300000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2336,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536168600000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2373,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536168900000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2373,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536169200000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2409,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536169500000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2397,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536169800000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.237,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536170100000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2422,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536170400000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2406,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536170700000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2434,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536171000000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2471,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536171300000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2483,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536171600000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2495,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536171900000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2458,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536172200000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2477,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536172500000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2471,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536172800000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2525,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536173100000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.255,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536173400000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.258,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536173700000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2593,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536174000000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2571,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536174300000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2556,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536174600000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2547,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536174900000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.259,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536175200000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2556,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536175500000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2593,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536175800000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2583,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536176100000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2593,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536176400000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2593,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536176700000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2593,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536177000000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2571,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536177300000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2577,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536177600000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2626,
            'totalFlux': 204.2267456055,
            'uploadTime': 1536184957217,
            'reverseTotalFlux': 142.866973877
          },
          {
            'recordTime': 1536177900000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2617,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536178200000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2617,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536178500000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2635,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536178800000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2629,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536179100000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2644,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536179400000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2657,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536179700000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2611,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536180000000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2605,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536180300000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2648,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536180600000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2638,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536180900000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2651,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536181200000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2641,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536181500000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2641,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536181800000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2654,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536182100000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2614,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536182400000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2617,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536182700000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2617,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536183000000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2648,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536183300000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2632,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536183600000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2617,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536183900000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2617,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536184200000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.258,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536184500000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2614,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536184800000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2629,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536184968110,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536185100000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.258,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536185400000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.259,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536185700000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2605,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536186000000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2599,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536186300000,
            'plusTotalFlux': 347.0937194824,
            'flux': 0.0,
            'pressure': 0.2568,
            'totalFlux': 204.2222137451,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536186600000,
            'plusTotalFlux': 347.1120300293,
            'flux': 0.3415073454,
            'pressure': 0.251,
            'totalFlux': 204.240524292,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536186900000,
            'plusTotalFlux': 347.1436462402,
            'flux': 0.0,
            'pressure': 0.2504,
            'totalFlux': 204.2721405029,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536187200000,
            'plusTotalFlux': 347.1842346191,
            'flux': 0.747913301,
            'pressure': 0.251,
            'totalFlux': 204.3127288818,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536187500000,
            'plusTotalFlux': 347.2252502441,
            'flux': 0.0,
            'pressure': 0.2403,
            'totalFlux': 204.3537445068,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536187800000,
            'plusTotalFlux': 347.2252502441,
            'flux': 0.0,
            'pressure': 0.2428,
            'totalFlux': 204.3537445068,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536188100000,
            'plusTotalFlux': 347.2295227051,
            'flux': 0.0,
            'pressure': 0.2409,
            'totalFlux': 204.3580169678,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8715057373
          },
          {
            'recordTime': 1536188400000,
            'plusTotalFlux': 347.2295227051,
            'flux': 0.0,
            'pressure': 0.2333,
            'totalFlux': 204.3547058105,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8748168945
          },
          {
            'recordTime': 1536188700000,
            'plusTotalFlux': 347.2295227051,
            'flux': 0.0,
            'pressure': 0.2281,
            'totalFlux': 204.3500213623,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8795013428
          },
          {
            'recordTime': 1536189000000,
            'plusTotalFlux': 347.2388916016,
            'flux': 0.0,
            'pressure': 0.229,
            'totalFlux': 204.3593902588,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8795013428
          },
          {
            'recordTime': 1536189300000,
            'plusTotalFlux': 347.2388916016,
            'flux': 0.0,
            'pressure': 0.2181,
            'totalFlux': 204.3593902588,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8795013428
          },
          {
            'recordTime': 1536189600000,
            'plusTotalFlux': 347.2388916016,
            'flux': 0.0,
            'pressure': 0.2159,
            'totalFlux': 204.3593902588,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8795013428
          },
          {
            'recordTime': 1536189900000,
            'plusTotalFlux': 347.2388916016,
            'flux': 0.0,
            'pressure': 0.2159,
            'totalFlux': 204.3593902588,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8795013428
          },
          {
            'recordTime': 1536190200000,
            'plusTotalFlux': 347.2415771484,
            'flux': 0.397906661,
            'pressure': 0.2825,
            'totalFlux': 204.359161377,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8824157715
          },
          {
            'recordTime': 1536190500000,
            'plusTotalFlux': 347.2726135254,
            'flux': 0.0,
            'pressure': 0.2861,
            'totalFlux': 204.3901977539,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8824157715
          },
          {
            'recordTime': 1536190800000,
            'plusTotalFlux': 347.2726135254,
            'flux': 0.0,
            'pressure': 0.2764,
            'totalFlux': 204.3901977539,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8824157715
          },
          {
            'recordTime': 1536191100000,
            'plusTotalFlux': 347.3033447266,
            'flux': 0.516248703,
            'pressure': 0.2602,
            'totalFlux': 204.4209289551,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8824157715
          },
          {
            'recordTime': 1536191400000,
            'plusTotalFlux': 347.3324279785,
            'flux': 0.5011904836,
            'pressure': 0.2492,
            'totalFlux': 204.450012207,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8824157715
          },
          {
            'recordTime': 1536191700000,
            'plusTotalFlux': 347.3659362793,
            'flux': 0.742507875,
            'pressure': 0.233,
            'totalFlux': 204.4835205078,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8824157715
          },
          {
            'recordTime': 1536192000000,
            'plusTotalFlux': 347.4023742676,
            'flux': 0.0,
            'pressure': 0.23,
            'totalFlux': 204.5199584961,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8824157715
          },
          {
            'recordTime': 1536192300000,
            'plusTotalFlux': 347.4105224609,
            'flux': 0.0,
            'pressure': 0.2153,
            'totalFlux': 204.5281066895,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8824157715
          },
          {
            'recordTime': 1536192600000,
            'plusTotalFlux': 347.4244689941,
            'flux': 0.4027329981,
            'pressure': 0.2043,
            'totalFlux': 204.5360565186,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8884124756
          },
          {
            'recordTime': 1536192900000,
            'plusTotalFlux': 347.4722900391,
            'flux': 0.5726204515,
            'pressure': 0.1909,
            'totalFlux': 204.5838775635,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8884124756
          },
          {
            'recordTime': 1536193200000,
            'plusTotalFlux': 347.4833679199,
            'flux': 0.0,
            'pressure': 0.1811,
            'totalFlux': 204.5949554443,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8884124756
          },
          {
            'recordTime': 1536193500000,
            'plusTotalFlux': 347.494934082,
            'flux': 0.0,
            'pressure': 0.1799,
            'totalFlux': 204.6065216064,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8884124756
          },
          {
            'recordTime': 1536193800000,
            'plusTotalFlux': 347.5111694336,
            'flux': 0.0,
            'pressure': 0.2174,
            'totalFlux': 204.622756958,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.8884124756
          },
          {
            'recordTime': 1536194100000,
            'plusTotalFlux': 347.5111694336,
            'flux': -0.3339606524,
            'pressure': 0.2177,
            'totalFlux': 204.604019165,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9071502686
          },
          {
            'recordTime': 1536194400000,
            'plusTotalFlux': 347.5127258301,
            'flux': 0.0,
            'pressure': 0.2184,
            'totalFlux': 204.601852417,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9108734131
          },
          {
            'recordTime': 1536194700000,
            'plusTotalFlux': 347.527130127,
            'flux': 0.3390252292,
            'pressure': 0.2138,
            'totalFlux': 204.6162567139,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9108734131
          },
          {
            'recordTime': 1536195000000,
            'plusTotalFlux': 347.5331420898,
            'flux': 0.0,
            'pressure': 0.2101,
            'totalFlux': 204.6207427979,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.912399292
          },
          {
            'recordTime': 1536195300000,
            'plusTotalFlux': 347.5437011719,
            'flux': 0.9471449852,
            'pressure': 0.219,
            'totalFlux': 204.6171417236,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9265594482
          },
          {
            'recordTime': 1536195600000,
            'plusTotalFlux': 347.548614502,
            'flux': 0.0,
            'pressure': 0.2239,
            'totalFlux': 204.6188659668,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9297485352
          },
          {
            'recordTime': 1536195900000,
            'plusTotalFlux': 347.548614502,
            'flux': 0.0,
            'pressure': 0.2159,
            'totalFlux': 204.6059265137,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9426879883
          },
          {
            'recordTime': 1536196200000,
            'plusTotalFlux': 347.548614502,
            'flux': 0.0,
            'pressure': 0.2174,
            'totalFlux': 204.6059265137,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9426879883
          },
          {
            'recordTime': 1536196500000,
            'plusTotalFlux': 347.548614502,
            'flux': 0.0,
            'pressure': 0.2165,
            'totalFlux': 204.6059265137,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9426879883
          },
          {
            'recordTime': 1536196800000,
            'plusTotalFlux': 347.5534057617,
            'flux': 0.0,
            'pressure': 0.215,
            'totalFlux': 204.6053771973,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9480285645
          },
          {
            'recordTime': 1536197100000,
            'plusTotalFlux': 347.5534057617,
            'flux': 0.0,
            'pressure': 0.2229,
            'totalFlux': 204.592666626,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9607391357
          },
          {
            'recordTime': 1536197400000,
            'plusTotalFlux': 347.555847168,
            'flux': 0.3390252292,
            'pressure': 0.2159,
            'totalFlux': 204.5681304932,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9877166748
          },
          {
            'recordTime': 1536197700000,
            'plusTotalFlux': 347.556427002,
            'flux': -0.8106108308,
            'pressure': 0.2129,
            'totalFlux': 204.5628051758,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 142.9936218262
          },
          {
            'recordTime': 1536198000000,
            'plusTotalFlux': 347.556427002,
            'flux': 0.0,
            'pressure': 0.2077,
            'totalFlux': 204.5554656982,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 143.0009613037
          },
          {
            'recordTime': 1536198300000,
            'plusTotalFlux': 347.566192627,
            'flux': 0.0,
            'pressure': 0.2043,
            'totalFlux': 204.5551300049,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 143.0110626221
          },
          {
            'recordTime': 1536198600000,
            'plusTotalFlux': 347.6135864258,
            'flux': 0.6430851221,
            'pressure': 0.2055,
            'totalFlux': 204.5989227295,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 143.0146636963
          },
          {
            'recordTime': 1536198900000,
            'plusTotalFlux': 347.660949707,
            'flux': 0.7183761001,
            'pressure': 0.1955,
            'totalFlux': 204.6462860107,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 143.0146636963
          },
          {
            'recordTime': 1536199200000,
            'plusTotalFlux': 347.7037658691,
            'flux': 0.0,
            'pressure': 0.1982,
            'totalFlux': 204.6876678467,
            'uploadTime': 1536206528823,
            'reverseTotalFlux': 143.0160980225
          },
          {
            'recordTime': 1536199500000,
            'plusTotalFlux': 347.7135925293,
            'flux': 0.995408535,
            'pressure': 0.1949,
            'totalFlux': 204.6974945068,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0160980225
          },
          {
            'recordTime': 1536199800000,
            'plusTotalFlux': 347.7444152832,
            'flux': 0.0,
            'pressure': 0.1936,
            'totalFlux': 204.7283172607,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0160980225
          },
          {
            'recordTime': 1536200100000,
            'plusTotalFlux': 347.7573547363,
            'flux': 0.0,
            'pressure': 0.1955,
            'totalFlux': 204.7260437012,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0313110352
          },
          {
            'recordTime': 1536200400000,
            'plusTotalFlux': 347.7702941895,
            'flux': 0.0,
            'pressure': 0.1848,
            'totalFlux': 204.7389831543,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0313110352
          },
          {
            'recordTime': 1536200700000,
            'plusTotalFlux': 347.7702941895,
            'flux': 0.0,
            'pressure': 0.1836,
            'totalFlux': 204.7389831543,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0313110352
          },
          {
            'recordTime': 1536201000000,
            'plusTotalFlux': 347.7980651855,
            'flux': 0.4962263107,
            'pressure': 0.1759,
            'totalFlux': 204.7667541504,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0313110352
          },
          {
            'recordTime': 1536201300000,
            'plusTotalFlux': 347.824798584,
            'flux': 0.0,
            'pressure': 0.1921,
            'totalFlux': 204.7934875488,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0313110352
          },
          {
            'recordTime': 1536201600000,
            'plusTotalFlux': 347.8663024902,
            'flux': 0.7656743526,
            'pressure': 0.1927,
            'totalFlux': 204.8349914551,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0313110352
          },
          {
            'recordTime': 1536201900000,
            'plusTotalFlux': 347.9001159668,
            'flux': -1.1407328844,
            'pressure': 0.1063,
            'totalFlux': 204.8464508057,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0536651611
          },
          {
            'recordTime': 1536202200000,
            'plusTotalFlux': 347.9445800781,
            'flux': 0.0,
            'pressure': 0.1857,
            'totalFlux': 204.8890075684,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0555725098
          },
          {
            'recordTime': 1536202500000,
            'plusTotalFlux': 347.9474182129,
            'flux': 0.0,
            'pressure': 0.1878,
            'totalFlux': 204.8918457031,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0555725098
          },
          {
            'recordTime': 1536202800000,
            'plusTotalFlux': 347.9698791504,
            'flux': 0.3824623525,
            'pressure': 0.1857,
            'totalFlux': 204.9143066406,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0555725098
          },
          {
            'recordTime': 1536203100000,
            'plusTotalFlux': 347.9705200195,
            'flux': 0.0,
            'pressure': 0.1854,
            'totalFlux': 204.9149475098,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0555725098
          },
          {
            'recordTime': 1536203400000,
            'plusTotalFlux': 347.9705200195,
            'flux': 0.0,
            'pressure': 0.1811,
            'totalFlux': 204.9057769775,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.064743042
          },
          {
            'recordTime': 1536203700000,
            'plusTotalFlux': 347.9838562012,
            'flux': 0.3612264693,
            'pressure': 0.1875,
            'totalFlux': 204.9191131592,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.064743042
          },
          {
            'recordTime': 1536204000000,
            'plusTotalFlux': 348.003692627,
            'flux': -0.44284302,
            'pressure': 0.1811,
            'totalFlux': 204.9305419922,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0731506348
          },
          {
            'recordTime': 1536204300000,
            'plusTotalFlux': 348.0503234863,
            'flux': 1.3757246733,
            'pressure': 0.1845,
            'totalFlux': 204.9764404297,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0738830566
          },
          {
            'recordTime': 1536204600000,
            'plusTotalFlux': 348.0952453613,
            'flux': 0.492503047,
            'pressure': 0.1884,
            'totalFlux': 205.0131988525,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0820465088
          },
          {
            'recordTime': 1536204900000,
            'plusTotalFlux': 348.1358947754,
            'flux': 1.0977270603,
            'pressure': 0.1869,
            'totalFlux': 205.0538482666,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0820465088
          },
          {
            'recordTime': 1536205200000,
            'plusTotalFlux': 348.1985778809,
            'flux': 0.4027329981,
            'pressure': 0.1958,
            'totalFlux': 205.1165313721,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0820465088
          },
          {
            'recordTime': 1536205500000,
            'plusTotalFlux': 348.2308654785,
            'flux': 0.6315018535,
            'pressure': 0.1945,
            'totalFlux': 205.1488189697,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0820465088
          },
          {
            'recordTime': 1536205800000,
            'plusTotalFlux': 348.2505493164,
            'flux': 0.425899446,
            'pressure': 0.2019,
            'totalFlux': 205.1685028076,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0820465088
          },
          {
            'recordTime': 1536206100000,
            'plusTotalFlux': 348.303314209,
            'flux': 0.6546683311,
            'pressure': 0.2061,
            'totalFlux': 205.2212677002,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0820465088
          },
          {
            'recordTime': 1536206400000,
            'plusTotalFlux': 348.3621520996,
            'flux': 0.0,
            'pressure': 0.2019,
            'totalFlux': 205.2801055908,
            'uploadTime': 1536206536287,
            'reverseTotalFlux': 143.0820465088
          }
        ],
        'latitude': 45.601699,
        'remark': null,
        'installTime': 1533091180000,
        'deviceId': 63,
        'deviceName': 'LUF',
        'userCode': '001003937',
        'onStage': 'ON_LINE',
        'manufacturer': 1,
        'diameter': 100,
        'provider': '拓安信',
        'sim': null,
        'id': 63,
        'department': '供水研究所',
        'longitude': 84.863102,
        'deviceType': '水表类',
        'address': null,
        'isRemote': true,
        'deviceStage': 'NORMAL',
        'lastCallTime': null,
        'maintainPersonPhone': '15989869508',
        'deviceCode': '1804202236',
        'userName': '克拉玛依区人民政府机关事务局',
        'siteName': '克拉玛依区人民政府机关事务局',
        'siteCode': '1804202236',
        'siteId': 1804202236,
        'policeFlag': 'COMPLETE',
        'modBusCode': null,
        'deviceModel': 'MA1011011300EH02P-L',
        'maintainPerson': '刘小芳',
        'installPerson': null
      }
    ],
    'target': [
      {
        'unit': 'MPa',
        'minValue': 0.0,
        'maxValue': 2.0,
        'id': 10,
        'label': '压力',
        'mark': 'pressure'
      },
      {
        'unit': 'm³/h',
        'minValue': 0.0,
        'maxValue': null,
        'id': 13,
        'label': '瞬时流量',
        'mark': 'flux'
      },
      {
        'unit': 'm³',
        'minValue': 0.0,
        'maxValue': null,
        'id': 14,
        'label': '正向累计',
        'mark': 'plusTotalFlux'
      },
      {
        'unit': 'm³',
        'minValue': 0.0,
        'maxValue': null,
        'id': 16,
        'label': '反向累计',
        'mark': 'reverseTotalFlux'
      },
      {
        'unit': 'm³',
        'minValue': 0.0,
        'maxValue': null,
        'id': 17,
        'label': '净累计',
        'mark': 'totalFlux'
      }
    ]
  }
}
