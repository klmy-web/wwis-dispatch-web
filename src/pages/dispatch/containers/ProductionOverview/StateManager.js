class StateManager {
  static setSessionStorageJson = (key, value) => {
    if (value) {
      sessionStorage.setItem(key, JSON.stringify(value));
    } else {
      sessionStorage.removeItem(key);
    }
    // this.notifyListener(key, value);
  }

  static getSessionStorageJson = (key) => {
    let value = sessionStorage.getItem(key);
    if (value) {
      value = JSON.parse(sessionStorage.getItem(key));
    }
    return value;
  }

  static setSessionStorageText = (key, value) => {
    if (value) {
      sessionStorage.setItem(key, value);
    } else {
      sessionStorage.removeItem(key);
    }
  }

  static getSessionStorageText = (key) => {
    let value = null;
    if (sessionStorage[key]) {
      value = sessionStorage[key];
    }
    return value;
  }


  static KEY_OBJECT_KEY = "KEY_OBJECT_KEY"
  static KEY_MXD_ID = "KEY_MXD_ID"

  getMxdID = () => {
    return StateManager.getSessionStorageText(StateManager.KEY_MXD_ID)
  }

  setMxdID = (mxdID) => {
    StateManager.setSessionStorageText(StateManager.KEY_MXD_ID, mxdID)
  }

  getObjectKey = () => {
    return StateManager.setSessionStorageText(StateManager.KEY_OBJECT_KEY)
  }

  setObjectKey = (key) => {
    StateManager.setSessionStorageText(StateManager.KEY_OBJECT_KEY, key)
  }

  static KEY_FID = "KEY_FID"

  getFid = () => {
    return StateManager.getSessionStorageText(StateManager.KEY_FID)
  }

  setFid = (fid) => {
    StateManager.setSessionStorageText(StateManager.KEY_FID, fid)
  }
}

let stateManager = new StateManager();
export {stateManager};
