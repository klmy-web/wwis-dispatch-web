import {CanvasUtil} from "./MyMap/CanvasUtil";
import {Const} from "./Const";

class MyMapShpJsonAdapter {
  getMapInfo = (basicMapInfo, data) => {
    let mapInfo = {
      ...basicMapInfo
    }
    let shpJson = data
    mapInfo = this.getMapInfoRange(shpJson, mapInfo)
    mapInfo = this.getMapInfoNode(shpJson, mapInfo)
    mapInfo = this.getMapInfoLine(shpJson, mapInfo)
    mapInfo = this.getMapInfoArea(shpJson, mapInfo)
    mapInfo = this.getMapInfoLabel(shpJson, mapInfo)
    return mapInfo
  }

  getNumberValue = (value) => {
    if (typeof value === 'string') {
      return parseFloat(value)
    }
    if (typeof value === 'number') {
      return value
    }
    return null
  }

  getMapInfoNode = (shpJson, mapInfo) => {
    let nodes = this.getLayerObjectsLayerNode(shpJson)
    // 转换坐标
    nodes = nodes.map(node => {
      let x = this.getNodeX(node)
      let y = this.getNodeY(node)
      x = this.getNewMapX(mapInfo, x)
      y = this.getNewMapY(mapInfo, y)
      let name = node.attributes.NAME
      let type = node.attributes.Type
      let iconName = node.attributes.icon;
      let iconWidth = node.attributes.width;
      let iconHeight = node.attributes.height;
      let data = node
      node = {
        x: x,
        y: y,
        name: name,
        type: type,
        iconName: iconName,
        iconWidth: this.getNumberValue(iconWidth) * 2,
        iconHeight: this.getNumberValue(iconHeight) * 2,
        data: data
      }
      return node;
    })

    mapInfo = {
      ...mapInfo,
      nodes,
    }
    return mapInfo
  }

  convertLinesToNewLines(mapInfo, lines) {
    let newLines = []
    lines.forEach(line => {
      let paths = this.getLinePaths(line)
      for (let i = 0; i < paths.length - 1; i++) {
        let startPoint = paths[i]
        let endPoint = paths[i + 1]

        let startX = this.getLineX(startPoint)
        let startY = this.getLineY(startPoint)
        let endX = this.getLineX(endPoint)
        let endY = this.getLineY(endPoint)

        startX = this.getNewMapX(mapInfo, startX)
        startY = this.getNewMapY(mapInfo, startY)
        endX = this.getNewMapX(mapInfo, endX)
        endY = this.getNewMapY(mapInfo, endY)
        let name = line.attributes.NAME
        let isForwardDirection = line.attributes.FLOW === "正向"
        let data = line
        if (isForwardDirection) {
          newLines.push(
            {
              startX: startX,
              startY: startY,
              endX: endX,
              endY: endY,
              name: name,
              data: data
            }
          )
        } else {
          newLines.push(
            {
              startX: endX,
              startY: endY,
              endX: startX,
              endY: startY,
              name: name,
              data: data
            }
          )
        }
      }
    })
    return newLines
  }

  getMapInfoLine = (shpJson, mapInfo) => {
    let lines = this.getLayerObjectsLayerLine(shpJson)
    // 第二层的线会包含折线，将折线拆成线段，这里可能有地雷，后面加label的时候，折线不知道加到哪里
    lines = this.convertLinesToNewLines(mapInfo, lines)

    let flashLines = lines.map(line => {
      let startX = line.startX
      let startY = line.startY
      let endX = line.endX
      let endY = line.endY
      let flashLine = CanvasUtil.flashLineCreate(startX, startY, endX, endY)
      return flashLine
    })

    mapInfo = {
      ...mapInfo,
      flashLines,
      lines
    }
    return mapInfo
  }

  getMapInfoArea = (shpJson, mapInfo) => {
    let areas = this.getLayerObjectsLayerArea(shpJson)
    areas = areas.map(area => {
        let paths = area.geometry.paths
        paths = paths[0]

        let points = []
        for (let i = 0; i < paths.length; i++) {
          let point = paths[i]
          let x = point[0]
          let y = point[1]
          x = this.getNewMapX(mapInfo, x)
          y = this.getNewMapY(mapInfo, y)
          point = [
            x,
            y
          ]
          points.push(point)
        }
        let lines = []
        for (let i = 0; i < paths.length - 1; i++) {
          let startPoint = paths[i]
          let endPoint = paths[i + 1]
          let startX = startPoint[0]
          let startY = startPoint[1]
          let endX = endPoint[0]
          let endY = endPoint[1]
          startX = this.getNewMapX(mapInfo, startX)
          startY = this.getNewMapY(mapInfo, startY)
          endX = this.getNewMapX(mapInfo, endX)
          endY = this.getNewMapY(mapInfo, endY)
          lines.push([
            {
              startX,
              startY
            },
            {
              endX,
              endY
            }
          ])
        }
        area = {
          points,
          lines
        }
        return area;
      }
    )
    mapInfo = {
      ...mapInfo,
      areas
    }
    return mapInfo

  }

  getMapInfoLabel = (shpJson, mapInfo) => {
    let labels = this.getLayerObjectsLayerLabel(shpJson)

    labels = labels.map(label => {
      let x = this.getNodeX(label)
      let y = this.getNodeY(label);
      x = this.getNewMapX(mapInfo, x)
      y = this.getNewMapY(mapInfo, y)
      let name = label.attributes.NAME
      let data = label
      label = {
        x,
        y,
        name,
        data
      }
      return label;
    })

    mapInfo = {
      ...mapInfo,
      labels
    }
    return mapInfo
  }

  getMapInfoRange = (shpJson, mapInfo) => {
    let canvasWidth = mapInfo.canvasWidth
    let canvasHeight = mapInfo.canvasHeight
    let borderSizeX = mapInfo.borderSizeX
    let borderSizeY = mapInfo.borderSizeY

    // let nodes = this.getLayerByName(shpJson, "Point").featureSet.features
    // let lines = this.getLayerByName(shpJson, "Line").featureSet.features
    // let areas = this.getLayerByName(shpJson, "Frame").featureSet.features
    // let labels = this.getLayerByName(shpJson, "Lable").featureSet.features

    let nodes = this.getLayerObjectsLayerNode(shpJson)
    let lines = this.getLayerObjectsLayerLine(shpJson)
    let areas = this.getLayerObjectsLayerArea(shpJson)
    let labels = this.getLayerObjectsLayerLabel(shpJson)

    let minX = Number.MAX_VALUE
    let minY = Number.MAX_VALUE
    let maxX = Number.MIN_VALUE
    let maxY = Number.MIN_VALUE
    nodes.forEach((node) => {
      let x = this.getNodeX(node)
      let y = this.getNodeY(node)
      if (x < minX) {
        minX = x
      }
      if (y < minY) {
        minY = y
      }
      if (x > maxX) {
        maxX = x
      }
      if (y > maxY) {
        maxY = y
      }
    })
    lines.forEach((line) => {
      let startX = this.getLineStartX(line)
      let startY = this.getLineStartY(line)

      if (startX < minX) {
        minX = startX
      }
      if (startY < minY) {
        minY = startY
      }
      if (startX > maxX) {
        maxX = startX
      }
      if (startY > maxY) {
        maxY = startY
      }

      let endX = this.getLineEndX(line)
      let endY = this.getLineEndY(line)

      if (endX < minX) {
        minX = endX
      }
      if (endY < minY) {
        minY = endY
      }
      if (endX > maxX) {
        maxX = endX
      }
      if (endY > maxY) {
        maxY = endY
      }
    })
    areas.forEach((area) => {
      let paths = area.geometry.paths
      paths = paths[0]
      for (let i = 0; i < paths.length; i++) {
        let point = paths[i]
        let x = point[0]
        let y = point[1]
        if (x < minX) {
          minX = x
        }
        if (y < minY) {
          minY = y
        }
        if (x > maxX) {
          maxX = x
        }
        if (y > maxY) {
          maxY = y
        }
      }
    })
    labels.forEach((label) => {
      let x = this.getNodeX(label)
      let y = this.getNodeY(label)
      if (x < minX) {
        minX = x
      }
      if (y < minY) {
        minY = y
      }
      if (x > maxX) {
        maxX = x
      }
      if (y > maxY) {
        maxY = y
      }
    })

    let rangeWidth = maxX - minX
    let rangeHeight = maxY - minY
    let canvasRangeWidth = canvasWidth - borderSizeX * 2
    let canvasRangeHeight = canvasHeight - borderSizeY * 2

    let rateX = rangeWidth / canvasRangeWidth
    let rateY = rangeHeight / canvasRangeHeight
    mapInfo = {
      ...mapInfo,
      rateX,
      rateY,
      minX,
      minY,
      canvasRangeWidth,
      canvasRangeHeight
    }
    return mapInfo
  }

  getLayerObjectsByName = (shpJson, layerName) => {
    let layer = this.getLayerByName(shpJson, layerName)
    if (layer) {
      return layer.featureSet.features
    }
    return null
  }

  getLayerObjectsLayerNode = (shpJson) => {
    let layerObjects = this.getLayerObjectsByName(shpJson, 'Point')
    if (!layerObjects) {
      return []
    }
    return layerObjects
  }

  getLayerObjectsLayerLine = (shpJson) => {
    let lines = this.getLayerObjectsByName(shpJson, 'Line')
    if (!lines) {
      lines = []
    }
    lines = lines.filter(line => {
      return (line.geometry && line.geometry.paths && line.geometry.paths.length > 0 && line.geometry.paths[0].length > 1)
    })
    return lines
  }

  getLayerObjectsLayerArea = (shpJson) => {
    let layerObjects = this.getLayerObjectsByName(shpJson, 'Frame')
    if (!layerObjects) {
      layerObjects = this.getLayerObjectsByName(shpJson, 'Freme')
    }
    if (!layerObjects) {
      return []
    }
    return layerObjects
  }

  getLayerObjectsLayerLabel = (shpJson) => {
    let layerObjects = this.getLayerObjectsByName(shpJson, 'Label')
    if (!layerObjects) {
      layerObjects = this.getLayerObjectsByName(shpJson, 'Lable')
    }
    if (!layerObjects) {
      return []
    }
    return layerObjects
  }

  getLayerByName = (shpJson, layerName) => {
    let layers = shpJson.layers;
    for (let i = 0; i < layers.length; i++) {
      let layer = shpJson.layers[i];
      let tempLayerName = layer.layerDefinition.name
      if (tempLayerName === layerName) {
        return layer
      }
    }
    return null
  }

  getNodeX = (node) => {
    return node.geometry.x
  }

  getNodeY = (node) => {
    return node.geometry.y
  }

  //////////////////////////////
  // 坐标系转换 (1)(2)
  //////////////////////////////
  // -y
  // |
  // |
  // |
  // -------------X
  // |
  // |  a   c
  // |    b
  // y
  //////////////////////////////
  // -y
  // |    b
  // |  a   c
  // |
  // -------------X
  // |
  // |
  // |
  // y
  //////////////////////////////
  // -y
  // |
  // |
  // |
  // -------------X
  // |
  // |    b
  // |  a   c
  // y
  //////////////////////////////
  // -y
  // |
  // |
  // |
  // -------------X
  // |
  // |    b
  // |  a   c
  // y
  //////////////////////////////
  getNewMapX = (mapInfo, x) => {
    let minX = mapInfo.minX
    let rateX = mapInfo.rateX
    let borderSizeX = mapInfo.borderSizeX
    let nodeX = x - minX
    let newX = nodeX / rateX
    newX = newX + borderSizeX
    return newX
  }

  getNewMapY = (mapInfo, y) => {
    let minY = mapInfo.minY
    let rateY = mapInfo.rateY
    let canvasRangeHeight = mapInfo.canvasRangeHeight
    let borderSizeY = mapInfo.borderSizeY
    let nodeY = y - minY
    let newY = nodeY / rateY
    newY = -newY
    newY = newY + canvasRangeHeight
    newY = newY + borderSizeY
    return newY
  }

  getLinePaths = (line) => {
    let paths = line.geometry.paths[0]
    return paths
  }

  getLineX = (point) => {
    return point[0]
  }

  getLineY = (point) => {
    return point[1]
  }

  getLineStartX = (line) => {
    return line.geometry.paths[0][0][0]
  }

  getLineStartY = (line) => {
    return line.geometry.paths[0][0][1]
  }

  getLineEndX = (line) => {
    return line.geometry.paths[0][1][0]
  }

  getLineEndY = (line) => {
    return line.geometry.paths[0][1][1]
  }

  getPageNameByMxdID = (mxdID, dataID) => {
    if (!mxdID || '' === mxdID.trim()) {
      return null
    }
    if (mxdID === Const.mxdID_水厂详情页_第二水厂 || mxdID === Const.mxdID_水厂详情页_第六水厂 || mxdID === Const.mxdID_水厂详情页_三坪五化) {
      return "/Page11ShuiChang"
    }
    if (mxdID === Const.mxdID_山上水库 || mxdID === Const.mxdID_减压水库) {
      return "/Page12ShuiKu"
    }
    if (mxdID === Const.mxdID_详情页_白碱滩管汇间 || mxdID === Const.mxdID_阀门室1 || mxdID === Const.mxdID_阀门室2) {
      return "/Page13GuanHuiJian"
    }
    if (mxdID === Const.mxdID_上环 || mxdID === Const.mxdID_中环 || mxdID === Const.mxdID_下环 || mxdID === Const.mxdID_自流环) {
      return "/Page14ShiQvFenQv"
    }
    if (mxdID === Const.mxdID_清水池) {
      // fixme 这里其他同事填写
      return null
    }
    if (mxdID === Const.mxdID_水泵) {
      // fixme 这里其他同事填写
    }
    if (mxdID === Const.mxdID_阀门) {
      // fixme 这里其他同事填写
      return null
    }
    if (mxdID === Const.mxdID_压力监测仪) {
      // fixme 这里其他同事填写
      return null
    }
    if (mxdID === Const.mxdID_分区流量计) {
      // fixme 这里其他同事填写
      return "/Page12ShuiKu"
    }
    return null
  }

  getIconNameByMxdIDAndValue = (mxdID, value) => {
    if (mxdID === Const.mxdID_水泵) {
      return value === 0 ? Const.icon_水泵_关 : Const.icon_水泵_开
    }
    if (mxdID === Const.mxdID_阀门) {
      return value === 0 ? Const.icon_阀门_关 : Const.icon_阀门_开
    }
    return null
  }

  getMxdIDByNodeData = (data) => {
    if (!data) {
      return null
    }
    if (!data.attributes) {
      return null
    }
    if (!data.attributes.mxdID) {
      return null
    }
    return data.attributes.mxdID
  }

  getTypeFromNodeData = (data) => {
    if (!data) {
      return null
    }
    if (!data.attributes) {
      return null
    }
    if (!data.attributes.type) {
      return null
    }
    return data.attributes.type
  }

  getValueFromInfoListByDataId(infoList, dataID) {
    for (let i = 0; i < infoList.length; i++) {
      let info = infoList[i]
      if (dataID === info.dataID) {
        return info.value
      }
    }
    return null
  }

  getMockInfoListByMonitorInfo = (monitorInfo) => {
    let dataIDList = monitorInfo.dataIDList
    let dataIDMxdIDMap = monitorInfo.dataIDMxdIDMap
    let infoList = dataIDList.map(dataID => {
      let mxdID = dataIDMxdIDMap[dataID];
      let random = Math.floor(Math.random() * 10000)
      let value = (mxdID === Const.mxdID_水泵 || mxdID === Const.mxdID_阀门) ? (random % 2) : (random % 100)
      return {
        dataID: dataID,
        value: value
      }
    })
    return infoList
  }

  getInfoListByMonitorInfo = (monitorInfo) => {
    //fixme 这里要调用后台接口
    return this.getMockInfoListByMonitorInfo(monitorInfo)
  }

  getMapInfoByInfoList = (mapInfo, monitorInfo, infoList) => {
    mapInfo.nodes.forEach(node => {
      let data = node.data
      let dataID = this.getDataIDFromNodeData(data)
      if (dataID) {
        let value = this.getValueFromInfoListByDataId(infoList, dataID)
        let mxdID = this.getMxdIDByNodeData(data)
        if (mxdID === Const.mxdID_水泵 || mxdID === Const.mxdID_阀门) {
          let iconName = this.getIconNameByMxdIDAndValue(mxdID, value)
          if (iconName) {
            node.iconName = iconName
          }
        }
        if (mxdID === Const.mxdID_清水池 || mxdID === Const.mxdID_压力监测仪 || mxdID === Const.mxdID_分区流量计) {
          node.drawNodeExt = (context) => {
            this.drawNodeExt(context, node, value)
          }
        }
      }
    })
    return mapInfo

  }

  drawNodeExt = (context, node, value) => {

    let originalValue = value
    console.log({value})
    value = Math.round(value / 10) * 10


    let radius = 8
    let baseLength = radius * Math.sin(0.25 * Math.PI)
    let iconWidth = node.iconWidth
    let x = node.x - iconWidth * 0.5 - radius
    let y = node.y
    let color = '#006400'
    let colorYellow = '#EEC900'

    if (value === 0) {
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.strokeArc(context, 0, 0, radius, -1 / 4, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }

    if (value === 10) {
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.strokeArc(context, 0, 0, radius, -1 / 4, 1 / 4, color)
      CanvasUtil.fillArc(context, 0, 0, radius, 1 / 4, 3 / 4, color)
      CanvasUtil.strokeArc(context, 0, 0, radius, 3 / 4, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }

    if (value === 20) {
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.strokeArc(context, 0, 0, radius, -1 / 4, 1 / 4 - 1 / 6, color)
      CanvasUtil.fillArc(context, 0, 0, radius, 1 / 4 - 1 / 6, 3 / 4 + 1 / 6, color)
      CanvasUtil.strokeArc(context, 0, 0, radius, 3 / 4, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }
    if (value === 30) {
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.strokeArc(context, 0, 0, radius, -1 / 4, 0, color)
      CanvasUtil.fillArc(context, 0, 0, radius, 0, 1, color)
      CanvasUtil.strokeArc(context, 0, 0, radius, 1, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }
    if (value === 40) {
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.strokeArc(context, 0, 0, radius, -1 / 4, -1 / 6, color)
      CanvasUtil.fillArc(context, 0, 0, radius, -1 / 6, 1 + 1 / 6, color)
      CanvasUtil.strokeArc(context, 0, 0, radius, 1 + 1 / 6, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }
    if (value === 50) {
      context.save()
      context.translate(x, y)
      context.beginPath()
      // CanvasUtil.strokeArc(context, 0, 0, radius, -1 / 4, 1 / 4, color)
      CanvasUtil.fillArc(context, 0, 0, radius, -1 / 4, 1 + 1 / 4, color)
      // CanvasUtil.strokeArc(context, 0, 0, radius, 3 / 4, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }
    if (value === 50) {
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.fillArc(context, 0, 0, radius, -1 / 4, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }
    if (value === 60) {
      color = colorYellow
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.fillArc(context, 0, 0, radius, -1 / 4, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.fillRect(context, -baseLength, -baseLength * 2, baseLength * 2, baseLength * 1, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }
    if (value === 70) {
      color = colorYellow
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.fillArc(context, 0, 0, radius, -1 / 4, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.fillRect(context, -baseLength, -baseLength * 3, baseLength * 2, baseLength * 2, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }
    if (value === 80) {
      color = 'red'
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.fillArc(context, 0, 0, radius, -1 / 4, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.fillRect(context, -baseLength, -baseLength * 4, baseLength * 2, baseLength * 3, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }
    if (value === 90) {
      color = 'red'
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.fillArc(context, 0, 0, radius, -1 / 4, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.strokeArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.fillRect(context, -baseLength, -baseLength * 5, baseLength * 2, baseLength * 4, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }
    if (value === 100) {
      color = 'red'
      context.save()
      context.translate(x, y)
      context.beginPath()
      CanvasUtil.fillArc(context, 0, 0, radius, -1 / 4, 1 + 1 / 4, color)
      CanvasUtil.drawLine(context, -baseLength, -baseLength, -baseLength, -baseLength * 5, 1, color)
      CanvasUtil.fillArc(context, 0, -baseLength * 5, baseLength, 1, 2, color)
      CanvasUtil.fillRect(context, -baseLength, -baseLength * 5, baseLength * 2, baseLength * 4, color)
      CanvasUtil.drawLine(context, baseLength, -baseLength, baseLength, -baseLength * 5, 1, color)
      context.closePath()
      context.restore()
      CanvasUtil.drawText(context, "" + originalValue + "%", x, y + radius * 2, 10, color, "center")
    }
  }

  getDataIDFromNodeData = (data) => {
    if (!data) {
      return null
    }
    if (!data.attributes) {
      return null
    }
    if (!data.attributes.dataID) {
      return null
    }
    return data.attributes.dataID
  }

  getIconFromNodeData = (data) => {
    if (!data) {
      return null
    }
    if (!data.attributes) {
      return null
    }
    if (!data.attributes.icon) {
      return null
    }
    return data.attributes.icon
  }

  getNameFromNodeData = (data) => {
    if (!data) {
      return null
    }
    if (!data.attributes) {
      return null
    }
    if (!data.attributes.NAME) {
      return null
    }
    return data.attributes.NAME
  }

  getFIDByNodeData = (data) => {
    if (!data) {
      return null
    }
    if (!data.attributes) {
      return null
    }
    if (!data.attributes.FID) {
      return null
    }
    return data.attributes.FID
  }

  getMonitorInfoFromMapInfo = (mapInfo) => {
    let dataIDList = []
    let dataIDTypeMap = {}
    let dataIDIconMap = {}
    let dataIDMxdIDMap = {}

    mapInfo.nodes.forEach(node => {
      let data = node.data
      let dataID = this.getDataIDFromNodeData(data)
      let type = this.getTypeFromNodeData(data)
      let icon = this.getIconFromNodeData(data)
      let mxdID = this.getMxdIDByNodeData(data)

      if (dataID && (dataID !== " ")) {
        if (mxdID === Const.mxdID_清水池
          || mxdID === Const.mxdID_水泵
          || mxdID === Const.mxdID_阀门
          || mxdID === Const.mxdID_压力监测仪
          || mxdID === Const.mxdID_分区流量计
        ) {
          dataIDList.push(dataID)
          dataIDTypeMap[dataID] = type
          dataIDIconMap[dataID] = icon
          dataIDMxdIDMap[dataID] = mxdID
        }
      }
    })
    return {
      dataIDList,
      dataIDTypeMap,
      dataIDIconMap,
      dataIDMxdIDMap
    }
  }
}

export {MyMapShpJsonAdapter}
