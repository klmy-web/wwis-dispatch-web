const shpJsonPage11ShuiChang_1DiErShuiChang = {
  "layers": [{
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 2,
      "name": "Frame",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186386,
        "ymin": 875,
        "xmax": 9187249,
        "ymax": 1260,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [165, 83, 183, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "性质",
        "type": "esriFieldTypeString",
        "alias": "性质",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"性质": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186698, 1047], [9186924, 1047], [9186924, 1115], [9186698, 1115], [9186698, 1047]]]
        }
      }, {
        "attributes": {"FID": 1, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186947, 1260], [9186947, 1186], [9187249, 1186], [9187249, 1260], [9186947, 1260]]]
        }
      }, {
        "attributes": {"FID": 2, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186698, 917], [9186924, 917], [9186924, 985], [9186698, 985], [9186698, 917]]]
        }
      }, {
        "attributes": {"FID": 3, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186386, 875], [9186537, 875], [9186537, 920], [9186386, 920], [9186386, 875]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 0,
      "name": "Lable",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9185986,
        "ymin": 704,
        "xmax": 9187211,
        "ymax": 1367,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/RedSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAAB3VJREFUeF7tmPlTlEcexnve94U5mANQbgQSbgiHXHINlxpRIBpRI6wHorLERUmIisKCQWM8cqigESVQS1Kx1piNi4mW2YpbcZONrilE140RCTcy3DDAcL/zbJP8CYPDL+9Ufau7uqb7eZ7P+/a8PS8hwkcgIBAQCAgEBAICAYGAQEAgIBAQCAgEBAICAYGAQEAgIBAQCDx/AoowKXFMUhD3lQrioZaQRVRS+fxl51eBTZUTdZ41U1Rox13/0JF9csGJ05Qv4jSz/YPWohtvLmSKN5iTGGqTm1+rc6weICOBRbZs1UVnrv87T1PUeovxyNsUP9P6n5cpHtCxu24cbrmwKLdj+osWiqrVKhI0xzbmZ7m1SpJ+1pFpvE2DPvGTomOxAoNLLKGLscZYvB10cbYYjrJCb7A5mrxleOBqim+cWJRakZY0JfnD/LieI9V1MrKtwokbrAtU4Vm0A3TJnphJD4B+RxD0u0LA7w7FTE4oprOCMbklEGNrfdGf4IqnQTb4wc0MFTYibZqM7JgjO8ZdJkpMln/sKu16pHZGb7IfptIWg389DPp9kcChWODoMuDdBOhL1JgpisbUvghM7AqFbtNiaFP80RLnhbuBdqi0N+1dbUpWGde9gWpuhFi95yL7sS7BA93JAb+Fn8mh4QujgPeTgb9kAZf3Apd2A+fXQ38yHjOHozB1IAJjOSEY2RSIwVUv4dd4X9wJccGHNrJ7CYQ4GGjLeNNfM+dyvgpzQstKf3pbB2A6m97uBRE0/Ergcxr8hyqg7hrwn0vAtRIKIRX6Y2pMl0RhIj8co9nBGFrvh55l3ngU7YObng7IVnFvGS+BYUpmHziY/Ls2zgP9SX50by/G9N5w6I+ogYvpwK1SoOlHQNsGfWcd9Peqof88B/rTyzF9hAIopAByQzC0JQB9ST5oVnvhnt+LOGsprvUhxNIwa0aY7cGR6Cp7tr8+whkjawIxkRWC6YJI6N+lAKq3Qf/Tx+B77oGfaQc/8hB8w2Xwtw9Bf3kzZspXY/JIDEbfpAB2BKLvVV90Jvjgoac9vpRxE8kciTVCBMMkNirJ7k/tRHyjtxwjKV4Yp3t/6s+R4E+/DH3N6+BrS8E314Dvvg2+/Sb4hxfBf5sP/up2TF3ZhonK1zD6dhwGdwail26DzqgX8MRKiq9ZBpkSkmeYOyPM3m9Jjl+1Z9D8AgNtlAq6bZ70qsZi+q+bwV/7I/hbB8D/dAr8Axq89iz474p/G5++koHJy1sx/lkGdBc2YjA3HF0rHNHuboomuQj/5DgclIvOGCGCYRKFFuTMV7YUAD3VDQaLMfyqBcZORGPy01QKYSNm/rYV/Nd/Av9NHvgbueBrsjDzRQamKKDxT9Kgq1iLkbIUDOSHoiNcgnYHgnYZi+9ZExSbiSoMc2eE2flKcuJLa4KGRQz6/U0wlGaP0feiMH4uFpMXEjBVlYjp6lWY+SSZtim0kulYMiYuJEJXuhTDJ9UYPByOvoIwdCxfgE4bAo0Jh39xLAoVpMwIEQyTyFCQvGpLon9sJ0K3J4OBDDcMH1dj9FQsxkrjMPFRPCbOx2GyfLal9VEcxstioTulxjAFNfROJPqLl6Bnfyg6V7ugz5yBhuHwrZjBdiU5YJg7I8wOpifAKoVIW7uQ3rpOBH2b3ekVjYT2WCRG3o+mIGKgO0OrlIaebU/HYOQDNbQnojB4NJyGD0NPfjA0bwTRE6Q7hsUcWhkWN8yZqSQlWWGECAZLmJfJmbrvVSI8taK37xpbdB/wQW8xPee/8xIGjvlj8IQ/hk4G0JbWcX8MHPVDX4kveoq8ocn3xLM33NCZRcPHOGJYZIKfpQyq7JjHS6yJjcHujLHADgkpuC7h8F8zEVqXSNC2awE69lqhs8AamkO26HrbDt2H7dBVQov2NcW26CiwQtu+BWjdY4n2nZboTbfCmKcCnRyDO/YmyLPnDlHvjDH8G6zhS9/wlEnYR7X00fWrFYuWdVI0ZpuhcbcczW/R2qdAcz6t/bRov4mONeaaoYl+p22rHF0bVNAmKtBvweIXGxNcfFH8eNlC4m6wMWMusEnKpn5hyo48pj9gLe4SNG9QoGGLAk8z5XiaJUd99u8122/IpBA2K9BGg2vWWKAvRYVeLzEa7E1R422m2+MsSTem97nSYnfKyN6/mzATv7AUgqcMrUnmaFlLX3ysM0fj+t/b5lQLtK22QEfyAmiSLKFZpUJ7kBRPXKW4HqCYynWVHKSG2LkyZex1uO1mZM9lKem9Tx9jjY5iNEYo0bKMhn7ZAu0r6H5PpLXCAq0rKJClSjSGynE/QIkrQYqBPe6S2X+AJsY2Ped6iWZk6RlL0c2r5szofRsO9R5S1IfQLRCpQL1aifoYFerpsbkuTImaUJXuXIDiH6/Ys8vm3Mg8L2i20YqsO7fItKLcSXyn0kXccclVqv3MS6at9JU/Ox+ouns+SF6Z4cSupz7l8+z1ucs7LF1AQjOdxfGZzmx8Iu1TRcfnrioICAQEAgIBgYBAQCAgEBAICAQEAgIBgYBAQCAgEBAICAQEAv8H44b/6ZiGvGAAAAAASUVORK5CYII=",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NOTE",
        "type": "esriFieldTypeString",
        "alias": "NOTE",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {"attributes": {"NOTE": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "NOTE": "调节水库"},
        "geometry": {"x": 9187182, "y": 805, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 1, "NOTE": "水处理工艺"},
        "geometry": {"x": 9187019, "y": 975, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 2, "NOTE": "白碱滩"},
        "geometry": {"x": 9185988, "y": 1271, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 3, "NOTE": "白碱滩"},
        "geometry": {"x": 9185986, "y": 1221, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 4, "NOTE": "白碱滩"},
        "geometry": {"x": 9186011, "y": 839, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 5, "NOTE": "管汇间"},
        "geometry": {"x": 9186011, "y": 784, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 6, "NOTE": "去石化"},
        "geometry": {"x": 9186011, "y": 704, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 7, "NOTE": "已停用"},
        "geometry": {"x": 9186080, "y": 757, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 8, "NOTE": "老泵房"},
        "geometry": {"x": 9187184, "y": 1150, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 9, "NOTE": "新泵房"},
        "geometry": {"x": 9186648, "y": 1070, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 10, "NOTE": "清水池"},
        "geometry": {"x": 9186898, "y": 882, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 11, "NOTE": "清水池"},
        "geometry": {"x": 9186558, "y": 849, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 12, "NOTE": "二化"},
        "geometry": {"x": 9186402, "y": 1128, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 13, "NOTE": "扬水泵站"},
        "geometry": {"x": 9187211, "y": 1367, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 3,
      "name": "Line",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186017,
        "ymin": 683,
        "xmax": 9187137,
        "ymax": 1270,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [241, 152, 60, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "FLOW",
        "type": "esriFieldTypeString",
        "alias": "FLOW",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管径",
        "type": "esriFieldTypeString",
        "alias": "管径",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "起点_X",
        "type": "esriFieldTypeDouble",
        "alias": "起点_X",
        "sqlType": "sqlTypeOther",
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "终点_X",
        "type": "esriFieldTypeDouble",
        "alias": "终点_X",
        "sqlType": "sqlTypeOther",
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "起点_Y",
        "type": "esriFieldTypeDouble",
        "alias": "起点_Y",
        "sqlType": "sqlTypeOther",
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "终点_Y",
        "type": "esriFieldTypeString",
        "alias": "终点_Y",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"FLOW": null, "管径": null, "起点_X": null, "终点_X": null, "起点_Y": null, "终点_Y": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {
          "FID": 0,
          "FLOW": "反向",
          "管径": "dn800",
          "起点_X": 327032.841373,
          "终点_X": 327032.841373,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186811, 1222], [9186811, 1115]]]
        }
      }, {
        "attributes": {
          "FID": 1,
          "FLOW": "正向",
          "管径": "dn700",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057800.18435,
          "终点_Y": "5057800.18435411"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186811, 1222], [9186193, 1222]]]
        }
      }, {
        "attributes": {
          "FID": 2,
          "FLOW": "无方向",
          "管径": " ",
          "起点_X": 326681.739222,
          "终点_X": 326681.739222,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186461, 1032], [9186461, 977]]]
        }
      }, {
        "attributes": {
          "FID": 3,
          "FLOW": "无方向",
          "管径": " ",
          "起点_X": 326681.739222,
          "终点_X": 326681.739222,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186461, 977], [9186461, 920]]]
        }
      }, {
        "attributes": {
          "FID": 4,
          "FLOW": "正向",
          "管径": " ",
          "起点_X": 326533.709488,
          "终点_X": 326533.709488,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186314, 812], [9186314, 784]]]
        }
      }, {
        "attributes": {
          "FID": 5,
          "FLOW": "反向",
          "管径": " ",
          "起点_X": 326533.709488,
          "终点_X": 326533.709488,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186314, 732], [9186314, 704]]]
        }
      }, {
        "attributes": {
          "FID": 6,
          "FLOW": "正向",
          "管径": "dn700",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057416.5657,
          "终点_Y": "5057416.56570235"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186314, 837], [9186031, 837]]]
        }
      }, {
        "attributes": {
          "FID": 7,
          "FLOW": "正向",
          "管径": "dn1000",
          "起点_X": 327032.841373,
          "终点_X": 327032.841373,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186811, 985], [9186811, 1047]]]
        }
      }, {
        "attributes": {
          "FID": 8,
          "FLOW": "正向",
          "管径": "dn600",
          "起点_X": 326412.347833,
          "终点_X": 326412.347833,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186193, 1222], [9186193, 837]]]
        }
      }, {
        "attributes": {
          "FID": 9,
          "FLOW": "正向",
          "管径": "dn800*2",
          "起点_X": 327359.492648,
          "终点_X": 327359.492648,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187137, 803], [9187137, 1186]]]
        }
      }, {
        "attributes": {
          "FID": 10,
          "FLOW": "正向",
          "管径": "dn1000",
          "起点_X": 327271.651344,
          "终点_X": 327271.651344,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187049, 1186], [9187049, 802]]]
        }
      }, {
        "attributes": {
          "FID": 11,
          "FLOW": "正向",
          "管径": "dn1000",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057381.17953,
          "终点_Y": "5057381.17953007"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187049, 802], [9186813, 802]]]
        }
      }, {
        "attributes": {
          "FID": 12,
          "FLOW": "正向",
          "管径": "dn1000",
          "起点_X": 327032.841373,
          "终点_X": 327032.841373,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186811, 802], [9186811, 917]]]
        }
      }, {
        "attributes": {
          "FID": 13,
          "FLOW": "反向",
          "管径": "dn800",
          "起点_X": 326464.022654,
          "终点_X": 326464.022654,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186244, 692], [9186244, 1270]]]
        }
      }, {
        "attributes": {
          "FID": 14,
          "FLOW": "正向",
          "管径": "dn600",
          "起点_X": 326412.347833,
          "终点_X": 326412.347833,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186193, 1270], [9186193, 1222]]]
        }
      }, {
        "attributes": {
          "FID": 15,
          "FLOW": "正向",
          "管径": "dn600",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057848.3928,
          "终点_Y": "5057848.39280386"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186244, 1270], [9186193, 1270]]]
        }
      }, {
        "attributes": {
          "FID": 16,
          "FLOW": "正向",
          "管径": "dn600",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057848.3928,
          "终点_Y": "5057848.39280386"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186193, 1270], [9186017, 1270]]]
        }
      }, {
        "attributes": {
          "FID": 17,
          "FLOW": "正向",
          "管径": "dn700",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057800.18435,
          "终点_Y": "5057800.18435411"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186193, 1222], [9186017, 1222]]]
        }
      }, {
        "attributes": {
          "FID": 18,
          "FLOW": "反向",
          "管径": "dn800",
          "起点_X": 327032.841373,
          "终点_X": 327032.841373,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186811, 1270], [9186811, 1222]]]
        }
      }, {
        "attributes": {
          "FID": 19,
          "FLOW": "反向",
          "管径": " ",
          "起点_X": 326533.709488,
          "终点_X": 326533.709488,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186314, 837], [9186314, 812]]]
        }
      }, {
        "attributes": {
          "FID": 20,
          "FLOW": "反向",
          "管径": " ",
          "起点_X": 326533.709488,
          "终点_X": 326533.709488,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186314, 757], [9186314, 732]]]
        }
      }, {
        "attributes": {
          "FID": 21,
          "FLOW": "反向",
          "管径": " ",
          "起点_X": 326533.709488,
          "终点_X": 326533.709488,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186314, 704], [9186314, 683]]]
        }
      }, {
        "attributes": {
          "FID": 22,
          "FLOW": "正向",
          "管径": "dn700",
          "起点_X": 326567.016879,
          "终点_X": 326567.016879,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186347, 812], [9186347, 732]]]
        }
      }, {
        "attributes": {
          "FID": 23,
          "FLOW": "无方向",
          "管径": " ",
          "起点_X": 326656.315662,
          "终点_X": 326656.315662,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186436, 875], [9186436, 812]]]
        }
      }, {
        "attributes": {
          "FID": 24,
          "FLOW": "无方向",
          "管径": " ",
          "起点_X": 326736.799175,
          "终点_X": 326736.799175,
          "起点_Y": 0,
          "终点_Y": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186516, 875], [9186516, 732]]]
        }
      }, {
        "attributes": {
          "FID": 25,
          "FLOW": "正向",
          "管径": "dn600",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057848.3928,
          "终点_Y": "5057848.39280369"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186244, 1270], [9186811, 1270]]]
        }
      }, {
        "attributes": {
          "FID": 26,
          "FLOW": "无方向",
          "管径": " ",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057391.95785,
          "终点_Y": "5057391.95785308"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186347, 812], [9186436, 812]]]
        }
      }, {
        "attributes": {
          "FID": 27,
          "FLOW": "无方向",
          "管径": " ",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057311.95775,
          "终点_Y": "5057311.9577477"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186347, 732], [9186516, 732]]]
        }
      }, {
        "attributes": {
          "FID": 28,
          "FLOW": "反向",
          "管径": "dn600",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057290.18471,
          "终点_Y": "5057290.18471313"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186314, 710], [9186031, 710]]]
        }
      }, {
        "attributes": {
          "FID": 29,
          "FLOW": "无方向",
          "管径": "dn529",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057336.3657,
          "终点_Y": "5057336.3656987"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186314, 757], [9186103, 757]]]
        }
      }, {
        "attributes": {
          "FID": 30,
          "FLOW": "正向",
          "管径": "dn1020",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057391.95785,
          "终点_Y": "5057391.95785308"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186347, 812], [9186314, 812]]]
        }
      }, {
        "attributes": {
          "FID": 31,
          "FLOW": "正向",
          "管径": "dn426",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057363.64893,
          "终点_Y": "5057363.64892983"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186314, 784], [9186038, 784]]]
        }
      }, {
        "attributes": {
          "FID": 32,
          "FLOW": "正向",
          "管径": "dn1020",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057311.95777,
          "终点_Y": "5057311.95776603"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186347, 732], [9186314, 732]]]
        }
      }, {
        "attributes": {
          "FID": 33,
          "FLOW": "反向",
          "管径": "dn800",
          "起点_X": 0,
          "终点_X": 0,
          "起点_Y": 5057271.84512,
          "终点_Y": "5057271.84512401"
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186244, 692], [9186314, 692]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 1,
      "name": "Point",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186271,
        "ymin": 696,
        "xmax": 9187229,
        "ymax": 1221,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/OrangeSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAABv9JREFUeF7tl31UVVUaxk/34uXCBeTjqqXQoA1oGgqOOprlFwKKOpqCCq4IyEJL1CEmyxUijkjDOBAGFgM6qSz8KPEbCQQFP2JQhmwmhmlVSwvTIbgIJOLl7vPMc8U/5v+Ll382az1rn7PP3ft9n99+9z4HRZF/koAkIAlIApKAJCAJSAKSgCQgCUgCkoAkIAlIApKAJCAJPH4CrqtHKN7bntX5pY7R+a8drvgwpNvjDzuwEbTpzyovHgvRp5ye73Tm6jJDU9Mq19tWXVtuaDoRbigtCXXekhGgzGCaDgObaj9HXzxYCawId9r7nxiXNvzBA0jxgiXVC72PZEkxAu8NAZKN+PZl97byeYZ9q55SJvZzGgMzXepYJboxyvCd+rYH1G1G9GQMg/kDb6i7R0L9+Jk+5Y5Cb9av0LN9OLDlKYikofhmpduNnRM0qwYm636K+sexStx3cW7tSPOGOcsPlsJA4OBvgU+nAiXUsenUi8BR6hBVNB0ifxLMmeNYEb5ojjV2ZARoVvdTOvad5tWRSsj38cY7oBlzPk0X0+zRF4CTNFo6GygPAT6fC5ydyftZwKlg4Dh1ZA6wbxbMufxt+gT8FDvs53f8lHD7Zm9jtHkuypC6COMV7PwNHuQ/32e+hMbPUFWLgNoEoD4ZuJoI1EQCFTRdRihnCOQEwRxhu28OHuSyL30K6pcZ62I8lRE2pmW/4e8HOa3r2MyVz6PxA1bzM4DTNF8Z3mf8273AjyeB74uBr9IIYRkrgpVQxgooDWUlhHGrsN1DCNmz0fP7Mdg9Wf+W/RzYFslwPdL9ssicDFFI84dp/iRLvIztpWigaRfQegW4/wPUuw1Qb+yDWr+OlUHD1io4ywo4NZ/bhbCKwtCbx+rYEoSr4Yb6UDfF07bU7DB6povyQnO8Vxv+MgHqfpo+ai3tRwBq42i4EKKzDkL8CNFzHaLlMMQ3qRDXYqBeXsrtsIDVMo9VwPYgIewJgznlObRFOPYk+ioz7WDBthBZ4x0Sb672EmqGL7Cf+/8493PpHKjn50NcXwtxcxdE6wlCqIZoL4No/itE0yaIL1+lXoOoI6TKCI7jWXFkMVAQDMvb3miN0GLHWCXJtuzsMPpssNP7pjeHQk3nOz3fj6+8aVzVcIj6WIivEmj2XYgb2X3Gb+Zy9bewfw1EQxz1OsQ/3iSENVBLeTgWEV7OeFiSvHA3UouDU5QcO1iwLcT5UOeczsRhUHd4A3kjeQiO4erPY4lHUdE0SRD/osmvk6gNrApCsZqvp67FP6wAURsL9RwPxr1TgJ2jITa4o2O5BiXTlQLbsrPD6PJgpz+1v0EA232AD0dBPTwJonoBxGVWwRXqC17XLoL4+++oxdQS6iX2U1eoi7yvZlsVCbWIZ8eOZyDWOOHuCg0OPf/Eh3awYFuIvCBN0s04o4o0AsgmgJKZNERTF2n40kKC4FnwUGGPxOqw3l/kM+tvapZCnI+EqFwJ9RDfDClPojdOi9aVDtgZ6PCObdnZYfQiTyXkhyi3DmweAWTye59vAXGeK1rN1X4IgatvBXGJlfBQ1mv21fB5Nc1fiKD55RDlBHCA3wYbnfEgRoO2aEfzhtHaMDtYsDmE+78X6hp61xkh0nwhiqfCfC4E5iqqei7MNcEwX/w/We9r2F8dCjPPCnPVApgrFsLMT2NLrj8sq7Xojdfh66WOjYtGGobZnJ09JsgNUDZ3Relh3jgEvR+NRnfpRHSXT0J35WR0X2BrVfUjXbD2TUF31VQ+n4buCupkILqLfWHe6oKe+EGwJDjjk1m6VOausUf+NscI9VB8GoKVf+IVR9zfZMQvxaPQeXocOs9OQGd5IDorgtBZSZ2b2KcKqpwq5fOSX6PzwFB05bngfqIOWGfAlyv1jbGjFD+bE7PnBBnjlGV3Fj7RpcYMQud7g2Ha/zRMn46G6VgATKcCYToTBNPpiRSvj7PvM3+YinxgKvSEabcrxzhDrDegda3TvYLZmmh75t5fsbRZ45Xklpc0PXhZg/ZNzvhvrhEthcPR8ren0bLft0+f+LDvSbTke6Elzx13dg1G+1Y3iGQXmDY4mwvCHN5lQtr+Ssre8zhkB2rWNy9x+NkS44DujXrc3uaK5j+741a2B259QOVQvG7O8sDtTA/cSxsMy2ZX3FqvN+0J1Vj/Axxk76T7Pd4b/kpw7fxBZc0rHH9RE5ygbnJF11Zui+0eMO3wRFeGJ0S6O9/5LvjpLed7dfG68uQAZW6/JzLAExq2BWgjGpboCupX6L+oj3JsbozRdzTGOXU0xOlvXU3Q115/Xbcnc5o2knm6DHCujz38iNf8lck5M7SzrVrLa0b0fuxRZQBJQBKQBCQBSUASkAQkAUlAEpAEJAFJQBKQBCQBSUAS+B/qhLD6M6wGWAAAAABJRU5ErkJggg==",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NAME",
        "type": "esriFieldTypeString",
        "alias": "NAME",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "icon",
        "type": "esriFieldTypeString",
        "alias": "icon",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "type",
        "type": "esriFieldTypeString",
        "alias": "type",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "isOnClick",
        "type": "esriFieldTypeString",
        "alias": "isOnClick",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "width",
        "type": "esriFieldTypeString",
        "alias": "width",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "height",
        "type": "esriFieldTypeString",
        "alias": "height",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "dataID",
        "type": "esriFieldTypeString",
        "alias": "dataID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {
          "attributes": {
            "NAME": null,
            "icon": null,
            "type": null,
            "isOnClick": null,
            "width": null,
            "height": null,
            "dataID": null
          }
        }
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {
          "FID": 0,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "width": "26.88",
          "height": "16.50",
          "dataID": " "
        }, "geometry": {"x": 9186740, "y": 1081, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 1,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "width": "26.88",
          "height": "16.50",
          "dataID": " "
        }, "geometry": {"x": 9186785, "y": 1081, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 2,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "width": "26.88",
          "height": "16.50",
          "dataID": " "
        }, "geometry": {"x": 9186830, "y": 1081, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 3,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "width": "26.88",
          "height": "16.50",
          "dataID": " "
        }, "geometry": {"x": 9186875, "y": 1081, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 4,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": " "
        }, "geometry": {"x": 9186749, "y": 951, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 5,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": " "
        }, "geometry": {"x": 9186864, "y": 951, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 6,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " "
        }, "geometry": {"x": 9186461, "y": 977, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 7,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": " "
        }, "geometry": {"x": 9186408, "y": 897, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 8,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": " "
        }, "geometry": {"x": 9186524, "y": 897, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 9,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "width": "26.88",
          "height": "16.50",
          "dataID": " "
        }, "geometry": {"x": 9186974, "y": 1221, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 10,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "width": "26.88",
          "height": "16.50",
          "dataID": " "
        }, "geometry": {"x": 9187019, "y": 1221, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 11,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "width": "26.88",
          "height": "16.50",
          "dataID": " "
        }, "geometry": {"x": 9187064, "y": 1221, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 12,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "width": "26.88",
          "height": "16.50",
          "dataID": " "
        }, "geometry": {"x": 9187229, "y": 1221, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 13,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "width": "26.88",
          "height": "16.50",
          "dataID": " "
        }, "geometry": {"x": 9187184, "y": 1221, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 14,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "width": "26.88",
          "height": "16.50",
          "dataID": " "
        }, "geometry": {"x": 9187139, "y": 1221, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 15,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " "
        }, "geometry": {"x": 9186271, "y": 842, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 16,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " "
        }, "geometry": {"x": 9186271, "y": 789, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 17,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " "
        }, "geometry": {"x": 9186272, "y": 762, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 18,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " "
        }, "geometry": {"x": 9186273, "y": 715, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 19,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " "
        }, "geometry": {"x": 9186273, "y": 696, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 20,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " "
        }, "geometry": {"x": 9186314, "y": 703, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 21,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " "
        }, "geometry": {"x": 9186347, "y": 772, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }], "showLegend": true
}
const shpJsonPage11ShuiChang_2DiLiuShuiChang = {
  "layers": [{
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 2,
      "name": "Frame",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9185595,
        "ymin": 211,
        "xmax": 9186697,
        "ymax": 731,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [165, 83, 183, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "性质",
        "type": "esriFieldTypeString",
        "alias": "性质",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"性质": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185691, 616], [9185791, 616], [9185791, 326], [9185691, 326], [9185691, 616]]]
        }
      }, {
        "attributes": {"FID": 1, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185988, 675], [9186088, 675], [9186088, 267], [9185988, 267], [9185988, 675]]]
        }
      }, {
        "attributes": {"FID": 2, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186221, 709], [9186353, 709], [9186353, 233], [9186221, 233], [9186221, 709]]]
        }
      }, {
        "attributes": {"FID": 3, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186697, 211], [9185595, 211]]]
        }
      }, {
        "attributes": {"FID": 4, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185595, 731], [9186697, 731]]]
        }
      }, {
        "attributes": {"FID": 5, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186697, 731], [9186697, 211]]]
        }
      }, {
        "attributes": {"FID": 6, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185595, 211], [9185595, 731]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 0,
      "name": "Label",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9185468,
        "ymin": 130,
        "xmax": 9186774,
        "ymax": 710,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/RedSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAAB3VJREFUeF7tmPlTlEcexnve94U5mANQbgQSbgiHXHINlxpRIBpRI6wHorLERUmIisKCQWM8cqigESVQS1Kx1piNi4mW2YpbcZONrilE140RCTcy3DDAcL/zbJP8CYPDL+9Ufau7uqb7eZ7P+/a8PS8hwkcgIBAQCAgEBAICAYGAQEAgIBAQCAgEBAICAYGAQEAgIBAQCDx/AoowKXFMUhD3lQrioZaQRVRS+fxl51eBTZUTdZ41U1Rox13/0JF9csGJ05Qv4jSz/YPWohtvLmSKN5iTGGqTm1+rc6weICOBRbZs1UVnrv87T1PUeovxyNsUP9P6n5cpHtCxu24cbrmwKLdj+osWiqrVKhI0xzbmZ7m1SpJ+1pFpvE2DPvGTomOxAoNLLKGLscZYvB10cbYYjrJCb7A5mrxleOBqim+cWJRakZY0JfnD/LieI9V1MrKtwokbrAtU4Vm0A3TJnphJD4B+RxD0u0LA7w7FTE4oprOCMbklEGNrfdGf4IqnQTb4wc0MFTYibZqM7JgjO8ZdJkpMln/sKu16pHZGb7IfptIWg389DPp9kcChWODoMuDdBOhL1JgpisbUvghM7AqFbtNiaFP80RLnhbuBdqi0N+1dbUpWGde9gWpuhFi95yL7sS7BA93JAb+Fn8mh4QujgPeTgb9kAZf3Apd2A+fXQ38yHjOHozB1IAJjOSEY2RSIwVUv4dd4X9wJccGHNrJ7CYQ4GGjLeNNfM+dyvgpzQstKf3pbB2A6m97uBRE0/Ergcxr8hyqg7hrwn0vAtRIKIRX6Y2pMl0RhIj8co9nBGFrvh55l3ngU7YObng7IVnFvGS+BYUpmHziY/Ls2zgP9SX50by/G9N5w6I+ogYvpwK1SoOlHQNsGfWcd9Peqof88B/rTyzF9hAIopAByQzC0JQB9ST5oVnvhnt+LOGsprvUhxNIwa0aY7cGR6Cp7tr8+whkjawIxkRWC6YJI6N+lAKq3Qf/Tx+B77oGfaQc/8hB8w2Xwtw9Bf3kzZspXY/JIDEbfpAB2BKLvVV90Jvjgoac9vpRxE8kciTVCBMMkNirJ7k/tRHyjtxwjKV4Yp3t/6s+R4E+/DH3N6+BrS8E314Dvvg2+/Sb4hxfBf5sP/up2TF3ZhonK1zD6dhwGdwail26DzqgX8MRKiq9ZBpkSkmeYOyPM3m9Jjl+1Z9D8AgNtlAq6bZ70qsZi+q+bwV/7I/hbB8D/dAr8Axq89iz474p/G5++koHJy1sx/lkGdBc2YjA3HF0rHNHuboomuQj/5DgclIvOGCGCYRKFFuTMV7YUAD3VDQaLMfyqBcZORGPy01QKYSNm/rYV/Nd/Av9NHvgbueBrsjDzRQamKKDxT9Kgq1iLkbIUDOSHoiNcgnYHgnYZi+9ZExSbiSoMc2eE2flKcuJLa4KGRQz6/U0wlGaP0feiMH4uFpMXEjBVlYjp6lWY+SSZtim0kulYMiYuJEJXuhTDJ9UYPByOvoIwdCxfgE4bAo0Jh39xLAoVpMwIEQyTyFCQvGpLon9sJ0K3J4OBDDcMH1dj9FQsxkrjMPFRPCbOx2GyfLal9VEcxstioTulxjAFNfROJPqLl6Bnfyg6V7ugz5yBhuHwrZjBdiU5YJg7I8wOpifAKoVIW7uQ3rpOBH2b3ekVjYT2WCRG3o+mIGKgO0OrlIaebU/HYOQDNbQnojB4NJyGD0NPfjA0bwTRE6Q7hsUcWhkWN8yZqSQlWWGECAZLmJfJmbrvVSI8taK37xpbdB/wQW8xPee/8xIGjvlj8IQ/hk4G0JbWcX8MHPVDX4kveoq8ocn3xLM33NCZRcPHOGJYZIKfpQyq7JjHS6yJjcHujLHADgkpuC7h8F8zEVqXSNC2awE69lqhs8AamkO26HrbDt2H7dBVQov2NcW26CiwQtu+BWjdY4n2nZboTbfCmKcCnRyDO/YmyLPnDlHvjDH8G6zhS9/wlEnYR7X00fWrFYuWdVI0ZpuhcbcczW/R2qdAcz6t/bRov4mONeaaoYl+p22rHF0bVNAmKtBvweIXGxNcfFH8eNlC4m6wMWMusEnKpn5hyo48pj9gLe4SNG9QoGGLAk8z5XiaJUd99u8122/IpBA2K9BGg2vWWKAvRYVeLzEa7E1R422m2+MsSTem97nSYnfKyN6/mzATv7AUgqcMrUnmaFlLX3ysM0fj+t/b5lQLtK22QEfyAmiSLKFZpUJ7kBRPXKW4HqCYynWVHKSG2LkyZex1uO1mZM9lKem9Tx9jjY5iNEYo0bKMhn7ZAu0r6H5PpLXCAq0rKJClSjSGynE/QIkrQYqBPe6S2X+AJsY2Ped6iWZk6RlL0c2r5szofRsO9R5S1IfQLRCpQL1aifoYFerpsbkuTImaUJXuXIDiH6/Ys8vm3Mg8L2i20YqsO7fItKLcSXyn0kXccclVqv3MS6at9JU/Ox+ouns+SF6Z4cSupz7l8+z1ucs7LF1AQjOdxfGZzmx8Iu1TRcfnrioICAQEAgIBgYBAQCAgEBAICAQEAgIBgYBAQCAgEBAICAQEAv8H44b/6ZiGvGAAAAAASUVORK5CYII=",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NOTE",
        "type": "esriFieldTypeString",
        "alias": "NOTE",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {"attributes": {"NOTE": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "NOTE": "至白碱滩"},
        "geometry": {"x": 9186589, "y": 130, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 1, "NOTE": "至乌尔禾"},
        "geometry": {"x": 9186774, "y": 582, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 2, "NOTE": "黄羊泉水库"},
        "geometry": {"x": 9185468, "y": 470, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 3, "NOTE": "第六净化水厂"},
        "geometry": {"x": 9186604, "y": 710, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 4, "NOTE": "清水池"},
        "geometry": {"x": 9185718, "y": 597, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 5, "NOTE": "吸水井"},
        "geometry": {"x": 9186018, "y": 650, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 6, "NOTE": "泵房"},
        "geometry": {"x": 9186239, "y": 690, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 3,
      "name": "Line",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9185516,
        "ymin": 148,
        "xmax": 9186739,
        "ymax": 666,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [241, 152, 60, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "FLOW",
        "type": "esriFieldTypeString",
        "alias": "FLOW",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管径",
        "type": "esriFieldTypeString",
        "alias": "管径",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"FLOW": null, "管径": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "FLOW": "正向", "管径": "dn900"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185791, 571], [9185988, 571]]]
        }
      }, {
        "attributes": {"FID": 1, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186088, 625], [9186221, 625]]]
        }
      }, {
        "attributes": {"FID": 2, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186088, 525], [9186221, 525]]]
        }
      }, {
        "attributes": {"FID": 3, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186088, 425], [9186221, 425]]]
        }
      }, {
        "attributes": {"FID": 4, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186088, 324], [9186221, 324]]]
        }
      }, {
        "attributes": {"FID": 5, "FLOW": "正向", "管径": "dn900*2"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185516, 471], [9185691, 471]]]
        }
      }, {
        "attributes": {"FID": 6, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186353, 666], [9186422, 666]]]
        }
      }, {
        "attributes": {"FID": 7, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186353, 412], [9186422, 412]]]
        }
      }, {
        "attributes": {"FID": 8, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186422, 666], [9186422, 582]]]
        }
      }, {
        "attributes": {"FID": 9, "FLOW": "反向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186422, 351], [9186422, 277]]]
        }
      }, {
        "attributes": {"FID": 10, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186423, 351], [9186589, 351]]]
        }
      }, {
        "attributes": {"FID": 11, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186422, 582], [9186589, 582]]]
        }
      }, {
        "attributes": {"FID": 12, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186589, 581], [9186589, 351]]]
        }
      }, {
        "attributes": {"FID": 13, "FLOW": "正向", "管径": "dn900"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185791, 371], [9185988, 371]]]
        }
      }, {
        "attributes": {"FID": 14, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186353, 620], [9186422, 620]]]
        }
      }, {
        "attributes": {"FID": 15, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186353, 575], [9186422, 575]]]
        }
      }, {
        "attributes": {"FID": 16, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186353, 530], [9186422, 530]]]
        }
      }, {
        "attributes": {"FID": 17, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186353, 367], [9186422, 367]]]
        }
      }, {
        "attributes": {"FID": 18, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186353, 322], [9186422, 322]]]
        }
      }, {
        "attributes": {"FID": 19, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186353, 277], [9186422, 277]]]
        }
      }, {
        "attributes": {"FID": 20, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186589, 351], [9186589, 148]]]
        }
      }, {
        "attributes": {"FID": 21, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186589, 582], [9186739, 582]]]
        }
      }, {
        "attributes": {"FID": 22, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186422, 412], [9186422, 351]]]
        }
      }, {
        "attributes": {"FID": 23, "FLOW": "反向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186422, 582], [9186422, 530]]]
        }
      }, {
        "attributes": {"FID": 24, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186353, 480], [9186422, 480]]]
        }
      }, {
        "attributes": {"FID": 25, "FLOW": "反向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186422, 532], [9186422, 480]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 1,
      "name": "Point",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9185741,
        "ymin": 277,
        "xmax": 9186663,
        "ymax": 666,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/OrangeSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAABv9JREFUeF7tl31UVVUaxk/34uXCBeTjqqXQoA1oGgqOOprlFwKKOpqCCq4IyEJL1CEmyxUijkjDOBAGFgM6qSz8KPEbCQQFP2JQhmwmhmlVSwvTIbgIJOLl7vPMc8U/5v+Ll382az1rn7PP3ft9n99+9z4HRZF/koAkIAlIApKAJCAJSAKSgCQgCUgCkoAkIAlIApKAJCAJPH4CrqtHKN7bntX5pY7R+a8drvgwpNvjDzuwEbTpzyovHgvRp5ye73Tm6jJDU9Mq19tWXVtuaDoRbigtCXXekhGgzGCaDgObaj9HXzxYCawId9r7nxiXNvzBA0jxgiXVC72PZEkxAu8NAZKN+PZl97byeYZ9q55SJvZzGgMzXepYJboxyvCd+rYH1G1G9GQMg/kDb6i7R0L9+Jk+5Y5Cb9av0LN9OLDlKYikofhmpduNnRM0qwYm636K+sexStx3cW7tSPOGOcsPlsJA4OBvgU+nAiXUsenUi8BR6hBVNB0ifxLMmeNYEb5ojjV2ZARoVvdTOvad5tWRSsj38cY7oBlzPk0X0+zRF4CTNFo6GygPAT6fC5ydyftZwKlg4Dh1ZA6wbxbMufxt+gT8FDvs53f8lHD7Zm9jtHkuypC6COMV7PwNHuQ/32e+hMbPUFWLgNoEoD4ZuJoI1EQCFTRdRihnCOQEwRxhu28OHuSyL30K6pcZ62I8lRE2pmW/4e8HOa3r2MyVz6PxA1bzM4DTNF8Z3mf8273AjyeB74uBr9IIYRkrgpVQxgooDWUlhHGrsN1DCNmz0fP7Mdg9Wf+W/RzYFslwPdL9ssicDFFI84dp/iRLvIztpWigaRfQegW4/wPUuw1Qb+yDWr+OlUHD1io4ywo4NZ/bhbCKwtCbx+rYEoSr4Yb6UDfF07bU7DB6povyQnO8Vxv+MgHqfpo+ai3tRwBq42i4EKKzDkL8CNFzHaLlMMQ3qRDXYqBeXsrtsIDVMo9VwPYgIewJgznlObRFOPYk+ioz7WDBthBZ4x0Sb672EmqGL7Cf+/8493PpHKjn50NcXwtxcxdE6wlCqIZoL4No/itE0yaIL1+lXoOoI6TKCI7jWXFkMVAQDMvb3miN0GLHWCXJtuzsMPpssNP7pjeHQk3nOz3fj6+8aVzVcIj6WIivEmj2XYgb2X3Gb+Zy9bewfw1EQxz1OsQ/3iSENVBLeTgWEV7OeFiSvHA3UouDU5QcO1iwLcT5UOeczsRhUHd4A3kjeQiO4erPY4lHUdE0SRD/osmvk6gNrApCsZqvp67FP6wAURsL9RwPxr1TgJ2jITa4o2O5BiXTlQLbsrPD6PJgpz+1v0EA232AD0dBPTwJonoBxGVWwRXqC17XLoL4+++oxdQS6iX2U1eoi7yvZlsVCbWIZ8eOZyDWOOHuCg0OPf/Eh3awYFuIvCBN0s04o4o0AsgmgJKZNERTF2n40kKC4FnwUGGPxOqw3l/kM+tvapZCnI+EqFwJ9RDfDClPojdOi9aVDtgZ6PCObdnZYfQiTyXkhyi3DmweAWTye59vAXGeK1rN1X4IgatvBXGJlfBQ1mv21fB5Nc1fiKD55RDlBHCA3wYbnfEgRoO2aEfzhtHaMDtYsDmE+78X6hp61xkh0nwhiqfCfC4E5iqqei7MNcEwX/w/We9r2F8dCjPPCnPVApgrFsLMT2NLrj8sq7Xojdfh66WOjYtGGobZnJ09JsgNUDZ3Relh3jgEvR+NRnfpRHSXT0J35WR0X2BrVfUjXbD2TUF31VQ+n4buCupkILqLfWHe6oKe+EGwJDjjk1m6VOausUf+NscI9VB8GoKVf+IVR9zfZMQvxaPQeXocOs9OQGd5IDorgtBZSZ2b2KcKqpwq5fOSX6PzwFB05bngfqIOWGfAlyv1jbGjFD+bE7PnBBnjlGV3Fj7RpcYMQud7g2Ha/zRMn46G6VgATKcCYToTBNPpiRSvj7PvM3+YinxgKvSEabcrxzhDrDegda3TvYLZmmh75t5fsbRZ45Xklpc0PXhZg/ZNzvhvrhEthcPR8ren0bLft0+f+LDvSbTke6Elzx13dg1G+1Y3iGQXmDY4mwvCHN5lQtr+Ssre8zhkB2rWNy9x+NkS44DujXrc3uaK5j+741a2B259QOVQvG7O8sDtTA/cSxsMy2ZX3FqvN+0J1Vj/Axxk76T7Pd4b/kpw7fxBZc0rHH9RE5ygbnJF11Zui+0eMO3wRFeGJ0S6O9/5LvjpLed7dfG68uQAZW6/JzLAExq2BWgjGpboCupX6L+oj3JsbozRdzTGOXU0xOlvXU3Q115/Xbcnc5o2knm6DHCujz38iNf8lck5M7SzrVrLa0b0fuxRZQBJQBKQBCQBSUASkAQkAUlAEpAEJAFJQBKQBCQBSUAS+B/qhLD6M6wGWAAAAABJRU5ErkJggg==",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NAME",
        "type": "esriFieldTypeString",
        "alias": "NAME",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "icon",
        "type": "esriFieldTypeString",
        "alias": "icon",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "type",
        "type": "esriFieldTypeString",
        "alias": "type",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "isOnClick",
        "type": "esriFieldTypeString",
        "alias": "isOnClick",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "dataID",
        "type": "esriFieldTypeString",
        "alias": "dataID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "width",
        "type": "esriFieldTypeString",
        "alias": "width",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "height",
        "type": "esriFieldTypeString",
        "alias": "height",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "mxdID",
        "type": "esriFieldTypeString",
        "alias": "mxdID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {
          "attributes": {
            "NAME": null,
            "icon": null,
            "type": null,
            "isOnClick": null,
            "dataID": null,
            "width": null,
            "height": null,
            "mxdID": null
          }
        }
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {
          "FID": 0,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SC-014",
          "width": "101.91",
          "height": "45.00",
          "mxdID": "1"
        }, "geometry": {"x": 9185741, "y": 539, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 1,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "17.45",
          "height": "18.75",
          "mxdID": "3"
        }, "geometry": {"x": 9186663, "y": 589, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 2,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "17.45",
          "height": "18.75",
          "mxdID": "3"
        }, "geometry": {"x": 9186549, "y": 359, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 3,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "17.45",
          "height": "18.75",
          "mxdID": "3"
        }, "geometry": {"x": 9186589, "y": 392, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 4,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "17.45",
          "height": "18.75",
          "mxdID": "3"
        }, "geometry": {"x": 9186589, "y": 316, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 5,
          "NAME": "流量计",
          "icon": "401流量计",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SCQ-008",
          "width": "15.14",
          "height": "18.75",
          "mxdID": "5"
        }, "geometry": {"x": 9186604, "y": 590, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 6,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SC-015",
          "width": "101.91",
          "height": "45.00",
          "mxdID": "1"
        }, "geometry": {"x": 9185741, "y": 411, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 7,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": "LH-PMP-001",
          "width": "36.65",
          "height": "22.50",
          "mxdID": "2"
        }, "geometry": {"x": 9186287, "y": 666, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 8,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": "LH-PMP-002",
          "width": "36.65",
          "height": "22.50",
          "mxdID": "2"
        }, "geometry": {"x": 9186287, "y": 620, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 9,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": "LH-PMP-003",
          "width": "36.65",
          "height": "22.50",
          "mxdID": "2"
        }, "geometry": {"x": 9186287, "y": 575, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 10,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": "LH-PMP-004",
          "width": "36.65",
          "height": "22.50",
          "mxdID": "2"
        }, "geometry": {"x": 9186287, "y": 530, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 11,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": "LH-PMP-006",
          "width": "36.65",
          "height": "22.50",
          "mxdID": "2"
        }, "geometry": {"x": 9186287, "y": 277, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 12,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": "LH-PMP-007",
          "width": "36.65",
          "height": "22.50",
          "mxdID": "2"
        }, "geometry": {"x": 9186287, "y": 322, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 13,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": "LH-PMP-008",
          "width": "36.65",
          "height": "22.50",
          "mxdID": "2"
        }, "geometry": {"x": 9186287, "y": 367, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 14,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": "LH-PMP-009",
          "width": "36.65",
          "height": "22.50",
          "mxdID": "2"
        }, "geometry": {"x": 9186287, "y": 412, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 15,
          "NAME": "流量计",
          "icon": "401流量计",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SCQ-009",
          "width": "15.14",
          "height": "18.75",
          "mxdID": "5"
        }, "geometry": {"x": 9186589, "y": 514, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 16,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": "LH-PMP-005",
          "width": "36.65",
          "height": "22.50",
          "mxdID": "2"
        }, "geometry": {"x": 9186286, "y": 485, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }], "showLegend": true
}
const shpJsonPage11ShuiChang_3SanPingWuHua = {
  "layers": [{
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 2,
      "name": "Frame",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9185519,
        "ymin": 267,
        "xmax": 9186712,
        "ymax": 764,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [165, 83, 183, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "性质",
        "type": "esriFieldTypeString",
        "alias": "性质",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"性质": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185519, 764], [9186712, 764], [9186712, 355], [9185519, 355], [9185519, 764]]]
        }
      }, {
        "attributes": {"FID": 1, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185738, 619], [9186105, 619], [9186105, 580], [9185738, 580], [9185738, 619]]]
        }
      }, {
        "attributes": {"FID": 2, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185651, 545], [9186080, 545], [9186080, 477], [9185651, 477], [9185651, 545]]]
        }
      }, {
        "attributes": {"FID": 3, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186166, 588], [9186566, 588], [9186566, 559], [9186166, 559], [9186166, 588]]]
        }
      }, {
        "attributes": {"FID": 4, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186199, 509], [9186601, 509], [9186601, 440], [9186199, 440], [9186199, 509]]]
        }
      }, {
        "attributes": {"FID": 5, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185631, 344], [9185755, 344], [9185755, 267], [9185631, 267], [9185631, 344]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 0,
      "name": "Lable",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9185462,
        "ymin": 120,
        "xmax": 9186774,
        "ymax": 808,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/RedSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAAB3VJREFUeF7tmPlTlEcexnve94U5mANQbgQSbgiHXHINlxpRIBpRI6wHorLERUmIisKCQWM8cqigESVQS1Kx1piNi4mW2YpbcZONrilE140RCTcy3DDAcL/zbJP8CYPDL+9Ufau7uqb7eZ7P+/a8PS8hwkcgIBAQCAgEBAICAYGAQEAgIBAQCAgEBAICAYGAQEAgIBAQCDx/AoowKXFMUhD3lQrioZaQRVRS+fxl51eBTZUTdZ41U1Rox13/0JF9csGJ05Qv4jSz/YPWohtvLmSKN5iTGGqTm1+rc6weICOBRbZs1UVnrv87T1PUeovxyNsUP9P6n5cpHtCxu24cbrmwKLdj+osWiqrVKhI0xzbmZ7m1SpJ+1pFpvE2DPvGTomOxAoNLLKGLscZYvB10cbYYjrJCb7A5mrxleOBqim+cWJRakZY0JfnD/LieI9V1MrKtwokbrAtU4Vm0A3TJnphJD4B+RxD0u0LA7w7FTE4oprOCMbklEGNrfdGf4IqnQTb4wc0MFTYibZqM7JgjO8ZdJkpMln/sKu16pHZGb7IfptIWg389DPp9kcChWODoMuDdBOhL1JgpisbUvghM7AqFbtNiaFP80RLnhbuBdqi0N+1dbUpWGde9gWpuhFi95yL7sS7BA93JAb+Fn8mh4QujgPeTgb9kAZf3Apd2A+fXQ38yHjOHozB1IAJjOSEY2RSIwVUv4dd4X9wJccGHNrJ7CYQ4GGjLeNNfM+dyvgpzQstKf3pbB2A6m97uBRE0/Ergcxr8hyqg7hrwn0vAtRIKIRX6Y2pMl0RhIj8co9nBGFrvh55l3ngU7YObng7IVnFvGS+BYUpmHziY/Ls2zgP9SX50by/G9N5w6I+ogYvpwK1SoOlHQNsGfWcd9Peqof88B/rTyzF9hAIopAByQzC0JQB9ST5oVnvhnt+LOGsprvUhxNIwa0aY7cGR6Cp7tr8+whkjawIxkRWC6YJI6N+lAKq3Qf/Tx+B77oGfaQc/8hB8w2Xwtw9Bf3kzZspXY/JIDEbfpAB2BKLvVV90Jvjgoac9vpRxE8kciTVCBMMkNirJ7k/tRHyjtxwjKV4Yp3t/6s+R4E+/DH3N6+BrS8E314Dvvg2+/Sb4hxfBf5sP/up2TF3ZhonK1zD6dhwGdwail26DzqgX8MRKiq9ZBpkSkmeYOyPM3m9Jjl+1Z9D8AgNtlAq6bZ70qsZi+q+bwV/7I/hbB8D/dAr8Axq89iz474p/G5++koHJy1sx/lkGdBc2YjA3HF0rHNHuboomuQj/5DgclIvOGCGCYRKFFuTMV7YUAD3VDQaLMfyqBcZORGPy01QKYSNm/rYV/Nd/Av9NHvgbueBrsjDzRQamKKDxT9Kgq1iLkbIUDOSHoiNcgnYHgnYZi+9ZExSbiSoMc2eE2flKcuJLa4KGRQz6/U0wlGaP0feiMH4uFpMXEjBVlYjp6lWY+SSZtim0kulYMiYuJEJXuhTDJ9UYPByOvoIwdCxfgE4bAo0Jh39xLAoVpMwIEQyTyFCQvGpLon9sJ0K3J4OBDDcMH1dj9FQsxkrjMPFRPCbOx2GyfLal9VEcxstioTulxjAFNfROJPqLl6Bnfyg6V7ugz5yBhuHwrZjBdiU5YJg7I8wOpifAKoVIW7uQ3rpOBH2b3ekVjYT2WCRG3o+mIGKgO0OrlIaebU/HYOQDNbQnojB4NJyGD0NPfjA0bwTRE6Q7hsUcWhkWN8yZqSQlWWGECAZLmJfJmbrvVSI8taK37xpbdB/wQW8xPee/8xIGjvlj8IQ/hk4G0JbWcX8MHPVDX4kveoq8ocn3xLM33NCZRcPHOGJYZIKfpQyq7JjHS6yJjcHujLHADgkpuC7h8F8zEVqXSNC2awE69lqhs8AamkO26HrbDt2H7dBVQov2NcW26CiwQtu+BWjdY4n2nZboTbfCmKcCnRyDO/YmyLPnDlHvjDH8G6zhS9/wlEnYR7X00fWrFYuWdVI0ZpuhcbcczW/R2qdAcz6t/bRov4mONeaaoYl+p22rHF0bVNAmKtBvweIXGxNcfFH8eNlC4m6wMWMusEnKpn5hyo48pj9gLe4SNG9QoGGLAk8z5XiaJUd99u8122/IpBA2K9BGg2vWWKAvRYVeLzEa7E1R422m2+MsSTem97nSYnfKyN6/mzATv7AUgqcMrUnmaFlLX3ysM0fj+t/b5lQLtK22QEfyAmiSLKFZpUJ7kBRPXKW4HqCYynWVHKSG2LkyZex1uO1mZM9lKem9Tx9jjY5iNEYo0bKMhn7ZAu0r6H5PpLXCAq0rKJClSjSGynE/QIkrQYqBPe6S2X+AJsY2Ped6iWZk6RlL0c2r5szofRsO9R5S1IfQLRCpQL1aifoYFerpsbkuTImaUJXuXIDiH6/Ys8vm3Mg8L2i20YqsO7fItKLcSXyn0kXccclVqv3MS6at9JU/Ox+ouns+SF6Z4cSupz7l8+z1ucs7LF1AQjOdxfGZzmx8Iu1TRcfnrioICAQEAgIBgYBAQCAgEBAICAQEAgIBgYBAQCAgEBAICAQEAv8H44b/6ZiGvGAAAAAASUVORK5CYII=",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NOTE",
        "type": "esriFieldTypeString",
        "alias": "NOTE",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {"attributes": {"NOTE": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "NOTE": "三坪水库"},
        "geometry": {"x": 9186136, "y": 808, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 1, "NOTE": "第五净化水厂"},
        "geometry": {"x": 9186646, "y": 729, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 2, "NOTE": "至DN600"},
        "geometry": {"x": 9186774, "y": 205, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 3, "NOTE": "至输炼地区"},
        "geometry": {"x": 9186732, "y": 129, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 4, "NOTE": "至南北DN600"},
        "geometry": {"x": 9186099, "y": 120, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 5, "NOTE": "至下环"},
        "geometry": {"x": 9185536, "y": 135, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 6, "NOTE": "至中环"},
        "geometry": {"x": 9185478, "y": 397, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 7, "NOTE": "至山上水库"},
        "geometry": {"x": 9185462, "y": 411, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 8, "NOTE": "至自流环"},
        "geometry": {"x": 9185477, "y": 189, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 9, "NOTE": "至下环"},
        "geometry": {"x": 9185475, "y": 204, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 10, "NOTE": "吸水井"},
        "geometry": {"x": 9185905, "y": 601, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 11, "NOTE": "吸水井"},
        "geometry": {"x": 9186371, "y": 571, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 12, "NOTE": "三坪水厂"},
        "geometry": {"x": 9185550, "y": 737, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 13, "NOTE": "泵房"},
        "geometry": {"x": 9186585, "y": 500, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 14, "NOTE": "泵房"},
        "geometry": {"x": 9185667, "y": 535, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 3,
      "name": "Line",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9185501,
        "ymin": 147,
        "xmax": 9186746,
        "ymax": 792,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [241, 152, 60, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "FLOW",
        "type": "esriFieldTypeString",
        "alias": "FLOW",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管径",
        "type": "esriFieldTypeString",
        "alias": "管径",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"FLOW": null, "管径": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186304, 737], [9186485, 737]]]
        }
      }, {
        "attributes": {"FID": 1, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185659, 444], [9185839, 444]]]
        }
      }, {
        "attributes": {"FID": 2, "FLOW": "正向", "管径": "dn1200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186304, 663], [9186304, 588]]]
        }
      }, {
        "attributes": {"FID": 3, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186307, 559], [9186307, 509]]]
        }
      }, {
        "attributes": {"FID": 4, "FLOW": "正向", "管径": "dn500"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186207, 440], [9186207, 417]]]
        }
      }, {
        "attributes": {"FID": 5, "FLOW": "正向", "管径": "dn1200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186157, 618], [9186647, 618]]]
        }
      }, {
        "attributes": {"FID": 6, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185806, 737], [9185806, 651]]]
        }
      }, {
        "attributes": {"FID": 7, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185659, 477], [9185659, 444]]]
        }
      }, {
        "attributes": {"FID": 8, "FLOW": "正向", "管径": "dn350"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186488, 440], [9186488, 417]]]
        }
      }, {
        "attributes": {"FID": 9, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185641, 356], [9185641, 331]]]
        }
      }, {
        "attributes": {"FID": 10, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185858, 477], [9185858, 444]]]
        }
      }, {
        "attributes": {"FID": 11, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185709, 477], [9185709, 444]]]
        }
      }, {
        "attributes": {"FID": 12, "FLOW": "反向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185641, 356], [9185641, 663]]]
        }
      }, {
        "attributes": {"FID": 13, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185641, 331], [9185641, 276]]]
        }
      }, {
        "attributes": {"FID": 14, "FLOW": "正向", "管径": "dn700"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185741, 276], [9185535, 276]]]
        }
      }, {
        "attributes": {"FID": 15, "FLOW": "正向", "管径": "dn500"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186257, 440], [9186257, 417]]]
        }
      }, {
        "attributes": {"FID": 16, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186357, 417], [9186357, 316]]]
        }
      }, {
        "attributes": {"FID": 17, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185741, 331], [9185741, 276]]]
        }
      }, {
        "attributes": {"FID": 18, "FLOW": "正向", "管径": "dn1200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186485, 663], [9186485, 588]]]
        }
      }, {
        "attributes": {"FID": 19, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185986, 691], [9185806, 691]]]
        }
      }, {
        "attributes": {"FID": 20, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185691, 331], [9185691, 276]]]
        }
      }, {
        "attributes": {"FID": 21, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185808, 477], [9185808, 444]]]
        }
      }, {
        "attributes": {"FID": 22, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186010, 580], [9186010, 545]]]
        }
      }, {
        "attributes": {"FID": 23, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185965, 444], [9186010, 444]]]
        }
      }, {
        "attributes": {"FID": 24, "FLOW": "正向", "管径": "dn500"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186406, 440], [9186406, 417]]]
        }
      }, {
        "attributes": {"FID": 25, "FLOW": "反向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185691, 331], [9185741, 331]]]
        }
      }, {
        "attributes": {"FID": 26, "FLOW": "正向", "管径": "dn1400"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186127, 663], [9186304, 663]]]
        }
      }, {
        "attributes": {"FID": 27, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185759, 580], [9185759, 545]]]
        }
      }, {
        "attributes": {"FID": 28, "FLOW": "正向", "管径": "dn1000"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186647, 189], [9186728, 189]]]
        }
      }, {
        "attributes": {"FID": 29, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186357, 316], [9186357, 222]]]
        }
      }, {
        "attributes": {"FID": 30, "FLOW": "正向", "管径": "dn1000"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185535, 189], [9185512, 189]]]
        }
      }, {
        "attributes": {"FID": 31, "FLOW": "无方向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186647, 189], [9186459, 189]]]
        }
      }, {
        "attributes": {"FID": 32, "FLOW": "正向", "管径": "dn1000"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186357, 189], [9185579, 189]]]
        }
      }, {
        "attributes": {"FID": 33, "FLOW": "反向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186136, 737], [9186136, 792]]]
        }
      }, {
        "attributes": {"FID": 34, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186178, 242], [9186559, 242]]]
        }
      }, {
        "attributes": {"FID": 35, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186178, 266], [9186357, 266]]]
        }
      }, {
        "attributes": {"FID": 36, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186559, 210], [9186746, 210]]]
        }
      }, {
        "attributes": {"FID": 37, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185579, 411], [9185579, 222]]]
        }
      }, {
        "attributes": {"FID": 38, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185600, 276], [9185600, 222]]]
        }
      }, {
        "attributes": {"FID": 39, "FLOW": "反向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185579, 222], [9186357, 222]]]
        }
      }, {
        "attributes": {"FID": 40, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185806, 663], [9185641, 663]]]
        }
      }, {
        "attributes": {"FID": 41, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185986, 737], [9185986, 651]]]
        }
      }, {
        "attributes": {"FID": 42, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186099, 580], [9186099, 147]]]
        }
      }, {
        "attributes": {"FID": 43, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185965, 477], [9185965, 444]]]
        }
      }, {
        "attributes": {"FID": 44, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186055, 477], [9186055, 444]]]
        }
      }, {
        "attributes": {"FID": 45, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186010, 444], [9186010, 398]]]
        }
      }, {
        "attributes": {"FID": 46, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186010, 398], [9185501, 398]]]
        }
      }, {
        "attributes": {"FID": 47, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185806, 651], [9185806, 619]]]
        }
      }, {
        "attributes": {"FID": 48, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185986, 651], [9185986, 619]]]
        }
      }, {
        "attributes": {"FID": 49, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186010, 477], [9186010, 444]]]
        }
      }, {
        "attributes": {"FID": 50, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185759, 477], [9185759, 444]]]
        }
      }, {
        "attributes": {"FID": 51, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186178, 559], [9186178, 276]]]
        }
      }, {
        "attributes": {"FID": 52, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186538, 559], [9186538, 509]]]
        }
      }, {
        "attributes": {"FID": 53, "FLOW": "正向", "管径": "dn350"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186588, 440], [9186588, 417]]]
        }
      }, {
        "attributes": {"FID": 54, "FLOW": "正向", "管径": "dn350"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186538, 440], [9186538, 417]]]
        }
      }, {
        "attributes": {"FID": 55, "FLOW": "正向", "管径": "dn500"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186307, 440], [9186307, 417]]]
        }
      }, {
        "attributes": {"FID": 56, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186207, 417], [9186257, 417]]]
        }
      }, {
        "attributes": {"FID": 57, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186257, 417], [9186307, 417]]]
        }
      }, {
        "attributes": {"FID": 58, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186307, 417], [9186357, 417]]]
        }
      }, {
        "attributes": {"FID": 59, "FLOW": "反向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186357, 417], [9186406, 417]]]
        }
      }, {
        "attributes": {"FID": 60, "FLOW": "无方向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186406, 417], [9186488, 417]]]
        }
      }, {
        "attributes": {"FID": 61, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186488, 417], [9186538, 417]]]
        }
      }, {
        "attributes": {"FID": 62, "FLOW": "反向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186538, 417], [9186588, 417]]]
        }
      }, {
        "attributes": {"FID": 63, "FLOW": "反向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186559, 220], [9186559, 234]]]
        }
      }, {
        "attributes": {"FID": 64, "FLOW": "反向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186559, 234], [9186559, 242]]]
        }
      }, {
        "attributes": {"FID": 65, "FLOW": "反向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186559, 242], [9186559, 417]]]
        }
      }, {
        "attributes": {"FID": 66, "FLOW": "正向", "管径": "dn700"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186127, 663], [9186127, 376]]]
        }
      }, {
        "attributes": {"FID": 67, "FLOW": "正向", "管径": "dn700"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186127, 376], [9185535, 376]]]
        }
      }, {
        "attributes": {"FID": 68, "FLOW": "正向", "管径": "dn700"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185535, 376], [9185535, 276]]]
        }
      }, {
        "attributes": {"FID": 69, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186485, 691], [9186304, 691]]]
        }
      }, {
        "attributes": {"FID": 70, "FLOW": "反向", "管径": "dn1200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186647, 663], [9186485, 663]]]
        }
      }, {
        "attributes": {"FID": 71, "FLOW": "反向", "管径": "dn500"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186357, 417], [9186357, 440]]]
        }
      }, {
        "attributes": {"FID": 72, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185839, 444], [9185839, 411]]]
        }
      }, {
        "attributes": {"FID": 73, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185839, 411], [9185579, 411]]]
        }
      }, {
        "attributes": {"FID": 74, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186136, 737], [9186304, 737]]]
        }
      }, {
        "attributes": {"FID": 75, "FLOW": "正向", "管径": "dn1200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186647, 663], [9186647, 618]]]
        }
      }, {
        "attributes": {"FID": 76, "FLOW": "正向", "管径": "dn700"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185535, 276], [9185535, 152]]]
        }
      }, {
        "attributes": {"FID": 77, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185579, 222], [9185579, 189]]]
        }
      }, {
        "attributes": {"FID": 78, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186357, 204], [9186357, 189]]]
        }
      }, {
        "attributes": {"FID": 79, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186357, 222], [9186357, 204]]]
        }
      }, {
        "attributes": {"FID": 80, "FLOW": "反向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185986, 737], [9186136, 737]]]
        }
      }, {
        "attributes": {"FID": 81, "FLOW": "反向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185806, 737], [9185986, 737]]]
        }
      }, {
        "attributes": {"FID": 82, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185579, 411], [9185502, 411]]]
        }
      }, {
        "attributes": {"FID": 83, "FLOW": "反向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185839, 444], [9185858, 444]]]
        }
      }, {
        "attributes": {"FID": 84, "FLOW": "反向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186010, 444], [9186055, 444]]]
        }
      }, {
        "attributes": {"FID": 85, "FLOW": "无方向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186410, 189], [9186357, 189]]]
        }
      }, {
        "attributes": {"FID": 86, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185641, 331], [9185691, 331]]]
        }
      }, {
        "attributes": {"FID": 87, "FLOW": "正向", "管径": "dn800"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186357, 204], [9185512, 204]]]
        }
      }, {
        "attributes": {"FID": 88, "FLOW": "正向", "管径": "dn1000"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185986, 663], [9186127, 663]]]
        }
      }, {
        "attributes": {"FID": 89, "FLOW": "正向", "管径": "dn900"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9185579, 189], [9185535, 189]]]
        }
      }, {
        "attributes": {"FID": 90, "FLOW": "正向", "管径": "dn1200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186304, 737], [9186304, 663]]]
        }
      }, {
        "attributes": {"FID": 91, "FLOW": "正向", "管径": "dn1200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186485, 737], [9186485, 663]]]
        }
      }, {
        "attributes": {"FID": 92, "FLOW": "正向", "管径": "dn1200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186157, 663], [9186157, 618]]]
        }
      }, {
        "attributes": {"FID": 93, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186178, 276], [9186178, 242]]]
        }
      }, {
        "attributes": {"FID": 94, "FLOW": "正向", "管径": "dn1000"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186728, 189], [9186728, 148]]]
        }
      }, {
        "attributes": {"FID": 95, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186559, 220], [9186559, 210]]]
        }
      }, {
        "attributes": {"FID": 96, "FLOW": "正向", "管径": "dn1200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186647, 618], [9186647, 189]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 1,
      "name": "Point",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9185535,
        "ymin": 192,
        "xmax": 9186663,
        "ymax": 691,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/OrangeSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAABv9JREFUeF7tl31UVVUaxk/34uXCBeTjqqXQoA1oGgqOOprlFwKKOpqCCq4IyEJL1CEmyxUijkjDOBAGFgM6qSz8KPEbCQQFP2JQhmwmhmlVSwvTIbgIJOLl7vPMc8U/5v+Ll382az1rn7PP3ft9n99+9z4HRZF/koAkIAlIApKAJCAJSAKSgCQgCUgCkoAkIAlIApKAJCAJPH4CrqtHKN7bntX5pY7R+a8drvgwpNvjDzuwEbTpzyovHgvRp5ye73Tm6jJDU9Mq19tWXVtuaDoRbigtCXXekhGgzGCaDgObaj9HXzxYCawId9r7nxiXNvzBA0jxgiXVC72PZEkxAu8NAZKN+PZl97byeYZ9q55SJvZzGgMzXepYJboxyvCd+rYH1G1G9GQMg/kDb6i7R0L9+Jk+5Y5Cb9av0LN9OLDlKYikofhmpduNnRM0qwYm636K+sexStx3cW7tSPOGOcsPlsJA4OBvgU+nAiXUsenUi8BR6hBVNB0ifxLMmeNYEb5ojjV2ZARoVvdTOvad5tWRSsj38cY7oBlzPk0X0+zRF4CTNFo6GygPAT6fC5ydyftZwKlg4Dh1ZA6wbxbMufxt+gT8FDvs53f8lHD7Zm9jtHkuypC6COMV7PwNHuQ/32e+hMbPUFWLgNoEoD4ZuJoI1EQCFTRdRihnCOQEwRxhu28OHuSyL30K6pcZ62I8lRE2pmW/4e8HOa3r2MyVz6PxA1bzM4DTNF8Z3mf8273AjyeB74uBr9IIYRkrgpVQxgooDWUlhHGrsN1DCNmz0fP7Mdg9Wf+W/RzYFslwPdL9ssicDFFI84dp/iRLvIztpWigaRfQegW4/wPUuw1Qb+yDWr+OlUHD1io4ywo4NZ/bhbCKwtCbx+rYEoSr4Yb6UDfF07bU7DB6povyQnO8Vxv+MgHqfpo+ai3tRwBq42i4EKKzDkL8CNFzHaLlMMQ3qRDXYqBeXsrtsIDVMo9VwPYgIewJgznlObRFOPYk+ioz7WDBthBZ4x0Sb672EmqGL7Cf+/8493PpHKjn50NcXwtxcxdE6wlCqIZoL4No/itE0yaIL1+lXoOoI6TKCI7jWXFkMVAQDMvb3miN0GLHWCXJtuzsMPpssNP7pjeHQk3nOz3fj6+8aVzVcIj6WIivEmj2XYgb2X3Gb+Zy9bewfw1EQxz1OsQ/3iSENVBLeTgWEV7OeFiSvHA3UouDU5QcO1iwLcT5UOeczsRhUHd4A3kjeQiO4erPY4lHUdE0SRD/osmvk6gNrApCsZqvp67FP6wAURsL9RwPxr1TgJ2jITa4o2O5BiXTlQLbsrPD6PJgpz+1v0EA232AD0dBPTwJonoBxGVWwRXqC17XLoL4+++oxdQS6iX2U1eoi7yvZlsVCbWIZ8eOZyDWOOHuCg0OPf/Eh3awYFuIvCBN0s04o4o0AsgmgJKZNERTF2n40kKC4FnwUGGPxOqw3l/kM+tvapZCnI+EqFwJ9RDfDClPojdOi9aVDtgZ6PCObdnZYfQiTyXkhyi3DmweAWTye59vAXGeK1rN1X4IgatvBXGJlfBQ1mv21fB5Nc1fiKD55RDlBHCA3wYbnfEgRoO2aEfzhtHaMDtYsDmE+78X6hp61xkh0nwhiqfCfC4E5iqqei7MNcEwX/w/We9r2F8dCjPPCnPVApgrFsLMT2NLrj8sq7Xojdfh66WOjYtGGobZnJ09JsgNUDZ3Relh3jgEvR+NRnfpRHSXT0J35WR0X2BrVfUjXbD2TUF31VQ+n4buCupkILqLfWHe6oKe+EGwJDjjk1m6VOausUf+NscI9VB8GoKVf+IVR9zfZMQvxaPQeXocOs9OQGd5IDorgtBZSZ2b2KcKqpwq5fOSX6PzwFB05bngfqIOWGfAlyv1jbGjFD+bE7PnBBnjlGV3Fj7RpcYMQud7g2Ha/zRMn46G6VgATKcCYToTBNPpiRSvj7PvM3+YinxgKvSEabcrxzhDrDegda3TvYLZmmh75t5fsbRZ45Xklpc0PXhZg/ZNzvhvrhEthcPR8ren0bLft0+f+LDvSbTke6Elzx13dg1G+1Y3iGQXmDY4mwvCHN5lQtr+Ssre8zhkB2rWNy9x+NkS44DujXrc3uaK5j+741a2B259QOVQvG7O8sDtTA/cSxsMy2ZX3FqvN+0J1Vj/Axxk76T7Pd4b/kpw7fxBZc0rHH9RE5ygbnJF11Zui+0eMO3wRFeGJ0S6O9/5LvjpLed7dfG68uQAZW6/JzLAExq2BWgjGpboCupX6L+oj3JsbozRdzTGOXU0xOlvXU3Q115/Xbcnc5o2knm6DHCujz38iNf8lck5M7SzrVrLa0b0fuxRZQBJQBKQBCQBSUASkAQkAUlAEpAEJAFJQBKQBCQBSUAS+B/qhLD6M6wGWAAAAABJRU5ErkJggg==",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NAME",
        "type": "esriFieldTypeString",
        "alias": "NAME",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "icon",
        "type": "esriFieldTypeString",
        "alias": "icon",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "type",
        "type": "esriFieldTypeString",
        "alias": "type",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "isOnClick",
        "type": "esriFieldTypeString",
        "alias": "isOnClick",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "dataID",
        "type": "esriFieldTypeString",
        "alias": "dataID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "width",
        "type": "esriFieldTypeString",
        "alias": "width",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "height",
        "type": "esriFieldTypeString",
        "alias": "height",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {
          "attributes": {
            "NAME": null,
            "icon": null,
            "type": null,
            "isOnClick": null,
            "dataID": null,
            "width": null,
            "height": null
          }
        }
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {
          "FID": 0,
          "NAME": "五化1#清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SC-012",
          "width": "67.94",
          "height": "30.00"
        }, "geometry": {"x": 9186485, "y": 691, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 1,
          "NAME": "五化2#清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SC-013",
          "width": "67.94",
          "height": "30.00"
        }, "geometry": {"x": 9186304, "y": 691, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 2,
          "NAME": "流量计",
          "icon": "401流量计",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SCQ-006",
          "width": "7.27",
          "height": "9.00"
        }, "geometry": {"x": 9186357, "y": 233, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 3,
          "NAME": "流量计",
          "icon": "401流量计",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SCQ-005",
          "width": "7.27",
          "height": "9.00"
        }, "geometry": {"x": 9186559, "y": 234, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 4,
          "NAME": "流量计",
          "icon": "401流量计",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SCQ-007",
          "width": "7.27",
          "height": "9.00"
        }, "geometry": {"x": 9186663, "y": 192, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 5,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9185610, "y": 401, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 6,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9185535, "y": 292, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 7,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9185549, "y": 279, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 8,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9185579, "y": 237, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 9,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9185600, "y": 237, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 10,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9185579, "y": 198, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 11,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9185563, "y": 192, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 12,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9185595, "y": 192, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 13,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9186447, "y": 420, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 14,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9186338, "y": 269, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 15,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9186535, "y": 245, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 16,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9186340, "y": 225, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 17,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9186357, "y": 211, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 18,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9186340, "y": 207, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 19,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9186357, "y": 197, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 20,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9186633, "y": 192, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 21,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9186647, "y": 198, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 22,
          "NAME": "三坪1#清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SC-010",
          "width": "67.94",
          "height": "30.00"
        }, "geometry": {"x": 9185806, "y": 691, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 23,
          "NAME": "三坪2#清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SC-011",
          "width": "67.94",
          "height": "30.00"
        }, "geometry": {"x": 9185986, "y": 691, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 24,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9186559, "y": 218, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 25,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9185659, "y": 511, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 26,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9185709, "y": 511, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 27,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9185759, "y": 511, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 28,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9185808, "y": 511, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 29,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9185858, "y": 511, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 30,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9185965, "y": 511, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 31,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9186010, "y": 511, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 32,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9186055, "y": 511, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 33,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9186207, "y": 475, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 34,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9186257, "y": 475, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 35,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9186307, "y": 475, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 36,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9186357, "y": 475, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 37,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9186406, "y": 475, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 38,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9186488, "y": 475, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 39,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9186533, "y": 475, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 40,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9186578, "y": 475, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 41,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9185641, "y": 304, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 42,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9185691, "y": 305, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 43,
          "NAME": "水泵",
          "icon": "601水泵",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "30.54",
          "height": "18.75"
        }, "geometry": {"x": 9185741, "y": 305, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 44,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "dataID": " ",
          "width": "8.38",
          "height": "9.00"
        }, "geometry": {"x": 9185641, "y": 355, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 45,
          "NAME": "流量计",
          "icon": "401流量计",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SCQ-001",
          "width": "7.27",
          "height": "9.00"
        }, "geometry": {"x": 9185754, "y": 414, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 46,
          "NAME": "流量计",
          "icon": "401流量计",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SCQ-002",
          "width": "7.27",
          "height": "9.00"
        }, "geometry": {"x": 9185800, "y": 401, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 47,
          "NAME": "流量计",
          "icon": "401流量计",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SCQ-004",
          "width": "7.27",
          "height": "9.00"
        }, "geometry": {"x": 9186099, "y": 426, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 48,
          "NAME": "流量计",
          "icon": "401流量计",
          "type": "10",
          "isOnClick": "是",
          "dataID": "SCQ-003",
          "width": "7.27",
          "height": "9.00"
        }, "geometry": {"x": 9185617, "y": 279, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }], "showLegend": true
}

export {shpJsonPage11ShuiChang_1DiErShuiChang}
export {shpJsonPage11ShuiChang_2DiLiuShuiChang}
export {shpJsonPage11ShuiChang_3SanPingWuHua}

