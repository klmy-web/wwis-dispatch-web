export const deviceTypeList = [
  {
    label: '大用户电磁水表',
    value: 'LUF'
  }, {
    label: 'DMA专用流量计',
    value: 'DMA'
  }, {
    label: '分区流量计',
    value: 'DPAF'
  }, {
    label: '水质监测仪',
    value: 'WQTM'
  }, {
    label: '瞬态压力记录仪',
    value: 'TPR'
  }, {
    label: '噪声记录仪',
    value: 'NMI'
  }, {
    label: '调流调压阀',
    value: 'REG'
  }, {
    label: '远控蝶阀',
    value: 'RCV'
  }, {
    label: '减压阀',
    value: 'PRV'
  }, {
    label: '泵站',
    value: '1'
  }, {
    label: '水池',
    value: 'WB'
  }, {
    label: '水厂',
    value: '2'
  }
]

export const caliber = [
  {
    label: '40',
    value: '40'
  }, {
    label: '50',
    value: '50'
  }, {
    label: '80',
    value: '80'
  }, {
    label: '100',
    value: '100'
  }, {
    label: '150',
    value: '150'
  }, {
    label: '200',
    value: '200'
  }, {
    label: '250',
    value: '250'
  }, {
    label: '300',
    value: '300'
  }, {
    label: '400',
    value: '400'
  }, {
    label: '500',
    value: '500'
  }, {
    label: '600',
    value: '600'
  }, {
    label: '800',
    value: '800'
  }
]

export const vender = [
  {
    label: '拓安信',
    value: '1'
  }, {
    label: '清时捷',
    value: '2'
  }, {
    label: 'HWM',
    value: '3'
  }, {
    label: 'VAG',
    value: '4'
  }
]

export const gis = [
  {
    label: '≥DN200',
    value: 'GJ >= 200'
  }, {
    label: '<DN200',
    value: 'GJ < 200'
  }, {
    label: '消防栓',
    value: '7'
  }, {
    label: '阀门',
    value: '8'
  }
]
// TODO: 待选项卡选项完全确认需求后删除
// , {
//   label: '水表',
//   value: '9'
// }

export const parameter = [
  {
    label: '标签',
    value: '1'
  }, {
    label: '聚合',
    value: '2'
  }, {
    label: '分区',
    value: 'areaLayer'
  }, {
    label: 'DMA',
    value: 'dmaAreaLayer'
  }
]
