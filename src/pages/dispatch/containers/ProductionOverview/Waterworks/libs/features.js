// import { on } from 'cluster'
export default {
  methods: {
    // 约定传入的数据结构
    createClusterLayer (clusterData) {
      // if (this.markersLayer) {
      //   this.markersLayer.clear()
      // }
      this.markersLayer.clear()
      // if (this.map.getLayer('clusterLayer')) {
      //   this.map.getLayer('clusterLayer').clear()
      // }
      // 转换点聚合图层需要的数据格式
      const clusterInfo = clusterData.map(item => {
        let latlng = new this.$arcgis.Point(item.data.longitude, item.data.latitude, new this.$arcgis.SpatialReference({ wkid: 4326 }))
        let webMercator = this.$arcgis.webMercatorUtils.geographicToWebMercator(latlng)
        let attributes = {
          attr: item
        }
        return {
          x: webMercator.x,
          y: webMercator.y,
          attributes: attributes
        }
      })
      // 创建点聚合图层
      this.clusterLayer = new this.$arcgis.ClusterLayer({
        id: 'clusterLayer',
        data: clusterInfo,
        distance: 100,
        labelColor: '#fff',
        labelOffset: -5,
        resolution: this.map.extent.getWidth() / this.map.width
      })
      // 设置点聚合的渲染器
      let defaultSym = new this.$arcgis.SimpleMarkerSymbol().setSize(0)
      let renderer = new this.$arcgis.ClassBreaksRenderer(defaultSym, 'clusterCount')
      // 创建不同等级的渲染器
      let yellow = new this.$arcgis.SimpleMarkerSymbol(
        this.$arcgis.SimpleMarkerSymbol.STYLE_CIRCLE,
        20,
        new this.$arcgis.SimpleLineSymbol(
          this.$arcgis.SimpleLineSymbol.STYLE_SOLID,
          new this.$arcgis.Color([230, 184, 92, 0.8]),
          1
        ),
        new this.$arcgis.Color([255, 204, 102, 0.8])
      )
      let blue = new this.$arcgis.SimpleMarkerSymbol(
        this.$arcgis.SimpleMarkerSymbol.STYLE_CIRCLE,
        40,
        new this.$arcgis.SimpleLineSymbol(
          this.$arcgis.SimpleLineSymbol.STYLE_SOLID,
          new this.$arcgis.Color([82, 163, 204, 0.8]),
          1
        ),
        new this.$arcgis.Color([102, 204, 255, 0.8])
      )
      let green = new this.$arcgis.SimpleMarkerSymbol(
        this.$arcgis.SimpleMarkerSymbol.STYLE_CIRCLE,
        60,
        new this.$arcgis.SimpleLineSymbol(
          this.$arcgis.SimpleLineSymbol.STYLE_SOLID,
          new this.$arcgis.Color([41, 163, 41, 0.8]),
          1
        ),
        new this.$arcgis.Color([51, 204, 51, 0.8])
      )
      // 设置聚合等级
      renderer.addBreak(2, 30, yellow)
      renderer.addBreak(30, 150, blue)
      renderer.addBreak(150, 1000, green)
      this.clusterLayer.setRenderer(renderer)
      this.map.addLayer(this.clusterLayer)
      // 检索未集群的设备点
      let singleMarkers = this.map.getLayer('clusterLayer').graphics.filter(item => {
        return item.attributes.clusterCount === 1
      })
      let singleClusterId = singleMarkers.map(item => {
        return item.attributes.clusterId
      })
      let singleMarkersData
      singleMarkersData = this.map.getLayer('clusterLayer')._clusterData.filter(item => {
        return singleClusterId.includes(item.attributes.clusterId)
      })
      console.log('singleMarkersData', singleMarkersData)
      singleMarkersData.map(item => {
        let symbol, point, graphic
        symbol = new this.$arcgis.PictureMarkerSymbol(...item.attributes.attr.symbol)
        point = new this.$arcgis.Point({
          x: item.attributes.attr.position[0],
          y: item.attributes.attr.position[1]
        })
        graphic = new this.$arcgis.Graphic(point, symbol, item.attributes.attr.data)
        this.markersLayer.add(graphic)
      })
      this.map.getLayer('clusterLayer').disableMouseEvents()
    },
    heatmap (markers) {
      let layerDefinition = {
        'geometryType': 'esriGeometryPoint',
        'fields': [{
          'name': 'ID',
          'type': 'esriFieldTypeInteger',
          'alias': 'ID'
        }]
      }
      let featureCollection = {
        layerDefinition: layerDefinition,
        featureSet: null
      }
      let featureLayer = new this.$arcgis.FeatureLayer(featureCollection, {
        mode: this.$arcgis.FeatureLayer.MODE_SNAPSHOT,
        outFields: ['*'],
        opacity: 1
      })
      let heatmapRenderer = new this.$arcgis.HeatmapRenderer({
        colors: ['rgba(255, 0, 0, 0)', 'rgb(0, 255, 0)', 'rgb(255, 255, 0)', 'rgb(255, 0, 0)'],
        blurRadius: 8,
        maxPixelIntensity: 230,
        minPixelIntensity: 10
      })
      featureLayer.setRenderer(heatmapRenderer)
      this.map.addLayer(featureLayer)
      markers.map(item => {
        let point = new this.$arcgis.Point(item.position[0], item.position[1], this.$arcgis.SpatialReference({ wkid: 4326 }))
        featureLayer.add(new this.$arcgis.Graphic(point))
      })
    }
  }
}
