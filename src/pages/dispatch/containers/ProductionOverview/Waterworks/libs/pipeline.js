export default {
  methods: {
    createPipelines (pipelines) {
      // console.log('pipelines: ' + pipelines)
      let symbol, point, graphic
      pipelines.forEach( pipeline => {
        // console.log('add pipeline: ' + JSON.stringify(pipeline))

        const ag = this.$arcgis;
        let lineSymbol = new ag.CartographicLineSymbol(
          ag.CartographicLineSymbol.STYLE_SOLID,
          new dojo.Color("#000000"), 2,
          ag.CartographicLineSymbol.CAP_ROUND,
          ag.CartographicLineSymbol.JOIN_MITER, 5
        );

        let line = new ag.Polyline({
          paths: pipeline.paths,
          spatialReference: pipeline.spatialReference,
        });

        graphic = new ag.Graphic(line, lineSymbol, pipeline.data);

        this.pipelineLayer.add(graphic);

      })
    },
  }
}
