export default {
  methods: {
    // 绘制分区并加入对应图层
    drawArea (areas, layer) {
      let symbol, polygon, graphic, textSymbol, labelPoint, areaTextSymbol
      const font = new this.$arcgis.Font('16px', this.$arcgis.Font.STYLE_NORMAL, this.$arcgis.Font.VARIANT_NORMAL, this.$arcgis.Font.WEIGHT_BOLDER)
      areas.map(({ color, rings, name, areaCenter, id }) => {
        // 实例化绘制画笔
        symbol = new this.$arcgis.SimpleFillSymbol(
          this.$arcgis.SimpleFillSymbol.STYLE_SOLID,
          new this.$arcgis.SimpleLineSymbol(
            this.$arcgis.SimpleLineSymbol.STYLE_SOLID,
            new this.$arcgis.Color(color),
            3
          ),
          this.conversion(color, 0.25)
        )
        // 实例化绘制画板
        polygon = new this.$arcgis.Polygon({
          rings: [rings]
        })
        // 实例化合成的图像并加入图层
        graphic = new this.$arcgis.Graphic(polygon, symbol, {id, name})
        layer.add(graphic)
        // 实例化分区name并加入图层
        textSymbol = new this.$arcgis.TextSymbol(name, font, new this.$arcgis.Color(color))
        labelPoint = new this.$arcgis.Point(areaCenter[0], areaCenter[1], this.$arcgis.SpatialReference({ wkid: 4326 }))
        areaTextSymbol = new this.$arcgis.Graphic(labelPoint, textSymbol)
        layer.add(areaTextSymbol)
      })
    },
    // 将16进制色值转换为rgba的函数
    conversion (color, alpha) {
      const temp = new this.$arcgis.Color(color)
      return temp.setColor(temp.toRgb().concat([alpha]))
    },
    // 鼠标事件
    areaOn () {
      this.$arcgis.on(this.areaLayer, 'mouse-over', e => {
        // if (!this.isClick.is) {
        //   this.getInfoWindow(e.graphic.attributes)
        // }
        this.$emit('getAreaInfowindows', e)
        // this.getInfoWindow(e.graphic.attributes)
        // this.map.infoWindow.show(e.screenPoint)
      })
      this.$arcgis.on(this.areaLayer, 'mouse-move', e => {
        if (!this.isClick.is) {
          this.map.infoWindow.show(e.screenPoint)
        }
        this.map.infoWindow.show(e.screenPoint)
      })
      this.$arcgis.on(this.areaLayer, 'mouse-out', e => {
        if (!this.isClick.is) {
          this.map.infoWindow.hide()
        }
      })
    }
  }
}
