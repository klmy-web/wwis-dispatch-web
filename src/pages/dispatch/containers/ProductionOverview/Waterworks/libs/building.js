import ICON_1 from '../asserts/images/1.png';
import ICON_2 from '../asserts/images/2.png';
import ICON_3 from '../asserts/images/3.png';
import ICON_4 from '../asserts/images/4.png';
import ICON_5 from '../asserts/images/5.png';
import ICON_6 from '../asserts/images/6.png';
import ICON_7 from '../asserts/images/7.png';
import ICON_8 from '../asserts/images/8.png';
import ICON_9 from '../asserts/images/9.png';

const deviceTypeList = {
  1: [ICON_1, 60, 60],
  2: [ICON_2, 60, 30],
  3: [ICON_3, 60, 60],
  4: [ICON_4, 60, 60],
  5: [ICON_5, 60, 60],
  6: [ICON_6, 60, 60],
  7: [ICON_7, 60, 60],
  8: [ICON_8, 60, 60],
  9: [ICON_9, 60, 60],
}

export default {
  methods: {
    createBuildings (buildings) {
      // console.log('buidings: ' + buildings)
      let symbol, point, graphic
      buildings.forEach( building => {
        // console.log('add building: ' + JSON.stringify(building))
        let symbol = deviceTypeList[building.type];
        if (symbol) {
          symbol = new this.$arcgis.PictureMarkerSymbol(...deviceTypeList[building.type])
        } else {
          symbol = new this.$arcgis.TextSymbol(building.name);
        }
        // symbol = new this.$arcgis.PictureMarkerSymbol(...building.symbol)
        point = new this.$arcgis.Point({
          x: building.position[0],
          y: building.position[1],
          spatialReference: building.spatialReference,
        })
        graphic = new this.$arcgis.Graphic(point, symbol, building.data)
        this.buildingLayer.add(graphic)
        // console.log('add graphic: ' + graphic)
      })
    },
  }
}
