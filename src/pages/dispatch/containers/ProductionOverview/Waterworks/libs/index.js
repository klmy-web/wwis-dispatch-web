import markersMixin from './marker'
import infoWindowMixin from './infoWindow'
import areaLayerMixin from './areaLayer'
import featuresMixin from './features'
import mapMixin from './map'
import building from './building'
import guideline from './guideline'
import pipeline from './pipeline'

export default [
  mapMixin,
  markersMixin,
  infoWindowMixin,
  areaLayerMixin,
  featuresMixin,
  building,
  guideline,
  pipeline,
]
