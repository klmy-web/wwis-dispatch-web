const shpJsonPage13GuanHuiJian_1BaiJianTanGuanHuiJian = {
  "layers": [{
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 2,
      "name": "Freme",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186153,
        "ymin": 763,
        "xmax": 9186976,
        "ymax": 1184,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [165, 83, 183, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "性质",
        "type": "esriFieldTypeString",
        "alias": "性质",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"性质": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186153, 763], [9186976, 763], [9186976, 1184], [9186153, 1184], [9186153, 763]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 0,
      "name": "Lable",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186331,
        "ymin": 639,
        "xmax": 9187200,
        "ymax": 1267,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/RedSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAAB3VJREFUeF7tmPlTlEcexnve94U5mANQbgQSbgiHXHINlxpRIBpRI6wHorLERUmIisKCQWM8cqigESVQS1Kx1piNi4mW2YpbcZONrilE140RCTcy3DDAcL/zbJP8CYPDL+9Ufau7uqb7eZ7P+/a8PS8hwkcgIBAQCAgEBAICAYGAQEAgIBAQCAgEBAICAYGAQEAgIBAQCDx/AoowKXFMUhD3lQrioZaQRVRS+fxl51eBTZUTdZ41U1Rox13/0JF9csGJ05Qv4jSz/YPWohtvLmSKN5iTGGqTm1+rc6weICOBRbZs1UVnrv87T1PUeovxyNsUP9P6n5cpHtCxu24cbrmwKLdj+osWiqrVKhI0xzbmZ7m1SpJ+1pFpvE2DPvGTomOxAoNLLKGLscZYvB10cbYYjrJCb7A5mrxleOBqim+cWJRakZY0JfnD/LieI9V1MrKtwokbrAtU4Vm0A3TJnphJD4B+RxD0u0LA7w7FTE4oprOCMbklEGNrfdGf4IqnQTb4wc0MFTYibZqM7JgjO8ZdJkpMln/sKu16pHZGb7IfptIWg389DPp9kcChWODoMuDdBOhL1JgpisbUvghM7AqFbtNiaFP80RLnhbuBdqi0N+1dbUpWGde9gWpuhFi95yL7sS7BA93JAb+Fn8mh4QujgPeTgb9kAZf3Apd2A+fXQ38yHjOHozB1IAJjOSEY2RSIwVUv4dd4X9wJccGHNrJ7CYQ4GGjLeNNfM+dyvgpzQstKf3pbB2A6m97uBRE0/Ergcxr8hyqg7hrwn0vAtRIKIRX6Y2pMl0RhIj8co9nBGFrvh55l3ngU7YObng7IVnFvGS+BYUpmHziY/Ls2zgP9SX50by/G9N5w6I+ogYvpwK1SoOlHQNsGfWcd9Peqof88B/rTyzF9hAIopAByQzC0JQB9ST5oVnvhnt+LOGsprvUhxNIwa0aY7cGR6Cp7tr8+whkjawIxkRWC6YJI6N+lAKq3Qf/Tx+B77oGfaQc/8hB8w2Xwtw9Bf3kzZspXY/JIDEbfpAB2BKLvVV90Jvjgoac9vpRxE8kciTVCBMMkNirJ7k/tRHyjtxwjKV4Yp3t/6s+R4E+/DH3N6+BrS8E314Dvvg2+/Sb4hxfBf5sP/up2TF3ZhonK1zD6dhwGdwail26DzqgX8MRKiq9ZBpkSkmeYOyPM3m9Jjl+1Z9D8AgNtlAq6bZ70qsZi+q+bwV/7I/hbB8D/dAr8Axq89iz474p/G5++koHJy1sx/lkGdBc2YjA3HF0rHNHuboomuQj/5DgclIvOGCGCYRKFFuTMV7YUAD3VDQaLMfyqBcZORGPy01QKYSNm/rYV/Nd/Av9NHvgbueBrsjDzRQamKKDxT9Kgq1iLkbIUDOSHoiNcgnYHgnYZi+9ZExSbiSoMc2eE2flKcuJLa4KGRQz6/U0wlGaP0feiMH4uFpMXEjBVlYjp6lWY+SSZtim0kulYMiYuJEJXuhTDJ9UYPByOvoIwdCxfgE4bAo0Jh39xLAoVpMwIEQyTyFCQvGpLon9sJ0K3J4OBDDcMH1dj9FQsxkrjMPFRPCbOx2GyfLal9VEcxstioTulxjAFNfROJPqLl6Bnfyg6V7ugz5yBhuHwrZjBdiU5YJg7I8wOpifAKoVIW7uQ3rpOBH2b3ekVjYT2WCRG3o+mIGKgO0OrlIaebU/HYOQDNbQnojB4NJyGD0NPfjA0bwTRE6Q7hsUcWhkWN8yZqSQlWWGECAZLmJfJmbrvVSI8taK37xpbdB/wQW8xPee/8xIGjvlj8IQ/hk4G0JbWcX8MHPVDX4kveoq8ocn3xLM33NCZRcPHOGJYZIKfpQyq7JjHS6yJjcHujLHADgkpuC7h8F8zEVqXSNC2awE69lqhs8AamkO26HrbDt2H7dBVQov2NcW26CiwQtu+BWjdY4n2nZboTbfCmKcCnRyDO/YmyLPnDlHvjDH8G6zhS9/wlEnYR7X00fWrFYuWdVI0ZpuhcbcczW/R2qdAcz6t/bRov4mONeaaoYl+p22rHF0bVNAmKtBvweIXGxNcfFH8eNlC4m6wMWMusEnKpn5hyo48pj9gLe4SNG9QoGGLAk8z5XiaJUd99u8122/IpBA2K9BGg2vWWKAvRYVeLzEa7E1R422m2+MsSTem97nSYnfKyN6/mzATv7AUgqcMrUnmaFlLX3ysM0fj+t/b5lQLtK22QEfyAmiSLKFZpUJ7kBRPXKW4HqCYynWVHKSG2LkyZex1uO1mZM9lKem9Tx9jjY5iNEYo0bKMhn7ZAu0r6H5PpLXCAq0rKJClSjSGynE/QIkrQYqBPe6S2X+AJsY2Ped6iWZk6RlL0c2r5szofRsO9R5S1IfQLRCpQL1aifoYFerpsbkuTImaUJXuXIDiH6/Ys8vm3Mg8L2i20YqsO7fItKLcSXyn0kXccclVqv3MS6at9JU/Ox+ouns+SF6Z4cSupz7l8+z1ucs7LF1AQjOdxfGZzmx8Iu1TRcfnrioICAQEAgIBgYBAQCAgEBAICAQEAgIBgYBAQCAgEBAICAQEAv8H44b/6ZiGvGAAAAAASUVORK5CYII=",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NOTE",
        "type": "esriFieldTypeString",
        "alias": "NOTE",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {"attributes": {"NOTE": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "NOTE": "输水首战"},
        "geometry": {"x": 9187200, "y": 968, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 1, "NOTE": "由输水首站至炼油"},
        "geometry": {"x": 9186700, "y": 639, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 2, "NOTE": "一化"},
        "geometry": {"x": 9186331, "y": 1267, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 3, "NOTE": "泥浆站"},
        "geometry": {"x": 9186523, "y": 1266, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 3,
      "name": "Line",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186330,
        "ymin": 673,
        "xmax": 9187152,
        "ymax": 1236,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [241, 152, 60, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "FLOW",
        "type": "esriFieldTypeString",
        "alias": "FLOW",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管径",
        "type": "esriFieldTypeString",
        "alias": "管径",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"FLOW": null, "管径": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "FLOW": "正向", "管径": "dn700"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187084, 971], [9186527, 971]]]
        }
      }, {
        "attributes": {"FID": 1, "FLOW": "反向", "管径": "dn700"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187084, 971], [9187152, 971]]]
        }
      }, {
        "attributes": {"FID": 2, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186527, 825], [9186330, 825]]]
        }
      }, {
        "attributes": {"FID": 3, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186330, 825], [9186330, 1236]]]
        }
      }, {
        "attributes": {"FID": 4, "FLOW": "正向", "管径": "dn500"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186701, 971], [9186701, 673]]]
        }
      }, {
        "attributes": {"FID": 5, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186527, 971], [9186527, 825]]]
        }
      }, {
        "attributes": {"FID": 6, "FLOW": "反向", "管径": "dn200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186527, 1236], [9186527, 971]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 1,
      "name": "Point",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186527,
        "ymin": 900,
        "xmax": 9187084,
        "ymax": 1030,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/OrangeSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAABv9JREFUeF7tl31UVVUaxk/34uXCBeTjqqXQoA1oGgqOOprlFwKKOpqCCq4IyEJL1CEmyxUijkjDOBAGFgM6qSz8KPEbCQQFP2JQhmwmhmlVSwvTIbgIJOLl7vPMc8U/5v+Ll382az1rn7PP3ft9n99+9z4HRZF/koAkIAlIApKAJCAJSAKSgCQgCUgCkoAkIAlIApKAJCAJPH4CrqtHKN7bntX5pY7R+a8drvgwpNvjDzuwEbTpzyovHgvRp5ye73Tm6jJDU9Mq19tWXVtuaDoRbigtCXXekhGgzGCaDgObaj9HXzxYCawId9r7nxiXNvzBA0jxgiXVC72PZEkxAu8NAZKN+PZl97byeYZ9q55SJvZzGgMzXepYJboxyvCd+rYH1G1G9GQMg/kDb6i7R0L9+Jk+5Y5Cb9av0LN9OLDlKYikofhmpduNnRM0qwYm636K+sexStx3cW7tSPOGOcsPlsJA4OBvgU+nAiXUsenUi8BR6hBVNB0ifxLMmeNYEb5ojjV2ZARoVvdTOvad5tWRSsj38cY7oBlzPk0X0+zRF4CTNFo6GygPAT6fC5ydyftZwKlg4Dh1ZA6wbxbMufxt+gT8FDvs53f8lHD7Zm9jtHkuypC6COMV7PwNHuQ/32e+hMbPUFWLgNoEoD4ZuJoI1EQCFTRdRihnCOQEwRxhu28OHuSyL30K6pcZ62I8lRE2pmW/4e8HOa3r2MyVz6PxA1bzM4DTNF8Z3mf8273AjyeB74uBr9IIYRkrgpVQxgooDWUlhHGrsN1DCNmz0fP7Mdg9Wf+W/RzYFslwPdL9ssicDFFI84dp/iRLvIztpWigaRfQegW4/wPUuw1Qb+yDWr+OlUHD1io4ywo4NZ/bhbCKwtCbx+rYEoSr4Yb6UDfF07bU7DB6povyQnO8Vxv+MgHqfpo+ai3tRwBq42i4EKKzDkL8CNFzHaLlMMQ3qRDXYqBeXsrtsIDVMo9VwPYgIewJgznlObRFOPYk+ioz7WDBthBZ4x0Sb672EmqGL7Cf+/8493PpHKjn50NcXwtxcxdE6wlCqIZoL4No/itE0yaIL1+lXoOoI6TKCI7jWXFkMVAQDMvb3miN0GLHWCXJtuzsMPpssNP7pjeHQk3nOz3fj6+8aVzVcIj6WIivEmj2XYgb2X3Gb+Zy9bewfw1EQxz1OsQ/3iSENVBLeTgWEV7OeFiSvHA3UouDU5QcO1iwLcT5UOeczsRhUHd4A3kjeQiO4erPY4lHUdE0SRD/osmvk6gNrApCsZqvp67FP6wAURsL9RwPxr1TgJ2jITa4o2O5BiXTlQLbsrPD6PJgpz+1v0EA232AD0dBPTwJonoBxGVWwRXqC17XLoL4+++oxdQS6iX2U1eoi7yvZlsVCbWIZ8eOZyDWOOHuCg0OPf/Eh3awYFuIvCBN0s04o4o0AsgmgJKZNERTF2n40kKC4FnwUGGPxOqw3l/kM+tvapZCnI+EqFwJ9RDfDClPojdOi9aVDtgZ6PCObdnZYfQiTyXkhyi3DmweAWTye59vAXGeK1rN1X4IgatvBXGJlfBQ1mv21fB5Nc1fiKD55RDlBHCA3wYbnfEgRoO2aEfzhtHaMDtYsDmE+78X6hp61xkh0nwhiqfCfC4E5iqqei7MNcEwX/w/We9r2F8dCjPPCnPVApgrFsLMT2NLrj8sq7Xojdfh66WOjYtGGobZnJ09JsgNUDZ3Relh3jgEvR+NRnfpRHSXT0J35WR0X2BrVfUjXbD2TUF31VQ+n4buCupkILqLfWHe6oKe+EGwJDjjk1m6VOausUf+NscI9VB8GoKVf+IVR9zfZMQvxaPQeXocOs9OQGd5IDorgtBZSZ2b2KcKqpwq5fOSX6PzwFB05bngfqIOWGfAlyv1jbGjFD+bE7PnBBnjlGV3Fj7RpcYMQud7g2Ha/zRMn46G6VgATKcCYToTBNPpiRSvj7PvM3+YinxgKvSEabcrxzhDrDegda3TvYLZmmh75t5fsbRZ45Xklpc0PXhZg/ZNzvhvrhEthcPR8ren0bLft0+f+LDvSbTke6Elzx13dg1G+1Y3iGQXmDY4mwvCHN5lQtr+Ssre8zhkB2rWNy9x+NkS44DujXrc3uaK5j+741a2B259QOVQvG7O8sDtTA/cSxsMy2ZX3FqvN+0J1Vj/Axxk76T7Pd4b/kpw7fxBZc0rHH9RE5ygbnJF11Zui+0eMO3wRFeGJ0S6O9/5LvjpLed7dfG68uQAZW6/JzLAExq2BWgjGpboCupX6L+oj3JsbozRdzTGOXU0xOlvXU3Q115/Xbcnc5o2knm6DHCujz38iNf8lck5M7SzrVrLa0b0fuxRZQBJQBKQBCQBSUASkAQkAUlAEpAEJAFJQBKQBCQBSUAS+B/qhLD6M6wGWAAAAABJRU5ErkJggg==",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NAME",
        "type": "esriFieldTypeString",
        "alias": "NAME",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "icon",
        "type": "esriFieldTypeString",
        "alias": "icon",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "type",
        "type": "esriFieldTypeString",
        "alias": "type",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "isOnClick",
        "type": "esriFieldTypeString",
        "alias": "isOnClick",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "width",
        "type": "esriFieldTypeString",
        "alias": "width",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "height",
        "type": "esriFieldTypeString",
        "alias": "height",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "dataID",
        "type": "esriFieldTypeString",
        "alias": "dataID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {
          "attributes": {
            "NAME": null,
            "icon": null,
            "type": null,
            "isOnClick": null,
            "width": null,
            "height": null,
            "dataID": null
          }
        }
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {
          "FID": 0,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "20.94",
          "height": "22.50",
          "dataID": " "
        }, "geometry": {"x": 9186701, "y": 901, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 1,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "20.94",
          "height": "22.50",
          "dataID": " "
        }, "geometry": {"x": 9186527, "y": 900, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 2,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "20.94",
          "height": "22.50",
          "dataID": " "
        }, "geometry": {"x": 9186527, "y": 1030, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 3,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "20.94",
          "height": "22.50",
          "dataID": " "
        }, "geometry": {"x": 9187084, "y": 971, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }], "showLegend": true
}
const shpJsonPage13GuanHuiJian_2FaMenShi1 = {
  "layers": [{
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 2,
      "name": "Frame",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9187162,
        "ymin": 1306,
        "xmax": 9187471,
        "ymax": 1692,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [165, 83, 183, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "性质",
        "type": "esriFieldTypeString",
        "alias": "性质",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"性质": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187162, 1692], [9187471, 1692], [9187471, 1306], [9187162, 1306], [9187162, 1692]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 0,
      "name": "Label",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9187187,
        "ymin": 1750,
        "xmax": 9187187,
        "ymax": 1750,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/RedSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAAB3VJREFUeF7tmPlTlEcexnve94U5mANQbgQSbgiHXHINlxpRIBpRI6wHorLERUmIisKCQWM8cqigESVQS1Kx1piNi4mW2YpbcZONrilE140RCTcy3DDAcL/zbJP8CYPDL+9Ufau7uqb7eZ7P+/a8PS8hwkcgIBAQCAgEBAICAYGAQEAgIBAQCAgEBAICAYGAQEAgIBAQCDx/AoowKXFMUhD3lQrioZaQRVRS+fxl51eBTZUTdZ41U1Rox13/0JF9csGJ05Qv4jSz/YPWohtvLmSKN5iTGGqTm1+rc6weICOBRbZs1UVnrv87T1PUeovxyNsUP9P6n5cpHtCxu24cbrmwKLdj+osWiqrVKhI0xzbmZ7m1SpJ+1pFpvE2DPvGTomOxAoNLLKGLscZYvB10cbYYjrJCb7A5mrxleOBqim+cWJRakZY0JfnD/LieI9V1MrKtwokbrAtU4Vm0A3TJnphJD4B+RxD0u0LA7w7FTE4oprOCMbklEGNrfdGf4IqnQTb4wc0MFTYibZqM7JgjO8ZdJkpMln/sKu16pHZGb7IfptIWg389DPp9kcChWODoMuDdBOhL1JgpisbUvghM7AqFbtNiaFP80RLnhbuBdqi0N+1dbUpWGde9gWpuhFi95yL7sS7BA93JAb+Fn8mh4QujgPeTgb9kAZf3Apd2A+fXQ38yHjOHozB1IAJjOSEY2RSIwVUv4dd4X9wJccGHNrJ7CYQ4GGjLeNNfM+dyvgpzQstKf3pbB2A6m97uBRE0/Ergcxr8hyqg7hrwn0vAtRIKIRX6Y2pMl0RhIj8co9nBGFrvh55l3ngU7YObng7IVnFvGS+BYUpmHziY/Ls2zgP9SX50by/G9N5w6I+ogYvpwK1SoOlHQNsGfWcd9Peqof88B/rTyzF9hAIopAByQzC0JQB9ST5oVnvhnt+LOGsprvUhxNIwa0aY7cGR6Cp7tr8+whkjawIxkRWC6YJI6N+lAKq3Qf/Tx+B77oGfaQc/8hB8w2Xwtw9Bf3kzZspXY/JIDEbfpAB2BKLvVV90Jvjgoac9vpRxE8kciTVCBMMkNirJ7k/tRHyjtxwjKV4Yp3t/6s+R4E+/DH3N6+BrS8E314Dvvg2+/Sb4hxfBf5sP/up2TF3ZhonK1zD6dhwGdwail26DzqgX8MRKiq9ZBpkSkmeYOyPM3m9Jjl+1Z9D8AgNtlAq6bZ70qsZi+q+bwV/7I/hbB8D/dAr8Axq89iz474p/G5++koHJy1sx/lkGdBc2YjA3HF0rHNHuboomuQj/5DgclIvOGCGCYRKFFuTMV7YUAD3VDQaLMfyqBcZORGPy01QKYSNm/rYV/Nd/Av9NHvgbueBrsjDzRQamKKDxT9Kgq1iLkbIUDOSHoiNcgnYHgnYZi+9ZExSbiSoMc2eE2flKcuJLa4KGRQz6/U0wlGaP0feiMH4uFpMXEjBVlYjp6lWY+SSZtim0kulYMiYuJEJXuhTDJ9UYPByOvoIwdCxfgE4bAo0Jh39xLAoVpMwIEQyTyFCQvGpLon9sJ0K3J4OBDDcMH1dj9FQsxkrjMPFRPCbOx2GyfLal9VEcxstioTulxjAFNfROJPqLl6Bnfyg6V7ugz5yBhuHwrZjBdiU5YJg7I8wOpifAKoVIW7uQ3rpOBH2b3ekVjYT2WCRG3o+mIGKgO0OrlIaebU/HYOQDNbQnojB4NJyGD0NPfjA0bwTRE6Q7hsUcWhkWN8yZqSQlWWGECAZLmJfJmbrvVSI8taK37xpbdB/wQW8xPee/8xIGjvlj8IQ/hk4G0JbWcX8MHPVDX4kveoq8ocn3xLM33NCZRcPHOGJYZIKfpQyq7JjHS6yJjcHujLHADgkpuC7h8F8zEVqXSNC2awE69lqhs8AamkO26HrbDt2H7dBVQov2NcW26CiwQtu+BWjdY4n2nZboTbfCmKcCnRyDO/YmyLPnDlHvjDH8G6zhS9/wlEnYR7X00fWrFYuWdVI0ZpuhcbcczW/R2qdAcz6t/bRov4mONeaaoYl+p22rHF0bVNAmKtBvweIXGxNcfFH8eNlC4m6wMWMusEnKpn5hyo48pj9gLe4SNG9QoGGLAk8z5XiaJUd99u8122/IpBA2K9BGg2vWWKAvRYVeLzEa7E1R422m2+MsSTem97nSYnfKyN6/mzATv7AUgqcMrUnmaFlLX3ysM0fj+t/b5lQLtK22QEfyAmiSLKFZpUJ7kBRPXKW4HqCYynWVHKSG2LkyZex1uO1mZM9lKem9Tx9jjY5iNEYo0bKMhn7ZAu0r6H5PpLXCAq0rKJClSjSGynE/QIkrQYqBPe6S2X+AJsY2Ped6iWZk6RlL0c2r5szofRsO9R5S1IfQLRCpQL1aifoYFerpsbkuTImaUJXuXIDiH6/Ys8vm3Mg8L2i20YqsO7fItKLcSXyn0kXccclVqv3MS6at9JU/Ox+ouns+SF6Z4cSupz7l8+z1ucs7LF1AQjOdxfGZzmx8Iu1TRcfnrioICAQEAgIBgYBAQCAgEBAICAQEAgIBgYBAQCAgEBAICAQEAv8H44b/6ZiGvGAAAAAASUVORK5CYII=",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NOTE",
        "type": "esriFieldTypeString",
        "alias": "NOTE",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {"attributes": {"NOTE": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "NOTE": "阀门室"},
        "geometry": {"x": 9187187, "y": 1750, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 3,
      "name": "Line",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9187117,
        "ymin": 1358,
        "xmax": 9187590,
        "ymax": 1724,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [241, 152, 60, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "FLOW",
        "type": "esriFieldTypeString",
        "alias": "FLOW",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管径",
        "type": "esriFieldTypeString",
        "alias": "管径",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"FLOW": null, "管径": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187330, 1653], [9187330, 1395]]]
        }
      }, {
        "attributes": {"FID": 1, "FLOW": "反向", "管径": "dn200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187271, 1653], [9187330, 1653]]]
        }
      }, {
        "attributes": {"FID": 2, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187271, 1653], [9187271, 1606]]]
        }
      }, {
        "attributes": {"FID": 3, "FLOW": "反向", "管径": "dn400"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187397, 1593], [9187397, 1621]]]
        }
      }, {
        "attributes": {"FID": 4, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187397, 1621], [9187412, 1621]]]
        }
      }, {
        "attributes": {"FID": 5, "FLOW": "反向", "管径": "dn400"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187304, 1358], [9187304, 1395]]]
        }
      }, {
        "attributes": {"FID": 6, "FLOW": "反向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187304, 1395], [9187330, 1395]]]
        }
      }, {
        "attributes": {"FID": 7, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187330, 1653], [9187412, 1653]]]
        }
      }, {
        "attributes": {"FID": 8, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187412, 1621], [9187425, 1621]]]
        }
      }, {
        "attributes": {"FID": 9, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187330, 1395], [9187353, 1395]]]
        }
      }, {
        "attributes": {"FID": 10, "FLOW": "正向", "管径": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187330, 1724], [9187330, 1653]]]
        }
      }, {
        "attributes": {"FID": 11, "FLOW": "反向", "管径": "dn630"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187397, 1593], [9187425, 1593]]]
        }
      }, {
        "attributes": {"FID": 12, "FLOW": "反向", "管径": "dn630"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187117, 1593], [9187397, 1593]]]
        }
      }, {
        "attributes": {"FID": 13, "FLOW": "反向", "管径": "dn630"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187304, 1358], [9187353, 1358]]]
        }
      }, {
        "attributes": {"FID": 14, "FLOW": "反向", "管径": "dn630"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187117, 1358], [9187304, 1358]]]
        }
      }, {
        "attributes": {"FID": 15, "FLOW": "反向", "管径": "dn200"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187228, 1653], [9187271, 1653]]]
        }
      }, {
        "attributes": {"FID": 16, "FLOW": "反向", "管径": "dn630"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187425, 1593], [9187574, 1593]]]
        }
      }, {
        "attributes": {"FID": 17, "FLOW": "反向", "管径": "dn630"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187353, 1358], [9187590, 1358]]]
        }
      }, {
        "attributes": {"FID": 18, "FLOW": "正向", "管径": "dn400"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187425, 1621], [9187425, 1593]]]
        }
      }, {
        "attributes": {"FID": 19, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187412, 1653], [9187412, 1621]]]
        }
      }, {
        "attributes": {"FID": 20, "FLOW": "正向", "管径": "dn400"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187353, 1395], [9187353, 1358]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 1,
      "name": "Point",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9187250,
        "ymin": 1374,
        "xmax": 9187425,
        "ymax": 1653,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/OrangeSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAABv9JREFUeF7tl31UVVUaxk/34uXCBeTjqqXQoA1oGgqOOprlFwKKOpqCCq4IyEJL1CEmyxUijkjDOBAGFgM6qSz8KPEbCQQFP2JQhmwmhmlVSwvTIbgIJOLl7vPMc8U/5v+Ll382az1rn7PP3ft9n99+9z4HRZF/koAkIAlIApKAJCAJSAKSgCQgCUgCkoAkIAlIApKAJCAJPH4CrqtHKN7bntX5pY7R+a8drvgwpNvjDzuwEbTpzyovHgvRp5ye73Tm6jJDU9Mq19tWXVtuaDoRbigtCXXekhGgzGCaDgObaj9HXzxYCawId9r7nxiXNvzBA0jxgiXVC72PZEkxAu8NAZKN+PZl97byeYZ9q55SJvZzGgMzXepYJboxyvCd+rYH1G1G9GQMg/kDb6i7R0L9+Jk+5Y5Cb9av0LN9OLDlKYikofhmpduNnRM0qwYm636K+sexStx3cW7tSPOGOcsPlsJA4OBvgU+nAiXUsenUi8BR6hBVNB0ifxLMmeNYEb5ojjV2ZARoVvdTOvad5tWRSsj38cY7oBlzPk0X0+zRF4CTNFo6GygPAT6fC5ydyftZwKlg4Dh1ZA6wbxbMufxt+gT8FDvs53f8lHD7Zm9jtHkuypC6COMV7PwNHuQ/32e+hMbPUFWLgNoEoD4ZuJoI1EQCFTRdRihnCOQEwRxhu28OHuSyL30K6pcZ62I8lRE2pmW/4e8HOa3r2MyVz6PxA1bzM4DTNF8Z3mf8273AjyeB74uBr9IIYRkrgpVQxgooDWUlhHGrsN1DCNmz0fP7Mdg9Wf+W/RzYFslwPdL9ssicDFFI84dp/iRLvIztpWigaRfQegW4/wPUuw1Qb+yDWr+OlUHD1io4ywo4NZ/bhbCKwtCbx+rYEoSr4Yb6UDfF07bU7DB6povyQnO8Vxv+MgHqfpo+ai3tRwBq42i4EKKzDkL8CNFzHaLlMMQ3qRDXYqBeXsrtsIDVMo9VwPYgIewJgznlObRFOPYk+ioz7WDBthBZ4x0Sb672EmqGL7Cf+/8493PpHKjn50NcXwtxcxdE6wlCqIZoL4No/itE0yaIL1+lXoOoI6TKCI7jWXFkMVAQDMvb3miN0GLHWCXJtuzsMPpssNP7pjeHQk3nOz3fj6+8aVzVcIj6WIivEmj2XYgb2X3Gb+Zy9bewfw1EQxz1OsQ/3iSENVBLeTgWEV7OeFiSvHA3UouDU5QcO1iwLcT5UOeczsRhUHd4A3kjeQiO4erPY4lHUdE0SRD/osmvk6gNrApCsZqvp67FP6wAURsL9RwPxr1TgJ2jITa4o2O5BiXTlQLbsrPD6PJgpz+1v0EA232AD0dBPTwJonoBxGVWwRXqC17XLoL4+++oxdQS6iX2U1eoi7yvZlsVCbWIZ8eOZyDWOOHuCg0OPf/Eh3awYFuIvCBN0s04o4o0AsgmgJKZNERTF2n40kKC4FnwUGGPxOqw3l/kM+tvapZCnI+EqFwJ9RDfDClPojdOi9aVDtgZ6PCObdnZYfQiTyXkhyi3DmweAWTye59vAXGeK1rN1X4IgatvBXGJlfBQ1mv21fB5Nc1fiKD55RDlBHCA3wYbnfEgRoO2aEfzhtHaMDtYsDmE+78X6hp61xkh0nwhiqfCfC4E5iqqei7MNcEwX/w/We9r2F8dCjPPCnPVApgrFsLMT2NLrj8sq7Xojdfh66WOjYtGGobZnJ09JsgNUDZ3Relh3jgEvR+NRnfpRHSXT0J35WR0X2BrVfUjXbD2TUF31VQ+n4buCupkILqLfWHe6oKe+EGwJDjjk1m6VOausUf+NscI9VB8GoKVf+IVR9zfZMQvxaPQeXocOs9OQGd5IDorgtBZSZ2b2KcKqpwq5fOSX6PzwFB05bngfqIOWGfAlyv1jbGjFD+bE7PnBBnjlGV3Fj7RpcYMQud7g2Ha/zRMn46G6VgATKcCYToTBNPpiRSvj7PvM3+YinxgKvSEabcrxzhDrDegda3TvYLZmmh75t5fsbRZ45Xklpc0PXhZg/ZNzvhvrhEthcPR8ren0bLft0+f+LDvSbTke6Elzx13dg1G+1Y3iGQXmDY4mwvCHN5lQtr+Ssre8zhkB2rWNy9x+NkS44DujXrc3uaK5j+741a2B259QOVQvG7O8sDtTA/cSxsMy2ZX3FqvN+0J1Vj/Axxk76T7Pd4b/kpw7fxBZc0rHH9RE5ygbnJF11Zui+0eMO3wRFeGJ0S6O9/5LvjpLed7dfG68uQAZW6/JzLAExq2BWgjGpboCupX6L+oj3JsbozRdzTGOXU0xOlvXU3Q115/Xbcnc5o2knm6DHCujz38iNf8lck5M7SzrVrLa0b0fuxRZQBJQBKQBCQBSUASkAQkAUlAEpAEJAFJQBKQBCQBSUAS+B/qhLD6M6wGWAAAAABJRU5ErkJggg==",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NAME",
        "type": "esriFieldTypeString",
        "alias": "NAME",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "icon",
        "type": "esriFieldTypeString",
        "alias": "icon",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "type",
        "type": "esriFieldTypeString",
        "alias": "type",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "isOnClick",
        "type": "esriFieldTypeString",
        "alias": "isOnClick",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "width",
        "type": "esriFieldTypeString",
        "alias": "width",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "height",
        "type": "esriFieldTypeString",
        "alias": "height",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "dataID",
        "type": "esriFieldTypeString",
        "alias": "dataID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "mxdID",
        "type": "esriFieldTypeString",
        "alias": "mxdID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {
          "attributes": {
            "NAME": null,
            "icon": null,
            "type": null,
            "isOnClick": null,
            "width": null,
            "height": null,
            "dataID": null,
            "mxdID": null
          }
        }
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {
          "FID": 0,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9187250, "y": 1653, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 1,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9187301, "y": 1653, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 2,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9187397, "y": 1606, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 3,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9187425, "y": 1606, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 4,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9187271, "y": 1638, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 5,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9187271, "y": 1616, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 6,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9187304, "y": 1374, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 7,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9187353, "y": 1375, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }], "showLegend": true
}
const shpJsonPage13GuanHuiJian_3FaMenShi2 = {
  "layers": [{
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 2,
      "name": "Freme",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9187693,
        "ymin": 1291,
        "xmax": 9188025,
        "ymax": 1694,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [165, 83, 183, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "FID_",
        "type": "esriFieldTypeInteger",
        "alias": "FID_",
        "sqlType": "sqlTypeOther",
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "Entity",
        "type": "esriFieldTypeString",
        "alias": "Entity",
        "sqlType": "sqlTypeOther",
        "length": 16,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "Layer",
        "type": "esriFieldTypeString",
        "alias": "Layer",
        "sqlType": "sqlTypeOther",
        "length": 254,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "Color",
        "type": "esriFieldTypeSmallInteger",
        "alias": "Color",
        "sqlType": "sqlTypeOther",
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "Linetype",
        "type": "esriFieldTypeString",
        "alias": "Linetype",
        "sqlType": "sqlTypeOther",
        "length": 254,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "Elevation",
        "type": "esriFieldTypeDouble",
        "alias": "Elevation",
        "sqlType": "sqlTypeOther",
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "LineWt",
        "type": "esriFieldTypeSmallInteger",
        "alias": "LineWt",
        "sqlType": "sqlTypeOther",
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "RefName",
        "type": "esriFieldTypeString",
        "alias": "RefName",
        "sqlType": "sqlTypeOther",
        "length": 254,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {
          "attributes": {
            "FID_": null,
            "Entity": null,
            "Layer": null,
            "Color": null,
            "Linetype": null,
            "Elevation": null,
            "LineWt": null,
            "RefName": null
          }
        }
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {
          "FID": 0,
          "FID_": 0,
          "Entity": " ",
          "Layer": " ",
          "Color": 0,
          "Linetype": " ",
          "Elevation": 0,
          "LineWt": 0,
          "RefName": " "
        },
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187693, 1694], [9188025, 1694], [9188025, 1291], [9187693, 1291], [9187693, 1694]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 0,
      "name": "Lable",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9187739,
        "ymin": 1752,
        "xmax": 9187739,
        "ymax": 1752,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/RedSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAAB3VJREFUeF7tmPlTlEcexnve94U5mANQbgQSbgiHXHINlxpRIBpRI6wHorLERUmIisKCQWM8cqigESVQS1Kx1piNi4mW2YpbcZONrilE140RCTcy3DDAcL/zbJP8CYPDL+9Ufau7uqb7eZ7P+/a8PS8hwkcgIBAQCAgEBAICAYGAQEAgIBAQCAgEBAICAYGAQEAgIBAQCDx/AoowKXFMUhD3lQrioZaQRVRS+fxl51eBTZUTdZ41U1Rox13/0JF9csGJ05Qv4jSz/YPWohtvLmSKN5iTGGqTm1+rc6weICOBRbZs1UVnrv87T1PUeovxyNsUP9P6n5cpHtCxu24cbrmwKLdj+osWiqrVKhI0xzbmZ7m1SpJ+1pFpvE2DPvGTomOxAoNLLKGLscZYvB10cbYYjrJCb7A5mrxleOBqim+cWJRakZY0JfnD/LieI9V1MrKtwokbrAtU4Vm0A3TJnphJD4B+RxD0u0LA7w7FTE4oprOCMbklEGNrfdGf4IqnQTb4wc0MFTYibZqM7JgjO8ZdJkpMln/sKu16pHZGb7IfptIWg389DPp9kcChWODoMuDdBOhL1JgpisbUvghM7AqFbtNiaFP80RLnhbuBdqi0N+1dbUpWGde9gWpuhFi95yL7sS7BA93JAb+Fn8mh4QujgPeTgb9kAZf3Apd2A+fXQ38yHjOHozB1IAJjOSEY2RSIwVUv4dd4X9wJccGHNrJ7CYQ4GGjLeNNfM+dyvgpzQstKf3pbB2A6m97uBRE0/Ergcxr8hyqg7hrwn0vAtRIKIRX6Y2pMl0RhIj8co9nBGFrvh55l3ngU7YObng7IVnFvGS+BYUpmHziY/Ls2zgP9SX50by/G9N5w6I+ogYvpwK1SoOlHQNsGfWcd9Peqof88B/rTyzF9hAIopAByQzC0JQB9ST5oVnvhnt+LOGsprvUhxNIwa0aY7cGR6Cp7tr8+whkjawIxkRWC6YJI6N+lAKq3Qf/Tx+B77oGfaQc/8hB8w2Xwtw9Bf3kzZspXY/JIDEbfpAB2BKLvVV90Jvjgoac9vpRxE8kciTVCBMMkNirJ7k/tRHyjtxwjKV4Yp3t/6s+R4E+/DH3N6+BrS8E314Dvvg2+/Sb4hxfBf5sP/up2TF3ZhonK1zD6dhwGdwail26DzqgX8MRKiq9ZBpkSkmeYOyPM3m9Jjl+1Z9D8AgNtlAq6bZ70qsZi+q+bwV/7I/hbB8D/dAr8Axq89iz474p/G5++koHJy1sx/lkGdBc2YjA3HF0rHNHuboomuQj/5DgclIvOGCGCYRKFFuTMV7YUAD3VDQaLMfyqBcZORGPy01QKYSNm/rYV/Nd/Av9NHvgbueBrsjDzRQamKKDxT9Kgq1iLkbIUDOSHoiNcgnYHgnYZi+9ZExSbiSoMc2eE2flKcuJLa4KGRQz6/U0wlGaP0feiMH4uFpMXEjBVlYjp6lWY+SSZtim0kulYMiYuJEJXuhTDJ9UYPByOvoIwdCxfgE4bAo0Jh39xLAoVpMwIEQyTyFCQvGpLon9sJ0K3J4OBDDcMH1dj9FQsxkrjMPFRPCbOx2GyfLal9VEcxstioTulxjAFNfROJPqLl6Bnfyg6V7ugz5yBhuHwrZjBdiU5YJg7I8wOpifAKoVIW7uQ3rpOBH2b3ekVjYT2WCRG3o+mIGKgO0OrlIaebU/HYOQDNbQnojB4NJyGD0NPfjA0bwTRE6Q7hsUcWhkWN8yZqSQlWWGECAZLmJfJmbrvVSI8taK37xpbdB/wQW8xPee/8xIGjvlj8IQ/hk4G0JbWcX8MHPVDX4kveoq8ocn3xLM33NCZRcPHOGJYZIKfpQyq7JjHS6yJjcHujLHADgkpuC7h8F8zEVqXSNC2awE69lqhs8AamkO26HrbDt2H7dBVQov2NcW26CiwQtu+BWjdY4n2nZboTbfCmKcCnRyDO/YmyLPnDlHvjDH8G6zhS9/wlEnYR7X00fWrFYuWdVI0ZpuhcbcczW/R2qdAcz6t/bRov4mONeaaoYl+p22rHF0bVNAmKtBvweIXGxNcfFH8eNlC4m6wMWMusEnKpn5hyo48pj9gLe4SNG9QoGGLAk8z5XiaJUd99u8122/IpBA2K9BGg2vWWKAvRYVeLzEa7E1R422m2+MsSTem97nSYnfKyN6/mzATv7AUgqcMrUnmaFlLX3ysM0fj+t/b5lQLtK22QEfyAmiSLKFZpUJ7kBRPXKW4HqCYynWVHKSG2LkyZex1uO1mZM9lKem9Tx9jjY5iNEYo0bKMhn7ZAu0r6H5PpLXCAq0rKJClSjSGynE/QIkrQYqBPe6S2X+AJsY2Ped6iWZk6RlL0c2r5szofRsO9R5S1IfQLRCpQL1aifoYFerpsbkuTImaUJXuXIDiH6/Ys8vm3Mg8L2i20YqsO7fItKLcSXyn0kXccclVqv3MS6at9JU/Ox+ouns+SF6Z4cSupz7l8+z1ucs7LF1AQjOdxfGZzmx8Iu1TRcfnrioICAQEAgIBgYBAQCAgEBAICAQEAgIBgYBAQCAgEBAICAQEAv8H44b/6ZiGvGAAAAAASUVORK5CYII=",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NOTE",
        "type": "esriFieldTypeString",
        "alias": "NOTE",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {"attributes": {"NOTE": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "NOTE": "阀门室"},
        "geometry": {"x": 9187739, "y": 1752, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 3,
      "name": "Line",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9187578,
        "ymin": 1220,
        "xmax": 9188066,
        "ymax": 1724,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [241, 152, 60, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "FLOW",
        "type": "esriFieldTypeString",
        "alias": "FLOW",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管径",
        "type": "esriFieldTypeString",
        "alias": "管径",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"FLOW": null, "管径": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "FLOW": "反向", "管径": "dn630"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187585, 1593], [9187941, 1593]]]
        }
      }, {
        "attributes": {"FID": 1, "FLOW": "反向", "管径": "dn630"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187578, 1358], [9187941, 1358]]]
        }
      }, {
        "attributes": {"FID": 2, "FLOW": "正向", "管径": "dn1000"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187841, 1473], [9187841, 1220]]]
        }
      }, {
        "attributes": {"FID": 3, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187941, 1593], [9187941, 1473]]]
        }
      }, {
        "attributes": {"FID": 4, "FLOW": "正向", "管径": "dn1000"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187841, 1724], [9187841, 1473]]]
        }
      }, {
        "attributes": {"FID": 5, "FLOW": "反向", "管径": "dn630"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187941, 1358], [9188066, 1358]]]
        }
      }, {
        "attributes": {"FID": 6, "FLOW": "反向", "管径": "dn630"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187941, 1593], [9188066, 1593]]]
        }
      }, {
        "attributes": {"FID": 7, "FLOW": "正向", "管径": "dn1000"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187841, 1473], [9187941, 1473]]]
        }
      }, {
        "attributes": {"FID": 8, "FLOW": "正向", "管径": "dn600"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187941, 1473], [9187941, 1358]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 1,
      "name": "Point",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9187841,
        "ymin": 1427,
        "xmax": 9187941,
        "ymax": 1519,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/OrangeSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAABv9JREFUeF7tl31UVVUaxk/34uXCBeTjqqXQoA1oGgqOOprlFwKKOpqCCq4IyEJL1CEmyxUijkjDOBAGFgM6qSz8KPEbCQQFP2JQhmwmhmlVSwvTIbgIJOLl7vPMc8U/5v+Ll382az1rn7PP3ft9n99+9z4HRZF/koAkIAlIApKAJCAJSAKSgCQgCUgCkoAkIAlIApKAJCAJPH4CrqtHKN7bntX5pY7R+a8drvgwpNvjDzuwEbTpzyovHgvRp5ye73Tm6jJDU9Mq19tWXVtuaDoRbigtCXXekhGgzGCaDgObaj9HXzxYCawId9r7nxiXNvzBA0jxgiXVC72PZEkxAu8NAZKN+PZl97byeYZ9q55SJvZzGgMzXepYJboxyvCd+rYH1G1G9GQMg/kDb6i7R0L9+Jk+5Y5Cb9av0LN9OLDlKYikofhmpduNnRM0qwYm636K+sexStx3cW7tSPOGOcsPlsJA4OBvgU+nAiXUsenUi8BR6hBVNB0ifxLMmeNYEb5ojjV2ZARoVvdTOvad5tWRSsj38cY7oBlzPk0X0+zRF4CTNFo6GygPAT6fC5ydyftZwKlg4Dh1ZA6wbxbMufxt+gT8FDvs53f8lHD7Zm9jtHkuypC6COMV7PwNHuQ/32e+hMbPUFWLgNoEoD4ZuJoI1EQCFTRdRihnCOQEwRxhu28OHuSyL30K6pcZ62I8lRE2pmW/4e8HOa3r2MyVz6PxA1bzM4DTNF8Z3mf8273AjyeB74uBr9IIYRkrgpVQxgooDWUlhHGrsN1DCNmz0fP7Mdg9Wf+W/RzYFslwPdL9ssicDFFI84dp/iRLvIztpWigaRfQegW4/wPUuw1Qb+yDWr+OlUHD1io4ywo4NZ/bhbCKwtCbx+rYEoSr4Yb6UDfF07bU7DB6povyQnO8Vxv+MgHqfpo+ai3tRwBq42i4EKKzDkL8CNFzHaLlMMQ3qRDXYqBeXsrtsIDVMo9VwPYgIewJgznlObRFOPYk+ioz7WDBthBZ4x0Sb672EmqGL7Cf+/8493PpHKjn50NcXwtxcxdE6wlCqIZoL4No/itE0yaIL1+lXoOoI6TKCI7jWXFkMVAQDMvb3miN0GLHWCXJtuzsMPpssNP7pjeHQk3nOz3fj6+8aVzVcIj6WIivEmj2XYgb2X3Gb+Zy9bewfw1EQxz1OsQ/3iSENVBLeTgWEV7OeFiSvHA3UouDU5QcO1iwLcT5UOeczsRhUHd4A3kjeQiO4erPY4lHUdE0SRD/osmvk6gNrApCsZqvp67FP6wAURsL9RwPxr1TgJ2jITa4o2O5BiXTlQLbsrPD6PJgpz+1v0EA232AD0dBPTwJonoBxGVWwRXqC17XLoL4+++oxdQS6iX2U1eoi7yvZlsVCbWIZ8eOZyDWOOHuCg0OPf/Eh3awYFuIvCBN0s04o4o0AsgmgJKZNERTF2n40kKC4FnwUGGPxOqw3l/kM+tvapZCnI+EqFwJ9RDfDClPojdOi9aVDtgZ6PCObdnZYfQiTyXkhyi3DmweAWTye59vAXGeK1rN1X4IgatvBXGJlfBQ1mv21fB5Nc1fiKD55RDlBHCA3wYbnfEgRoO2aEfzhtHaMDtYsDmE+78X6hp61xkh0nwhiqfCfC4E5iqqei7MNcEwX/w/We9r2F8dCjPPCnPVApgrFsLMT2NLrj8sq7Xojdfh66WOjYtGGobZnJ09JsgNUDZ3Relh3jgEvR+NRnfpRHSXT0J35WR0X2BrVfUjXbD2TUF31VQ+n4buCupkILqLfWHe6oKe+EGwJDjjk1m6VOausUf+NscI9VB8GoKVf+IVR9zfZMQvxaPQeXocOs9OQGd5IDorgtBZSZ2b2KcKqpwq5fOSX6PzwFB05bngfqIOWGfAlyv1jbGjFD+bE7PnBBnjlGV3Fj7RpcYMQud7g2Ha/zRMn46G6VgATKcCYToTBNPpiRSvj7PvM3+YinxgKvSEabcrxzhDrDegda3TvYLZmmh75t5fsbRZ45Xklpc0PXhZg/ZNzvhvrhEthcPR8ren0bLft0+f+LDvSbTke6Elzx13dg1G+1Y3iGQXmDY4mwvCHN5lQtr+Ssre8zhkB2rWNy9x+NkS44DujXrc3uaK5j+741a2B259QOVQvG7O8sDtTA/cSxsMy2ZX3FqvN+0J1Vj/Axxk76T7Pd4b/kpw7fxBZc0rHH9RE5ygbnJF11Zui+0eMO3wRFeGJ0S6O9/5LvjpLed7dfG68uQAZW6/JzLAExq2BWgjGpboCupX6L+oj3JsbozRdzTGOXU0xOlvXU3Q115/Xbcnc5o2knm6DHCujz38iNf8lck5M7SzrVrLa0b0fuxRZQBJQBKQBCQBSUASkAQkAUlAEpAEJAFJQBKQBCQBSUAS+B/qhLD6M6wGWAAAAABJRU5ErkJggg==",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NAME",
        "type": "esriFieldTypeString",
        "alias": "NAME",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "ID",
        "type": "esriFieldTypeString",
        "alias": "ID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {"attributes": {"NAME": null, "ID": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "NAME": "阀门", "ID": "409"},
        "geometry": {"x": 9187841, "y": 1439, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 1, "NAME": "阀门", "ID": "410"},
        "geometry": {"x": 9187877, "y": 1473, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 2, "NAME": "阀门", "ID": "411"},
        "geometry": {"x": 9187941, "y": 1519, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 3, "NAME": "阀门", "ID": "412"},
        "geometry": {"x": 9187941, "y": 1427, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }], "showLegend": true
}

export {shpJsonPage13GuanHuiJian_1BaiJianTanGuanHuiJian}
export {shpJsonPage13GuanHuiJian_2FaMenShi1}
export {shpJsonPage13GuanHuiJian_3FaMenShi2}
