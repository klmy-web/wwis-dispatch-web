const shpJsonPage0ZongLan = {
  "layers": [{
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 2,
      "name": "Frame",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186493,
        "ymin": 1282,
        "xmax": 9187868,
        "ymax": 1854,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [165, 83, 183, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": true,
      "allowUpdateWithoutMValues": true,
      "hasZ": true,
      "enableZDefaults": true,
      "zDefault": 0,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "性质",
        "type": "esriFieldTypeString",
        "alias": "性质",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"性质": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186792, 1481], [9186782, 1481]]]
        }
      }, {
        "attributes": {"FID": 1, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186792, 1471], [9186782, 1471]]]
        }
      }, {
        "attributes": {"FID": 2, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186792, 1481], [9186792, 1471]]]
        }
      }, {
        "attributes": {"FID": 3, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186782, 1481], [9186782, 1471]]]
        }
      }, {
        "attributes": {"FID": 4, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187190, 1559], [9187224, 1559], [9187224, 1540], [9187190, 1540], [9187190, 1559]]]
        }
      }, {
        "attributes": {"FID": 5, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187623, 1736], [9187654, 1736], [9187654, 1715], [9187623, 1715], [9187623, 1736]]]
        }
      }, {
        "attributes": {"FID": 6, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186888, 1429], [9187094, 1429], [9187094, 1343], [9186888, 1343], [9186888, 1429]]]
        }
      }, {
        "attributes": {"FID": 7, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186554, 1697], [9186648, 1697], [9186648, 1621], [9186554, 1621], [9186554, 1697]]]
        }
      }, {
        "attributes": {"FID": 8, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186579, 1621], [9186648, 1621], [9186648, 1546], [9186579, 1546], [9186579, 1621]]]
        }
      }, {
        "attributes": {"FID": 9, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186621, 1716], [9186648, 1716], [9186648, 1697], [9186621, 1697], [9186621, 1716]]]
        }
      }, {
        "attributes": {"FID": 10, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186648, 1546], [9186557, 1546], [9186557, 1466], [9186648, 1466], [9186648, 1546]]]
        }
      }, {
        "attributes": {"FID": 11, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186557, 1466], [9186683, 1466], [9186683, 1415], [9186557, 1415], [9186557, 1466]]]
        }
      }, {
        "attributes": {"FID": 12, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187539, 1613], [9187566, 1613], [9187566, 1596], [9187539, 1596], [9187539, 1613]]]
        }
      }, {
        "attributes": {"FID": 13, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187586, 1641], [9187613, 1641], [9187613, 1624], [9187586, 1624], [9187586, 1641]]]
        }
      }, {
        "attributes": {"FID": 14, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187646, 1677], [9187673, 1677], [9187673, 1660], [9187646, 1660], [9187646, 1677]]]
        }
      }, {
        "attributes": {"FID": 15, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187055, 1545], [9187081, 1545], [9187081, 1530], [9187055, 1530], [9187055, 1545]]]
        }
      }, {
        "attributes": {"FID": 16, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187743, 1795], [9187769, 1795], [9187769, 1775], [9187743, 1775], [9187743, 1795]]]
        }
      }, {
        "attributes": {"FID": 17, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186508, 1707], [9186699, 1707], [9186699, 1408], [9186508, 1408], [9186508, 1707]]]
        }
      }, {
        "attributes": {"FID": 18, "性质": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186593, 1282], [9186493, 1282], [9186493, 1339], [9186593, 1339], [9186593, 1282]]]
        }
      }, {
        "attributes": {"FID": 19, "性质": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187799, 1806], [9187868, 1806], [9187868, 1854], [9187799, 1854], [9187799, 1806]]]
        }
      }, {
        "attributes": {"FID": 20, "性质": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186999, 1691], [9186858, 1691], [9186858, 1599], [9186999, 1599], [9186999, 1691]]]
        }
      }, {
        "attributes": {"FID": 21, "性质": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187557, 1557], [9187632, 1557], [9187632, 1496], [9187557, 1496], [9187557, 1557]]]
        }
      }, {
        "attributes": {"FID": 22, "性质": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187361, 1539], [9187422, 1539], [9187422, 1567], [9187361, 1567], [9187361, 1539]]]
        }
      }, {
        "attributes": {"FID": 23, "性质": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187440, 1707], [9187500, 1707], [9187500, 1738], [9187440, 1738], [9187440, 1707]]]
        }
      }, {
        "attributes": {"FID": 24, "性质": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187358, 1691], [9187435, 1691], [9187435, 1748], [9187358, 1748], [9187358, 1691]]]
        }
      }, {
        "attributes": {"FID": 25, "性质": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187169, 1724], [9187247, 1724], [9187247, 1668], [9187169, 1668], [9187169, 1724]]]
        }
      }, {
        "attributes": {"FID": 26, "性质": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187250, 1599], [9187169, 1599], [9187169, 1652], [9187250, 1652], [9187250, 1599]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 0,
      "name": "Lable",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186510,
        "ymin": 1331,
        "xmax": 9187811,
        "ymax": 1871,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/RedSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAAB3VJREFUeF7tmPlTlEcexnve94U5mANQbgQSbgiHXHINlxpRIBpRI6wHorLERUmIisKCQWM8cqigESVQS1Kx1piNi4mW2YpbcZONrilE140RCTcy3DDAcL/zbJP8CYPDL+9Ufau7uqb7eZ7P+/a8PS8hwkcgIBAQCAgEBAICAYGAQEAgIBAQCAgEBAICAYGAQEAgIBAQCDx/AoowKXFMUhD3lQrioZaQRVRS+fxl51eBTZUTdZ41U1Rox13/0JF9csGJ05Qv4jSz/YPWohtvLmSKN5iTGGqTm1+rc6weICOBRbZs1UVnrv87T1PUeovxyNsUP9P6n5cpHtCxu24cbrmwKLdj+osWiqrVKhI0xzbmZ7m1SpJ+1pFpvE2DPvGTomOxAoNLLKGLscZYvB10cbYYjrJCb7A5mrxleOBqim+cWJRakZY0JfnD/LieI9V1MrKtwokbrAtU4Vm0A3TJnphJD4B+RxD0u0LA7w7FTE4oprOCMbklEGNrfdGf4IqnQTb4wc0MFTYibZqM7JgjO8ZdJkpMln/sKu16pHZGb7IfptIWg389DPp9kcChWODoMuDdBOhL1JgpisbUvghM7AqFbtNiaFP80RLnhbuBdqi0N+1dbUpWGde9gWpuhFi95yL7sS7BA93JAb+Fn8mh4QujgPeTgb9kAZf3Apd2A+fXQ38yHjOHozB1IAJjOSEY2RSIwVUv4dd4X9wJccGHNrJ7CYQ4GGjLeNNfM+dyvgpzQstKf3pbB2A6m97uBRE0/Ergcxr8hyqg7hrwn0vAtRIKIRX6Y2pMl0RhIj8co9nBGFrvh55l3ngU7YObng7IVnFvGS+BYUpmHziY/Ls2zgP9SX50by/G9N5w6I+ogYvpwK1SoOlHQNsGfWcd9Peqof88B/rTyzF9hAIopAByQzC0JQB9ST5oVnvhnt+LOGsprvUhxNIwa0aY7cGR6Cp7tr8+whkjawIxkRWC6YJI6N+lAKq3Qf/Tx+B77oGfaQc/8hB8w2Xwtw9Bf3kzZspXY/JIDEbfpAB2BKLvVV90Jvjgoac9vpRxE8kciTVCBMMkNirJ7k/tRHyjtxwjKV4Yp3t/6s+R4E+/DH3N6+BrS8E314Dvvg2+/Sb4hxfBf5sP/up2TF3ZhonK1zD6dhwGdwail26DzqgX8MRKiq9ZBpkSkmeYOyPM3m9Jjl+1Z9D8AgNtlAq6bZ70qsZi+q+bwV/7I/hbB8D/dAr8Axq89iz474p/G5++koHJy1sx/lkGdBc2YjA3HF0rHNHuboomuQj/5DgclIvOGCGCYRKFFuTMV7YUAD3VDQaLMfyqBcZORGPy01QKYSNm/rYV/Nd/Av9NHvgbueBrsjDzRQamKKDxT9Kgq1iLkbIUDOSHoiNcgnYHgnYZi+9ZExSbiSoMc2eE2flKcuJLa4KGRQz6/U0wlGaP0feiMH4uFpMXEjBVlYjp6lWY+SSZtim0kulYMiYuJEJXuhTDJ9UYPByOvoIwdCxfgE4bAo0Jh39xLAoVpMwIEQyTyFCQvGpLon9sJ0K3J4OBDDcMH1dj9FQsxkrjMPFRPCbOx2GyfLal9VEcxstioTulxjAFNfROJPqLl6Bnfyg6V7ugz5yBhuHwrZjBdiU5YJg7I8wOpifAKoVIW7uQ3rpOBH2b3ekVjYT2WCRG3o+mIGKgO0OrlIaebU/HYOQDNbQnojB4NJyGD0NPfjA0bwTRE6Q7hsUcWhkWN8yZqSQlWWGECAZLmJfJmbrvVSI8taK37xpbdB/wQW8xPee/8xIGjvlj8IQ/hk4G0JbWcX8MHPVDX4kveoq8ocn3xLM33NCZRcPHOGJYZIKfpQyq7JjHS6yJjcHujLHADgkpuC7h8F8zEVqXSNC2awE69lqhs8AamkO26HrbDt2H7dBVQov2NcW26CiwQtu+BWjdY4n2nZboTbfCmKcCnRyDO/YmyLPnDlHvjDH8G6zhS9/wlEnYR7X00fWrFYuWdVI0ZpuhcbcczW/R2qdAcz6t/bRov4mONeaaoYl+p22rHF0bVNAmKtBvweIXGxNcfFH8eNlC4m6wMWMusEnKpn5hyo48pj9gLe4SNG9QoGGLAk8z5XiaJUd99u8122/IpBA2K9BGg2vWWKAvRYVeLzEa7E1R422m2+MsSTem97nSYnfKyN6/mzATv7AUgqcMrUnmaFlLX3ysM0fj+t/b5lQLtK22QEfyAmiSLKFZpUJ7kBRPXKW4HqCYynWVHKSG2LkyZex1uO1mZM9lKem9Tx9jjY5iNEYo0bKMhn7ZAu0r6H5PpLXCAq0rKJClSjSGynE/QIkrQYqBPe6S2X+AJsY2Ped6iWZk6RlL0c2r5szofRsO9R5S1IfQLRCpQL1aifoYFerpsbkuTImaUJXuXIDiH6/Ys8vm3Mg8L2i20YqsO7fItKLcSXyn0kXccclVqv3MS6at9JU/Ox+ouns+SF6Z4cSupz7l8+z1ucs7LF1AQjOdxfGZzmx8Iu1TRcfnrioICAQEAgIBgYBAQCAgEBAICAQEAgIBgYBAQCAgEBAICAQEAv8H44b/6ZiGvGAAAAAASUVORK5CYII=",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": true,
      "allowUpdateWithoutMValues": true,
      "hasZ": true,
      "enableZDefaults": true,
      "zDefault": 0,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NAME",
        "type": "esriFieldTypeString",
        "alias": "NAME",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {"attributes": {"NAME": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "NAME": "克拉玛依市区"},
        "geometry": {"x": 9186510, "y": 1724, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 1, "NAME": "石化区"},
        "geometry": {"x": 9187129, "y": 1368, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 2, "NAME": "白碱滩区"},
        "geometry": {"x": 9187177, "y": 1742, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 3, "NAME": "上环"},
        "geometry": {"x": 9186523, "y": 1660, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 4, "NAME": "中环"},
        "geometry": {"x": 9186535, "y": 1588, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 5, "NAME": "下环"},
        "geometry": {"x": 9186524, "y": 1508, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 6, "NAME": "自流环"},
        "geometry": {"x": 9186530, "y": 1434, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 7, "NAME": "红浅"},
        "geometry": {"x": 9186524, "y": 1331, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 8, "NAME": "末端泵站"},
        "geometry": {"x": 9186849, "y": 1476, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 9, "NAME": "三坪"},
        "geometry": {"x": 9186872, "y": 1676, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 10, "NAME": "五化"},
        "geometry": {"x": 9186934, "y": 1676, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 11, "NAME": "阀门室"},
        "geometry": {"x": 9186900, "y": 1566, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 12, "NAME": "阀门室"},
        "geometry": {"x": 9186976, "y": 1547, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 13, "NAME": "金龙镇"},
        "geometry": {"x": 9186926, "y": 1362, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 14, "NAME": "石化园区"},
        "geometry": {"x": 9186995, "y": 1360, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 15, "NAME": "三坪镇"},
        "geometry": {"x": 9187068, "y": 1362, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 16, "NAME": "技校三厂"},
        "geometry": {"x": 9187062, "y": 1567, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 17, "NAME": "一化"},
        "geometry": {"x": 9187190, "y": 1678, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 18, "NAME": "白碱滩"},
        "geometry": {"x": 9187141, "y": 1624, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 19, "NAME": "管汇间"},
        "geometry": {"x": 9187181, "y": 1566, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 20, "NAME": "二化、三化"},
        "geometry": {"x": 9187364, "y": 1762, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 21, "NAME": "扬水泵站"},
        "geometry": {"x": 9187486, "y": 1690, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 22, "NAME": "输水首站"},
        "geometry": {"x": 9187427, "y": 1582, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 23, "NAME": "三坪水库"},
        "geometry": {"x": 9186915, "y": 1758, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 24, "NAME": "调节水库"},
        "geometry": {"x": 9187429, "y": 1791, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 25, "NAME": "黄羊泉水库"},
        "geometry": {"x": 9187617, "y": 1803, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 26, "NAME": "五厂"},
        "geometry": {"x": 9187628, "y": 1614, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 27, "NAME": "百联站"},
        "geometry": {"x": 9187685, "y": 1643, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 28, "NAME": "水源管理站"},
        "geometry": {"x": 9187582, "y": 1576, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 29, "NAME": "重油5000方"},
        "geometry": {"x": 9187674, "y": 1534, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 30, "NAME": "六化"},
        "geometry": {"x": 9187614, "y": 1737, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 31, "NAME": "乌尔禾（137团、风城作业区）"},
        "geometry": {"x": 9187809, "y": 1755, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 32, "NAME": "魔鬼城、稠油处理站"},
        "geometry": {"x": 9187811, "y": 1871, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 33, "NAME": "山上水库"},
        "geometry": {"x": 9186621, "y": 1728, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 3,
      "name": "Line",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186531,
        "ymin": 1314,
        "xmax": 9187799,
        "ymax": 1812,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [241, 152, 60, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": true,
      "allowUpdateWithoutMValues": true,
      "hasZ": true,
      "enableZDefaults": true,
      "zDefault": 0,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管径",
        "type": "esriFieldTypeString",
        "alias": "管径",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管材",
        "type": "esriFieldTypeString",
        "alias": "管材",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "FLOW",
        "type": "esriFieldTypeString",
        "alias": "FLOW",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"管径": null, "管材": null, "FLOW": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "管径": "dn630", "管材": "内衬PE", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187012, 1712], [9187170, 1712]]]
        }
      }, {
        "attributes": {"FID": 1, "管径": "dn630", "管材": "球墨", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186648, 1703], [9186942, 1703]]]
        }
      }, {
        "attributes": {"FID": 2, "管径": "dn600", "管材": " ", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186942, 1703], [9186942, 1691]]]
        }
      }, {
        "attributes": {"FID": 3, "管径": "dn600", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187012, 1621], [9187012, 1712]]]
        }
      }, {
        "attributes": {"FID": 4, "管径": "dn1000", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186672, 1678], [9186672, 1466]]]
        }
      }, {
        "attributes": {"FID": 5, "管径": "dn630", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186792, 1481], [9186931, 1559]]]
        }
      }, {
        "attributes": {"FID": 6, "管径": "dn630", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186797, 1473], [9186931, 1548]]]
        }
      }, {
        "attributes": {"FID": 7, "管径": "dn500", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186792, 1471], [9186888, 1417]]]
        }
      }, {
        "attributes": {"FID": 8, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186791, 1460], [9186888, 1405]]]
        }
      }, {
        "attributes": {"FID": 9, "管径": "dn800", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187341, 1712], [9187341, 1568]]]
        }
      }, {
        "attributes": {"FID": 10, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186713, 1434], [9186683, 1418]]]
        }
      }, {
        "attributes": {"FID": 11, "管径": "dn500", "管材": "球墨", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186760, 1408], [9186593, 1314]]]
        }
      }, {
        "attributes": {"FID": 12, "管径": "dn500", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186648, 1549], [9186782, 1474]]]
        }
      }, {
        "attributes": {"FID": 13, "管径": "dn700", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186922, 1560], [9186786, 1484]]]
        }
      }, {
        "attributes": {"FID": 14, "管径": "dn700", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186786, 1484], [9186648, 1484]]]
        }
      }, {
        "attributes": {"FID": 15, "管径": "dn400", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186782, 1473], [9186648, 1473]]]
        }
      }, {
        "attributes": {"FID": 16, "管径": "dn800", "管材": "球墨", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187380, 1531], [9187557, 1531]]]
        }
      }, {
        "attributes": {"FID": 17, "管径": "dn800", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187380, 1539], [9187380, 1531]]]
        }
      }, {
        "attributes": {"FID": 18, "管径": "dn800", "管材": "球墨", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187341, 1624], [9187462, 1624]]]
        }
      }, {
        "attributes": {"FID": 19, "管径": "dn800", "管材": "球墨", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187574, 1687], [9187625, 1715]]]
        }
      }, {
        "attributes": {"FID": 20, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187119, 1487], [9187014, 1429]]]
        }
      }, {
        "attributes": {"FID": 21, "管径": "dn700", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187247, 1702], [9187326, 1702]]]
        }
      }, {
        "attributes": {"FID": 22, "管径": "dn529", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187247, 1689], [9187286, 1689]]]
        }
      }, {
        "attributes": {"FID": 23, "管径": "dn700", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187224, 1553], [9187361, 1553]]]
        }
      }, {
        "attributes": {"FID": 24, "管径": "dn400", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187224, 1545], [9187361, 1545]]]
        }
      }, {
        "attributes": {"FID": 25, "管径": "dn600", "管材": "球墨", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187361, 1545], [9187095, 1401]]]
        }
      }, {
        "attributes": {"FID": 26, "管径": "dn400", "管材": " ", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186990, 1581], [9187055, 1545]]]
        }
      }, {
        "attributes": {"FID": 27, "管径": "dn500", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187492, 1640], [9187539, 1613]]]
        }
      }, {
        "attributes": {"FID": 28, "管径": "dn500", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187540, 1667], [9187586, 1641]]]
        }
      }, {
        "attributes": {"FID": 29, "管径": "dn500", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187602, 1702], [9187646, 1677]]]
        }
      }, {
        "attributes": {"FID": 30, "管径": "dn700", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186922, 1598], [9186922, 1560]]]
        }
      }, {
        "attributes": {"FID": 31, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186931, 1598], [9186931, 1559]]]
        }
      }, {
        "attributes": {"FID": 32, "管径": "dn800", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186858, 1601], [9186648, 1601]]]
        }
      }, {
        "attributes": {"FID": 33, "管径": "dn800", "管材": "玻璃钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186858, 1616], [9186648, 1616]]]
        }
      }, {
        "attributes": {"FID": 34, "管径": "dn1000", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186858, 1678], [9186672, 1678]]]
        }
      }, {
        "attributes": {"FID": 35, "管径": "dn600", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187000, 1621], [9187012, 1621]]]
        }
      }, {
        "attributes": {"FID": 36, "管径": "dn1000", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186942, 1598], [9186942, 1564]]]
        }
      }, {
        "attributes": {"FID": 37, "管径": "dn800", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187654, 1731], [9187743, 1781]]]
        }
      }, {
        "attributes": {"FID": 38, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187326, 1702], [9187326, 1568]]]
        }
      }, {
        "attributes": {"FID": 39, "管径": "dn800", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187654, 1731], [9187743, 1781]]]
        }
      }, {
        "attributes": {"FID": 40, "管径": "dn600", "管材": "球墨", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187769, 1795], [9187799, 1812]]]
        }
      }, {
        "attributes": {"FID": 41, "管径": "dn600", "管材": "球墨", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187769, 1795], [9187784, 1804]]]
        }
      }, {
        "attributes": {"FID": 42, "管径": "dn150", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187139, 1497], [9187204, 1461]]]
        }
      }, {
        "attributes": {"FID": 43, "管径": "dn630", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186931, 1559], [9186942, 1564]]]
        }
      }, {
        "attributes": {"FID": 44, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186931, 1559], [9186931, 1548]]]
        }
      }, {
        "attributes": {"FID": 45, "管径": "dn630", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186942, 1564], [9187169, 1691]]]
        }
      }, {
        "attributes": {"FID": 46, "管径": "dn1000", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186942, 1564], [9186942, 1554]]]
        }
      }, {
        "attributes": {"FID": 47, "管径": "dn630", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186931, 1548], [9186942, 1554]]]
        }
      }, {
        "attributes": {"FID": 48, "管径": "dn630", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186942, 1554], [9187169, 1680]]]
        }
      }, {
        "attributes": {"FID": 49, "管径": "dn1000", "管材": "铸铁", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186942, 1554], [9186942, 1429]]]
        }
      }, {
        "attributes": {"FID": 50, "管径": "dn500", "管材": "球墨", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186784, 1421], [9186784, 1471]]]
        }
      }, {
        "attributes": {"FID": 51, "管径": "dn600", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186791, 1460], [9186791, 1471]]]
        }
      }, {
        "attributes": {"FID": 52, "管径": "dn630", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186792, 1473], [9186797, 1473]]]
        }
      }, {
        "attributes": {"FID": 53, "管径": "dn630", "管材": "内衬PE", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187341, 1712], [9187358, 1730]]]
        }
      }, {
        "attributes": {"FID": 54, "管径": "dn700", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187348, 1702], [9187358, 1712]]]
        }
      }, {
        "attributes": {"FID": 55, "管径": "dn400", "管材": " ", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187081, 1530], [9187139, 1497]]]
        }
      }, {
        "attributes": {"FID": 56, "管径": "dn500", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187216, 1540], [9187119, 1487]]]
        }
      }, {
        "attributes": {"FID": 57, "管径": "dn800", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187341, 1531], [9187380, 1531]]]
        }
      }, {
        "attributes": {"FID": 58, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187326, 1712], [9187326, 1702]]]
        }
      }, {
        "attributes": {"FID": 59, "管径": "dn630", "管材": "内衬PE", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187326, 1712], [9187341, 1712]]]
        }
      }, {
        "attributes": {"FID": 60, "管径": "dn700", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187326, 1702], [9187348, 1702]]]
        }
      }, {
        "attributes": {"FID": 61, "管径": "dn630", "管材": "钢", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187286, 1689], [9187326, 1689]]]
        }
      }, {
        "attributes": {"FID": 62, "管径": "dn800", "管材": "球墨", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187462, 1624], [9187496, 1643]]]
        }
      }, {
        "attributes": {"FID": 63, "管径": "dn800", "管材": "球墨", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187496, 1643], [9187534, 1664]]]
        }
      }, {
        "attributes": {"FID": 64, "管径": "dn800", "管材": "球墨", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187534, 1664], [9187574, 1687]]]
        }
      }, {
        "attributes": {"FID": 65, "管径": "700", "管材": " ", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187326, 1568], [9187341, 1568]]]
        }
      }, {
        "attributes": {"FID": 66, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187326, 1568], [9187326, 1553]]]
        }
      }, {
        "attributes": {"FID": 67, "管径": "dn250", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186723, 1440], [9186761, 1418]]]
        }
      }, {
        "attributes": {"FID": 68, "管径": "dn250", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186713, 1434], [9186751, 1413]]]
        }
      }, {
        "attributes": {"FID": 69, "管径": "dn250", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186751, 1413], [9186760, 1408]]]
        }
      }, {
        "attributes": {"FID": 70, "管径": "dn250", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186761, 1418], [9186770, 1413]]]
        }
      }, {
        "attributes": {"FID": 71, "管径": "dn400", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186766, 1427], [9186780, 1419]]]
        }
      }, {
        "attributes": {"FID": 72, "管径": "dn400", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186733, 1446], [9186766, 1427]]]
        }
      }, {
        "attributes": {"FID": 73, "管径": "dn800", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187341, 1568], [9187341, 1531]]]
        }
      }, {
        "attributes": {"FID": 74, "管径": "dn500", "管材": "球墨", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186784, 1421], [9186780, 1419]]]
        }
      }, {
        "attributes": {"FID": 75, "管径": "dn500", "管材": "球墨", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186780, 1419], [9186770, 1413]]]
        }
      }, {
        "attributes": {"FID": 76, "管径": "dn500", "管材": "球墨", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186770, 1413], [9186760, 1408]]]
        }
      }, {
        "attributes": {"FID": 77, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186782, 1473], [9186733, 1446]]]
        }
      }, {
        "attributes": {"FID": 78, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186733, 1446], [9186723, 1440]]]
        }
      }, {
        "attributes": {"FID": 79, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186723, 1440], [9186713, 1434]]]
        }
      }, {
        "attributes": {"FID": 80, "管径": "dn630", "管材": "内衬PE", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186942, 1712], [9186982, 1712]]]
        }
      }, {
        "attributes": {"FID": 81, "管径": "dn630", "管材": "内衬PE", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186648, 1712], [9186942, 1712]]]
        }
      }, {
        "attributes": {"FID": 82, "管径": "dn600", "管材": " ", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186942, 1712], [9186942, 1703]]]
        }
      }, {
        "attributes": {"FID": 83, "管径": "dn600", "管材": "钢", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186660, 1408], [9186531, 1338]]]
        }
      }, {
        "attributes": {"FID": 84, "管径": "dn630", "管材": "内衬PE", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9186982, 1712], [9187012, 1712]]]
        }
      }, {
        "attributes": {"FID": 85, "管径": "dn630", "管材": "内衬PE", "FLOW": "反向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187247, 1712], [9187326, 1712]]]
        }
      }, {
        "attributes": {"FID": 86, "管径": "dn600", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187208, 1668], [9187208, 1652]]]
        }
      }, {
        "attributes": {"FID": 87, "管径": "dn600", "管材": " ", "FLOW": "正向"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9187208, 1599], [9187208, 1559]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 1,
      "name": "Point",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9186548,
        "ymin": 1305,
        "xmax": 9187829,
        "ymax": 1835,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/OrangeSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAABv9JREFUeF7tl31UVVUaxk/34uXCBeTjqqXQoA1oGgqOOprlFwKKOpqCCq4IyEJL1CEmyxUijkjDOBAGFgM6qSz8KPEbCQQFP2JQhmwmhmlVSwvTIbgIJOLl7vPMc8U/5v+Ll382az1rn7PP3ft9n99+9z4HRZF/koAkIAlIApKAJCAJSAKSgCQgCUgCkoAkIAlIApKAJCAJPH4CrqtHKN7bntX5pY7R+a8drvgwpNvjDzuwEbTpzyovHgvRp5ye73Tm6jJDU9Mq19tWXVtuaDoRbigtCXXekhGgzGCaDgObaj9HXzxYCawId9r7nxiXNvzBA0jxgiXVC72PZEkxAu8NAZKN+PZl97byeYZ9q55SJvZzGgMzXepYJboxyvCd+rYH1G1G9GQMg/kDb6i7R0L9+Jk+5Y5Cb9av0LN9OLDlKYikofhmpduNnRM0qwYm636K+sexStx3cW7tSPOGOcsPlsJA4OBvgU+nAiXUsenUi8BR6hBVNB0ifxLMmeNYEb5ojjV2ZARoVvdTOvad5tWRSsj38cY7oBlzPk0X0+zRF4CTNFo6GygPAT6fC5ydyftZwKlg4Dh1ZA6wbxbMufxt+gT8FDvs53f8lHD7Zm9jtHkuypC6COMV7PwNHuQ/32e+hMbPUFWLgNoEoD4ZuJoI1EQCFTRdRihnCOQEwRxhu28OHuSyL30K6pcZ62I8lRE2pmW/4e8HOa3r2MyVz6PxA1bzM4DTNF8Z3mf8273AjyeB74uBr9IIYRkrgpVQxgooDWUlhHGrsN1DCNmz0fP7Mdg9Wf+W/RzYFslwPdL9ssicDFFI84dp/iRLvIztpWigaRfQegW4/wPUuw1Qb+yDWr+OlUHD1io4ywo4NZ/bhbCKwtCbx+rYEoSr4Yb6UDfF07bU7DB6povyQnO8Vxv+MgHqfpo+ai3tRwBq42i4EKKzDkL8CNFzHaLlMMQ3qRDXYqBeXsrtsIDVMo9VwPYgIewJgznlObRFOPYk+ioz7WDBthBZ4x0Sb672EmqGL7Cf+/8493PpHKjn50NcXwtxcxdE6wlCqIZoL4No/itE0yaIL1+lXoOoI6TKCI7jWXFkMVAQDMvb3miN0GLHWCXJtuzsMPpssNP7pjeHQk3nOz3fj6+8aVzVcIj6WIivEmj2XYgb2X3Gb+Zy9bewfw1EQxz1OsQ/3iSENVBLeTgWEV7OeFiSvHA3UouDU5QcO1iwLcT5UOeczsRhUHd4A3kjeQiO4erPY4lHUdE0SRD/osmvk6gNrApCsZqvp67FP6wAURsL9RwPxr1TgJ2jITa4o2O5BiXTlQLbsrPD6PJgpz+1v0EA232AD0dBPTwJonoBxGVWwRXqC17XLoL4+++oxdQS6iX2U1eoi7yvZlsVCbWIZ8eOZyDWOOHuCg0OPf/Eh3awYFuIvCBN0s04o4o0AsgmgJKZNERTF2n40kKC4FnwUGGPxOqw3l/kM+tvapZCnI+EqFwJ9RDfDClPojdOi9aVDtgZ6PCObdnZYfQiTyXkhyi3DmweAWTye59vAXGeK1rN1X4IgatvBXGJlfBQ1mv21fB5Nc1fiKD55RDlBHCA3wYbnfEgRoO2aEfzhtHaMDtYsDmE+78X6hp61xkh0nwhiqfCfC4E5iqqei7MNcEwX/w/We9r2F8dCjPPCnPVApgrFsLMT2NLrj8sq7Xojdfh66WOjYtGGobZnJ09JsgNUDZ3Relh3jgEvR+NRnfpRHSXT0J35WR0X2BrVfUjXbD2TUF31VQ+n4buCupkILqLfWHe6oKe+EGwJDjjk1m6VOausUf+NscI9VB8GoKVf+IVR9zfZMQvxaPQeXocOs9OQGd5IDorgtBZSZ2b2KcKqpwq5fOSX6PzwFB05bngfqIOWGfAlyv1jbGjFD+bE7PnBBnjlGV3Fj7RpcYMQud7g2Ha/zRMn46G6VgATKcCYToTBNPpiRSvj7PvM3+YinxgKvSEabcrxzhDrDegda3TvYLZmmh75t5fsbRZ45Xklpc0PXhZg/ZNzvhvrhEthcPR8ren0bLft0+f+LDvSbTke6Elzx13dg1G+1Y3iGQXmDY4mwvCHN5lQtr+Ssre8zhkB2rWNy9x+NkS44DujXrc3uaK5j+741a2B259QOVQvG7O8sDtTA/cSxsMy2ZX3FqvN+0J1Vj/Axxk76T7Pd4b/kpw7fxBZc0rHH9RE5ygbnJF11Zui+0eMO3wRFeGJ0S6O9/5LvjpLed7dfG68uQAZW6/JzLAExq2BWgjGpboCupX6L+oj3JsbozRdzTGOXU0xOlvXU3Q115/Xbcnc5o2knm6DHCujz38iNf8lck5M7SzrVrLa0b0fuxRZQBJQBKQBCQBSUASkAQkAUlAEpAEJAFJQBKQBCQBSUAS+B/qhLD6M6wGWAAAAABJRU5ErkJggg==",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": true,
      "allowUpdateWithoutMValues": true,
      "hasZ": true,
      "enableZDefaults": true,
      "zDefault": 0,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NAME",
        "type": "esriFieldTypeString",
        "alias": "NAME",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "icon",
        "type": "esriFieldTypeString",
        "alias": "icon",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "type",
        "type": "esriFieldTypeString",
        "alias": "type",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "isOnClick",
        "type": "esriFieldTypeString",
        "alias": "isOnClick",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "width",
        "type": "esriFieldTypeString",
        "alias": "width",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "height",
        "type": "esriFieldTypeString",
        "alias": "height",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "dataID",
        "type": "esriFieldTypeString",
        "alias": "dataID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "mxdID",
        "type": "esriFieldTypeString",
        "alias": "mxdID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {
          "attributes": {
            "NAME": null,
            "icon": null,
            "type": null,
            "isOnClick": null,
            "width": null,
            "height": null,
            "dataID": null,
            "mxdID": null
          }
        }
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {
          "FID": 0,
          "NAME": "三坪水库",
          "icon": "901三坪水库",
          "type": "3",
          "isOnClick": "否",
          "width": "52.42",
          "height": "37.50",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9186992, "y": 1762, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 1,
          "NAME": "调节水库",
          "icon": "902调节水库",
          "type": "3",
          "isOnClick": "否",
          "width": "23.85",
          "height": "33.75",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187480, "y": 1781, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 2,
          "NAME": "黄羊泉水库",
          "icon": "903黄羊泉水库",
          "type": "3",
          "isOnClick": "否",
          "width": "27.11",
          "height": "26.25",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187636, "y": 1765, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 3,
          "NAME": "井盖",
          "icon": "201管汇间",
          "type": "4",
          "isOnClick": "是",
          "width": "18.62",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "阀门室1"
        }, "geometry": {"x": 9186926, "y": 1552, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 4,
          "NAME": "井盖",
          "icon": "201管汇间",
          "type": "4",
          "isOnClick": "是",
          "width": "18.62",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "阀门室2"
        }, "geometry": {"x": 9186947, "y": 1564, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 5,
          "NAME": "管汇间",
          "icon": "201管汇间",
          "type": "4",
          "isOnClick": "是",
          "width": "18.62",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "详情页-白碱滩管汇间"
        }, "geometry": {"x": 9187208, "y": 1551, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 6,
          "NAME": "二化",
          "icon": "101水厂",
          "type": "1",
          "isOnClick": "是",
          "width": "30.00",
          "height": "22.50",
          "dataID": " ",
          "mxdID": "水厂详情页-第二水厂"
        }, "geometry": {"x": 9187395, "y": 1724, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 7,
          "NAME": "扬水泵站",
          "icon": "泵站",
          "type": "10",
          "isOnClick": "是",
          "width": "23.30",
          "height": "9.00",
          "dataID": " ",
          "mxdID": "水厂详情页-第二水厂"
        }, "geometry": {"x": 9187469, "y": 1724, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 8,
          "NAME": "输水首战",
          "icon": "泵站",
          "type": "10",
          "isOnClick": "是",
          "width": "23.30",
          "height": "9.00",
          "dataID": " ",
          "mxdID": "水厂详情页-第二水厂"
        }, "geometry": {"x": 9187393, "y": 1554, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 9,
          "NAME": "重油5000方",
          "icon": "002用水区—石化园区",
          "type": "8",
          "isOnClick": "否",
          "width": "27.25",
          "height": "18.75",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187593, "y": 1531, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 10,
          "NAME": "水源管理站",
          "icon": "1101水源管理站",
          "type": "10",
          "isOnClick": "否",
          "width": "15.86",
          "height": "18.75",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187554, "y": 1605, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 11,
          "NAME": "五厂",
          "icon": "007用水区",
          "type": "10",
          "isOnClick": "否",
          "width": "26.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187602, "y": 1632, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 12,
          "NAME": "百联站",
          "icon": "007用水区",
          "type": "10",
          "isOnClick": "否",
          "width": "26.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187664, "y": 1667, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 13,
          "NAME": "六化",
          "icon": "101水厂",
          "type": "1",
          "isOnClick": "是",
          "width": "30.00",
          "height": "22.50",
          "dataID": "(SCQ-008)+(SCQ-009)",
          "mxdID": "水厂详情页-第六水厂"
        }, "geometry": {"x": 9187638, "y": 1726, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 14,
          "NAME": "五化",
          "icon": "101水厂",
          "type": "1",
          "isOnClick": "是",
          "width": "30.00",
          "height": "22.50",
          "dataID": "(SCQ-001)+(SCQ-002)+(SCQ-003)+(SCQ-004)",
          "mxdID": "水厂详情页-三坪五化"
        }, "geometry": {"x": 9186960, "y": 1639, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 15,
          "NAME": "金龙镇（输炼地区）",
          "icon": "001用水区—输炼地区",
          "type": "9",
          "isOnClick": "否",
          "width": "36.06",
          "height": "15.00",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9186923, "y": 1388, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 16,
          "NAME": "石化园区",
          "icon": "002用水区—石化园区",
          "type": "8",
          "isOnClick": "否",
          "width": "27.25",
          "height": "18.75",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9186995, "y": 1404, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 17,
          "NAME": "三坪镇",
          "icon": "007用水区",
          "type": "10",
          "isOnClick": "否",
          "width": "26.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187067, "y": 1394, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 18,
          "NAME": "红浅",
          "icon": "007用水区",
          "type": "10",
          "isOnClick": "否",
          "width": "26.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9186548, "y": 1305, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 19,
          "NAME": "自流环",
          "icon": "003用水区—自流环",
          "type": "10",
          "isOnClick": "是",
          "width": "46.42",
          "height": "30.00",
          "dataID": " ",
          "mxdID": "自流环"
        }, "geometry": {"x": 9186621, "y": 1441, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 20,
          "NAME": "下环",
          "icon": "005用水区—下环",
          "type": "10",
          "isOnClick": "是",
          "width": "34.76",
          "height": "22.50",
          "dataID": " ",
          "mxdID": "下环"
        }, "geometry": {"x": 9186602, "y": 1512, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 21,
          "NAME": "中环",
          "icon": "006用水区—中环",
          "type": "10",
          "isOnClick": "是",
          "width": "29.66",
          "height": "18.75",
          "dataID": " ",
          "mxdID": "中环"
        }, "geometry": {"x": 9186616, "y": 1586, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 22,
          "NAME": "上环",
          "icon": "003用水区—上环",
          "type": "10",
          "isOnClick": "是",
          "width": "34.82",
          "height": "22.50",
          "dataID": " ",
          "mxdID": "上环"
        }, "geometry": {"x": 9186601, "y": 1662, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 23,
          "NAME": "山上水库",
          "icon": "701山上水库",
          "type": "3",
          "isOnClick": "是",
          "width": "14.37",
          "height": "15.00",
          "dataID": " ",
          "mxdID": "山上水库"
        }, "geometry": {"x": 9186634, "y": 1707, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 24,
          "NAME": "末端泵站",
          "icon": "泵站",
          "type": "10",
          "isOnClick": "否",
          "width": "23.30",
          "height": "9.00",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9186787, "y": 1475, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 25,
          "NAME": "技校三厂",
          "icon": "007用水区",
          "type": "10",
          "isOnClick": "否",
          "width": "26.96",
          "height": "15.00",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187068, "y": 1537, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 26,
          "NAME": "一化",
          "icon": "101水厂",
          "type": "1",
          "isOnClick": "否",
          "width": "30.00",
          "height": "22.50",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187207, "y": 1699, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 27,
          "NAME": "白碱滩",
          "icon": "004用水区—白碱滩",
          "type": "10",
          "isOnClick": "否",
          "width": "34.61",
          "height": "26.25",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187212, "y": 1628, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 28,
          "NAME": "乌尔禾（137团、风城作业区）",
          "icon": "006用水区—乌尔禾",
          "type": "10",
          "isOnClick": "否",
          "width": "36.17",
          "height": "22.50",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187755, "y": 1790, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 29,
          "NAME": "魔鬼城、稠油水处理站",
          "icon": "002用水区—石化园区",
          "type": "10",
          "isOnClick": "否",
          "width": "27.25",
          "height": "18.75",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9187829, "y": 1835, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 30,
          "NAME": "分区流量计_末端泵站_1820853",
          "icon": "401流量计",
          "type": "6",
          "isOnClick": "是",
          "width": "6.06",
          "height": "7.50",
          "dataID": " ",
          "mxdID": " "
        }, "geometry": {"x": 9186797, "y": 1457, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 31,
          "NAME": "压力监测仪_首站_YL-010",
          "icon": "501压力表",
          "type": "7",
          "isOnClick": "是",
          "width": "7.50",
          "height": "7.50",
          "dataID": "YL-010",
          "mxdID": " "
        }, "geometry": {"x": 9187326, "y": 1628, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 32,
          "NAME": "三坪",
          "icon": "101水厂",
          "type": "1",
          "isOnClick": "是",
          "width": "30.00",
          "height": "22.50",
          "dataID": "(SCQ-005)+(SCQ-006)+(SCQ-007)",
          "mxdID": "水厂详情页-三坪五化"
        }, "geometry": {"x": 9186896, "y": 1638, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 33,
          "NAME": "压力监测仪_矿石陈列馆_YL-018",
          "icon": "501压力表",
          "type": "7",
          "isOnClick": "是",
          "width": "7.50",
          "height": "7.50",
          "dataID": "YL-018",
          "mxdID": " "
        }, "geometry": {"x": 9186670, "y": 1611, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }], "showLegend": true
}
export {shpJsonPage0ZongLan}
