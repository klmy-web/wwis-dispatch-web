const shpJsonPage12ShuiKu_1ShanShangShuiKu = {
  "layers": [{
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 2,
      "name": "Frame",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9189121,
        "ymin": 4370,
        "xmax": 9190155,
        "ymax": 4870,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [165, 83, 183, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "性质",
        "type": "esriFieldTypeString",
        "alias": "性质",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"性质": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189628, 4869], [9189733, 4869], [9189733, 4823], [9189628, 4823], [9189628, 4869]]]
        }
      }, {
        "attributes": {"FID": 1, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189628, 4783], [9189733, 4783], [9189733, 4737], [9189628, 4737], [9189628, 4783]]]
        }
      }, {
        "attributes": {"FID": 2, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189628, 4692], [9189733, 4692], [9189733, 4646], [9189628, 4646], [9189628, 4692]]]
        }
      }, {
        "attributes": {"FID": 3, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189830, 4870], [9189927, 4870], [9189927, 4647], [9189830, 4647], [9189830, 4870]]]
        }
      }, {
        "attributes": {"FID": 4, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189121, 4651], [9189610, 4652], [9189610, 4442], [9189121, 4442], [9189121, 4651]]]
        }
      }, {
        "attributes": {"FID": 5, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189934, 4651], [9190155, 4651], [9190155, 4370], [9189934, 4370], [9189934, 4651]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 0,
      "name": "Lable",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9189148,
        "ymin": 4352,
        "xmax": 9190305,
        "ymax": 4427,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/RedSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAAB3VJREFUeF7tmPlTlEcexnve94U5mANQbgQSbgiHXHINlxpRIBpRI6wHorLERUmIisKCQWM8cqigESVQS1Kx1piNi4mW2YpbcZONrilE140RCTcy3DDAcL/zbJP8CYPDL+9Ufau7uqb7eZ7P+/a8PS8hwkcgIBAQCAgEBAICAYGAQEAgIBAQCAgEBAICAYGAQEAgIBAQCDx/AoowKXFMUhD3lQrioZaQRVRS+fxl51eBTZUTdZ41U1Rox13/0JF9csGJ05Qv4jSz/YPWohtvLmSKN5iTGGqTm1+rc6weICOBRbZs1UVnrv87T1PUeovxyNsUP9P6n5cpHtCxu24cbrmwKLdj+osWiqrVKhI0xzbmZ7m1SpJ+1pFpvE2DPvGTomOxAoNLLKGLscZYvB10cbYYjrJCb7A5mrxleOBqim+cWJRakZY0JfnD/LieI9V1MrKtwokbrAtU4Vm0A3TJnphJD4B+RxD0u0LA7w7FTE4oprOCMbklEGNrfdGf4IqnQTb4wc0MFTYibZqM7JgjO8ZdJkpMln/sKu16pHZGb7IfptIWg389DPp9kcChWODoMuDdBOhL1JgpisbUvghM7AqFbtNiaFP80RLnhbuBdqi0N+1dbUpWGde9gWpuhFi95yL7sS7BA93JAb+Fn8mh4QujgPeTgb9kAZf3Apd2A+fXQ38yHjOHozB1IAJjOSEY2RSIwVUv4dd4X9wJccGHNrJ7CYQ4GGjLeNNfM+dyvgpzQstKf3pbB2A6m97uBRE0/Ergcxr8hyqg7hrwn0vAtRIKIRX6Y2pMl0RhIj8co9nBGFrvh55l3ngU7YObng7IVnFvGS+BYUpmHziY/Ls2zgP9SX50by/G9N5w6I+ogYvpwK1SoOlHQNsGfWcd9Peqof88B/rTyzF9hAIopAByQzC0JQB9ST5oVnvhnt+LOGsprvUhxNIwa0aY7cGR6Cp7tr8+whkjawIxkRWC6YJI6N+lAKq3Qf/Tx+B77oGfaQc/8hB8w2Xwtw9Bf3kzZspXY/JIDEbfpAB2BKLvVV90Jvjgoac9vpRxE8kciTVCBMMkNirJ7k/tRHyjtxwjKV4Yp3t/6s+R4E+/DH3N6+BrS8E314Dvvg2+/Sb4hxfBf5sP/up2TF3ZhonK1zD6dhwGdwail26DzqgX8MRKiq9ZBpkSkmeYOyPM3m9Jjl+1Z9D8AgNtlAq6bZ70qsZi+q+bwV/7I/hbB8D/dAr8Axq89iz474p/G5++koHJy1sx/lkGdBc2YjA3HF0rHNHuboomuQj/5DgclIvOGCGCYRKFFuTMV7YUAD3VDQaLMfyqBcZORGPy01QKYSNm/rYV/Nd/Av9NHvgbueBrsjDzRQamKKDxT9Kgq1iLkbIUDOSHoiNcgnYHgnYZi+9ZExSbiSoMc2eE2flKcuJLa4KGRQz6/U0wlGaP0feiMH4uFpMXEjBVlYjp6lWY+SSZtim0kulYMiYuJEJXuhTDJ9UYPByOvoIwdCxfgE4bAo0Jh39xLAoVpMwIEQyTyFCQvGpLon9sJ0K3J4OBDDcMH1dj9FQsxkrjMPFRPCbOx2GyfLal9VEcxstioTulxjAFNfROJPqLl6Bnfyg6V7ugz5yBhuHwrZjBdiU5YJg7I8wOpifAKoVIW7uQ3rpOBH2b3ekVjYT2WCRG3o+mIGKgO0OrlIaebU/HYOQDNbQnojB4NJyGD0NPfjA0bwTRE6Q7hsUcWhkWN8yZqSQlWWGECAZLmJfJmbrvVSI8taK37xpbdB/wQW8xPee/8xIGjvlj8IQ/hk4G0JbWcX8MHPVDX4kveoq8ocn3xLM33NCZRcPHOGJYZIKfpQyq7JjHS6yJjcHujLHADgkpuC7h8F8zEVqXSNC2awE69lqhs8AamkO26HrbDt2H7dBVQov2NcW26CiwQtu+BWjdY4n2nZboTbfCmKcCnRyDO/YmyLPnDlHvjDH8G6zhS9/wlEnYR7X00fWrFYuWdVI0ZpuhcbcczW/R2qdAcz6t/bRov4mONeaaoYl+p22rHF0bVNAmKtBvweIXGxNcfFH8eNlC4m6wMWMusEnKpn5hyo48pj9gLe4SNG9QoGGLAk8z5XiaJUd99u8122/IpBA2K9BGg2vWWKAvRYVeLzEa7E1R422m2+MsSTem97nSYnfKyN6/mzATv7AUgqcMrUnmaFlLX3ysM0fj+t/b5lQLtK22QEfyAmiSLKFZpUJ7kBRPXKW4HqCYynWVHKSG2LkyZex1uO1mZM9lKem9Tx9jjY5iNEYo0bKMhn7ZAu0r6H5PpLXCAq0rKJClSjSGynE/QIkrQYqBPe6S2X+AJsY2Ped6iWZk6RlL0c2r5szofRsO9R5S1IfQLRCpQL1aifoYFerpsbkuTImaUJXuXIDiH6/Ys8vm3Mg8L2i20YqsO7fItKLcSXyn0kXccclVqv3MS6at9JU/Ox+ouns+SF6Z4cSupz7l8+z1ucs7LF1AQjOdxfGZzmx8Iu1TRcfnrioICAQEAgIBgYBAQCAgEBAICAQEAgIBgYBAQCAgEBAICAQEAv8H44b/6ZiGvGAAAAAASUVORK5CYII=",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NOTE",
        "type": "esriFieldTypeString",
        "alias": "NOTE",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {"attributes": {"NOTE": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "NOTE": "西环路"},
        "geometry": {"x": 9189148, "y": 4380, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 1, "NOTE": "西霞小区"},
        "geometry": {"x": 9189196, "y": 4379, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 2, "NOTE": "进风华、体委"},
        "geometry": {"x": 9189237, "y": 4379, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 3, "NOTE": "去市区"},
        "geometry": {"x": 9189285, "y": 4378, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 4, "NOTE": "去市区"},
        "geometry": {"x": 9189331, "y": 4378, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 5, "NOTE": "矿石展览馆门前"},
        "geometry": {"x": 9189438, "y": 4376, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 6, "NOTE": "西环路"},
        "geometry": {"x": 9189536, "y": 4371, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 7, "NOTE": "给市区供水"},
        "geometry": {"x": 9189982, "y": 4352, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 8, "NOTE": "搅拌站"},
        "geometry": {"x": 9190196, "y": 4368, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 9, "NOTE": "去西环路驾校和殡仪馆"},
        "geometry": {"x": 9190012, "y": 4352, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 10, "NOTE": "一化来水"},
        "geometry": {"x": 9190305, "y": 4388, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {"FID": 11, "NOTE": "三坪来水"},
        "geometry": {"x": 9190305, "y": 4427, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 3,
      "name": "Line",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9189147,
        "ymin": 4343,
        "xmax": 9190291,
        "ymax": 4844,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [241, 152, 60, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "FLOW",
        "type": "esriFieldTypeString",
        "alias": "FLOW",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管径",
        "type": "esriFieldTypeString",
        "alias": "管径",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管材",
        "type": "esriFieldTypeString",
        "alias": "管材",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"FLOW": null, "管径": null, "管材": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "FLOW": "正向", "管径": "dn529", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189628, 4843], [9189365, 4843]]]
        }
      }, {
        "attributes": {"FID": 1, "FLOW": "正向", "管径": "dn529", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189365, 4843], [9189365, 4606]]]
        }
      }, {
        "attributes": {"FID": 2, "FLOW": "正向", "管径": "dn529", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189628, 4754], [9189382, 4754]]]
        }
      }, {
        "attributes": {"FID": 3, "FLOW": "正向", "管径": "dn529", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189382, 4754], [9189382, 4606]]]
        }
      }, {
        "attributes": {"FID": 4, "FLOW": "正向", "管径": "dn529", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189628, 4664], [9189400, 4664]]]
        }
      }, {
        "attributes": {"FID": 5, "FLOW": "反向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189927, 4844], [9190045, 4844]]]
        }
      }, {
        "attributes": {"FID": 6, "FLOW": "反向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190045, 4844], [9190045, 4606]]]
        }
      }, {
        "attributes": {"FID": 7, "FLOW": "正向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190008, 4606], [9189544, 4606]]]
        }
      }, {
        "attributes": {"FID": 8, "FLOW": "正向", "管径": "dn529", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189400, 4664], [9189400, 4606]]]
        }
      }, {
        "attributes": {"FID": 9, "FLOW": "正向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189927, 4821], [9189981, 4821]]]
        }
      }, {
        "attributes": {"FID": 10, "FLOW": "正向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189981, 4821], [9189981, 4521]]]
        }
      }, {
        "attributes": {"FID": 11, "FLOW": "反向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189927, 4714], [9190008, 4714]]]
        }
      }, {
        "attributes": {"FID": 12, "FLOW": "正向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189927, 4693], [9189955, 4693]]]
        }
      }, {
        "attributes": {"FID": 13, "FLOW": "正向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189955, 4693], [9189955, 4521]]]
        }
      }, {
        "attributes": {"FID": 14, "FLOW": "反向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190104, 4606], [9190104, 4413]]]
        }
      }, {
        "attributes": {"FID": 15, "FLOW": "正向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189996, 4521], [9190020, 4521]]]
        }
      }, {
        "attributes": {"FID": 16, "FLOW": "正向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190020, 4521], [9190020, 4343]]]
        }
      }, {
        "attributes": {"FID": 17, "FLOW": "反向", "管径": "dn630", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189587, 4413], [9190106, 4413]]]
        }
      }, {
        "attributes": {"FID": 18, "FLOW": "反向", "管径": "dn630", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189497, 4388], [9190104, 4388]]]
        }
      }, {
        "attributes": {"FID": 19, "FLOW": "正向", "管径": "dn529", "管材": "铸铁"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189172, 4558], [9189172, 4343]]]
        }
      }, {
        "attributes": {"FID": 20, "FLOW": "正向", "管径": "dn426", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189208, 4606], [9189208, 4343]]]
        }
      }, {
        "attributes": {"FID": 21, "FLOW": "正向", "管径": "dn426", "管材": "水泥"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189245, 4606], [9189245, 4343]]]
        }
      }, {
        "attributes": {"FID": 22, "FLOW": "正向", "管径": "dn426", "管材": "水泥"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189294, 4606], [9189294, 4343]]]
        }
      }, {
        "attributes": {"FID": 23, "FLOW": "正向", "管径": "dn273", "管材": "钢"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189341, 4606], [9189341, 4343]]]
        }
      }, {
        "attributes": {"FID": 24, "FLOW": "正向", "管径": "dn325", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189448, 4606], [9189448, 4343]]]
        }
      }, {
        "attributes": {"FID": 25, "FLOW": "正向", "管径": "dn630", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189497, 4606], [9189497, 4388]]]
        }
      }, {
        "attributes": {"FID": 26, "FLOW": "正向", "管径": "dn630", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189544, 4540], [9189544, 4343]]]
        }
      }, {
        "attributes": {"FID": 27, "FLOW": "正向", "管径": "dn630", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189544, 4540], [9189587, 4540]]]
        }
      }, {
        "attributes": {"FID": 28, "FLOW": "正向", "管径": "dn630", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189587, 4540], [9189587, 4413]]]
        }
      }, {
        "attributes": {"FID": 29, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189415, 4558], [9189415, 4519]]]
        }
      }, {
        "attributes": {"FID": 30, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189318, 4606], [9189318, 4627]]]
        }
      }, {
        "attributes": {"FID": 31, "FLOW": "正向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189996, 4521], [9189996, 4357]]]
        }
      }, {
        "attributes": {"FID": 32, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189996, 4357], [9190181, 4357]]]
        }
      }, {
        "attributes": {"FID": 33, "FLOW": "反向", "管径": "dn630", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190106, 4413], [9190291, 4413]]]
        }
      }, {
        "attributes": {"FID": 34, "FLOW": "反向", "管径": "dn630", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190104, 4388], [9190291, 4388]]]
        }
      }, {
        "attributes": {"FID": 35, "FLOW": "正向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189955, 4521], [9189981, 4521]]]
        }
      }, {
        "attributes": {"FID": 36, "FLOW": "正向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189981, 4521], [9189996, 4521]]]
        }
      }, {
        "attributes": {"FID": 37, "FLOW": "正向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190020, 4521], [9190043, 4521]]]
        }
      }, {
        "attributes": {"FID": 38, "FLOW": "反向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189917, 4521], [9189955, 4521]]]
        }
      }, {
        "attributes": {"FID": 39, "FLOW": "反向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190104, 4413], [9190104, 4388]]]
        }
      }, {
        "attributes": {"FID": 40, "FLOW": "正向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190104, 4388], [9190104, 4376]]]
        }
      }, {
        "attributes": {"FID": 41, "FLOW": "正向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189996, 4357], [9189996, 4343]]]
        }
      }, {
        "attributes": {"FID": 42, "FLOW": "正向", "管径": "dn529", "管材": "铸铁"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189172, 4606], [9189172, 4558]]]
        }
      }, {
        "attributes": {"FID": 43, "FLOW": "正向", "管径": "dn630", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189544, 4606], [9189544, 4558]]]
        }
      }, {
        "attributes": {"FID": 44, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189497, 4606], [9189448, 4606]]]
        }
      }, {
        "attributes": {"FID": 45, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189544, 4606], [9189497, 4606]]]
        }
      }, {
        "attributes": {"FID": 46, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189294, 4606], [9189245, 4606]]]
        }
      }, {
        "attributes": {"FID": 47, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189245, 4606], [9189208, 4606]]]
        }
      }, {
        "attributes": {"FID": 48, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189172, 4606], [9189147, 4606]]]
        }
      }, {
        "attributes": {"FID": 49, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189208, 4606], [9189172, 4606]]]
        }
      }, {
        "attributes": {"FID": 50, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189341, 4606], [9189318, 4606]]]
        }
      }, {
        "attributes": {"FID": 51, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189318, 4606], [9189294, 4606]]]
        }
      }, {
        "attributes": {"FID": 52, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189448, 4606], [9189400, 4606]]]
        }
      }, {
        "attributes": {"FID": 53, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189400, 4606], [9189382, 4606]]]
        }
      }, {
        "attributes": {"FID": 54, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189382, 4606], [9189365, 4606]]]
        }
      }, {
        "attributes": {"FID": 55, "FLOW": "正向", "管径": "dn800", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189365, 4606], [9189341, 4606]]]
        }
      }, {
        "attributes": {"FID": 56, "FLOW": "正向", "管径": "dn630", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189544, 4558], [9189544, 4540]]]
        }
      }, {
        "attributes": {"FID": 57, "FLOW": "正向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190104, 4606], [9190045, 4606]]]
        }
      }, {
        "attributes": {"FID": 58, "FLOW": "正向", "管径": "dn1020", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190045, 4606], [9190008, 4606]]]
        }
      }, {
        "attributes": {"FID": 59, "FLOW": "反向", "管径": "dn720", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9190008, 4714], [9190008, 4606]]]
        }
      }, {
        "attributes": {"FID": 60, "FLOW": "反向", "管径": "dn529", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189172, 4558], [9189544, 4558]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 1,
      "name": "Point",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9189172,
        "ymin": 4393,
        "xmax": 9190129,
        "ymax": 4845,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/OrangeSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAABv9JREFUeF7tl31UVVUaxk/34uXCBeTjqqXQoA1oGgqOOprlFwKKOpqCCq4IyEJL1CEmyxUijkjDOBAGFgM6qSz8KPEbCQQFP2JQhmwmhmlVSwvTIbgIJOLl7vPMc8U/5v+Ll382az1rn7PP3ft9n99+9z4HRZF/koAkIAlIApKAJCAJSAKSgCQgCUgCkoAkIAlIApKAJCAJPH4CrqtHKN7bntX5pY7R+a8drvgwpNvjDzuwEbTpzyovHgvRp5ye73Tm6jJDU9Mq19tWXVtuaDoRbigtCXXekhGgzGCaDgObaj9HXzxYCawId9r7nxiXNvzBA0jxgiXVC72PZEkxAu8NAZKN+PZl97byeYZ9q55SJvZzGgMzXepYJboxyvCd+rYH1G1G9GQMg/kDb6i7R0L9+Jk+5Y5Cb9av0LN9OLDlKYikofhmpduNnRM0qwYm636K+sexStx3cW7tSPOGOcsPlsJA4OBvgU+nAiXUsenUi8BR6hBVNB0ifxLMmeNYEb5ojjV2ZARoVvdTOvad5tWRSsj38cY7oBlzPk0X0+zRF4CTNFo6GygPAT6fC5ydyftZwKlg4Dh1ZA6wbxbMufxt+gT8FDvs53f8lHD7Zm9jtHkuypC6COMV7PwNHuQ/32e+hMbPUFWLgNoEoD4ZuJoI1EQCFTRdRihnCOQEwRxhu28OHuSyL30K6pcZ62I8lRE2pmW/4e8HOa3r2MyVz6PxA1bzM4DTNF8Z3mf8273AjyeB74uBr9IIYRkrgpVQxgooDWUlhHGrsN1DCNmz0fP7Mdg9Wf+W/RzYFslwPdL9ssicDFFI84dp/iRLvIztpWigaRfQegW4/wPUuw1Qb+yDWr+OlUHD1io4ywo4NZ/bhbCKwtCbx+rYEoSr4Yb6UDfF07bU7DB6povyQnO8Vxv+MgHqfpo+ai3tRwBq42i4EKKzDkL8CNFzHaLlMMQ3qRDXYqBeXsrtsIDVMo9VwPYgIewJgznlObRFOPYk+ioz7WDBthBZ4x0Sb672EmqGL7Cf+/8493PpHKjn50NcXwtxcxdE6wlCqIZoL4No/itE0yaIL1+lXoOoI6TKCI7jWXFkMVAQDMvb3miN0GLHWCXJtuzsMPpssNP7pjeHQk3nOz3fj6+8aVzVcIj6WIivEmj2XYgb2X3Gb+Zy9bewfw1EQxz1OsQ/3iSENVBLeTgWEV7OeFiSvHA3UouDU5QcO1iwLcT5UOeczsRhUHd4A3kjeQiO4erPY4lHUdE0SRD/osmvk6gNrApCsZqvp67FP6wAURsL9RwPxr1TgJ2jITa4o2O5BiXTlQLbsrPD6PJgpz+1v0EA232AD0dBPTwJonoBxGVWwRXqC17XLoL4+++oxdQS6iX2U1eoi7yvZlsVCbWIZ8eOZyDWOOHuCg0OPf/Eh3awYFuIvCBN0s04o4o0AsgmgJKZNERTF2n40kKC4FnwUGGPxOqw3l/kM+tvapZCnI+EqFwJ9RDfDClPojdOi9aVDtgZ6PCObdnZYfQiTyXkhyi3DmweAWTye59vAXGeK1rN1X4IgatvBXGJlfBQ1mv21fB5Nc1fiKD55RDlBHCA3wYbnfEgRoO2aEfzhtHaMDtYsDmE+78X6hp61xkh0nwhiqfCfC4E5iqqei7MNcEwX/w/We9r2F8dCjPPCnPVApgrFsLMT2NLrj8sq7Xojdfh66WOjYtGGobZnJ09JsgNUDZ3Relh3jgEvR+NRnfpRHSXT0J35WR0X2BrVfUjXbD2TUF31VQ+n4buCupkILqLfWHe6oKe+EGwJDjjk1m6VOausUf+NscI9VB8GoKVf+IVR9zfZMQvxaPQeXocOs9OQGd5IDorgtBZSZ2b2KcKqpwq5fOSX6PzwFB05bngfqIOWGfAlyv1jbGjFD+bE7PnBBnjlGV3Fj7RpcYMQud7g2Ha/zRMn46G6VgATKcCYToTBNPpiRSvj7PvM3+YinxgKvSEabcrxzhDrDegda3TvYLZmmh75t5fsbRZ45Xklpc0PXhZg/ZNzvhvrhEthcPR8ren0bLft0+f+LDvSbTke6Elzx13dg1G+1Y3iGQXmDY4mwvCHN5lQtr+Ssre8zhkB2rWNy9x+NkS44DujXrc3uaK5j+741a2B259QOVQvG7O8sDtTA/cSxsMy2ZX3FqvN+0J1Vj/Axxk76T7Pd4b/kpw7fxBZc0rHH9RE5ygbnJF11Zui+0eMO3wRFeGJ0S6O9/5LvjpLed7dfG68uQAZW6/JzLAExq2BWgjGpboCupX6L+oj3JsbozRdzTGOXU0xOlvXU3Q115/Xbcnc5o2knm6DHCujz38iNf8lck5M7SzrVrLa0b0fuxRZQBJQBKQBCQBSUASkAQkAUlAEpAEJAFJQBKQBCQBSUAS+B/qhLD6M6wGWAAAAABJRU5ErkJggg==",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NAME",
        "type": "esriFieldTypeString",
        "alias": "NAME",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "icon",
        "type": "esriFieldTypeString",
        "alias": "icon",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "type",
        "type": "esriFieldTypeString",
        "alias": "type",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "isOnClick",
        "type": "esriFieldTypeString",
        "alias": "isOnClick",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "width",
        "type": "esriFieldTypeString",
        "alias": "width",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "height",
        "type": "esriFieldTypeString",
        "alias": "height",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "dataID",
        "type": "esriFieldTypeString",
        "alias": "dataID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "mxdID",
        "type": "esriFieldTypeString",
        "alias": "mxdID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {
          "attributes": {
            "NAME": null,
            "icon": null,
            "type": null,
            "isOnClick": null,
            "width": null,
            "height": null,
            "dataID": null,
            "mxdID": null
          }
        }
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {
          "FID": 0,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": "SC-005",
          "mxdID": "1"
        }, "geometry": {"x": 9189622, "y": 4845, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 1,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": "SC-006",
          "mxdID": "1"
        }, "geometry": {"x": 9189740, "y": 4845, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 2,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": "SC-003",
          "mxdID": "1"
        }, "geometry": {"x": 9189622, "y": 4757, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 3,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": "SC-004",
          "mxdID": "1"
        }, "geometry": {"x": 9189741, "y": 4756, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 4,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": "SC-001",
          "mxdID": "1"
        }, "geometry": {"x": 9189622, "y": 4665, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 5,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": "SC-002",
          "mxdID": "1"
        }, "geometry": {"x": 9189742, "y": 4665, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 6,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": "SC-007",
          "mxdID": "1"
        }, "geometry": {"x": 9189878, "y": 4828, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 7,
          "NAME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "67.94",
          "height": "30.00",
          "dataID": "SC-008",
          "mxdID": "1"
        }, "geometry": {"x": 9189879, "y": 4700, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 8,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189365, "y": 4635, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 9,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189382, "y": 4636, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 10,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189400, "y": 4635, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 11,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189172, "y": 4584, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 12,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189208, "y": 4584, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 13,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189245, "y": 4584, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 14,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189294, "y": 4584, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 15,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189341, "y": 4584, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 16,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189448, "y": 4584, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 17,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189497, "y": 4584, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 18,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189544, "y": 4584, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 19,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189415, "y": 4543, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 20,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189351, "y": 4563, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 21,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189544, "y": 4505, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 22,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189318, "y": 4615, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 23,
          "NAME": "压力表",
          "icon": "501压力表",
          "type": "10",
          "isOnClick": "是",
          "width": "11.25",
          "height": "11.25",
          "dataID": " ",
          "mxdID": ""
        }, "geometry": {"x": 9189318, "y": 4637, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 24,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189955, "y": 4635, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 25,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189981, "y": 4635, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 26,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9190008, "y": 4635, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 27,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9190045, "y": 4635, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 28,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189996, "y": 4611, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 29,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9190129, "y": 4418, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 30,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9190129, "y": 4393, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 31,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9190020, "y": 4487, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 32,
          "NAME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "10.47",
          "height": "11.25",
          "dataID": " ",
          "mxdID": "3"
        }, "geometry": {"x": 9189996, "y": 4487, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }], "showLegend": true
}
const shpJsonPage12ShuiKu_2JianYaShuiKu = {
  "layers": [{
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 2,
      "name": "Frame",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9189278,
        "ymin": 4383,
        "xmax": 9189977,
        "ymax": 4795,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [165, 83, 183, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "性质",
        "type": "esriFieldTypeString",
        "alias": "性质",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"性质": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189278, 4795], [9189426, 4795], [9189427, 4483], [9189278, 4483], [9189278, 4795]]]
        }
      }, {
        "attributes": {"FID": 1, "性质": "虚线"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189818, 4795], [9189977, 4795], [9189977, 4383], [9189818, 4383], [9189818, 4795]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 0,
      "name": "Lable",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9189617,
        "ymin": 4860,
        "xmax": 9189617,
        "ymax": 4860,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/RedSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAAB3VJREFUeF7tmPlTlEcexnve94U5mANQbgQSbgiHXHINlxpRIBpRI6wHorLERUmIisKCQWM8cqigESVQS1Kx1piNi4mW2YpbcZONrilE140RCTcy3DDAcL/zbJP8CYPDL+9Ufau7uqb7eZ7P+/a8PS8hwkcgIBAQCAgEBAICAYGAQEAgIBAQCAgEBAICAYGAQEAgIBAQCDx/AoowKXFMUhD3lQrioZaQRVRS+fxl51eBTZUTdZ41U1Rox13/0JF9csGJ05Qv4jSz/YPWohtvLmSKN5iTGGqTm1+rc6weICOBRbZs1UVnrv87T1PUeovxyNsUP9P6n5cpHtCxu24cbrmwKLdj+osWiqrVKhI0xzbmZ7m1SpJ+1pFpvE2DPvGTomOxAoNLLKGLscZYvB10cbYYjrJCb7A5mrxleOBqim+cWJRakZY0JfnD/LieI9V1MrKtwokbrAtU4Vm0A3TJnphJD4B+RxD0u0LA7w7FTE4oprOCMbklEGNrfdGf4IqnQTb4wc0MFTYibZqM7JgjO8ZdJkpMln/sKu16pHZGb7IfptIWg389DPp9kcChWODoMuDdBOhL1JgpisbUvghM7AqFbtNiaFP80RLnhbuBdqi0N+1dbUpWGde9gWpuhFi95yL7sS7BA93JAb+Fn8mh4QujgPeTgb9kAZf3Apd2A+fXQ38yHjOHozB1IAJjOSEY2RSIwVUv4dd4X9wJccGHNrJ7CYQ4GGjLeNNfM+dyvgpzQstKf3pbB2A6m97uBRE0/Ergcxr8hyqg7hrwn0vAtRIKIRX6Y2pMl0RhIj8co9nBGFrvh55l3ngU7YObng7IVnFvGS+BYUpmHziY/Ls2zgP9SX50by/G9N5w6I+ogYvpwK1SoOlHQNsGfWcd9Peqof88B/rTyzF9hAIopAByQzC0JQB9ST5oVnvhnt+LOGsprvUhxNIwa0aY7cGR6Cp7tr8+whkjawIxkRWC6YJI6N+lAKq3Qf/Tx+B77oGfaQc/8hB8w2Xwtw9Bf3kzZspXY/JIDEbfpAB2BKLvVV90Jvjgoac9vpRxE8kciTVCBMMkNirJ7k/tRHyjtxwjKV4Yp3t/6s+R4E+/DH3N6+BrS8E314Dvvg2+/Sb4hxfBf5sP/up2TF3ZhonK1zD6dhwGdwail26DzqgX8MRKiq9ZBpkSkmeYOyPM3m9Jjl+1Z9D8AgNtlAq6bZ70qsZi+q+bwV/7I/hbB8D/dAr8Axq89iz474p/G5++koHJy1sx/lkGdBc2YjA3HF0rHNHuboomuQj/5DgclIvOGCGCYRKFFuTMV7YUAD3VDQaLMfyqBcZORGPy01QKYSNm/rYV/Nd/Av9NHvgbueBrsjDzRQamKKDxT9Kgq1iLkbIUDOSHoiNcgnYHgnYZi+9ZExSbiSoMc2eE2flKcuJLa4KGRQz6/U0wlGaP0feiMH4uFpMXEjBVlYjp6lWY+SSZtim0kulYMiYuJEJXuhTDJ9UYPByOvoIwdCxfgE4bAo0Jh39xLAoVpMwIEQyTyFCQvGpLon9sJ0K3J4OBDDcMH1dj9FQsxkrjMPFRPCbOx2GyfLal9VEcxstioTulxjAFNfROJPqLl6Bnfyg6V7ugz5yBhuHwrZjBdiU5YJg7I8wOpifAKoVIW7uQ3rpOBH2b3ekVjYT2WCRG3o+mIGKgO0OrlIaebU/HYOQDNbQnojB4NJyGD0NPfjA0bwTRE6Q7hsUcWhkWN8yZqSQlWWGECAZLmJfJmbrvVSI8taK37xpbdB/wQW8xPee/8xIGjvlj8IQ/hk4G0JbWcX8MHPVDX4kveoq8ocn3xLM33NCZRcPHOGJYZIKfpQyq7JjHS6yJjcHujLHADgkpuC7h8F8zEVqXSNC2awE69lqhs8AamkO26HrbDt2H7dBVQov2NcW26CiwQtu+BWjdY4n2nZboTbfCmKcCnRyDO/YmyLPnDlHvjDH8G6zhS9/wlEnYR7X00fWrFYuWdVI0ZpuhcbcczW/R2qdAcz6t/bRov4mONeaaoYl+p22rHF0bVNAmKtBvweIXGxNcfFH8eNlC4m6wMWMusEnKpn5hyo48pj9gLe4SNG9QoGGLAk8z5XiaJUd99u8122/IpBA2K9BGg2vWWKAvRYVeLzEa7E1R422m2+MsSTem97nSYnfKyN6/mzATv7AUgqcMrUnmaFlLX3ysM0fj+t/b5lQLtK22QEfyAmiSLKFZpUJ7kBRPXKW4HqCYynWVHKSG2LkyZex1uO1mZM9lKem9Tx9jjY5iNEYo0bKMhn7ZAu0r6H5PpLXCAq0rKJClSjSGynE/QIkrQYqBPe6S2X+AJsY2Ped6iWZk6RlL0c2r5szofRsO9R5S1IfQLRCpQL1aifoYFerpsbkuTImaUJXuXIDiH6/Ys8vm3Mg8L2i20YqsO7fItKLcSXyn0kXccclVqv3MS6at9JU/Ox+ouns+SF6Z4cSupz7l8+z1ucs7LF1AQjOdxfGZzmx8Iu1TRcfnrioICAQEAgIBgYBAQCAgEBAICAQEAgIBgYBAQCAgEBAICAQEAv8H44b/6ZiGvGAAAAAASUVORK5CYII=",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NOTE",
        "type": "esriFieldTypeString",
        "alias": "NOTE",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {"attributes": {"NOTE": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "NOTE": "减压水库"},
        "geometry": {"x": 9189617, "y": 4860, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 3,
      "name": "Line",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "supportsMultiScaleGeometry": true,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsReturningGeometryProperties": true,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPolyline",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9189427,
        "ymin": 4309,
        "xmax": 9189977,
        "ymax": 4763,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {"type": "esriSLS", "style": "esriSLSSolid", "color": [241, 152, 60, 255], "width": 1}
        }, "transparency": 0, "labelingInfo": null
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "FLOW",
        "type": "esriFieldTypeString",
        "alias": "FLOW",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管径",
        "type": "esriFieldTypeString",
        "alias": "管径",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "管材",
        "type": "esriFieldTypeString",
        "alias": "管材",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolLine",
        "prototype": {"attributes": {"FLOW": null, "管径": null, "管材": null}}
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 4000,
      "tileMaxRecordCount": 4000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {"FID": 0, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189427, 4763], [9189622, 4763]]]
        }
      }, {
        "attributes": {"FID": 1, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189427, 4685], [9189624, 4685]]]
        }
      }, {
        "attributes": {"FID": 2, "FLOW": "正向", "管径": "dn500", "管材": "玻璃钢"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189622, 4436], [9189622, 4309]]]
        }
      }, {
        "attributes": {"FID": 3, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189427, 4584], [9189545, 4584]]]
        }
      }, {
        "attributes": {"FID": 4, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189622, 4537], [9189977, 4537]]]
        }
      }, {
        "attributes": {"FID": 5, "FLOW": "反向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189622, 4436], [9189818, 4436]]]
        }
      }, {
        "attributes": {"FID": 6, "FLOW": "反向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189622, 4481], [9189818, 4481]]]
        }
      }, {
        "attributes": {"FID": 7, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189522, 4584], [9189522, 4537]]]
        }
      }, {
        "attributes": {"FID": 8, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189545, 4584], [9189622, 4584]]]
        }
      }, {
        "attributes": {"FID": 9, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189427, 4537], [9189522, 4537]]]
        }
      }, {
        "attributes": {"FID": 10, "FLOW": "正向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189522, 4537], [9189622, 4537]]]
        }
      }, {
        "attributes": {"FID": 11, "FLOW": "反向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189622, 4763], [9189818, 4763]]]
        }
      }, {
        "attributes": {"FID": 12, "FLOW": "反向", "管径": " ", "管材": " "},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189624, 4685], [9189818, 4685]]]
        }
      }, {
        "attributes": {"FID": 13, "FLOW": "正向", "管径": "dn500", "管材": "玻璃钢"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189622, 4763], [9189622, 4685]]]
        }
      }, {
        "attributes": {"FID": 14, "FLOW": "正向", "管径": "dn500", "管材": "玻璃钢"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189622, 4685], [9189622, 4584]]]
        }
      }, {
        "attributes": {"FID": 15, "FLOW": "正向", "管径": "dn500", "管材": "玻璃钢"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189622, 4584], [9189622, 4537]]]
        }
      }, {
        "attributes": {"FID": 16, "FLOW": "正向", "管径": "dn500", "管材": "玻璃钢"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189622, 4537], [9189622, 4481]]]
        }
      }, {
        "attributes": {"FID": 17, "FLOW": "正向", "管径": "dn500", "管材": "玻璃钢"},
        "geometry": {
          "spatialReference": {"wkid": 102100, "latestWkid": 3857},
          "paths": [[[9189622, 4481], [9189622, 4436]]]
        }
      }], "geometryType": "esriGeometryPolyline"
    }
  }, {
    "layerDefinition": {
      "currentVersion": 10.61,
      "id": 1,
      "name": "Point",
      "type": "Feature Layer",
      "displayField": "",
      "description": "",
      "copyrightText": "",
      "defaultVisibility": true,
      "relationships": [],
      "isDataVersioned": false,
      "supportsAppend": true,
      "supportsCalculate": true,
      "supportsTruncate": false,
      "supportsAttachmentsByUploadId": true,
      "supportsAttachmentsResizing": true,
      "supportsRollbackOnFailureParameter": true,
      "supportsStatistics": true,
      "supportsAdvancedQueries": true,
      "supportsValidateSql": true,
      "supportsCoordinatesQuantization": true,
      "supportsQuantizationEditMode": true,
      "supportsApplyEditsWithGlobalIds": false,
      "advancedQueryCapabilities": {
        "supportsPagination": true,
        "supportsPaginationOnAggregatedQueries": true,
        "supportsQueryRelatedPagination": true,
        "supportsQueryWithDistance": true,
        "supportsReturningQueryExtent": true,
        "supportsStatistics": true,
        "supportsOrderBy": true,
        "supportsDistinct": true,
        "supportsQueryWithResultType": true,
        "supportsSqlExpression": true,
        "supportsAdvancedQueryRelated": true,
        "supportsCountDistinct": true,
        "supportsReturningGeometryCentroid": false,
        "supportsQueryWithDatumTransformation": true,
        "supportsHavingClause": true,
        "supportsOutFieldSQLExpression": true,
        "supportsMaxRecordCountFactor": true,
        "supportsTopFeaturesQuery": true
      },
      "useStandardizedQueries": false,
      "geometryType": "esriGeometryPoint",
      "minScale": 0,
      "maxScale": 0,
      "extent": {
        "xmin": 9189343,
        "ymin": 4441,
        "xmax": 9189889,
        "ymax": 4768,
        "spatialReference": {"wkid": 102100, "latestWkid": 3857}
      },
      "drawingInfo": {
        "renderer": {
          "type": "simple",
          "symbol": {
            "type": "esriPMS",
            "url": "http://static.arcgis.com/images/Symbols/Basic/OrangeSphere.png",
            "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAABv9JREFUeF7tl31UVVUaxk/34uXCBeTjqqXQoA1oGgqOOprlFwKKOpqCCq4IyEJL1CEmyxUijkjDOBAGFgM6qSz8KPEbCQQFP2JQhmwmhmlVSwvTIbgIJOLl7vPMc8U/5v+Ll382az1rn7PP3ft9n99+9z4HRZF/koAkIAlIApKAJCAJSAKSgCQgCUgCkoAkIAlIApKAJCAJPH4CrqtHKN7bntX5pY7R+a8drvgwpNvjDzuwEbTpzyovHgvRp5ye73Tm6jJDU9Mq19tWXVtuaDoRbigtCXXekhGgzGCaDgObaj9HXzxYCawId9r7nxiXNvzBA0jxgiXVC72PZEkxAu8NAZKN+PZl97byeYZ9q55SJvZzGgMzXepYJboxyvCd+rYH1G1G9GQMg/kDb6i7R0L9+Jk+5Y5Cb9av0LN9OLDlKYikofhmpduNnRM0qwYm636K+sexStx3cW7tSPOGOcsPlsJA4OBvgU+nAiXUsenUi8BR6hBVNB0ifxLMmeNYEb5ojjV2ZARoVvdTOvad5tWRSsj38cY7oBlzPk0X0+zRF4CTNFo6GygPAT6fC5ydyftZwKlg4Dh1ZA6wbxbMufxt+gT8FDvs53f8lHD7Zm9jtHkuypC6COMV7PwNHuQ/32e+hMbPUFWLgNoEoD4ZuJoI1EQCFTRdRihnCOQEwRxhu28OHuSyL30K6pcZ62I8lRE2pmW/4e8HOa3r2MyVz6PxA1bzM4DTNF8Z3mf8273AjyeB74uBr9IIYRkrgpVQxgooDWUlhHGrsN1DCNmz0fP7Mdg9Wf+W/RzYFslwPdL9ssicDFFI84dp/iRLvIztpWigaRfQegW4/wPUuw1Qb+yDWr+OlUHD1io4ywo4NZ/bhbCKwtCbx+rYEoSr4Yb6UDfF07bU7DB6povyQnO8Vxv+MgHqfpo+ai3tRwBq42i4EKKzDkL8CNFzHaLlMMQ3qRDXYqBeXsrtsIDVMo9VwPYgIewJgznlObRFOPYk+ioz7WDBthBZ4x0Sb672EmqGL7Cf+/8493PpHKjn50NcXwtxcxdE6wlCqIZoL4No/itE0yaIL1+lXoOoI6TKCI7jWXFkMVAQDMvb3miN0GLHWCXJtuzsMPpssNP7pjeHQk3nOz3fj6+8aVzVcIj6WIivEmj2XYgb2X3Gb+Zy9bewfw1EQxz1OsQ/3iSENVBLeTgWEV7OeFiSvHA3UouDU5QcO1iwLcT5UOeczsRhUHd4A3kjeQiO4erPY4lHUdE0SRD/osmvk6gNrApCsZqvp67FP6wAURsL9RwPxr1TgJ2jITa4o2O5BiXTlQLbsrPD6PJgpz+1v0EA232AD0dBPTwJonoBxGVWwRXqC17XLoL4+++oxdQS6iX2U1eoi7yvZlsVCbWIZ8eOZyDWOOHuCg0OPf/Eh3awYFuIvCBN0s04o4o0AsgmgJKZNERTF2n40kKC4FnwUGGPxOqw3l/kM+tvapZCnI+EqFwJ9RDfDClPojdOi9aVDtgZ6PCObdnZYfQiTyXkhyi3DmweAWTye59vAXGeK1rN1X4IgatvBXGJlfBQ1mv21fB5Nc1fiKD55RDlBHCA3wYbnfEgRoO2aEfzhtHaMDtYsDmE+78X6hp61xkh0nwhiqfCfC4E5iqqei7MNcEwX/w/We9r2F8dCjPPCnPVApgrFsLMT2NLrj8sq7Xojdfh66WOjYtGGobZnJ09JsgNUDZ3Relh3jgEvR+NRnfpRHSXT0J35WR0X2BrVfUjXbD2TUF31VQ+n4buCupkILqLfWHe6oKe+EGwJDjjk1m6VOausUf+NscI9VB8GoKVf+IVR9zfZMQvxaPQeXocOs9OQGd5IDorgtBZSZ2b2KcKqpwq5fOSX6PzwFB05bngfqIOWGfAlyv1jbGjFD+bE7PnBBnjlGV3Fj7RpcYMQud7g2Ha/zRMn46G6VgATKcCYToTBNPpiRSvj7PvM3+YinxgKvSEabcrxzhDrDegda3TvYLZmmh75t5fsbRZ45Xklpc0PXhZg/ZNzvhvrhEthcPR8ren0bLft0+f+LDvSbTke6Elzx13dg1G+1Y3iGQXmDY4mwvCHN5lQtr+Ssre8zhkB2rWNy9x+NkS44DujXrc3uaK5j+741a2B259QOVQvG7O8sDtTA/cSxsMy2ZX3FqvN+0J1Vj/Axxk76T7Pd4b/kpw7fxBZc0rHH9RE5ygbnJF11Zui+0eMO3wRFeGJ0S6O9/5LvjpLed7dfG68uQAZW6/JzLAExq2BWgjGpboCupX6L+oj3JsbozRdzTGOXU0xOlvXU3Q115/Xbcnc5o2knm6DHCujz38iNf8lck5M7SzrVrLa0b0fuxRZQBJQBKQBCQBSUASkAQkAUlAEpAEJAFJQBKQBCQBSUAS+B/qhLD6M6wGWAAAAABJRU5ErkJggg==",
            "contentType": "image/png",
            "width": 15,
            "height": 15
          }
        }
      },
      "allowGeometryUpdates": true,
      "hasAttachments": false,
      "htmlPopupType": "",
      "hasM": false,
      "hasZ": false,
      "objectIdField": "FID",
      "globalIdField": "",
      "typeIdField": "",
      "fields": [{
        "name": "FID",
        "type": "esriFieldTypeOID",
        "alias": "FID",
        "sqlType": "sqlTypeOther",
        "nullable": false,
        "editable": false,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "NEME",
        "type": "esriFieldTypeString",
        "alias": "NEME",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "icon",
        "type": "esriFieldTypeString",
        "alias": "icon",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "type",
        "type": "esriFieldTypeString",
        "alias": "type",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "isOnClick",
        "type": "esriFieldTypeString",
        "alias": "isOnClick",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "width",
        "type": "esriFieldTypeString",
        "alias": "width",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "height",
        "type": "esriFieldTypeString",
        "alias": "height",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }, {
        "name": "dataID",
        "type": "esriFieldTypeString",
        "alias": "dataID",
        "sqlType": "sqlTypeOther",
        "length": 50,
        "nullable": true,
        "editable": true,
        "domain": null,
        "defaultValue": null
      }],
      "indexes": [],
      "types": [],
      "templates": [{
        "name": "New Feature",
        "description": "",
        "drawingTool": "esriFeatureEditToolPoint",
        "prototype": {
          "attributes": {
            "NEME": null,
            "icon": null,
            "type": null,
            "isOnClick": null,
            "width": null,
            "height": null,
            "dataID": null
          }
        }
      }],
      "supportedQueryFormats": "JSON, geoJSON",
      "hasStaticData": false,
      "maxRecordCount": -1,
      "standardMaxRecordCount": 32000,
      "tileMaxRecordCount": 8000,
      "maxRecordCountFactor": 1,
      "capabilities": "Create,Delete,Query,Update,Editing"
    },
    "featureSet": {
      "features": [{
        "attributes": {
          "FID": 0,
          "NEME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "93.42",
          "height": "41.25",
          "dataID": "SC-009"
        }, "geometry": {"x": 9189343, "y": 4734, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 1,
          "NEME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "93.42",
          "height": "41.25",
          "dataID": "SC-009"
        }, "geometry": {"x": 9189889, "y": 4735, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 2,
          "NEME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "93.42",
          "height": "41.25",
          "dataID": "SC-009"
        }, "geometry": {"x": 9189344, "y": 4564, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 3,
          "NEME": "清水池",
          "icon": "801清水池",
          "type": "10",
          "isOnClick": "是",
          "width": "93.42",
          "height": "41.25",
          "dataID": "SC-009"
        }, "geometry": {"x": 9189886, "y": 4455, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 4,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189525, "y": 4768, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 5,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189729, "y": 4768, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 6,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189732, "y": 4690, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 7,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189522, "y": 4690, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 8,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189622, "y": 4640, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 9,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189622, "y": 4715, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 10,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189522, "y": 4554, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 11,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189479, "y": 4589, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 12,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189580, "y": 4589, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 13,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189478, "y": 4542, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 14,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189582, "y": 4542, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 15,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189729, "y": 4486, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }, {
        "attributes": {
          "FID": 16,
          "NEME": "阀门",
          "icon": "301普通阀门",
          "type": "10",
          "isOnClick": "是",
          "width": "13.96",
          "height": "15.00",
          "dataID": " "
        }, "geometry": {"x": 9189726, "y": 4441, "spatialReference": {"wkid": 102100, "latestWkid": 3857}}
      }], "geometryType": "esriGeometryPoint"
    }
  }], "showLegend": true
}

export {shpJsonPage12ShuiKu_1ShanShangShuiKu}
export {shpJsonPage12ShuiKu_2JianYaShuiKu}
