const arcgisdemo = {
  "layers": [
    {
      "layerDefinition": {
        "currentVersion": 10.61,
        "id": 0,
        "name": "总览",
        "type": "Feature Layer",
        "displayField": "",
        "description": "",
        "copyrightText": "",
        "defaultVisibility": true,
        "relationships": [],
        "isDataVersioned": false,
        "supportsAppend": true,
        "supportsCalculate": true,
        "supportsTruncate": false,
        "supportsAttachmentsByUploadId": true,
        "supportsAttachmentsResizing": true,
        "supportsRollbackOnFailureParameter": true,
        "supportsStatistics": true,
        "supportsAdvancedQueries": true,
        "supportsValidateSql": true,
        "supportsCoordinatesQuantization": true,
        "supportsQuantizationEditMode": true,
        "supportsApplyEditsWithGlobalIds": false,
        "advancedQueryCapabilities": {
          "supportsPagination": true,
          "supportsPaginationOnAggregatedQueries": true,
          "supportsQueryRelatedPagination": true,
          "supportsQueryWithDistance": true,
          "supportsReturningQueryExtent": true,
          "supportsStatistics": true,
          "supportsOrderBy": true,
          "supportsDistinct": true,
          "supportsQueryWithResultType": true,
          "supportsSqlExpression": true,
          "supportsAdvancedQueryRelated": true,
          "supportsCountDistinct": true,
          "supportsReturningGeometryCentroid": false,
          "supportsQueryWithDatumTransformation": true,
          "supportsHavingClause": true,
          "supportsOutFieldSQLExpression": true,
          "supportsMaxRecordCountFactor": true,
          "supportsTopFeaturesQuery": true
        },
        "useStandardizedQueries": false,
        "geometryType": "esriGeometryPoint",
        "minScale": 0,
        "maxScale": 0,
        "extent": {
          "xmin": 9186510,
          "ymin": 1318,
          "xmax": 9187810,
          "ymax": 1815,
          "spatialReference": {
            "wkid": 102100,
            "latestWkid": 3857
          }
        },
        "drawingInfo": {
          "renderer": {
            "type": "simple",
            "symbol": {
              "type": "esriPMS",
              "url": "http://static.arcgis.com/images/Symbols/Basic/RedSphere.png",
              "imageData": "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuNS4xTuc4+QAAB3VJREFUeF7tmPlTlEcexnve94U5mANQbgQSbgiHXHINlxpRIBpRI6wHorLERUmIisKCQWM8cqigESVQS1Kx1piNi4mW2YpbcZONrilE140RCTcy3DDAcL/zbJP8CYPDL+9Ufau7uqb7eZ7P+/a8PS8hwkcgIBAQCAgEBAICAYGAQEAgIBAQCAgEBAICAYGAQEAgIBAQCDx/AoowKXFMUhD3lQrioZaQRVRS+fxl51eBTZUTdZ41U1Rox13/0JF9csGJ05Qv4jSz/YPWohtvLmSKN5iTGGqTm1+rc6weICOBRbZs1UVnrv87T1PUeovxyNsUP9P6n5cpHtCxu24cbrmwKLdj+osWiqrVKhI0xzbmZ7m1SpJ+1pFpvE2DPvGTomOxAoNLLKGLscZYvB10cbYYjrJCb7A5mrxleOBqim+cWJRakZY0JfnD/LieI9V1MrKtwokbrAtU4Vm0A3TJnphJD4B+RxD0u0LA7w7FTE4oprOCMbklEGNrfdGf4IqnQTb4wc0MFTYibZqM7JgjO8ZdJkpMln/sKu16pHZGb7IfptIWg389DPp9kcChWODoMuDdBOhL1JgpisbUvghM7AqFbtNiaFP80RLnhbuBdqi0N+1dbUpWGde9gWpuhFi95yL7sS7BA93JAb+Fn8mh4QujgPeTgb9kAZf3Apd2A+fXQ38yHjOHozB1IAJjOSEY2RSIwVUv4dd4X9wJccGHNrJ7CYQ4GGjLeNNfM+dyvgpzQstKf3pbB2A6m97uBRE0/Ergcxr8hyqg7hrwn0vAtRIKIRX6Y2pMl0RhIj8co9nBGFrvh55l3ngU7YObng7IVnFvGS+BYUpmHziY/Ls2zgP9SX50by/G9N5w6I+ogYvpwK1SoOlHQNsGfWcd9Peqof88B/rTyzF9hAIopAByQzC0JQB9ST5oVnvhnt+LOGsprvUhxNIwa0aY7cGR6Cp7tr8+whkjawIxkRWC6YJI6N+lAKq3Qf/Tx+B77oGfaQc/8hB8w2Xwtw9Bf3kzZspXY/JIDEbfpAB2BKLvVV90Jvjgoac9vpRxE8kciTVCBMMkNirJ7k/tRHyjtxwjKV4Yp3t/6s+R4E+/DH3N6+BrS8E314Dvvg2+/Sb4hxfBf5sP/up2TF3ZhonK1zD6dhwGdwail26DzqgX8MRKiq9ZBpkSkmeYOyPM3m9Jjl+1Z9D8AgNtlAq6bZ70qsZi+q+bwV/7I/hbB8D/dAr8Axq89iz474p/G5++koHJy1sx/lkGdBc2YjA3HF0rHNHuboomuQj/5DgclIvOGCGCYRKFFuTMV7YUAD3VDQaLMfyqBcZORGPy01QKYSNm/rYV/Nd/Av9NHvgbueBrsjDzRQamKKDxT9Kgq1iLkbIUDOSHoiNcgnYHgnYZi+9ZExSbiSoMc2eE2flKcuJLa4KGRQz6/U0wlGaP0feiMH4uFpMXEjBVlYjp6lWY+SSZtim0kulYMiYuJEJXuhTDJ9UYPByOvoIwdCxfgE4bAo0Jh39xLAoVpMwIEQyTyFCQvGpLon9sJ0K3J4OBDDcMH1dj9FQsxkrjMPFRPCbOx2GyfLal9VEcxstioTulxjAFNfROJPqLl6Bnfyg6V7ugz5yBhuHwrZjBdiU5YJg7I8wOpifAKoVIW7uQ3rpOBH2b3ekVjYT2WCRG3o+mIGKgO0OrlIaebU/HYOQDNbQnojB4NJyGD0NPfjA0bwTRE6Q7hsUcWhkWN8yZqSQlWWGECAZLmJfJmbrvVSI8taK37xpbdB/wQW8xPee/8xIGjvlj8IQ/hk4G0JbWcX8MHPVDX4kveoq8ocn3xLM33NCZRcPHOGJYZIKfpQyq7JjHS6yJjcHujLHADgkpuC7h8F8zEVqXSNC2awE69lqhs8AamkO26HrbDt2H7dBVQov2NcW26CiwQtu+BWjdY4n2nZboTbfCmKcCnRyDO/YmyLPnDlHvjDH8G6zhS9/wlEnYR7X00fWrFYuWdVI0ZpuhcbcczW/R2qdAcz6t/bRov4mONeaaoYl+p22rHF0bVNAmKtBvweIXGxNcfFH8eNlC4m6wMWMusEnKpn5hyo48pj9gLe4SNG9QoGGLAk8z5XiaJUd99u8122/IpBA2K9BGg2vWWKAvRYVeLzEa7E1R422m2+MsSTem97nSYnfKyN6/mzATv7AUgqcMrUnmaFlLX3ysM0fj+t/b5lQLtK22QEfyAmiSLKFZpUJ7kBRPXKW4HqCYynWVHKSG2LkyZex1uO1mZM9lKem9Tx9jjY5iNEYo0bKMhn7ZAu0r6H5PpLXCAq0rKJClSjSGynE/QIkrQYqBPe6S2X+AJsY2Ped6iWZk6RlL0c2r5szofRsO9R5S1IfQLRCpQL1aifoYFerpsbkuTImaUJXuXIDiH6/Ys8vm3Mg8L2i20YqsO7fItKLcSXyn0kXccclVqv3MS6at9JU/Ox+ouns+SF6Z4cSupz7l8+z1ucs7LF1AQjOdxfGZzmx8Iu1TRcfnrioICAQEAgIBgYBAQCAgEBAICAQEAgIBgYBAQCAgEBAICAQEAv8H44b/6ZiGvGAAAAAASUVORK5CYII=",
              "contentType": "image/png",
              "width": 15,
              "height": 15
            }
          }
        },
        "allowGeometryUpdates": true,
        "hasAttachments": false,
        "htmlPopupType": "",
        "hasM": true,
        "allowUpdateWithoutMValues": true,
        "hasZ": true,
        "enableZDefaults": true,
        "zDefault": 0,
        "objectIdField": "FID",
        "globalIdField": "",
        "typeIdField": "",
        "fields": [
          {
            "name": "FID",
            "type": "esriFieldTypeOID",
            "alias": "FID",
            "sqlType": "sqlTypeOther",
            "nullable": false,
            "editable": false,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "NAME",
            "type": "esriFieldTypeString",
            "alias": "NAME",
            "sqlType": "sqlTypeOther",
            "length": 50,
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "Type",
            "type": "esriFieldTypeSmallInteger",
            "alias": "Type",
            "sqlType": "sqlTypeOther",
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          }
        ],
        "indexes": [],
        "types": [],
        "templates": [
          {
            "name": "New Feature",
            "description": "",
            "drawingTool": "esriFeatureEditToolPoint",
            "prototype": {
              "attributes": {
                "NAME": null,
                "Type": null
              }
            }
          }
        ],
        "supportedQueryFormats": "JSON, geoJSON",
        "hasStaticData": false,
        "maxRecordCount": -1,
        "standardMaxRecordCount": 32000,
        "tileMaxRecordCount": 8000,
        "maxRecordCountFactor": 1,
        "capabilities": "Create,Delete,Query,Update,Editing"
      },
      "featureSet": {
        "features": [
          {
            "attributes": {
              "FID": 0,
              "NAME": "克拉玛依市区",
              "Type": 0
            },
            "geometry": {
              "x": 9186510,
              "y": 1724,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 1,
              "NAME": "白碱滩区",
              "Type": 0
            },
            "geometry": {
              "x": 9187177,
              "y": 1732,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 2,
              "NAME": "二化",
              "Type": 1
            },
            "geometry": {
              "x": 9187377,
              "y": 1724,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 3,
              "NAME": "五厂",
              "Type": 1
            },
            "geometry": {
              "x": 9187596,
              "y": 1629,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 4,
              "NAME": "六化",
              "Type": 1
            },
            "geometry": {
              "x": 9187638,
              "y": 1726,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 5,
              "NAME": "三坪",
              "Type": 1
            },
            "geometry": {
              "x": 9186913,
              "y": 1649,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 6,
              "NAME": "五化",
              "Type": 1
            },
            "geometry": {
              "x": 9186950,
              "y": 1650,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 7,
              "NAME": "三坪水库",
              "Type": 3
            },
            "geometry": {
              "x": 9186992,
              "y": 1742,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 8,
              "NAME": "调节水库",
              "Type": 3
            },
            "geometry": {
              "x": 9187460,
              "y": 1741,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 9,
              "NAME": "黄羊泉水库",
              "Type": 3
            },
            "geometry": {
              "x": 9187636,
              "y": 1765,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 10,
              "NAME": "井盖",
              "Type": 2
            },
            "geometry": {
              "x": 9186931,
              "y": 1554,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 11,
              "NAME": "井盖",
              "Type": 2
            },
            "geometry": {
              "x": 9186942,
              "y": 1559,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 12,
              "NAME": "管汇间",
              "Type": 4
            },
            "geometry": {
              "x": 9187208,
              "y": 1549,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 13,
              "NAME": "扬水泵站",
              "Type": 10
            },
            "geometry": {
              "x": 9187425,
              "y": 1724,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 14,
              "NAME": "输水首战",
              "Type": 10
            },
            "geometry": {
              "x": 9187383,
              "y": 1554,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 15,
              "NAME": "重油5000方",
              "Type": 9
            },
            "geometry": {
              "x": 9187576,
              "y": 1536,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 16,
              "NAME": "水源管理站",
              "Type": 10
            },
            "geometry": {
              "x": 9187558,
              "y": 1608,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 17,
              "NAME": "百联站",
              "Type": 9
            },
            "geometry": {
              "x": 9187637,
              "y": 1652,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 18,
              "NAME": "金龙镇（输炼地区）",
              "Type": 9
            },
            "geometry": {
              "x": 9186923,
              "y": 1403,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 19,
              "NAME": "石化园区",
              "Type": 8
            },
            "geometry": {
              "x": 9186980,
              "y": 1404,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 20,
              "NAME": "三坪镇",
              "Type": 5
            },
            "geometry": {
              "x": 9187062,
              "y": 1379,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 21,
              "NAME": "红浅",
              "Type": 0
            },
            "geometry": {
              "x": 9186568,
              "y": 1318,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 22,
              "NAME": "自流环",
              "Type": 5
            },
            "geometry": {
              "x": 9186608,
              "y": 1443,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 23,
              "NAME": "下环",
              "Type": 5
            },
            "geometry": {
              "x": 9186600,
              "y": 1499,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 24,
              "NAME": "中环",
              "Type": 5
            },
            "geometry": {
              "x": 9186611,
              "y": 1586,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 25,
              "NAME": "上环",
              "Type": 5
            },
            "geometry": {
              "x": 9186609,
              "y": 1662,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 26,
              "NAME": "山上水库",
              "Type": 3
            },
            "geometry": {
              "x": 9186636,
              "y": 1707,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 27,
              "NAME": "末端泵站",
              "Type": 9
            },
            "geometry": {
              "x": 9186787,
              "y": 1475,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 28,
              "NAME": "技校三厂",
              "Type": 1
            },
            "geometry": {
              "x": 9187068,
              "y": 1537,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 29,
              "NAME": "一化",
              "Type": 1
            },
            "geometry": {
              "x": 9187207,
              "y": 1699,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 30,
              "NAME": "白碱滩",
              "Type": 5
            },
            "geometry": {
              "x": 9187199,
              "y": 1625,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 31,
              "NAME": "乌尔禾（137团、风城作业区）",
              "Type": 1
            },
            "geometry": {
              "x": 9187755,
              "y": 1785,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 32,
              "NAME": "魔鬼城、稠油水处理站",
              "Type": 1
            },
            "geometry": {
              "x": 9187810,
              "y": 1815,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 33,
              "NAME": "石化区",
              "Type": 8
            },
            "geometry": {
              "x": 9187110,
              "y": 1368,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 34,
              "NAME": "分区流量计_末端泵站_1820853",
              "Type": 6
            },
            "geometry": {
              "x": 9186791,
              "y": 1464,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 35,
              "NAME": "压力监测仪_首站_YL-010",
              "Type": 7
            },
            "geometry": {
              "x": 9187326,
              "y": 1628,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          },
          {
            "attributes": {
              "FID": 36,
              "NAME": "压力监测仪_电厂后院_YL-020",
              "Type": 7
            },
            "geometry": {
              "x": 9187043,
              "y": 1712,
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              }
            }
          }
        ],
        "geometryType": "esriGeometryPoint"
      }
    },
    {
      "layerDefinition": {
        "currentVersion": 10.61,
        "id": 1,
        "name": "管线",
        "type": "Feature Layer",
        "displayField": "",
        "description": "",
        "copyrightText": "",
        "defaultVisibility": true,
        "relationships": [],
        "isDataVersioned": false,
        "supportsAppend": true,
        "supportsCalculate": true,
        "supportsTruncate": false,
        "supportsAttachmentsByUploadId": true,
        "supportsAttachmentsResizing": true,
        "supportsRollbackOnFailureParameter": true,
        "supportsStatistics": true,
        "supportsAdvancedQueries": true,
        "supportsValidateSql": true,
        "supportsCoordinatesQuantization": true,
        "supportsQuantizationEditMode": true,
        "supportsApplyEditsWithGlobalIds": false,
        "supportsMultiScaleGeometry": true,
        "advancedQueryCapabilities": {
          "supportsPagination": true,
          "supportsPaginationOnAggregatedQueries": true,
          "supportsQueryRelatedPagination": true,
          "supportsQueryWithDistance": true,
          "supportsReturningQueryExtent": true,
          "supportsStatistics": true,
          "supportsOrderBy": true,
          "supportsDistinct": true,
          "supportsQueryWithResultType": true,
          "supportsSqlExpression": true,
          "supportsAdvancedQueryRelated": true,
          "supportsCountDistinct": true,
          "supportsReturningGeometryCentroid": false,
          "supportsReturningGeometryProperties": true,
          "supportsQueryWithDatumTransformation": true,
          "supportsHavingClause": true,
          "supportsOutFieldSQLExpression": true,
          "supportsMaxRecordCountFactor": true,
          "supportsTopFeaturesQuery": true
        },
        "useStandardizedQueries": false,
        "geometryType": "esriGeometryPolyline",
        "minScale": 0,
        "maxScale": 0,
        "extent": {
          "xmin": 9186593,
          "ymin": 1314,
          "xmax": 9187799,
          "ymax": 1812,
          "spatialReference": {
            "wkid": 102100,
            "latestWkid": 3857
          }
        },
        "drawingInfo": {
          "renderer": {
            "type": "simple",
            "symbol": {
              "type": "esriSLS",
              "style": "esriSLSSolid",
              "color": [
                165,
                83,
                183,
                255
              ],
              "width": 1
            }
          },
          "transparency": 0,
          "labelingInfo": null
        },
        "allowGeometryUpdates": true,
        "hasAttachments": false,
        "htmlPopupType": "",
        "hasMetadata": true,
        "hasM": true,
        "allowUpdateWithoutMValues": true,
        "hasZ": true,
        "enableZDefaults": true,
        "zDefault": 0,
        "objectIdField": "FID",
        "globalIdField": "",
        "typeIdField": "",
        "fields": [
          {
            "name": "FID",
            "type": "esriFieldTypeOID",
            "alias": "FID",
            "sqlType": "sqlTypeOther",
            "nullable": false,
            "editable": false,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "管径",
            "type": "esriFieldTypeString",
            "alias": "管径",
            "sqlType": "sqlTypeOther",
            "length": 50,
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "管材",
            "type": "esriFieldTypeString",
            "alias": "管材",
            "sqlType": "sqlTypeOther",
            "length": 50,
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "FLOW",
            "type": "esriFieldTypeString",
            "alias": "FLOW",
            "sqlType": "sqlTypeOther",
            "length": 50,
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          }
        ],
        "indexes": [],
        "types": [],
        "templates": [
          {
            "name": "New Feature",
            "description": "",
            "drawingTool": "esriFeatureEditToolLine",
            "prototype": {
              "attributes": {
                "管径": null,
                "管材": null,
                "FLOW": null
              }
            }
          }
        ],
        "supportedQueryFormats": "JSON, geoJSON",
        "hasStaticData": false,
        "maxRecordCount": -1,
        "standardMaxRecordCount": 4000,
        "tileMaxRecordCount": 4000,
        "maxRecordCountFactor": 1,
        "capabilities": "Create,Delete,Query,Update,Editing"
      },
      "featureSet": {
        "features": [
          {
            "attributes": {
              "FID": 0,
              "管径": "dn630",
              "管材": "内衬PE",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186982,
                    1712
                  ],
                  [
                    9187326,
                    1712
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 1,
              "管径": "dn600",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187208,
                    1689
                  ],
                  [
                    9187208,
                    1559
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 2,
              "管径": "dn630",
              "管材": "球墨",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186648,
                    1703
                  ],
                  [
                    9186942,
                    1703
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 3,
              "管径": "dn600",
              "管材": " ",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186942,
                    1712
                  ],
                  [
                    9186942,
                    1691
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 4,
              "管径": "dn600",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186982,
                    1621
                  ],
                  [
                    9186982,
                    1712
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 5,
              "管径": "dn1000",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186672,
                    1678
                  ],
                  [
                    9186672,
                    1466
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 6,
              "管径": "dn630",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186792,
                    1481
                  ],
                  [
                    9186931,
                    1559
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 7,
              "管径": "dn630",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186797,
                    1473
                  ],
                  [
                    9186931,
                    1548
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 8,
              "管径": "dn500",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186792,
                    1471
                  ],
                  [
                    9186888,
                    1417
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 9,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186791,
                    1460
                  ],
                  [
                    9186888,
                    1405
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 10,
              "管径": "dn800",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187341,
                    1712
                  ],
                  [
                    9187341,
                    1568
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 11,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186713,
                    1434
                  ],
                  [
                    9186683,
                    1418
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 12,
              "管径": "dn500",
              "管材": "球墨",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186760,
                    1408
                  ],
                  [
                    9186593,
                    1314
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 13,
              "管径": "dn500",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186648,
                    1549
                  ],
                  [
                    9186782,
                    1474
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 14,
              "管径": "dn700",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186922,
                    1560
                  ],
                  [
                    9186786,
                    1484
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 15,
              "管径": "dn700",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186786,
                    1484
                  ],
                  [
                    9186648,
                    1484
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 16,
              "管径": "dn400",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186782,
                    1473
                  ],
                  [
                    9186648,
                    1473
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 17,
              "管径": "dn800",
              "管材": "球墨",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187380,
                    1531
                  ],
                  [
                    9187557,
                    1531
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 18,
              "管径": "dn800",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187380,
                    1539
                  ],
                  [
                    9187380,
                    1531
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 19,
              "管径": "dn800",
              "管材": "球墨",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187341,
                    1624
                  ],
                  [
                    9187462,
                    1624
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 20,
              "管径": "dn800",
              "管材": "球墨",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187574,
                    1687
                  ],
                  [
                    9187625,
                    1715
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 21,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187119,
                    1487
                  ],
                  [
                    9187014,
                    1429
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 22,
              "管径": "dn700",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187221,
                    1702
                  ],
                  [
                    9187326,
                    1702
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 23,
              "管径": "dn529",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187208,
                    1689
                  ],
                  [
                    9187286,
                    1689
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 24,
              "管径": "dn700",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187224,
                    1553
                  ],
                  [
                    9187361,
                    1553
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 25,
              "管径": "dn400",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187224,
                    1545
                  ],
                  [
                    9187361,
                    1545
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 26,
              "管径": "dn600",
              "管材": "球墨",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187361,
                    1545
                  ],
                  [
                    9187095,
                    1401
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 27,
              "管径": "dn400",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186990,
                    1581
                  ],
                  [
                    9187055,
                    1545
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 28,
              "管径": "dn500",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187496,
                    1643
                  ],
                  [
                    9187543,
                    1616
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 29,
              "管径": "dn500",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187534,
                    1664
                  ],
                  [
                    9187580,
                    1638
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 30,
              "管径": "dn500",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187574,
                    1687
                  ],
                  [
                    9187618,
                    1662
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 31,
              "管径": "dn700",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186922,
                    1598
                  ],
                  [
                    9186922,
                    1560
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 32,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186931,
                    1598
                  ],
                  [
                    9186931,
                    1559
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 33,
              "管径": "dn800",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186892,
                    1601
                  ],
                  [
                    9186648,
                    1601
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 34,
              "管径": "dn800",
              "管材": "玻璃钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186892,
                    1611
                  ],
                  [
                    9186648,
                    1611
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 35,
              "管径": "dn1000",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186892,
                    1678
                  ],
                  [
                    9186672,
                    1678
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 36,
              "管径": "dn600",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186970,
                    1621
                  ],
                  [
                    9186982,
                    1621
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 37,
              "管径": "dn1000",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186942,
                    1598
                  ],
                  [
                    9186942,
                    1564
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 38,
              "管径": "dn800",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187654,
                    1731
                  ],
                  [
                    9187743,
                    1781
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 39,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187326,
                    1702
                  ],
                  [
                    9187326,
                    1568
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 40,
              "管径": "dn800",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187654,
                    1731
                  ],
                  [
                    9187743,
                    1781
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 41,
              "管径": "dn600",
              "管材": "球墨",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187769,
                    1795
                  ],
                  [
                    9187799,
                    1812
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 42,
              "管径": "dn600",
              "管材": "球墨",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187769,
                    1795
                  ],
                  [
                    9187784,
                    1804
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 43,
              "管径": "dn150",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187139,
                    1497
                  ],
                  [
                    9187204,
                    1461
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 44,
              "管径": "dn630",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186931,
                    1559
                  ],
                  [
                    9186942,
                    1564
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 45,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186931,
                    1559
                  ],
                  [
                    9186931,
                    1548
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 46,
              "管径": "dn630",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186942,
                    1564
                  ],
                  [
                    9187191,
                    1703
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 47,
              "管径": "dn1000",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186942,
                    1564
                  ],
                  [
                    9186942,
                    1554
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 48,
              "管径": "dn630",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186931,
                    1548
                  ],
                  [
                    9186942,
                    1554
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 49,
              "管径": "dn630",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186942,
                    1554
                  ],
                  [
                    9187191,
                    1692
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 50,
              "管径": "dn1000",
              "管材": "铸铁",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186942,
                    1554
                  ],
                  [
                    9186942,
                    1429
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 51,
              "管径": "dn500",
              "管材": "球墨",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186784,
                    1421
                  ],
                  [
                    9186784,
                    1471
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 52,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186791,
                    1460
                  ],
                  [
                    9186791,
                    1471
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 53,
              "管径": "dn630",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186792,
                    1473
                  ],
                  [
                    9186797,
                    1473
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 54,
              "管径": "dn630",
              "管材": "内衬PE",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187341,
                    1712
                  ],
                  [
                    9187358,
                    1730
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 55,
              "管径": "dn700",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187348,
                    1702
                  ],
                  [
                    9187358,
                    1712
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 56,
              "管径": "dn400",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187081,
                    1530
                  ],
                  [
                    9187139,
                    1497
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 57,
              "管径": "dn500",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187216,
                    1540
                  ],
                  [
                    9187119,
                    1487
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 58,
              "管径": "dn800",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187341,
                    1531
                  ],
                  [
                    9187380,
                    1531
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 59,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187326,
                    1712
                  ],
                  [
                    9187326,
                    1702
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 60,
              "管径": "dn630",
              "管材": "内衬PE",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187326,
                    1712
                  ],
                  [
                    9187341,
                    1712
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 61,
              "管径": "dn700",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187326,
                    1702
                  ],
                  [
                    9187348,
                    1702
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 62,
              "管径": "dn630",
              "管材": "钢",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187286,
                    1689
                  ],
                  [
                    9187326,
                    1689
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 63,
              "管径": "dn800",
              "管材": "球墨",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187462,
                    1624
                  ],
                  [
                    9187496,
                    1643
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 64,
              "管径": "dn800",
              "管材": "球墨",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187496,
                    1643
                  ],
                  [
                    9187534,
                    1664
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 65,
              "管径": "dn800",
              "管材": "球墨",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187534,
                    1664
                  ],
                  [
                    9187574,
                    1687
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 66,
              "管径": "700",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187326,
                    1568
                  ],
                  [
                    9187341,
                    1568
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 67,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187326,
                    1568
                  ],
                  [
                    9187326,
                    1553
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 68,
              "管径": "dn250",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186723,
                    1440
                  ],
                  [
                    9186761,
                    1418
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 69,
              "管径": "dn250",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186713,
                    1434
                  ],
                  [
                    9186751,
                    1413
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 70,
              "管径": "dn250",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186751,
                    1413
                  ],
                  [
                    9186760,
                    1408
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 71,
              "管径": "dn250",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186761,
                    1418
                  ],
                  [
                    9186770,
                    1413
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 72,
              "管径": "dn400",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186766,
                    1427
                  ],
                  [
                    9186780,
                    1419
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 73,
              "管径": "dn400",
              "管材": " ",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186733,
                    1446
                  ],
                  [
                    9186766,
                    1427
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 74,
              "管径": "dn800",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187341,
                    1568
                  ],
                  [
                    9187341,
                    1531
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 75,
              "管径": "dn500",
              "管材": "球墨",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186784,
                    1421
                  ],
                  [
                    9186780,
                    1419
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 76,
              "管径": "dn500",
              "管材": "球墨",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186780,
                    1419
                  ],
                  [
                    9186770,
                    1413
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 77,
              "管径": "dn500",
              "管材": "球墨",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186770,
                    1413
                  ],
                  [
                    9186760,
                    1408
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 78,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186782,
                    1473
                  ],
                  [
                    9186733,
                    1446
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 79,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186733,
                    1446
                  ],
                  [
                    9186723,
                    1440
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 80,
              "管径": "dn600",
              "管材": "钢",
              "FLOW": "1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186723,
                    1440
                  ],
                  [
                    9186713,
                    1434
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 81,
              "管径": "dn630",
              "管材": "内衬PE",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186942,
                    1712
                  ],
                  [
                    9186982,
                    1712
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 82,
              "管径": "dn630",
              "管材": "内衬PE",
              "FLOW": "-1"
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186648,
                    1712
                  ],
                  [
                    9186942,
                    1712
                  ]
                ]
              ]
            }
          }
        ],
        "geometryType": "esriGeometryPolyline"
      }
    },
    {
      "layerDefinition": {
        "currentVersion": 10.61,
        "id": 2,
        "name": "辅助线",
        "type": "Feature Layer",
        "displayField": "",
        "description": "",
        "copyrightText": "",
        "defaultVisibility": true,
        "relationships": [],
        "isDataVersioned": false,
        "supportsAppend": true,
        "supportsCalculate": true,
        "supportsTruncate": false,
        "supportsAttachmentsByUploadId": true,
        "supportsAttachmentsResizing": true,
        "supportsRollbackOnFailureParameter": true,
        "supportsStatistics": true,
        "supportsAdvancedQueries": true,
        "supportsValidateSql": true,
        "supportsCoordinatesQuantization": true,
        "supportsQuantizationEditMode": true,
        "supportsApplyEditsWithGlobalIds": false,
        "supportsMultiScaleGeometry": true,
        "advancedQueryCapabilities": {
          "supportsPagination": true,
          "supportsPaginationOnAggregatedQueries": true,
          "supportsQueryRelatedPagination": true,
          "supportsQueryWithDistance": true,
          "supportsReturningQueryExtent": true,
          "supportsStatistics": true,
          "supportsOrderBy": true,
          "supportsDistinct": true,
          "supportsQueryWithResultType": true,
          "supportsSqlExpression": true,
          "supportsAdvancedQueryRelated": true,
          "supportsCountDistinct": true,
          "supportsReturningGeometryCentroid": false,
          "supportsReturningGeometryProperties": true,
          "supportsQueryWithDatumTransformation": true,
          "supportsHavingClause": true,
          "supportsOutFieldSQLExpression": true,
          "supportsMaxRecordCountFactor": true,
          "supportsTopFeaturesQuery": true
        },
        "useStandardizedQueries": false,
        "geometryType": "esriGeometryPolyline",
        "minScale": 0,
        "maxScale": 0,
        "extent": {
          "xmin": 9186508,
          "ymin": 1298,
          "xmax": 9187819,
          "ymax": 1826,
          "spatialReference": {
            "wkid": 102100,
            "latestWkid": 3857
          }
        },
        "drawingInfo": {
          "renderer": {
            "type": "simple",
            "symbol": {
              "type": "esriSLS",
              "style": "esriSLSSolid",
              "color": [
                241,
                152,
                60,
                255
              ],
              "width": 1
            }
          },
          "transparency": 0,
          "labelingInfo": null
        },
        "allowGeometryUpdates": true,
        "hasAttachments": false,
        "htmlPopupType": "",
        "hasM": true,
        "allowUpdateWithoutMValues": true,
        "hasZ": true,
        "enableZDefaults": true,
        "zDefault": 0,
        "objectIdField": "FID",
        "globalIdField": "",
        "typeIdField": "",
        "fields": [
          {
            "name": "FID",
            "type": "esriFieldTypeOID",
            "alias": "FID",
            "sqlType": "sqlTypeOther",
            "nullable": false,
            "editable": false,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "FID_",
            "type": "esriFieldTypeInteger",
            "alias": "FID_",
            "sqlType": "sqlTypeOther",
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "Entity",
            "type": "esriFieldTypeString",
            "alias": "Entity",
            "sqlType": "sqlTypeOther",
            "length": 16,
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "Layer",
            "type": "esriFieldTypeString",
            "alias": "Layer",
            "sqlType": "sqlTypeOther",
            "length": 254,
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "Color",
            "type": "esriFieldTypeSmallInteger",
            "alias": "Color",
            "sqlType": "sqlTypeOther",
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "Linetype",
            "type": "esriFieldTypeString",
            "alias": "Linetype",
            "sqlType": "sqlTypeOther",
            "length": 254,
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "Elevation",
            "type": "esriFieldTypeDouble",
            "alias": "Elevation",
            "sqlType": "sqlTypeOther",
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "LineWt",
            "type": "esriFieldTypeSmallInteger",
            "alias": "LineWt",
            "sqlType": "sqlTypeOther",
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          },
          {
            "name": "RefName",
            "type": "esriFieldTypeString",
            "alias": "RefName",
            "sqlType": "sqlTypeOther",
            "length": 254,
            "nullable": true,
            "editable": true,
            "domain": null,
            "defaultValue": null
          }
        ],
        "indexes": [],
        "types": [],
        "templates": [
          {
            "name": "New Feature",
            "description": "",
            "drawingTool": "esriFeatureEditToolLine",
            "prototype": {
              "attributes": {
                "FID_": null,
                "Entity": null,
                "Layer": null,
                "Color": null,
                "Linetype": null,
                "Elevation": null,
                "LineWt": null,
                "RefName": null
              }
            }
          }
        ],
        "supportedQueryFormats": "JSON, geoJSON",
        "hasStaticData": false,
        "maxRecordCount": -1,
        "standardMaxRecordCount": 4000,
        "tileMaxRecordCount": 4000,
        "maxRecordCountFactor": 1,
        "capabilities": "Create,Delete,Query,Update,Editing"
      },
      "featureSet": {
        "features": [
          {
            "attributes": {
              "FID": 0,
              "FID_": 0,
              "Entity": "Line",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186792,
                    1481
                  ],
                  [
                    9186782,
                    1481
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 1,
              "FID_": 0,
              "Entity": "Line",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186792,
                    1471
                  ],
                  [
                    9186782,
                    1471
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 2,
              "FID_": 0,
              "Entity": "Line",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186792,
                    1481
                  ],
                  [
                    9186792,
                    1471
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 3,
              "FID_": 0,
              "Entity": "Line",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186782,
                    1481
                  ],
                  [
                    9186782,
                    1471
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 4,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186892,
                    1691
                  ],
                  [
                    9186970,
                    1691
                  ],
                  [
                    9186970,
                    1598
                  ],
                  [
                    9186892,
                    1598
                  ],
                  [
                    9186892,
                    1691
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 5,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187221,
                    1689
                  ],
                  [
                    9187191,
                    1689
                  ],
                  [
                    9187191,
                    1711
                  ],
                  [
                    9187221,
                    1711
                  ],
                  [
                    9187221,
                    1689
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 6,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187190,
                    1559
                  ],
                  [
                    9187224,
                    1559
                  ],
                  [
                    9187224,
                    1540
                  ],
                  [
                    9187190,
                    1540
                  ],
                  [
                    9187190,
                    1559
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 7,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187623,
                    1736
                  ],
                  [
                    9187654,
                    1736
                  ],
                  [
                    9187654,
                    1715
                  ],
                  [
                    9187623,
                    1715
                  ],
                  [
                    9187623,
                    1736
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 8,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187361,
                    1572
                  ],
                  [
                    9187401,
                    1572
                  ],
                  [
                    9187401,
                    1539
                  ],
                  [
                    9187361,
                    1539
                  ],
                  [
                    9187361,
                    1572
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 9,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187394,
                    1740
                  ],
                  [
                    9187394,
                    1707
                  ],
                  [
                    9187358,
                    1707
                  ],
                  [
                    9187358,
                    1740
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 10,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187405,
                    1740
                  ],
                  [
                    9187441,
                    1740
                  ],
                  [
                    9187441,
                    1707
                  ],
                  [
                    9187405,
                    1707
                  ],
                  [
                    9187405,
                    1740
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 11,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186888,
                    1429
                  ],
                  [
                    9187094,
                    1429
                  ],
                  [
                    9187094,
                    1343
                  ],
                  [
                    9186888,
                    1343
                  ],
                  [
                    9186888,
                    1429
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 12,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186554,
                    1697
                  ],
                  [
                    9186648,
                    1697
                  ],
                  [
                    9186648,
                    1621
                  ],
                  [
                    9186554,
                    1621
                  ],
                  [
                    9186554,
                    1697
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 13,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186579,
                    1621
                  ],
                  [
                    9186648,
                    1621
                  ],
                  [
                    9186648,
                    1546
                  ],
                  [
                    9186579,
                    1546
                  ],
                  [
                    9186579,
                    1621
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 14,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186621,
                    1716
                  ],
                  [
                    9186648,
                    1716
                  ],
                  [
                    9186648,
                    1697
                  ],
                  [
                    9186621,
                    1697
                  ],
                  [
                    9186621,
                    1716
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 15,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186648,
                    1546
                  ],
                  [
                    9186557,
                    1546
                  ],
                  [
                    9186557,
                    1466
                  ],
                  [
                    9186648,
                    1466
                  ],
                  [
                    9186648,
                    1546
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 16,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186557,
                    1466
                  ],
                  [
                    9186683,
                    1466
                  ],
                  [
                    9186683,
                    1415
                  ],
                  [
                    9186557,
                    1415
                  ],
                  [
                    9186557,
                    1466
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 17,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186544,
                    1339
                  ],
                  [
                    9186593,
                    1339
                  ],
                  [
                    9186593,
                    1298
                  ],
                  [
                    9186544,
                    1298
                  ],
                  [
                    9186544,
                    1339
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 18,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187557,
                    1557
                  ],
                  [
                    9187600,
                    1557
                  ],
                  [
                    9187600,
                    1515
                  ],
                  [
                    9187557,
                    1515
                  ],
                  [
                    9187557,
                    1557
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 19,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187543,
                    1616
                  ],
                  [
                    9187570,
                    1616
                  ],
                  [
                    9187570,
                    1599
                  ],
                  [
                    9187543,
                    1599
                  ],
                  [
                    9187543,
                    1616
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 20,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187580,
                    1638
                  ],
                  [
                    9187607,
                    1638
                  ],
                  [
                    9187607,
                    1621
                  ],
                  [
                    9187580,
                    1621
                  ],
                  [
                    9187580,
                    1638
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 21,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187618,
                    1662
                  ],
                  [
                    9187645,
                    1662
                  ],
                  [
                    9187645,
                    1645
                  ],
                  [
                    9187618,
                    1645
                  ],
                  [
                    9187618,
                    1662
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 22,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187055,
                    1545
                  ],
                  [
                    9187081,
                    1545
                  ],
                  [
                    9187081,
                    1530
                  ],
                  [
                    9187055,
                    1530
                  ],
                  [
                    9187055,
                    1545
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 23,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187177,
                    1647
                  ],
                  [
                    9187241,
                    1647
                  ],
                  [
                    9187241,
                    1609
                  ],
                  [
                    9187177,
                    1609
                  ],
                  [
                    9187177,
                    1647
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 24,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187743,
                    1795
                  ],
                  [
                    9187769,
                    1795
                  ],
                  [
                    9187769,
                    1775
                  ],
                  [
                    9187743,
                    1775
                  ],
                  [
                    9187743,
                    1795
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 25,
              "FID_": 0,
              "Entity": "LWPolyline",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187799,
                    1826
                  ],
                  [
                    9187819,
                    1826
                  ],
                  [
                    9187819,
                    1806
                  ],
                  [
                    9187799,
                    1806
                  ],
                  [
                    9187799,
                    1826
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 26,
              "FID_": 0,
              "Entity": "Line",
              "Layer": "0",
              "Color": 7,
              "Linetype": "Continuous",
              "Elevation": 0,
              "LineWt": 25,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9187358,
                    1740
                  ],
                  [
                    9187394,
                    1740
                  ]
                ]
              ]
            }
          },
          {
            "attributes": {
              "FID": 27,
              "FID_": 0,
              "Entity": " ",
              "Layer": " ",
              "Color": 0,
              "Linetype": " ",
              "Elevation": 0,
              "LineWt": 0,
              "RefName": " "
            },
            "geometry": {
              "spatialReference": {
                "wkid": 102100,
                "latestWkid": 3857
              },
              "paths": [
                [
                  [
                    9186508,
                    1707
                  ],
                  [
                    9186699,
                    1707
                  ],
                  [
                    9186699,
                    1408
                  ],
                  [
                    9186508,
                    1408
                  ],
                  [
                    9186508,
                    1707
                  ]
                ]
              ]
            }
          }
        ],
        "geometryType": "esriGeometryPolyline"
      }
    }
  ],
  "showLegend": true
}

export default arcgisdemo
