import VueRouter from 'vue-router'
import { logout, pathRedirect } from '@/utils/auth'
import Home from 'containers/Home'
import Login from 'containers/Login'
import NotFound from 'containers/Common/NotFound'
import WaterPrediction from './containers/WaterPrediction'
import WaterPump from './containers/WaterPump'
import Manage from './containers/Manage'
import SolutionAssessment from './containers/SolutionAssessment'
import Plan from './containers/Plan'
import Monitor from './containers/Monitor'
import PressureAreaVersion from './containers/PressureAreaVersion'
import PartitionInfo from './containers/PartitionInfo'
// import ZonalStatistic from './containers/ZonalStatistic'
import Eventhand from './containers/Eventhand'
import TapOpenDegree from './containers/ProductionOverview/Detail/TapOpenDegree'
import DispatchParticipationDetail from './containers/DispatchParticipation/Detail'
import OpenStatus from './containers/ProductionOverview/Detail/OpenStatus'
import CleanWaterDetails from './containers/ProductionOverview/Detail/cleanWaterDetails'
import ProductionOverview from './containers/ProductionOverview/Page0ZongLan'
import Page11ShuiChang from './containers/ProductionOverview/Page11ShuiChang'
import Page12ShuiKu from './containers/ProductionOverview/Page12ShuiKu'
import Page13GuanHuiJian from './containers/ProductionOverview/Page13GuanHuiJian'
import Page14ShiQvFenQv from './containers/ProductionOverview/Page14ShiQvFenQv'
// 指令调度
import InstructionScheduling from './containers/Order/instructionScheduling'
import WaterCurrentState from './containers/Order/waterCurrentState'
import PumpHistorialState from './containers/Order/pumpHistorialState'
import InstructionStatistic from './containers/Order/instructionStatistic'
const routes = [
  { path: '/',
    component: Home,
    redirect: to => pathRedirect(to, '/dispatch'),
    children: [
      { path: 'waterPump', component: WaterPump },
      { path: 'waterPrediction', component: WaterPrediction },
      { path: 'tap-open-degree', component: TapOpenDegree },
      { path: 'openStatus', component: OpenStatus },
      { path: 'cleanWaterDetails', component: CleanWaterDetails },
      { path: 'Page11ShuiChang', component: Page11ShuiChang },
      { path: 'Page12ShuiKu', component: Page12ShuiKu },
      { path: 'Page13GuanHuiJian', component: Page13GuanHuiJian },
      { path: 'Page14ShiQvFenQv', component: Page14ShiQvFenQv },
      { path: 'production-overview', component: ProductionOverview },
      { path: 'manage', component: Manage },
      { path: 'monitor', component: Monitor },
      { path: 'pressureAreaVersion', component: PressureAreaVersion },
      { path: 'partitionInfo', component: PartitionInfo, name: 'partitionInfo' },
      { path: 'zonalStatistic', component: DispatchParticipationDetail, name: 'zonalStatistic' },
      { path: 'eventhand', component: Eventhand },
      { path: 'solutionAssessment', component: SolutionAssessment },
      { path: 'plan', component: Plan },
      { path: 'InstructionScheduling', component: InstructionScheduling },
      { path: 'WaterCurrentState', component: WaterCurrentState },
      { path: 'PumpHistorialState', component: PumpHistorialState },
      { path: 'InstructionStatistic', component: InstructionStatistic },
      { path: '*', component: NotFound }
    ]
  },
  { path: '/login', component: Login },
  { path: '/logout',
    beforeEnter (to, from, next) {
      logout()
    }
  },
  { path: '*', component: NotFound }
]

const router = new VueRouter({
  mode: 'hash',
  base: __dirname,
  routes
})
export default router
