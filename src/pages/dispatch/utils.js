const ymd = date => {
  return [date.getFullYear(), date.getMonth() + 1, date.getDay()].join('-')
}

module.exports = { ymd }