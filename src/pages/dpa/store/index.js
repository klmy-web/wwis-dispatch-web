import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  state: {
    loadingMask: true,
    menuActive: true,
    treeStorage: {},
    pageStorage: {},
    clientWidth: 0,
    clientHeight: 0
  },
  getters: {
    loadingMask: (state) => state.loadingMask,
    menuActive: (state) => state.menuActive,
    pageStorage: (state) => state.pageStorage,
    treeStorage: (state) => state.treeStorage,
    clientWidth: (state) => state.clientWidth,
    clientHeight: (state) => state.clientHeight
  },
  mutations: {
    showLoadingMask (state) {
      state.loadingMask = true
    },
    hideLoadingMask (state) {
      state.loadingMask = false
    },
    windowResize (state) {
      state.clientWidth = document.body.clientWidth
      state.clientHeight = document.body.clientHeight
    },
    toggleMenu (state) {
      state.menuActive = !state.menuActive
    },
    initPageStorage (state, props) {
      const { page } = props
      if (!state.pageStorage[page]) {
        state.pageStorage[page] = {
          tableList: [],
          filterBy: {},
          orderBy: [],
          pageIndex: 1,
          pageSize: 10,
          expands: new Set()
        }
      }
    },
    changePageStorage (state, props) {
      const { page, type, value, callback } = props
      if (!state.pageStorage[page]) {
        Vue.set(state.pageStorage, page, {})
      }
      Vue.set(state.pageStorage[page], type, value)
      if (callback) callback()
    },
    changeTreeStorage (state, props) {
      const { page, id, expanded } = props
      if (!state.treeStorage[page]) {
        state.treeStorage[page] = new Set()
      }
      if (expanded) {
        state.treeStorage[page].add(id)
      } else {
        state.treeStorage[page].delete(id)
      }
    }
  },
  strict: debug
})
