import moment from 'moment'

export const intervalList = [
  {
    value: 'HOUR',
    label: '时'
  },
  {
    value: 'DAY',
    label: '日'
  },
  {
    value: 'MONTH',
    label: '月'
  },
  {
    value: 'YEAR',
    label: '年'
  }
]

export const shortcuts = [{
  text: '今天',
  onClick: (picker) => {
    const end = moment()
    const start = moment()
    picker.$emit('pick', [start, end])
  }
}, {
  text: '昨天',
  onClick: (picker) => {
    const end = moment().subtract(1, 'days')
    const start = moment().subtract(1, 'days')
    picker.$emit('pick', [start, end])
  }
}, {
  text: '本周',
  onClick: (picker) => {
    const end = moment().endOf('week')
    const start = moment().startOf('week')
    picker.$emit('pick', [start, end])
  }
}, {
  text: '上周',
  onClick: (picker) => {
    const end = moment().subtract(1, 'weeks').endOf('week')
    const start = moment().subtract(1, 'weeks').startOf('week')
    picker.$emit('pick', [start, end])
  }
}, {
  text: '最近一周',
  onClick: (picker) => {
    const end = moment().endOf('day')
    const start = moment().subtract(6, 'days').startOf('day')
    picker.$emit('pick', [start, end])
  }
}, {
  text: '本月',
  onClick: (picker) => {
    // const end =  moment()
    const end = moment().endOf('month')
    const start = moment().startOf('month')
    picker.$emit('pick', [start, end])
  }
}, {
  text: '上月',
  onClick: (picker) => {
    const end = moment().subtract(1, 'months').endOf('month')
    const start = moment().subtract(1, 'months').startOf('month')
    picker.$emit('pick', [start, end])
  }
}, {
  text: '最近一个月',
  onClick: (picker) => {
    const end = moment().endOf('day')
    const start = moment().subtract(1, 'months').subtract(-1, 'days')
    picker.$emit('pick', [start, end])
  }
}, {
  text: '最近三个月',
  onClick: (picker) => {
    const end = moment().endOf('day')
    const start = moment().subtract(3, 'months').subtract(-1, 'days')
    picker.$emit('pick', [start, end])
  }
}]
