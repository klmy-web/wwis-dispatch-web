export function getDyncRelation ({target = this, columns = {}} = {}) {
  const relationKeys = Object.keys(columns).filter(item => columns[item].relation).map(res => columns[res].relation)
  // console.log('relationKeys:', relationKeys)
  if (relationKeys.length) {
    target.$getRelation({
      relations: relationKeys.join(',')
    }).then(data => {
    //  this.isGetRelation = true
      Object.keys(columns).map(key => {
        const relation = columns[key].relation
        if (data[relation]) {
          const nOptions = [...columns[key].optionsInit || [], ...data[relation]].map(option => {
            const label = option[columns[key].labelName || 'label']
            return {
              value: option.value !== undefined ? option.value : option.id,
              label,
              ...option
            }
          })
          columns[key].options = nOptions
          target.$set(columns[key], 'options', nOptions)
        }
      })
    })
  }
  return {
    columns,
    isGetRelation: true
  }
}

export function deleteWithDialog (target = this, {url, id, mesg = ''}) {
  return new Promise((resolve, reject) => {
    target.$swal({
      title: '提示',
      text: `确定删除${mesg}, 是否继续?`,
      type: 'question',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-danger',
      cancelButtonClass: 'btn btn-default',
      confirmButtonText: '删除',
      cancelButtonText: '取消'
    }).then((result) => {
      if (result) {
        target.$httpDelete({
          url: url + '/' + id
        }).then(data => {
          if (data) {
            target.$message({
              type: 'success',
              message: '删除成功!'
            })
            resolve(result)
          }
        })
      }
    }, (dismiss) => {})
  })
}
