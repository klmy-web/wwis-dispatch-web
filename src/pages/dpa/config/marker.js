import { keyBy } from 'lodash'

import NormalCm from '../assets/img/NormalCm.png'
import CompleteCm from '../assets/img/CompleteCm.png'
import WarningCm from '../assets/img/WarningCm.png'
import DangerCm from '../assets/img/DangerCm.png'
import DefaultCm from '../assets/img/DefaultCm.png'

import NormalDm from '../assets/img/NormalDm.png'
import CompleteDm from '../assets/img/CompleteDm.png'
import WarningDm from '../assets/img/WarningDm.png'
import DangerDm from '../assets/img/DangerDm.png'
import DefaultDm from '../assets/img/DefaultDm.png'

import NormalZm from '../assets/img/NormalZm.png'
import CompleteZm from '../assets/img/CompleteZm.png'
import WarningZm from '../assets/img/WarningZm.png'
import DangerZm from '../assets/img/DangerZm.png'
import DefaultZm from '../assets/img/DefaultZm.png'

import NormalP from '../assets/img/NormalP.png'
import CompleteP from '../assets/img/CompleteP.png'
import WarningP from '../assets/img/WarningP.png'
import DangerP from '../assets/img/DangerP.png'
import DefaultP from '../assets/img/DefaultP.png'

export const deviceTypeList = [
  {
    label: '大用户电磁水表',
    value: 'LUF',
    iconName: 'cm',
    icon: {
      NORMAL: NormalCm,
      COMPLETE: CompleteCm,
      WARNING: WarningCm,
      DANGER: DangerCm,
      DEFAULT: DefaultCm
    }
  },
  {
    label: 'DMA专用水表',
    value: 'DMA',
    iconName: 'dm',
    icon: {
      NORMAL: NormalDm,
      COMPLETE: CompleteDm,
      WARNING: WarningDm,
      DANGER: DangerDm,
      DEFAULT: DefaultDm
    }
  },
  {
    label: '分区流量计',
    value: 'DPAF',
    iconName: 'zm',
    icon: {
      NORMAL: NormalZm,
      COMPLETE: CompleteZm,
      WARNING: WarningZm,
      DANGER: DangerZm,
      DEFAULT: DefaultZm
    }
  },
  {
    label: '压力监测仪',
    value: 'PT',
    iconName: 'p',
    icon: {
      NORMAL: NormalP,
      COMPLETE: CompleteP,
      WARNING: WarningP,
      DANGER: DangerP,
      DEFAULT: DefaultP
    }
  }
]

export const deviceType = keyBy(deviceTypeList, 'value')

export const iconColorList = [
  {
    value: 'WARNING',
    color: '#f2a538'
  },
  {
    value: 'COMPLETE',
    color: '#4080db'
  },
  {
    value: 'DANGER',
    color: '#e36262'
  },
  {
    value: 'NORMAL',
    color: '#53964d'
  }
]

export const iconColor = keyBy(iconColorList, 'value')

export const areaInfoRelation = [
  {
    label: '采集时间',
    value: 'recordTime'
  },
  {
    label: '瞬时供水量',
    value: 'dws'
  },
  {
    label: '大用户用水量',
    value: 'lucw'
  },
  {
    label: '平均压力',
    value: 'avgPressure'
  },
  {
    label: '产销差水量',
    value: 'nrw'
  },
  {
    label: '产销差比率',
    value: 'nrwRate'
  }
]

export const areaInfoMesg = keyBy(areaInfoRelation, 'value')

export const floatingWindow = {
  // WQTM: ['2', '4', '5', 'policeFlag', '9', '8', '7', '6'],
  LUF: ['13', '14', '16', '10', 'policeFlag'],
  DMA: ['13', '14', '16', '10', 'policeFlag'],
  DPAF: ['13', '14', '16', '10', 'policeFlag']
  // NMI: ['23', '24', '25', 'policeFlag'],
  // TPR: ['10', '18']
  // , '19', '20', '21', '22'
}

export const policeFlagList = {
  label: '状态',
  NORMAL: '正常',
  COMPLETE: '关注',
  WARNING: '预警',
  DANGER: '报警'
}
