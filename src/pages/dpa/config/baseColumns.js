export default {
  enabled: {
    label: '是否启用',
    render: 'select',
    relation: 'yesOrNo',
    form: {
      type: 'switch'
    }
  },
  remark: {
    label: '备注',
    form: {
      type: 'textarea'
    }
  },
  createUserId: {
    label: '创建人',
    relation: 'users',
    render: 'select'
  },
  createTime: {
    label: '创建时间',
    render: {
      type: 'time',
      dateType: 'YYYY-MM-DD HH:mm'
    }
  },
  updateUserId: {
    label: '更新人',
    relation: 'users',
    render: 'select'
  },
  updateTime: {
    label: '更新时间',
    render: {
      type: 'time',
      dateType: 'YYYY-MM-DD HH:mm'
    }
  }
}
