export default {
  KLMY: {
    name: 'KLMY',
    city: '克拉玛依',
    lonlat: [84.8892700000, 45.5799900000],
    // mapIcon: user,
    title: '克拉玛依',
    company: '克拉玛依',
    copyright: '2018'
  },
  initColumns: {
    areaId: {
      label: '',
      filter: {
        type: ''
      }
    },
    startTime: {
      label: '',
      filter: {
        type: ''
      }
    },
    endTime: {
      label: '',
      filter: {
        type: ''
      }
    }
  }
}
