import {keyBy} from 'lodash'
import ISOFF from '../../assets/img/isOff.png'
import ISON from '../../assets/img/isOn.png'

export const iconColorList = [
  {
    value: 'OFF',
    color: '#AA71FF'
  },
  {
    value: 'ON',
    color: '#F47437'
  }
]

export const iconColor = keyBy(iconColorList, 'value')

export const flagList = {
  'OFF': {
    label: '关闭',
    img: ISOFF,
    color: '#AA71FF'
  },
  'ON': {
    label: '开启',
    img: ISON,
    color: '#F47437'
  }
}
