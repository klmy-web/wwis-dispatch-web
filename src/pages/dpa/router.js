import VueRouter from 'vue-router'

import { logout, pathRedirect } from '@/utils/auth'

import Home from 'containers/Home'
import Login from 'containers/Login'
import NotFound from 'containers/Common/NotFound'

import Manage from './containers/Manage'
import Monitor from './containers/Monitor'
import PressureAreaVersion from './containers/PressureAreaVersion'
import PartitionInfo from './containers/PartitionInfo'
import ZonalStatistic from './containers/ZonalStatistic'
import Eventhand from './containers/Eventhand'

const routes = [
  { path: '/',
    component: Home,
    redirect: to => pathRedirect(to, '/dpa'),
    children: [
      { path: 'manage', component: Manage },
      { path: 'monitor', component: Monitor },
      { path: 'pressureAreaVersion', component: PressureAreaVersion },
      { path: 'partitionInfo', component: PartitionInfo, name: 'partitionInfo' },
      { path: 'zonalStatistic', component: ZonalStatistic, name: 'zonalStatistic' },
      { path: 'eventhand', component: Eventhand },
      { path: '*', component: NotFound }
    ]
  },
  { path: '/login', component: Login },
  { path: '/logout',
    beforeEnter (to, from, next) {
      logout()
    }
  },
  { path: '*', component: NotFound }
]

const router = new VueRouter({
  mode: 'hash',
  base: __dirname,
  routes
})
export default router
