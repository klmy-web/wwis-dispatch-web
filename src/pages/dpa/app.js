import Vue from 'vue'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import store from './store'
import api from '@/utils/api'
import swal from 'sweetalert2'
import moment from 'moment'
import VueAMap, {lazyAMapApiLoaderInstance} from 'vue-amap'
import { momentReset, ArcGIS } from '@/utils/reset'
import { swalPrompt } from '@/utils/common'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'sweetalert2/dist/sweetalert2.min.css'
import 'vue-multiselect/dist/vue-multiselect.min.css'
import 'vue-image-lightbox/dist/vue-image-lightbox.min.css'
import 'assets/scss/styles.scss'
import 'assets/css/custom.sass'
import 'assets/css/iconfont.css'
// 引入地图
// echarts
import ECharts from 'vue-echarts/components/ECharts.vue'
import 'echarts/lib/chart/line'
import 'echarts/lib/chart/bar'
import 'echarts/lib/chart/pie'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/legend'
import 'echarts/lib/chart/map'
// import 'echarts/lib/chart/radar'
// import 'echarts/lib/chart/scatter'
// import 'echarts/lib/chart/effectScatter'
import 'echarts/lib/component/polar'
import 'echarts/lib/component/geo'
import 'echarts/lib/component/title'
// import 'echarts/lib/component/visualMap'
import 'echarts/lib/component/dataset'

// 引入ztree以及jq环境
import $ from 'jquery'
import 'ztree/js/jquery.ztree.all.min'
import 'ztree/js/jquery.ztree.exhide.min'
import 'ztree/css/zTreeStyle/zTreeStyle.css'

Vue.prototype.$$ = $

Vue.use(VueRouter)
Vue.use(ElementUI)
Vue.use(api)
Vue.use(VueAMap)
Vue.component('ECharts', ECharts)
// Vue.use(VueTouch, { name: 'v-touch' })
momentReset(moment)
Object.defineProperty(Vue.prototype, '$moment', { value: moment })
Object.defineProperty(Vue.prototype, '$swal', { value: swal })
Object.defineProperty(Vue.prototype, '$swalPrompt', { value: swalPrompt })

// 用VueAMap名引入不与原生高德AMap冲突
VueAMap.initAMapApiLoader({
  key: '055423e24a8756b527927b781bb95b57',
  plugin: [
    'Scale',
    'OverView',
    'ToolBar',
    'MapType',
    'PolyEditor',
    'PlaceSearch',
    'Geolocation',
    'Autocomplete',
    'Geocoder',
    'MouseTool',
    'Text'],
  uiVersion: '1.0', // ui库版本，不配置不加载,
  v: '1.4.4'
})
lazyAMapApiLoaderInstance.load().then(() => {
  // AMap.setMapStyle('amap://styles/blue')
})
ArcGIS(Vue, (Vue) => {
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    router,
    store,
    template: '<router-view/>'
  })
})
