import VueRouter from 'vue-router'

import { logout, pathRedirect, getAccount } from '@/utils/auth'

import Home from 'containers/Home'
import Login from 'containers/Login'
import NotFound from 'containers/Common/NotFound'

import Config from './containers/Config'
import Role from './containers/Role'
import Account from './containers/Account'
// import Dictionary from './containers/Dictionary'
import Menu from './containers/Menu'
import Organization from './containers/Organization'
import Log from './containers/Log'
import Notice from './containers/Notice'
import Test from './containers/Test'
import TestForm from './containers/TestForm'
import WorkFlow from './containers/WorkFlow'
import TaskDetail from './containers/WorkFlow/TaskDetail'
import FlowStep from './containers/WorkFlow/FlowStep'
import FlowAuth from './containers/WorkFlow/FlowAuth'
import Version from './containers/Version'

import ChangePassword from '@/containers/Setting/changePassword'

const routes = [
  { path: '/',
    component: Home,
    redirect: to => pathRedirect(to, '/system'),
    beforeEnter (to, from, next) {
      getAccount()
      next()
    },
    children: [
      { path: 'config', component: Config },
      { path: 'role', component: Role },
      { path: 'account', component: Account },
      { path: 'menu', component: Menu },
      { path: 'organization', component: Organization },
      { path: 'log', component: Log },
      { path: 'notice', component: Notice },
      { path: 'test', component: Test },
      { path: 'testForm', component: TestForm },
      { path: 'workFlow', component: WorkFlow },
      { path: 'taskDetail', component: TaskDetail },
      { path: 'flowStep', component: FlowStep },
      { path: 'flowAuth', component: FlowAuth },
      { path: 'version', component: Version },
      { path: 'setting/changePassword', component: ChangePassword },
      { path: '404', component: NotFound }
    ]
  },
  { path: '/login', component: Login },
  { path: '/logout',
    beforeEnter (to, from, next) {
      logout()
    }
  },
  { path: '*', component: NotFound }
]

const router = new VueRouter({
  mode: 'hash',
  base: __dirname,
  routes
})
export default router
