import Vue from 'vue'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui'
// import dxui from 'dx-ui'

import router from './router'
import store from './store'
import api from '@/utils/api'
import init from '@/utils/init'
import VueAMap from 'vue-amap'
import Viewer from 'v-viewer'

import 'viewerjs/dist/viewer.css'

// Vue.use(dxui)
Vue.use(api)
Vue.use(init)
Vue.use(VueAMap)

Vue.use(VueRouter)
Vue.use(ElementUI)
Vue.use(Viewer)

VueAMap.initAMapApiLoader({
  key: '055423e24a8756b527927b781bb95b57',
  plugin: ['AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.PlaceSearch', 'AMap.Geolocation', 'AMap.Autocomplete']
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<router-view/>'
})
