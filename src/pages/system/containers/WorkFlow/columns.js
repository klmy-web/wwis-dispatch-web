export const initColumns = {
  id: {
    label: '工作流实例ID',
    form: {
      type: 'input'
    }
  },
  flowDefineId: {
    label: '工作流',
    width: 130,
    relation: 'flowDefines',
    render: 'select',
    filter: {
      type: 'select',
      live: true,
      width: 140
    },
    form: {
      type: 'select',
      live: true,
      width: 250,
      rules: [{
        type: 'required',
        message: '请选择工作流'
      }]
    }
  },
  createUserId: {
    label: '创建人',
    width: 100,
    relation: 'users',
    render: {
      type: 'select'
    }
  },
  createTime: {
    label: '创建时间',
    width: 150,
    render: {
      type: 'time'
    },
    sort: true,
    dateType: 'YYYY-MM-DD HH:mm'
  },
  updateUserId: {
    label: '更新人',
    width: 100,
    relation: 'users',
    render: {
      type: 'select'
    }
  },
  updateTime: {
    label: '更新时间',
    width: 150,
    render: {
      type: 'time'
    },
    sort: true,
    dateType: 'YYYY-MM-DD HH:mm'
  },
  remark: {
    label: '备注说明',
    form: {
      type: 'textarea'
    }
  },
  enable: {
    label: '是否启用',
    form: {
      type: 'switch',
      width: 80
    },
    relation: 'yesOrNo',
    render: 'select'
  }
}
