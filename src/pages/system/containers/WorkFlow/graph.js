export const renderGraph = ({
  text = '流程图展示',
  data = [],
  link = []
}) => {
  return {
    title: {
      text: text
    },
    tooltip: {},
    animationDurationUpdate: 1500,
    animationEasingUpdate: 'quinticInOut',
    series: [
      {
        type: 'graph',
        layout: 'none',
        symbolSize: 100,
        roam: true,
        hoverAnimation: true,
        focusNodeAdjacency: true, // 高亮对应的所有节点及流向
        label: {
          normal: {
            show: true
          }
        },
        edgeSymbol: ['circle', 'arrow'], // 箭头2端样式
        edgeSymbolSize: [10, 20], // 箭头2端大小
        edgeLabel: {
          normal: {
            textStyle: {
              fontSize: 16
            }
          }
        },
        /* 动态接受节点数组 */
        data: data,
        // data: [{
        //   name: '节点2',
        //   x: 800, // 节点用户绘画时候的x轴位置
        //   y: 300, // 节点用户绘画时候的y轴位置
        //   itemStyle: {
        //     color: 'red' // 节点背景颜色
        //   },
        //   symbol: 'circular' // 标记类型包括 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none', 默认是circle
        // }],
        /* 动态配置指向数组 */
        links: link,
        /* links: [{
          source: 0,
          target: 1,
          symbolSize: [5, 20],
          label: {
            normal: {
              show: true // 显示线上数据
            }
          },
          lineStyle: {
            normal: {
              width: 5,
              curveness: 0.2,
              color: '#91c7ae'
            }
          }
        }], */
        /* 线样式 - 全局配置 */
        lineStyle: {
          normal: {
            opacity: 0.9,
            width: 2,
            curveness: 0 // 曲度，值0-1，成正比
          }
        }
      }
    ]
  }
}
