import { uniq } from 'lodash'
/**
 *renderForm
 * @param {columns} 渲染的以id为key的动态columns列表
 * @param {data} 动态columns对应的data
 * @return {formList} smartForm的字段列表
 * @return {formColumns} smartForm对应的字段解释
 * @return {relationKeys} columns中对应的relation的关键字列表
 * @author linmn
 */
export function renderForm ({columns = {}, data = {}} = {}) {
  const formColumns = {}
  const relationKeys = []
  const formValues = {}
  const formList = Object.keys(columns).sort((a, b) => {
    // 排序
    return columns[a].sort - columns[b].sort > 0
  }).map(key => {
    // columns
    formColumns[columns[key].name] = {
      ...columns[key],
      form: {
        ...columns[key].form,
        ...hasQuickReply(columns[key])
      }
    }
    // columns.form
    if (columns[key].relation) {
      // relation
      relationKeys.push(columns[key].relation)
    }
    // 默认值
    formValues[columns[key].name] = columns[key].defaultValue
    return columns[key].name
  })
  return {
    formList: uniq(formList),
    formColumns,
    formValues,
    relationKeys
  }
}
// 是否有快捷回复
const hasQuickReply = (column = []) => {
  if (!column.quickReply || !column.quickReply.length) return {}
  return {
    quickReply: {
      value: column.quickReply || [],
      initShow: 4
    }
  }
}

export function getDyncRelation ({target = this, columns = {}} = {}) {
  const relationKeys = Object.keys(columns).filter(item => columns[item].relation).map(res => columns[res].relation)
  // console.log('relationKeys:', relationKeys)
  if (relationKeys.length) {
    target.$getRelation({
      relations: relationKeys.join(',')
    }).then(data => {
    //  this.isGetRelation = true
      Object.keys(columns).map(key => {
        const relation = columns[key].relation
        if (data[relation]) {
          const nOptions = [...columns[key].optionsInit || [], ...data[relation]].map(option => {
            const label = option[columns[key].labelName || 'label']
            return {
              value: option.value !== undefined ? option.value : option.id,
              label,
              ...option
            }
          })
          // console.log('key', key)
          // console.log('nOptions', nOptions)
          target.$set(columns[key], 'options', nOptions)
        }
      })
      // target.$set(target.columns[key], 'options', nOptions)
    })
  }
  return {
    columns,
    isGetRelation: true
  }
}
export function renderFormByArray (stepDatas) {
  let columns = {}
  let formList = []
  stepDatas.map((step, index) => {
    const userOptions = step.users.map(res => {
      return {
        value: res.id,
        label: res.name
      }
    })
    columns[`${step.stepDefineId}`] = {
      label: step.stepDefineName,
      form: {
        type: 'select',
        width: 200,
        live: true
      },
      options: userOptions
    }
    formList = [...formList, `${step.stepDefineId}`].sort((a, b) => {
      return a - b > 0
    })
  })
  return {
    columns: columns,
    formList: formList
  }
}
export function renderValuesForForm (formValues) {
  return Object.keys(formValues).map(key => {
    return {
      'stepDefineId': Number(key),
      'userId': formValues[key]
    }
  })
}

/**
     * 根据实例列表筛选出不同状态的实例数组
     */
export function filterInstanceByStatus (iList, dList) {
  // 业务节点列表
  const BIZList = dList.filter(list => list.stepNature !== 'FUNC')
  // 已完成列表 ： 实例列表中状态已完成的业务节点
  const doneList = iList.filter(list => list.status === 'FINISH' && list.stepNature !== 'FUNC')
  // 已完成节点
  const doneNode = doneList.map(res => res.stepDefineId)
  // 活动列表 ： 实例列表中状态活动中的业务节点
  const actList = iList.filter(list => list.status === 'ACTIVATY' && list.stepNature !== 'FUNC').map(res => {
    return {
      ...res,
      isGroup: true,
      content: `${res.stepDefineName || res.name} - ${res.operatorName || ''}`
    }
  })
  const actNode = actList.map(res => res.stepDefineId)
  // 待完成节点： 业务节点中剩下的部分节点
  const todoNode = BIZList.filter(list => {
    return [...doneNode, ...actNode].indexOf(list.id) === -1
  })
  return {
    doneList,
    doneNode,
    actList,
    actNode,
    BIZList,
    todoNode
  }
}
