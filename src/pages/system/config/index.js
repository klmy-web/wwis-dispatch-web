import account from './account'
import log from './log'
import organization from './organization'
import menu from './menu'
import role from './role'
import workFlow from './workFlow'
import columns from './columns'

export default {
  account,
  log,
  organization,
  menu,
  role,
  columns,
  workFlow
}
