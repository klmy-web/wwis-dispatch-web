import systemColumns from './columns.js'
export default {
  columns: {
    ...systemColumns,
    name: {
      label: '角色名称',
      filter: {
        type: 'input',
        like: true
      },
      form: {
        type: 'input',
        rules: [{
          type: 'required',
          message: '请输入角色名称'
        }]
      }
    },
    menuIds: {
      label: '权限',
      render: {
        type: 'multiselect'
      },
      relation: 'menus',
      form: {
        type: 'tree'
      }
    },
    enabled: {
      label: '启用状态',
      width: 120,
      render: {
        type: 'select'
      },
      sort: true,
      relation: 'enabled',
      filter: {
        type: 'select'
      },
      form: {
        type: 'select'
      }
    }
  }
}
