import systemColumns from './columns.js'
export default {
  columns: {
    ...systemColumns,
    name: {
      label: '组织名称',
      filter: {
        type: 'input',
        like: true
      },
      form: {
        type: 'input',
        rules: [{
          type: 'required',
          message: '请输入组织名称'
        }, {
          type: 'maxLength',
          value: 30,
          message: '长度不能大于30'
        }]
      }
    },
    parentId: {
      label: '上级组织ID',
      form: {
        type: 'hidden',
        dataSource: 'string'
      }
    },
    orgTypeId: {
      label: '组织类型',
      relation: 'orgType',
      filter: {
        type: 'select',
        width: 100
      },
      form: {
        type: 'select',
        rules: [{
          type: 'required',
          message: '请输入组织类型'
        }]
      }
    },
    orgTypeName: {
      label: '组织类型',
      width: 98,
      filter: {
        type: 'input',
        like: true
      }
    }
  }
}
