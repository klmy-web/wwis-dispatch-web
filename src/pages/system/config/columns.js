/*
**系统管理公用字段配置
* Auhor: Manielam
*/
export default {
  id: {
    label: 'ID',
    width: 60,
    style: {
      textAlign: 'center'
    }
  },
  remark: {
    label: '备注',
    filter: {
      type: 'input',
      like: true
    },
    form: {
      type: 'input',
      large: true,
      isColumn: true,
      rules: [{
        type: 'maxLength',
        value: 120,
        message: '字数不能大于120个字'
      }]
    }
  },
  loginPassword: {
    label: '密码',
    form: {
      type: 'password',
      rules: [{
        type: 'required',
        message: '请输入密码'
      }, {
        type: 'reg',
        value: /^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z]).*$/,
        message: '密码为字母加数字，最少六位'
      }]
    }
  },
  newPassword: {
    label: '新密码',
    form: {
      type: 'password',
      rules: [{
        type: 'required',
        message: '请输入密码'
      }, {
        type: 'reg',
        value: /^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z]).*$/,
        message: '密码为字母加数字，最少六位'
      }]
    }
  },
  startTime: {
    label: '开始时间',
    width: 160,
    render: {
      type: 'time'
    },
    sort: true,
    dataType: 'YYYY-MM-DD hh:mm'
  },
  endTime: {
    label: '结束时间',
    render: {
      type: 'time'
    },
    width: 160,
    sort: true,
    dateType: 'YYYY-MM-DD hh:mm'
  },
  updateTime: {
    label: '更新时间',
    render: {
      type: 'time'
    },
    width: 160,
    sort: true,
    dataType: 'YYYY-MM-DD HH:mm:ss'
  },
  createTime: {
    label: '创建时间',
    render: {
      type: 'time'
    },
    width: 160,
    sort: true,
    dataType: 'YYYY-MM-DD HH:mm:ss'
  },
  applyTime: {
    label: '发起时间',
    render: {
      type: 'time'
    },
    width: 160,
    sort: true,
    dateType: 'YYYY-MM-DD HH:mm'
  },
  address: {
    label: '地址',
    form: {
      type: 'input',
      width: 160
    },
    filter: {
      width: 200,
      type: 'input',
      like: true
    }
  },
  linkTel: {
    label: '联系电话',
    width: 124,
    minLength: 200,
    style: {
      textAlign: 'center'
    },
    filter: {
      width: 100,
      type: 'input'
    },
    form: {
      type: 'input'
    }
  },
  linkPhone: {
    label: '联系手机',
    style: {
      textAlign: 'center'
    },
    width: 100,
    minLength: 200,
    filter: {
      width: 100,
      type: 'input'
    },
    form: {
      type: 'input'
    }
  },
  caliber: {
    label: '口径/位数',
    width: 120,
    style: {
      textAlign: 'center'
    },
    render: ({data}) => {
      return data ? 'DN' + data : ''
    }
  },
  areaCode: {
    label: '区',
    width: 70,
    sort: true,
    style: {
      textAlign: 'center'
    },
    filter: {
      width: 70,
      type: 'input'
    },
    render: {
      type: 'padString',
      padRender: 'start',
      length: 3,
      charWith: 0
    }
  },
  bookCode: {
    label: '本',
    width: 70,
    sort: true,
    style: {
      textAlign: 'center'
    },
    filter: {
      width: 70,
      type: 'input'
    },
    render: {
      type: 'padString',
      padRender: 'start',
      length: 2,
      charWith: 0
    }
  },
  pageCode: {
    label: '页',
    width: 70,
    style: {
      textAlign: 'center'
    },
    filter: {
      width: 70,
      type: 'input'
    },
    render: {
      type: 'padString',
      padRender: 'start',
      length: 3,
      charWith: 0
    }
  },
  origin: {
    label: '水表产地',
    width: 100,
    form: {
      type: 'text'
    }
  },
  bookId: {
    label: `册本号`,
    width: 90,
    relation: 'books',
    filter: {
      type: 'select',
      width: 115,
      live: true,
      rules: [{
        type: 'required',
        message: '请选择册本'
      }]
    }
  },
  bookName: {
    label: `册本名称`,
    width: 115
  },
  // 数组渲染树级的客服分区
  orgPath: {
    label: '客服分区',
    width: 112,
    relation: 'orgsDepartmentByAuth',
    render: ({ data, column }) => {
      const item = column.options.find(col => { return col.value === data[1] })
      return item ? item.label : '-'
    }
  },
  readingUserId: {
    label: '抄表员',
    render: {
      type: 'select'
    },
    width: 84,
    style: {
      textAlign: 'center'
    },
    minLength: 110,
    relation: 'readingUsersByAuth',
    form: {
      type: 'select',
      live: true,
      rules: [{
        type: 'required',
        message: '请选择抄表员'
      }]
    },
    filter: {
      width: 124,
      type: 'select',
      live: true
    },
    printWidth: 60
  },
  readingUserName: {
    label: '抄表员',
    width: 84,
    style: {
      textAlign: 'center'
    }
  },
  scheduleId: {
    label: '排期',
    filter: {
      type: ''
    }
  },
  bookTypeId: { // 做filterByAlways用的
    label: '抄表类型',
    relation: 'bookType',
    render: {
      type: 'select'
    },
    filter: {
      type: ''
    }
  },
  scheduleTypeId: {
    label: '抄表类型',
    relation: 'scheduleType',
    render: {
      type: 'select'
    },
    filter: {
      type: ''
    },
    form: {
      type: 'select',
      live: true
    }
  },
  jobNumber: {
    label: '工号',
    width: 100,
    sort: true,
    filter: {
      type: 'input',
      like: true
    },
    form: {
      type: 'input',
      rules: [{
        type: 'required',
        message: '请输入工号'
      }]
    }
  },
  loginName: {
    label: '账号',
    width: 100,
    filter: {
      type: 'input',
      width: 110,
      like: true
    },
    form: {
      type: 'input',
      rules: [{
        type: 'required',
        message: '请输入账号'
      }]
    }
  },
  name: {
    label: '姓名',
    width: 120,
    filter: {
      type: 'input',
      width: 110,
      like: true
    },
    form: {
      type: 'input',
      rules: [{
        type: 'required',
        message: '请输入姓名'
      }]
    }
  },
  orgId: {
    label: '所属部门',
    width: 120,
    render: {
      type: 'select'
    },
    relation: 'orgs',
    filter: {
      type: 'cascader',
      live: true,
      onlySelectLeaf: false,
      placeholder: '请选择部门'
    },
    form: {
      type: 'cascader',
      live: true,
      onlySelectLeaf: false,
      placeholder: '请选择部门',
      rules: [{
        type: 'required',
        message: '请选择部门'
      }]
    }
  },
  roleIds: {
    label: '角色',
    render: {
      type: 'multiselect'
    },
    relation: 'roles',
    width: 160,
    form: {
      type: 'select',
      multiple: true,
      live: true
    }
  },
  title: {
    label: '头衔',
    form: {
      type: 'input'
    }
  },
  email: {
    label: '电子邮箱',
    filter: {
      type: 'input',
      like: true
    },
    form: {
      type: 'input'
    }
  },
  phone: {
    label: '电话',
    filter: {
      type: 'input',
      like: true
    },
    form: {
      type: 'input'
    }
  },
  mobile: {
    label: '手机',
    filter: {
      type: 'input',
      like: true
    },
    form: {
      type: 'input'
    }
  },
  qq: {
    label: 'QQ',
    form: {
      type: 'input'
    }
  },
  nickname: {
    label: '昵称',
    form: {
      type: 'input'
    }
  },
  sex: {
    label: '性别',
    render: {
      type: 'select'
    },
    form: {
      type: 'select'
    }
  },
  enabled: {
    label: '是否可见',
    width: 98,
    render: {
      type: 'select'
    },
    relation: 'yesOrNo',
    filter: {
      type: 'select',
      width: 100
    },
    form: {
      type: 'switch',
      width: 100
    }
  }
}
