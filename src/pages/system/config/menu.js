import systemColumns from './columns.js'
export default {
  columns: {
    ...systemColumns,
    name: {
      label: '菜单名称',
      style: {
        'text-align': 'left'
      },
      filter: {
        type: 'input',
        like: true
      },
      form: {
        type: 'input',
        rules: [{
          type: 'required',
          message: '请输入菜单名称'
        }, {
          type: 'maxLength',
          value: 30,
          message: '长度不能大于30'
        }]
      }
    },
    enName: {
      label: '英文名称',
      form: {
        type: 'input',
        rules: [{
          type: 'maxLength',
          value: 30,
          message: '长度不能大于30'
        }]
      }
    },
    actionIds: {
      label: '接口权限',
      render: {
        type: 'multiselect'
      },
      relation: 'actions',
      form: {
        type: 'tree'
      }
    },
    menuTypeId: {
      label: '菜单类型',
      relation: 'menuType',
      filter: {
        type: 'select',
        width: 100
      },
      form: {
        type: 'select',
        rules: [{
          type: 'required',
          message: '请输入菜单类型'
        }]
      }
    },
    menuTypeName: {
      label: '菜单类型',
      width: 98,
      filter: {
        type: 'input',
        like: true
      }
    },
    url: {
      label: '菜单路径',
      style: {
        'text-align': 'left'
      },
      filter: {
        type: 'input',
        like: true
      },
      form: {
        type: 'input',
        like: true
      }
    },
    icon: {
      label: '菜单图标',
      width: 100,
      form: {
        type: 'input'
      }
    },
    sort: {
      label: '排序',
      width: 70,
      form: {
        type: 'input',
        dataType: 'number'
      }
    },
    color: {
      label: '颜色',
      width: 70,
      form: {
        type: 'input'
      }
    }
  }
}
