import systemColumns from './columns.js'
export default {
  tableInitList: ['userId', 'loginName', 'moduleId', 'controllerId', 'actionId', 'actionPath', 'desc', 'success', 'logTime'],
  tableFullList: ['userId', 'loginName', 'moduleId', 'controllerId', 'actionId', 'actionPath', 'desc', 'success', 'logTime'],
  filterInitList: ['userId', 'moduleId', 'success'],
  filterFullList: ['userId', 'moduleId', 'success'],
  columnGroupBy: [{
    label: '操作人信息',
    filter: ['userId', 'loginName', 'controllerId', 'desc', 'success', 'logTime']
  }, {
    label: '功能点信息',
    filter: ['moduleId', 'controllerId', 'actionId', 'actionPath', 'desc', 'success', 'logTime']
  }],
  columns: {
    ...systemColumns,
    userId: {
      label: '操作用户',
      width: 120,
      render: {
        type: 'select'
      },
      relation: 'users',
      filter: {
        type: 'select',
        live: true
      }
    },
    loginName: {
      label: '操作账号'
    },
    clientIp: {
      label: '操作用户IP',
      width: 120
    },
    moduleId: {
      label: '模块',
      width: 140,
      render: {
        type: 'select'
      },
      relation: 'menus',
      filter: {
        type: 'select',
        width: 140
      }
    },
    controllerId: {
      label: '功能',
      width: 120,
      render: {
        type: 'select'
      },
      relation: 'controllers',
      filter: {
        type: 'select',
        width: 100
      }
    },
    actionId: {
      label: '功能点',
      width: 120,
      render: {
        type: 'select'
      },
      relation: 'actions',
      filter: {
        type: 'select',
        width: 100
      }
    },
    actionPath: {
      label: '功能点路径'
    },
    desc: {
      label: '说明'
    },
    success: {
      label: '是否成功',
      width: 98,
      render: {
        type: 'select'
      },
      relation: 'yesOrNo',
      filter: {
        type: 'select',
        width: 100
      }
    },
    logTime: {
      label: '操作时间',
      render: {
        type: 'time'
      },
      sort: true,
      dateType: 'YYYY-MM-DD HH:mm:ss'
    }
  }
}
