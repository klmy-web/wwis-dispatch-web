export const columns = {
  orderCode: {
    label: '工单编号'
  },
  applyOrgName: {
    label: '发起人部门'
  },
  applyUserName: {
    label: '发起人'
  },
  applyTime: {
    label: '申请时间',
    dateType: 'YYYY-MM-DD HH:mm',
    render: 'time'
  },
  status: {
    label: '状态',
    relation: 'flowInstanceStatus',
    render: 'select',
    minLength: 99999,
    form: {
      type: 'select',
      live: true
    }
  },
  createTime: {
    label: '创建时间',
    dateType: 'YYYY-MM-DD HH:mm',
    render: {
      type: 'time',
      dateType: 'YYYY-MM-DD HH:mm'
    }
  },
  updateTime: {
    label: '最新更新时间',
    dateType: 'YYYY-MM-DD HH:mm',
    render: {
      type: 'time',
      dateType: 'YYYY-MM-DD HH:mm'
    }
  }
}
