import systemColumns from './columns.js'
export default {
  tableInitList: ['loginName', 'jobNumber', 'name', 'orgId', 'roleIds', 'linkTel', 'linkPhone', 'enabled', 'remark'],
  tableFullList: ['loginName', 'jobNumber', 'name', 'orgId', 'roleIds', 'linkTel', 'linkPhone', 'enabled', 'remark'],
  filterInitList: ['loginName', 'jobNumber', 'name', 'enabled'],
  filterFullList: ['loginName', 'jobNumber', 'name', 'enabled', 'remark'],
  createList: ['loginName', 'jobNumber', 'loginPassword', 'name', 'orgId', 'linkTel', 'linkPhone', 'enabled', 'remark', 'roleIds'],
  updateList: ['name', 'orgId', 'linkTel', 'linkPhone', 'enabled', 'remark', 'roleIds'],
  columnGroupBy: [{
    label: '基本信息',
    filter: ['loginName', 'name', 'roleIds', 'linkTel', 'linkPhone', 'enabled', 'remark']
  }, {
    label: '公司信息',
    filter: ['loginName', 'jobNumber', 'name', 'orgId', 'linkPhone', 'roleIds']
  }],
  columns: {
    ...systemColumns,
    id: {
      label: 'ID',
      width: 60
    },
    loginName: {
      label: '账号',
      width: 115,
      filter: {
        type: 'input',
        like: true
      },
      form: {
        type: 'input',
        rules: [{
          type: 'required',
          message: '请输入账号'
        }]
      }
    },
    jobNumber: {
      label: '工号',
      width: 115,
      sort: true,
      filter: {
        type: 'input',
        like: true
      },
      form: {
        type: 'input',
        rules: [{
          type: 'required',
          message: '请输入工号'
        }]
      }
    },
    loginPassword: {
      label: '密码',
      form: {
        type: 'password',
        rules: [{
          type: 'required',
          message: '请输入密码'
        }, {
          type: 'reg',
          value: /^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z]).*$/,
          message: '密码为字母加数字，最少六位'
        }]
      }
    },
    newPassword: {
      label: '新密码',
      form: {
        type: 'password',
        rules: [{
          type: 'required',
          message: '请输入密码'
        }, {
          type: 'reg',
          value: /^.*(?=.{6,})(?=.*\d)(?=.*[a-zA-Z]).*$/,
          message: '密码为字母加数字，最少六位'
        }]
      }
    },
    name: {
      label: '姓名',
      width: 120,
      filter: {
        type: 'input',
        like: true
      },
      form: {
        type: 'input',
        rules: [{
          type: 'required',
          message: '请输入姓名'
        }]
      }
    },
    orgId: {
      label: '所属部门',
      width: 160,
      render: {
        type: 'select'
      },
      relation: 'orgs',
      filter: {
        type: 'cascader',
        live: true,
        onlySelectLeaf: false,
        placeholder: '请选择部门'
      },
      form: {
        type: 'cascader',
        live: true,
        onlySelectLeaf: false,
        placeholder: '请选择部门',
        rules: [{
          type: 'required',
          message: '请选择部门'
        }]
      }
    },
    roleIds: {
      label: '角色',
      render: {
        type: 'multiselect'
      },
      relation: 'roles',
      width: 160,
      form: {
        type: 'select',
        multiple: true,
        live: true,
        rules: [{
          type: 'required',
          message: '请选择角色'
        }]
      }
    },
    enabled: {
      label: '启用状态',
      width: 105,
      sort: true,
      render: {
        type: 'select'
      },
      relation: 'enabled',
      style: {
        textAlign: 'center'
      },
      filter: {
        type: 'select',
        width: 120
      },
      form: {
        type: 'select'
      }
    }
  }
}
