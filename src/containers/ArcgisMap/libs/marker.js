export default {
  data () {
    return {
      markersObject: {}
    }
  },
  methods: {
    createMarker (marker) {
      let symbol, point, graphic
      symbol = new this.$arcgis.PictureMarkerSymbol(...marker.symbol)
      point = new this.$arcgis.Point({
        x: marker.position[0],
        y: marker.position[1]
      })
      graphic = new this.$arcgis.Graphic(point, symbol, marker.data)
      // 将所有marker实例存放入markersObject去管理
      this.markersObject[marker.data.id] = graphic
    },
    changeMarkers (markers) {
      this.markersLayer.clear()
      // FIXME: 存在问题：当marker自身内部数据改变时，无法更新
      markers.map(marker => {
        // 控制调用createMarker的次数
        if (!this.markersObject[marker.data.id]) {
          this.createMarker(marker)
        }
        // console.log("==change markers==" + JSON.stringify(this.markersObject[marker.data.id]))
        this.markersLayer.add(this.markersObject[marker.data.id])
      })
      // FIXME: 做相应配置，当不使用点聚合功能时不调用该函数
      // if (this.map.getZoom() <= 13) {
      //   this.createClusterLayer(markers)
      // }
    },
    // 设置坐标点的显示size
    setSymbolSize (graphic, width, height) {
      let symbol = new this.$arcgis.PictureMarkerSymbol(graphic.symbol.url, width, height)
      graphic.setSymbol(symbol)
    },
    // 选中搜索定位的marker
    selectMarker (position, zoom) {
      this.map.centerAndZoom(position, zoom)
    },
    // 鼠标事件
    markerOn () {
      this.$arcgis.on(this.markersLayer, 'click', e => {
        this.$emit('selectMarker', e.graphic.attributes)
        this.isClick = {
          is: true,
          lastMarker: e
        }
        this.setSymbolSize(e.graphic, 38, 47)
        setTimeout(() => {
          this.selectMarker([e.graphic.geometry.x, e.graphic.geometry.y], 16)
        }, 1000)
        this.map.infoWindow.show(e.graphic.geometry)
      })
      this.$arcgis.on(this.markersLayer, 'mouse-over', e => {
        this.isClick.is = false
        if (this.isClick.lastMarker) {
          this.setSymbolSize(this.isClick.lastMarker.graphic, 31, 40)
        }
        this.setSymbolSize(e.graphic, 38, 47)
        this.$emit('setMarkerContent', e)
        // this.map.infoWindow.show(e.graphic.geometry)
        this.map.infoWindow.show(e.screenPoint, this.map.getInfoWindowAnchor(e.screenPoint))
      })
      this.$arcgis.on(this.markersLayer, 'mouse-out', e => {
        if (!this.isClick.is) {
          this.setSymbolSize(e.graphic, 31, 40)
          this.map.infoWindow.hide()
        }
      })
    }
  }
}
