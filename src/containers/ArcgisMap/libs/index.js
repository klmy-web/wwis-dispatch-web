import markersMixin from './marker'
import infoWindowMixin from './infoWindow'
import areaLayerMixin from './areaLayer'
import featuresMixin from './features'
import mapMixin from './map'

export default [
  mapMixin,
  markersMixin,
  infoWindowMixin,
  areaLayerMixin,
  featuresMixin
]
