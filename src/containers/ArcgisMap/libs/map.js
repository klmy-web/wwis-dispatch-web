export default {
  methods: {
    // 回归地图初始状态
    returnToMap () {
      // TODO: 保存回归图像最大范围功能，勿删
      // let FeatureExtent
      // if (this.markersLayer.graphics.length !== 0) {
      //   FeatureExtent = this.$arcgis.graphicsUtils.graphicsExtent(this.markersLayer.graphics)
      //   this.map.setExtent(FeatureExtent.expand(2))
      // }
      this.map.centerAndZoom([84.873946, 45.595886], 13)
    },
    // 设置地图的缩放级别
    setZoom (zoom) {
      this.map.setZoom(this.map.getZoom() + zoom)
    },
    mapOn () {
      // 双击张开地图事件,回归地图进入左拉框前的原始状态
      this.$arcgis.on(this.map, 'dbl-click', e => {
        this.$emit('openMap', e)
        setTimeout(() => {
          // this.selectMarker(this.mapOptions.center, this.mapOptions.zoom)
          this.map.centerAndZoom([84.873946, 45.595886], 13)
        }, 1000)
      })
      // 监听比例尺变化事件，控制点聚合图层与坐标点图层的切换
      // this.$arcgis.on(this.map, 'zoom-end', () => {
      //   // FIXME: 引用this.markers作为参数会不会存在问题
      //   if (this.map.getZoom() <= 13) {
      //     // TODO: this.clusterLayer变量指向不明
      //     if (this.map.getLayer('clusterLayer').visible === false) {
      //       this.map.getLayer('clusterLayer').show()
      //     }
      //     this.createClusterLayer(this.markers)
      //   } else {
      //     if (this.map.getLayer('clusterLayer').visible === true) {
      //       // this.clusterLayer.hide()
      //       this.map.getLayer('clusterLayer').hide()
      //     }
      //     this.changeMarkers(this.markers)
      //   }
      // })
      // 单击地图事件
      this.$arcgis.on(this.map, 'click', () => {
        this.map.infoWindow.hide()
        this.$emit('mapClick')
      })
    },
    // FIXME: 有没有必要合并三个函数
    // 设置infowindow的内容
    setAreaMesg (title, content) {
      this.map.infoWindow.setTitle(title)
      this.map.infoWindow.setContent(content)
    },
    // 设置Infowindow的样式
    renderInfoWindow (status) {
      let wrapDOM = document.getElementsByClassName('esriPopup')[0]
      wrapDOM.setAttribute('class', `esriPopup esriPopupVisible popup-${status}`)
      // let maximize = document.getElementsByClassName('maximize')[0]
      // 关闭infowindow放大功能按键
      // maximize.setAttribute('style', 'display: none;')
    },
    // 设置Infowindow的样式和内容
    setInfowindow (title, content, style) {
      this.renderInfoWindow(style)
      this.setAreaMesg(title, content)
    },
    // 切换地图图层的显示
    changeLayer (layerNames, layers) {
      this.map[layers].map(layer => {
        if (layer === 'markersLayer') {
          return false
        } else {
          this[layer].hide()
        }
      })
      // 采用数组来存放要显示的图层，为了适应单个图层显示与多个图层同时显示
      layerNames.map(layerName => {
        if (this.map[layers].includes(layerName)) {
          this[layerName].show()
        }
      })
    },
    // FIXME: 逻辑问题
    // 改变管线图层的图层定义
    changeGLLayer (GLList) {
      let layerDefinitions = []
      if (GLList.length) {
        if (GLList.indexOf('GJ >= 200') >= 0 && GLList.indexOf('GJ < 200') >= 0) {
          layerDefinitions.push('GJ >= 0')
        } else if (GLList.indexOf('GJ >= 200') >= 0) {
          layerDefinitions.push('GJ >= 200')
        } else if (GLList.indexOf('GJ < 200') >= 0) {
          layerDefinitions.push('GJ < 200')
        } else {
          layerDefinitions.push('GJ < 0')
        }
      } else {
        layerDefinitions.push('GJ < 0')
      }
      this.$nextTick(() => {
        if (layerDefinitions.length && this.layerN) {
          this.layerN.setLayerDefinitions(layerDefinitions)
        }
      })
    }
  }
}
