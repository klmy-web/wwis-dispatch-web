import { cloneDeep } from 'lodash'

export const operationCreate = {
  label: '新增',
  name: 'create',
  icon: 'plus',
  tooltip: true,
  placement: 'left',
  func: operationEventCreate
}

export const operationUpdate = {
  label: '编辑',
  name: 'update',
  icon: 'edit',
  tooltip: true,
  func: operationEventUpdate
}

export const operationDelete = {
  label: '删除',
  name: 'delete',
  icon: 'delete',
  tooltip: true,
  func: operationEventDelete
}

export function operationEventCreate ({funcProps}) {
  const { table, data } = funcProps
  // const { labelName } = table
  const labelName = table.labelName ? '【' + data[table.labelName] + '】' : ''
  table.dialogLabel = '新增' + labelName + '子' + table.label
  table.dialogFormValues = cloneDeep(table.createFormValues)
  table.dialogFormValues[table.pid] = data[table.cid]
  table.dialogFormList = table.createList
  table.dialogButtonList = table.createButtonList
  table.dialogVisible = true
}

export function operationEventUpdate ({funcProps}) {
  const { table, data } = funcProps
  const labelName = table.labelName ? '【' + data[table.labelName] + '】' : ''
  table.dialogLabel = '编辑' + table.label + labelName
  table.dialogFormValues = cloneDeep(data)
  table.dialogFormList = table.updateList
  table.dialogButtonList = table.updateButtonList
  table.dialogVisible = true
}

export function operationEventDelete ({funcProps}) {
  const { table, data } = funcProps
  const labelName = table.labelName ? '【' + data[table.labelName] + '】' : ''
  table.$swal({
    title: '提示',
    text: '删除' + table.label + labelName + ', 是否继续?',
    type: 'question',
    showCloseButton: true,
    showCancelButton: true,
    focusConfirm: false,
    buttonsStyling: false,
    confirmButtonClass: 'btn btn-danger',
    cancelButtonClass: 'btn btn-default',
    confirmButtonText: '删除',
    cancelButtonText: '取消'
  }).then((result) => {
    if (result.value === undefined && result) {
      console.log('deleteResource-', table.deleteResource, data.id)
      const resource = table.deleteResource || table.resource
      table.$httpDelete({
        url: resource + '/' + data.id
      }).then(data => {
        if (data) {
          table.getData()
          table.$message({
            type: 'success',
            message: '删除成功!'
          })
        }
      })
    }
  }, (dismiss) => {})
}
