import { setCurrentNav, getCurrentNav } from '@/utils/auth'

export const currentPath = (currentRoute) => {
  const paths = currentRoute.matched
  return paths[paths.length - 1].path
}

export const fullPath = (currentRoute, navList) => {
  const path = `${window.location.pathname}#${currentPath(currentRoute)}`
  const currentNav = navList.find(nav => nav.url === path) || getCurrentNav()
  setCurrentNav(currentNav)
  return pathArray([currentNav], navList)
}

export const pathArray = (paths, navList) => {
  if (paths[0].type === 'SYSTEM') {
    return paths
  } else {
    const parentNav = navList.find(nav => nav.id === paths[0].parentId)
    return pathArray([parentNav, ...paths], navList)
  }
}
