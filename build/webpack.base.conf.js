'use strict'

const { join, resolve } = require('path')
const webpack = require('webpack')
const fs = require('fs')
const glob = require('glob')
const { execSync } = require('child_process')

const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const extractCSS = new ExtractTextPlugin({
  filename: '[name]/[name].css',
  allChunks: true
})
const extractSASS = new ExtractTextPlugin({
  filename: '[name]/[name].css',
  allChunks: true
})

const entries = {}
const chunks = []
const htmlWebpackPluginArray = []
try {
  fs.accessSync('version.json', fs.constants.R_OK | fs.constants.W_OK)
} catch (err) {
  execSync('cp build/cpversion.json version.json', function (error) {
  })
}
const version = require('../version.json')
// 多页面入口
glob.sync('./src/pages/**/app.js').forEach(path => {
  const chunk = path.split('./src/pages/')[1].split('/app.js')[0]
  if(chunk !== 'dispatch' && chunk !== 'home') return
  entries[chunk] = path
  chunks.push(chunk)
  const filename = chunk === 'home' ? 'index.html' : chunk + '/index.html'
  const htmlConf = {
    filename: filename,
    version: `${version.env}-V${version.version}.${version.subversion}`,
    template: path.replace(/.js/g, '.html'),
    inject: 'body',
    favicon: './src/assets/img/logo.png',
    hash: true,
    chunks: ['commons', chunk]
  }
  htmlWebpackPluginArray.push(new HtmlWebpackPlugin(htmlConf))
})

const styleLoaderOptions = {
  loader: 'style-loader',
  options: {
    sourceMap: true
  }
}
const cssOptions = [
  { loader: 'css-loader', options: { sourceMap: true } },
  { loader: 'postcss-loader', options: { sourceMap: true } }
]
const sassOptions = [...cssOptions, {
  loader: 'sass-loader',
  options: {
    sourceMap: true
  }
}]
const config = {
  entry: entries,
  output: {
    path: resolve(__dirname, '../dist/release'),
    filename: '[name]/[name].js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.vue'],
    alias: {
      'assets': join(__dirname, '../src/assets'),
      'components': join(__dirname, '../src/components'),
      'containers': join(__dirname, '../src/containers'),
      '@': join(__dirname, '../src'),
      'async-validator': 'async-validator/lib/index.js',
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  externals: {
    CLODOP: 'window.CLODOP',
    getCLodop: 'window.getCLodop'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            css: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
              use: cssOptions,
              fallback: styleLoaderOptions
            })),
            scss: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
              use: sassOptions,
              fallback: styleLoaderOptions
            })),
            postcss: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
              use: cssOptions,
              fallback: styleLoaderOptions
            }))
          }
        }
      },
      {
        test: /\.pug$/,
        use: 'pug-loader',
        exclude: /node_modules/
      },
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
          use: cssOptions,
          fallback: styleLoaderOptions
        }))
      },
      {
        test: /\.(sass|scss)(\?.+)?$/,
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
          use: sassOptions,
          fallback: styleLoaderOptions
        }))
      },
      {
        test: /\.(png|jpg|jpeg|gif|eot|ttf|woff|woff2|svg|svgz)(\?.+)?$/,
        exclude: /favicon\.png$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: 'assets/img/[name].[hash:7].[ext]'
          }
        }]
      }
    ]
  },
  performance: {
    hints: false
  },
  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),
    extractSASS,
    extractCSS,
    new webpack.ProvidePlugin({
      jQuery: "jquery",
      $: "jquery"
    })
  ],
}
config.plugins = [...config.plugins, ...htmlWebpackPluginArray]
module.exports = config
