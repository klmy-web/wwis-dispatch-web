'use strict'
const merge = require('webpack-merge')
const path = require('path')
const webpack = require('webpack')
const baseWebpackConfig = require('./webpack.base.conf')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const { resolve } = require('path')

const prodWebpackConfig = merge(baseWebpackConfig, {
  mode: 'production',
  devtool: false,
  optimization: {
    minimize: false
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], {
      root: resolve(__dirname, '../'),
      verbose: true
    }),
    new CopyWebpackPlugin([
      {
        from: resolve(__dirname, '../static'),
        to: 'static',
        ignore: ['.*']
      }
    ]),
    new webpack.DefinePlugin({
      'process.env': {
          NODE_ENV: '"production"'
      }
    }),
    new webpack.DllReferencePlugin({
      context: __dirname,
      manifest: require(path.join(__dirname, '..', 'static/vendor-manifest.json'))
    }),
    new OptimizeCSSPlugin()
  ]
})

module.exports = prodWebpackConfig