const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
// webpack.dll.config.js

// 需要打包到一起的js文件
const vendors = [
  'vue',
  'vuex',
  'vue-router',
  'axios',
  // 'moment',
  // 'vue-echarts'
  'element-ui',
  // 'iscroll',
  // 'lodash',
  // 'lrz',
  // 'qrcode',
  // 'sweetalert2',
  // 'table-export',
  // 'vue-image-lightbox',
  // 'vuelidate'
]

module.exports = {
  mode: 'production',
  // 也可以设置多个入口，多个vendor，就可以生成多个bundle
  entry: {
    vendor: vendors
  },

  // 输出文件的名称和路径
  output: {
    filename: '[name].bundle.js',
    path: path.join(__dirname, '..', 'static'),
    library: '[name]_[chunkhash]'
  },
  optimization: {
    minimize: false
  },

  plugins: [
    // 这时候打包需要设置环境为production，因为像vue之类在
    // dev环境下会比prod环境多一些信息，在生产环境如果打包的是dev代码，
    // vue也会给出警告
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.DllPlugin({
      path: path.join(__dirname, '..', 'static', '[name]-manifest.json'),
      name: '[name]_[chunkhash]',
      context: __dirname
    }),

    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, '../src/assets/static'),
        ignore: ['.*']
      }
    ]),

    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    new webpack.optimize.OccurrenceOrderPlugin()
  ]
}
