#!/bin/sh
sftp -P 20101 develop@192.168.0.252<<EOF
cd /data/klmy/web
lcd dist/release
rm -rf base
rm -rf system
rm -rf home
rm -rf static
rm index.html
rm logo.png
put -r base/
put -r system/
put -r home/
put -r static/
put -r assets/
put index.html
put logo.png
quit  
EOF