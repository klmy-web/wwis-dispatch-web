#!/bin/sh
version=`cat version.json | jq -r '.version'`
subversion=`cat version.json | jq -r '.subversion'`
myPath="dist/version/V$version.$subversion"
projectPath="dist/version/"
if [ ! -x "$projectPath" ]; then  
mkdir "$projectPath"  
fi
cp -R dist/release $myPath