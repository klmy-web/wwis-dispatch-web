#!/bin/sh
if [ ! -f "version.json" ];then
  cp build/cpversion.json version.json
fi
version=`cat version.json | jq -r '.version'`
subversion=`cat version.json | jq -r '.subversion'`
pkgversion=`cat package.json | jq -r '.version'`
if [ $version != $pkgversion ]; then
  sed -i '' "s/^  \"version\": .*/  \"version\": \"$pkgversion\",/g" version.json
  sed -i '' "s/^  \"subversion\": .*/  \"subversion\": 0/g" version.json
else
  newsubversion=`expr $subversion + 1`
  sed -i '' "s/^  \"subversion\": .*/  \"subversion\": $newsubversion/g" version.json
fi
