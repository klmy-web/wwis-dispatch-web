"use strict";
//node for loading
const ora = require("ora")
//console for node
const chalk = require("chalk")
// webpack
const webpack = require("webpack")
//webpack production setting
const config = require("./webpack.prod.conf")
// moment
const moment = require('moment')
// package.json
const packageConfig = require('../package.json')
const startTime = moment().format('X')
const spinner = ora(`您正在打包${packageConfig.name}...`)
spinner.start()

//构建全量压缩包！
webpack(config, function (err, stats) {
  spinner.stop()
  if (err) throw err
  process.stdout.write(
    stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + "\n\n"
  );
  if (stats.hasErrors()) {
    console.log(chalk.red("Build failed with errors.\n"))
    process.exit(1)
  }
  const endTime = moment().format('X')
  timePrinter(startTime, endTime, '打包')
  console.log(chalk.cyan(`  打包成功!!现在是：${moment().format('YYYY-MM-DD HH:mm:ss')}\n`))
})

// 时间打印函数
const timePrinter = (startTime, endTime, text) => {
  const totalRange = endTime - startTime
  const totalRangeDuration = moment.duration(totalRange, 'seconds')
  console.log(chalk.cyan(`  ${text}时长${parseInt(totalRangeDuration.asMinutes())}分${totalRange % 60}秒`))
}
