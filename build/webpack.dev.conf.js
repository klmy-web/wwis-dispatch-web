'use strict'
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.base.conf')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')

const devConfig = {
  host: 'http://localhost',
  // target: 'http://117.145.189.178:9008/',
  // target: 'http://192.168.0.252:9008/',
  target: 'http://183.237.226.167:39008/',
  port: 20200
}

const devWebpackConfig = merge(baseWebpackConfig, {
  mode: 'development',
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    port: devConfig.port,
    historyApiFallback: false,
    noInfo: true,
    proxy: {
      '/api/dispatch': {
        target: 'http://132.232.162.213:8090',
        changeOrigin: true,
        pathRewrite: {
          '/api/dispatch': '/dispatch'
        }
      },
      '/api': {
        target: devConfig.target,
        changeOrigin: true,
        pathRewrite: {
          '^/api': 'api'
        }
      },
    },
    open: true,
    quiet: true,
    overlay: {
      warnings: true,
      errors: true
    }
  },
  plugins: [
    new FriendlyErrorsPlugin({
      compilationSuccessInfo: {
        messages: [`Congratulations, No error, please visit here: ${devConfig.host}:${devConfig.port}`]
      }
    })
  ]
})

module.exports = devWebpackConfig
